const jwt = require('jsonwebtoken');
const { jwtKey } = require('../keys/keys');
const User = require('../models/User');
const { getDataByArrayKeys } = require('../handlers/db');

function authenticate() {

    return function(req, res, next) {

        const authKeys = getDataByArrayKeys(['authkeys']);

        const isAuth = authKeys.some(k => req.url.includes('api/' + k));

        if(!isAuth) return next();

        const { authorization } = req.headers;

        jwt.verify(authorization, jwtKey, async function(err, decoded) {
    
            if (err) {
    
                if(err.name) {
                    
                    switch(err.name) {
                        case 'TokenExpiredError': {
                            return res.status(400).json({ message: 'Истекло время авторизации' });
                        }
                    }
                    /*
                    err = {
                        name: 'TokenExpiredError',
                        message: 'jwt expired',
                        expiredAt: 1408621000
                    }
                    */
                } 
                
                return res.status(400).json(err);
            }
            
            const { _id } = decoded;

            const user = await User.findById(_id);

            if(user) {
                req.user = { _id };
                return next();
            }
    
            
            return res.status(401).json({message: 'Используйте свои данные для входа в систему.'})
        })
    }
}; 

module.exports = authenticate;