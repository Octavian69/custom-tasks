const { ExtractJwt, Strategy } = require('passport-jwt');
const User = require('../models/User');
const { jwtKey } = require('../keys/keys');

const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtKey
}


const strategy = new Strategy(options, async (user, done) => {
    try {
        const candidate = await User.findById(user._id);

        if(candidate) return done(null, { _id: candidate._id, Login: candidate.Login });
        return done(null, false);

    } catch(e) {
        return done(err, false);     
    }


})

module.exports = strategy;

