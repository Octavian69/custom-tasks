const fs = require('fs');
const path = require('path');
const jsmediatags = require('jsmediatags');
const ffmpeg = require('fluent-ffmpeg');
const { getAudioDurationInSeconds } = require('get-audio-duration');
const { ObjectId } = require('mongoose').Types;
const { getDataByArrayKeys } = require('../handlers/db');

function videoPlaylistFileFilter(req, file, cb) {
    const typesPath = file.fieldname === 'logo' ? 'img' : 'video';
    const mimetypes = getDataByArrayKeys(['files', 'mimetypes', typesPath]);

    const isValid = mimetypes.includes(file.mimetype)

    return cb(null, isValid);

}

function createUnseenEvent(action, key, value, refeId) {
    const referenceId = String(refeId)
    return { action, key, value, referenceId };
}

function isBoolean(value) {
    return typeof value === 'boolean';
}

function modDate(key, value) {

    const date = new Date(value);
    const paramsKey = key.includes('Start') ? 'startDateParams' : 'endDateParams';
    const params = getDataByArrayKeys([paramsKey]);

    date.setHours(...params);

    return date;
}

function paginationAggregate(skip, limit) {
    return [
        { $group: {
            _id: null,
            totalCount: { $sum: 1 },
            results: { $push: '$$ROOT' }
        } },
        
        { $project: {
            totalCount: 1,
            rows: { $slice: ['$results', skip * limit, +limit] }
        } }
    ]
}

function completeTaskFilter(path, filters) {
    const mongoKeys = getDataByArrayKeys(path);

    const complete = Object.keys(filters).reduce((object, current) => {

        const candidate = filters[current];

        if(candidate || isBoolean(candidate)) {

            const value = ~current.search(/date/gi) ? modDate(current, candidate) : candidate;
            const key = mongoKeys[current];

            switch(key) {
                
                case '$regex': {
                    object[current] = { [key]: new RegExp(value, 'gi') }
                    break;
                }

                default: {
                    object[current] = { [key]: value }
                }
            }
        }

        return object;

    }, {});


    return complete;
}

function createAccountFolders(userId) {
    const folders = getDataByArrayKeys(['account', 'accountFolders']);
    const userPath = `uploads`;

    createFolders(userPath, [userId]);

    folders.forEach(folder => {
        const { parent, childs } = folder;
        const parentPath = `${userPath}/${userId}`;

        createFolders(parentPath, [parent]);

        if(childs) {
            const childPath = `${parentPath}/${parent}`;
            createFolders(childPath, childs);
        }; 
    });
};

function createFolders(path, foldersNames) {
    foldersNames.forEach(folder => {
        fs.mkdirSync(`${path}/${folder}`);
    })
}

function getFillValuesObj(obj) {
    
    const filled = Object.keys(obj).reduce((accum, current) => {
        const isFilled = obj[current] !== null;
        
        if(isFilled) accum[current] = obj[current];

        return accum;
    }, {});

    return filled;
}


function createFilters(filters, path) {
    const workFilterObj = getDataByArrayKeys(path, true);
    const filled = getFillValuesObj(filters);

    const complete = Object.keys(filled).reduce((accum, current) => {
        const value = filled[current];
        const filter = workFilterObj[current](value);
        
        accum[current] = filter;

        return accum;
    }, {});

    return complete;
}

function generateFileName(filename) {
    return `${Date.now()}-${filename}`;
}

function filename(req, file, cb) {
    const name = generateFileName(file.originalname);

    cb(null, name);
}

function sortBy(array, field, sort, isDate = false) {
    array.sort((a, b) => {

        const val1 =  isDate ? +new Date(a[field]) : a[field];
        const val2 =  isDate ? +new Date(b[field]) : b[field];

        return sort === 'asc' ? val1 - val2 : val2 - val1;
    })
}

function detectedFilters(obj) {
    const values = Object.values(obj).filter(v => v !== null);

    return values;
}


function mongoObjectId(id) {
    return ObjectId(id);
}

function mapMongoIds(ids) {
    return ids.map(id => mongoObjectId(id));
}

function getMediaTags(file, User) {

    const { path: filepath, filename } = file;

       return new Promise((resolve, reject) => {
           new jsmediatags.Reader(filepath).read(
                {
                    onSuccess(tag) {

                        const { title, artist, picture } = tag.tags;
                        const Artist = artist || path.basename(filename, path.extname(filename));
                        const Title = title || null;
                        const Logo = picture && picture.data ? picture.data : null;

                        const media = { Artist, Title, Logo };

                        if(!Logo) return resolve(media);

                        const bin = Buffer.from(Logo);

                        
                        const name = path.basename(filename, path.extname(filename));
                        const logoPath = `./uploads/${User}/audio/audio-logos/${name}.png`

                        fs.writeFile(logoPath, bin, 'binary', (err) => {

                            if(err) return  console.log(err);

                            media.Logo = logoPath;
                            resolve(media);

                        });
                    },

                    onError(error) {
                        reject(error);
                    } 
                },
            ); 
        });
}

async function videoMediaTags(file, User) {

    const { path: Path, filename } = file;

    return new Promise(async (resolve, reject) => {
        
        const Duration = await getAudioDurationInSeconds(Path);
        const timeMark = (Duration / 20).toFixed(0);
        const posterPath = `uploads/${User}/video/posters`;
        const posterName = path.basename(filename, path.extname(filename));
        const Poster =`${posterPath}/${posterName}.png`;

        
        new ffmpeg(Path)
        .on('end', (filenames) => {
            resolve({ Duration, Poster, Path });
        })
        .takeScreenshots({
            count: 1,
            timemarks: [timeMark],
            filename: posterName,
            size: '260x180'
        }, posterPath );
    }) 
}

function removeListeners(socket) {
    const subscribers = getDataByArrayKeys(['sockets', 'subscribers']);

    Object.entries(subscribers).forEach(([key, events]) => {
        events.forEach((event, method) => {
            socket.removeAllListeners(event);
        })
    })
}



module.exports = {
    videoPlaylistFileFilter,
    completeTaskFilter,
    createAccountFolders,
    paginationAggregate,
    generateFileName,
    filename,
    sortBy,
    mongoObjectId,
    mapMongoIds,
    getMediaTags,
    videoMediaTags,
    detectedFilters,
    createFilters,
    createUnseenEvent,
    removeListeners
}