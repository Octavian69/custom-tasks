function errorHandler(error, res) {
    res.status(500).json({
        success: false,
        message: error.message || err
    })
}

module.exports = errorHandler;