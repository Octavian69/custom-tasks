const SOKET_EVENTS = require('../sockets/utils/socket-events');

const db = {
    taskMongoKeys: {
        "StartDate": '$gte',
        "EndDate": '$lte',
        "Title": '$regex',
        "Description": "$regex",
        "Completed": "$eq"
    },
    startDateParams: [0, 0, 0],
    endDateParams: [23, 59, 59],

    authkeys: [
        'users',
        'tasks',
        'account',
        'edit',
        'stats',
        'pictures',
        'audio',
        'video',
        'comments',
        'likes',
        'friends',
        'messages',
        'unseen',
    ],

    sockets: {
        subscribers: {
            messages: [
                {event: SOKET_EVENTS.MESSAGE_NEW, method: 'sendNewMessage'},
                {event: SOKET_EVENTS.MESSAGE_CREATE_DISCUSSION, method: 'createNewDiscussion'},
                {event: SOKET_EVENTS.MESSAGE_SWITCH_ROOM, method: 'switchRoom'},
                {event: SOKET_EVENTS.MESSAGE_TYPING, method: 'typingMessage'},
                {event: SOKET_EVENTS.MESSAGE_EDIT, method: 'editMessage'},
                {event: SOKET_EVENTS.MESSAGE_REMOVE, method: 'removeMessages'},
            ],
            unseen: [
                {event: SOKET_EVENTS.UNSEEN_UPDATE, method: 'updateUnseen'}
            ],
            friends: [
                {event: SOKET_EVENTS.FRIENDS_CHANGE_STATE, method: 'changeFriendsState'},
            ]
        }
    },

    defaultInterfaceFileds: {
        video: [
            '_id',
            'Title',
            'Description',
            'Path',
            'Poster',
            'Duration',
            'CreatorName',
            'Created',
            'Updated',
            'Playlist',
            'User'
        ],
        'video-playlist': [
            'Title',
            'Description',
            'User',
            'Type',
            'Logo',
            'CreatorName',
            'Created',
            'Updated',
            '_id'
        ],
        
    },

    excludeInterfacesField: {
        picture: ['Likes'],
        'picture-album': ['ListPaths']
    },

    aggregate: {
        user: {
            project: {
                Name: 1,
                Avatar: 1,
                Status: 1,
                Online: 1,
                MainInfo: { $arrayElemAt: ['$InfoObject.Main', 0] },
                FriendsTotal: { $arrayElemAt: ['$TotalStatsObject.Friends', 0] },
                AudioTotal: { $arrayElemAt: ['$TotalStatsObject.Audio', 0] },
                VideoTotal: { $arrayElemAt: ['$TotalStatsObject.Video', 0] },
                PicturesTotal: { $arrayElemAt: ['$TotalStatsObject.Pictures', 0] },
            },

        },

        lookup: {
            creator: {
                from: 'users',
                localField: 'User',
                foreignField: '_id',
                as: 'CreatorObject' 
            },
            totalStats: {
                from: 'total-stats',
                localField: '_id',
                foreignField: 'User',
                as: 'TotalStatsObject' 
            },
            info: {
                from: 'infos',
                localField: '_id',
                foreignField: 'User',
                as: 'InfoObject' 
            },
            participants: (User) => ({
                from: 'users',
                let: { users: '$Participants' },
                pipeline: [
                    { $match: { $expr: { $in: [ '$_id', '$$users' ] }, _id: { $ne: User  } }},
                    {$project: {
                        Name: 1,
                        Avatar: 1
                    }}
                ],
                as: 'ParticipantsUsers'
            }),
            lastMessage: {
                from: 'messages',
                let: { dialogue_id: '$_id' },
                pipeline: [
                    { $match: { $expr: { $eq: ['$Dialogue', '$$dialogue_id'] } }},
                    { $sort:  { Created: -1 }},
                    { $limit: 1 },
                    { $project: { Text: 1, Dialogue: 1, User: 1, Created: 1 } }
                ],
                as: 'LastMessages'
            }
        },

        video: {
            project: {
                Title: 1,
                Description: 1,
                Poster: 1,
                Playlist: 1,
                User: 1,
                Path: 1,
                Duration: 1,
                CreatorName: 1,
                Created: 1,
                Updated: 1,
                UserAvatar: { $arrayElemAt: ['$CreatorObject.Avatar', 0] },
                LikesTotal: { $size: '$Likes' },
            }
        },

        'video-playlist': {
            project: {
                Title: 1,
                Description: 1,
                User: 1,
                Logo: 1,
                Created: 1,
                Updated: 1,
                VideoTotal: { $size: '$ListIds'},
                LikesTotal: { $size: '$Likes' },
                CreatorName: { $arrayElemAt: ['$CreatorObject.Name', 0] },
                UserAvatar: { $arrayElemAt: ['$CreatorObject.Avatar', 0] },
                
            }
        },

        picture: {
            project: {
                Description: 1,
                Created: 1,
                Type: 1,
                Album: 1,
                User: 1,
                CreatorName: { $arrayElemAt: ['$CreatorObject.Name', 0] },
                CreatorAvatar: { $arrayElemAt: ['$CreatorObject.Avatar', 0] },
                CreatorId: { $arrayElemAt: ['$CreatorObject._id', 0] },
                Path: 1,
                LikesTotal: { $size: '$Likes' },
                IsHave: true
            }
        },

        'picture-album': {
            project: {
                Title: 1,
                Type: 1,
                Logo: 1,
                Description: 1,
                Created: 1,
                TotalCount: 1,
                Updated: 1,
                User: 1,
                CreatorName: { $arrayElemAt: ['$CreatorObject.Name', 0] },
                UserAvatar: { $arrayElemAt: ['$CreatorObject.Avatar', 0] },
            }
        },
        'picture-replace-album': {
            project: {
                Title: 1,
                Type: 1,
                TotalCount: 1,
                Logo: 1
            }
        },

        dialogue: {
            project: (User, unreadMongoIds) => ({
                Title: { 
                    $cond: [ { $in: ['$Type', ['dialogue'] ] }, { $arrayElemAt: ['$ParticipantsUsers.Name', 0] }, '$Title' ] 
                },
                Logo: {
                    $cond: [ { $in: ['$Type', ['dialogue']] }, { $arrayElemAt: ['$ParticipantsUsers.Avatar', 0] }, '$Logo' ] 
                },
                LastMessage: {
                    Text: { $arrayElemAt: ['$LastMessages.Text', 0] },
                    User: { $arrayElemAt: ['$LastMessages.User', 0] },
                    Created: { $arrayElemAt: ['$LastMessages.Created', 0] },
                    _id: { $arrayElemAt: ['$LastMessages._id', 0] },
                },
                Participants: '$ParticipantsUsers',
                Unread: { $in: [ '$_id', unreadMongoIds ] },
                // IsOwn: { $in: [ '$User', [User] ] },
                Type: 1,
                Created: 1,
                Updated: 1,
                TotalMessages: 1,
                User: 1
            })
        },
        message: {
            project: (DialogueType) => ({
                Text: 1,
                Created: 1,
                Dialogue: 1,
                User: 1,
                DialogueType,
                // IsOwn: { $in: ['$User', [mongoObjectId(User)]]},
                UserName: { $arrayElemAt: ['$CreatorObject.Name', 0] },
                UserAvatar: { $arrayElemAt: ['$CreatorObject.Avatar', 0] }
            })
        }
    },

    account: {
        accountFolders: [
            {
                parent: 'img',
                childs: ['album-logos', 'dialogues']
            },
            {
                parent: 'audio',
                childs: ['audio-logos', 'playlist-logos']
            },
            {
                parent: 'video',
                childs: ['playlist-logos', 'posters']
            }
        ]
    },

    filters: {
        friends: {
            Name: (val) => ({ $regex: new RegExp(val, 'gi') }),
            Online: (val) => ({ $eq: val }) 
        },
        dialogues: {
            Title: (val) => ({ $regex: new RegExp(val, 'gi') }),
            Type: (val) => ({ $eq: val })
        }
    },
    files: {
        mimetypes: {
            img: ["image/jpeg", "image/jpg", "image/png"],
            audio: ['audio/mpeg', 'audio/mp3'],
            video: ['video/mp4', 'video/webm', 'video/ogg']
        }
    },
    audio: {
        filterKeys: {
        }
    },
    video: {
        fileFields: [
            {name: "logo", maxCount: 1},
            {name: "video", maxCount: 5}
        ],

        excludeFields: {
            video: ['AmountAdditions', 'ListIds', 'Likes']
        },
        filterKeys: {
            Duration: (duration) => {
               const key = duration === 'long' ? '$gt' : "$lte";
               const value = 60;

               return { [key]: value };
            },
            Title: (value) => ({'$regex': new RegExp(value, 'gi')}),
        }  
    }
}

function getDataByArrayKeys(keys = [], isFunctionObject = false) {

    let counter = 1;

    let data = db[keys[0]];

    while(counter < keys.length) {
        
        data = data[keys[counter]];
        
        counter++;
    }
	
	if(data) {
        // Object.assign({}, data)
        const response = isFunctionObject ? data : JSON.parse( ( JSON.stringify(data) ) );

		return response;
	}

    return null;
}

module.exports = {
    getDataByArrayKeys
}