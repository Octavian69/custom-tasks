const multer = require('multer');
const { filename } = require('../../handlers/handlers');

const storage = multer.diskStorage({
    destination(req, file, cb) {
        const { _id: User } = req.user;
        const path = `uploads/${User}/audio`;

        return cb(null, path);
    },
    filename
})

module.exports = storage;