const multer = require('multer');
const { getDataByArrayKeys } = require('../handlers/db');

function validationTypes(typesPath) {

    const mimetypes = getDataByArrayKeys(['files', 'mimetypes', typesPath])
    
    return (req, file, cb) => {
        const isValid = mimetypes.includes(file.mimetype);
        
        return cb(null, isValid);
    }
}

function completePath(folder, file) {
    return `./${folder}/${file}.storage`;
}

function fileConfig(path, storageName, mimetypes) {
    const fileFilter = validationTypes(mimetypes);
    const storagePath = completePath(path, storageName);

    const storage = require(storagePath);

    return multer({ storage, fileFilter })

}

module.exports = fileConfig;