const mongoose = require('mongoose');
const { Schema } = mongoose;


const AudioPlaylistSchema = new Schema({
    Type: {
        type: String,
        required: true
    },
    Title: {
        type: String,
        required: true
    },
    Description: {
        type: String,
        default: null
    },
    CreatorName: {
        type: String,
        required: true
    },
    Created: {
        type: Date,
        default: Date.now
    },
    Updated: {
        type: Date,
        default: Date.now
    },
    Logo: {
        type: String,
        default: null
    },
    TotalCount: {
        type: Number,
        default: 1
    },
    ListPaths: {
        type: [String],
        default: []
    },
    OriginalPlaylist: {
        type: String,
        default: null
    },
    Creator: {
        type: String,
        required: true
    },
    User: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
})

module.exports = mongoose.model('audio-playlists', AudioPlaylistSchema);