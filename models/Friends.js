const mongoose = require('mongoose');
const { Schema } = mongoose;

const FriendsSchema = new Schema({
    Friends: {
        type: [String],
        default: []
    },
    IncomingRequestIds: {
        type: [String],
        default: []
    },
    OutgoingRequestIds: {
        type: [String],
        default: []
    },
    User: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
});


module.exports = mongoose.model('friends', FriendsSchema);