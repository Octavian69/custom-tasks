const mongoose = require('mongoose');
const { Schema } = mongoose;


const InfoSchema = new Schema({
    Main: {
        Name: {
            type: String,
            default: null
        },
        LastName: {
            type: String,
            default: null
        },
        Gender: {
            type: Number,
            default: null
        },
        Birthday: {
            type: Date,
            default: null
        },
        City: {
            type: String,
            default: null
        },
        Language: {
            type: [String],
            default: []
        }
    },
    Contacts: {
        Country: {
            type: Schema.Types.Mixed,
            default: null
        },
        City: {
            type: Schema.Types.Mixed,
            default: null
        },
        Address: {
            type: String,
            default: null
        },
        Phone: {
            type: String,
            default: null
        },
        AdditionalPhone: {
            type: String,
            default: null
        },
        Skype: {
            type: String,
            default: null
        },
        WebSite: {
            type: String,
            default: null
        }
    },
    Interesting: {
        Activity: {
            type: String,
            default: null
        },
        Interesting: {
            type: String,
            default: null
        },
        Music: {
            type: String,
            default: null
        },
        Films: {
            type: String,
            default: null
        },
        Games: {
            type: String,
            default: null
        },
        Quotes: {
            type: String,
            default: null
        },
        About: {
            type: String,
            default: null
        }
    },
    Education: [{
        Country: {
            type: Schema.Types.Mixed,
            default: null
        },
        City: {
            type: Schema.Types.Mixed,
            default: null
        },
        School: {
            type: Schema.Types.Mixed,
            default: null
        },
        StartDate: {
            type: Date,
            default: null
        },
        EndDate: {
            type: Date,
            default: null
        },
        _id: {
            type: String,
            required: true
        }
    }],
    HigherEducation: [{
        Country: {
            type: Schema.Types.Mixed,
            default: null
        },
        City: {
            type: Schema.Types.Mixed,
            default: null
        },
        University: {
            type: Schema.Types.Mixed,
            default: null
        },
        Faculty: {
            type: Schema.Types.Mixed,
            default: null
        },
        Status: {
            type: Number,
            default: null
        },
        FormOfEducation: {
            type: Number,
            default: null
        },
        StartDate: {
            type: Date,
            default: null
        },
        EndDate: {
            type: Date,
            default: null
        },
        _id: {
            type: String,
            required: true
        }
    }],
    Career: [{
        CompanyName: {
            type: String,
            default: null
        },
        Country: {
            type: Schema.Types.Mixed,
            default: null
        },
        City: {
            type: Schema.Types.Mixed,
            default: null
        },
        StartDate: {
            type: Date,
            default: null
        },
        EndDate: {
            type: Date,
            default: null
        },
        Position: {
            type: String,
            default: null
        },
        _id: {
            type: String,
            required: true
        }
    }],
    Military: [{
         Country: {
            type: Schema.Types.Mixed,
            default: null
         },
         ArmyPart: {
            type: String,
            deafult: null
         },
         StartDate: {
            type: Date,
            deafult: null
         },
         EndDate: {
            type: Date,
            deafult: null
         },
         _id: {
            type: String,
            required: true
         }
    }],
    LifePosition: {
        PoliticalPreference: {
            type: Number,
            default: null
        },
        Outlook: {
            type: Number,
            default: null
        },
        MainInLife: {
            type: Number,
            default: null
        },
        MainInPeople: {
            type: Number,
            default: null
        },
        AttitudeToSmoking: {
            type: Number,
            default: null
        },
        AttitudeToAlcohol: {
            type: Number,
            default: null
        },
        Inspiration: {
            type: String,
            default: null
        }
    },
    User: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
})


module.exports = mongoose.model('infos', InfoSchema);