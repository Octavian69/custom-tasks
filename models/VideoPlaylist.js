const mongoose = require('mongoose');
const { Schema } = mongoose;

const VideoPlaylistSchema = new Schema({
    Title: {
        type: String,
        required: true
    },
    Description: {
        type: String,
        default: null
    },
    Type: {
        type: String,
        required: true
    },
    Logo: {
        type: String,
        default: null
    },
    Likes: {
        type: [String],
        default: []
    },
    AmountAdditions: {
        type: Number,
        default: 1
    },
    ListIds: {
        type: [String],
        default: []
    },
    Created: {
        type: Date,
        default: Date.now
    },
    Updated: {
        type: Date,
        default: Date.now
    },
    User: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
});


module.exports = mongoose.model('video-playlists', VideoPlaylistSchema);