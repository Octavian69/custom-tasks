const mongoose = require('mongoose');
const { Schema } = mongoose;

const UserSchema = new Schema({
    Login: {
        type: String,
        required: true
    },
    Password: {
        type: String,
        reuqired: true
    },
    Name: {
        type: String,
        required: true
    },
    Avatar: {
        type: String,
        default: null
    },
    Status: {
        type: String,
        default: null
    },
    Online: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model('users', UserSchema);