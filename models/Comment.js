const mongoose = require('mongoose');
const { Schema } = mongoose;

const CommentSchema = new Schema({
    Text: {
        type: String,
        required: true
    },
    Type: {
        type: String,
        reuired: true
    },
    RefersID: {
        type: Schema.Types.ObjectId,
        required: true
    },
    Avatar: {
        type: String,
        default: null
    },
    Likes: {
        type: [String],
        default: []
    },
    Created: {
        type: Date,
        default: new Date()
    },
    User: {
        type: Schema.Types.ObjectId,
        refs: 'users'
    }
})

module.exports = mongoose.model('comments', CommentSchema);