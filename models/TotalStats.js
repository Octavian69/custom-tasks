const mongoose = require('mongoose');
const { Schema } = mongoose;

const TotalStatsSchema = new Schema({
    Friends: {
        type: Number,
        default: 0
    },
    Audio: {
        type: Number,
        default: 0
    },
    Video: {
        type: Number,
        default: 0
    },
    Pictures: {
        type: Number,
        default: 0
    },
    AudioPlaylists: {
        type: Number,
        default: 0
    },
    VideoPlaylists: {
        type: Number,
        default: 0
    },
    PictureAlbums: {
        type: Number,
        default: 0
    },
    User: {
        type: Schema.Types.ObjectId,
        refs: 'users'
    }
})

module.exports = mongoose.model('total-stats', TotalStatsSchema);