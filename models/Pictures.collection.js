const mongoose = require('mongoose');
const { Schema } = mongoose;

const PicturesCollectionSchema = new Schema({
    User: {
        type: Schema.Types.ObjectId,
        refs: 'users',
    },
    CollectionPaths: {
        type: [String],
        default: []
    }
})

module.exports = mongoose.model('pictures-collections', PicturesCollectionSchema);