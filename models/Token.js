const mongoose = require('mongoose');
const { Schema } = mongoose;


const TokenSchema = new Schema({
    access_token: {
        type: String,
        default: null
    },
    refresh_token: {
        type: String,
        default: null
    },
    User: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('tokens', TokenSchema);