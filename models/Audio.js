const mongoose = require('mongoose');
const { Schema } = mongoose;


const AudioSchema = new Schema({
    Artist: {
        type: String,
        required: true 
    },
    Title: {
        type: String,
        default: null    
    },
    Genre: {
        type: Number,
        default: null
    },
    Path: {
        type: String,
        required: true
    },
    Duration: {
        type: Number,
        required: true
    },
    Logo: {
        type: String,
        default: null
    },
    Created: {
        type: Date,
        default: Date.now
    },
    Type: {
        type: String,
        required: true
    },
    Playlist: {
        type: String,
        reuqired: true
    },
    User: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
});

module.exports = mongoose.model('audios', AudioSchema);