const mongoose = require('mongoose');
const { Schema } = mongoose;

const DialogueSchema = new Schema({
    Title: {
        type: String,
        default: null
    },
    Participants: {
        type: [Schema.Types.ObjectId],
        required: true
    },
    Type: {
        type: String,
        default: 'dialogue'
    },
    Logo: {
        type: String,
        default: null
    },
    Created: {
        type: Date,
        default: Date.now,
    },
    Updated: {
        type: Date,
        default: Date.now
    },
    TotalMessages: {
        type: Number,
        default: 1
    },
    User: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
});

module.exports = mongoose.model('dialogues', DialogueSchema);