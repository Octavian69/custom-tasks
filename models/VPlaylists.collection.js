const mongoose = require('mongoose');
const { Schema } = mongoose;

const VPlaylistsSchema = new Schema({
    User: {
        type: Schema.Types.ObjectId,
        refs: 'users',
    },
    CollectionID: {
        type: [String],
        default: []
    }
})


module.exports = mongoose.model('video-playlist-collections', VPlaylistsSchema);