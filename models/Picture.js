const mongoose = require('mongoose');
const { Schema } = mongoose;

const PitcureSchema = new Schema({
    Path: {
        type: String,
        required: true
    },
    Description: {
        type: String,
        default: null
    },
    Type: {
        type: String,
        default: "Created"
    },
    Album: {
        type: String,
        required: true
    },
    Likes: {
        type: [String],
        default: []
    },
    Created: {
        type: Date,
        default: Date.now
    },
    Updated: {
        type: Date,
        default: Date.now
    },
    User: {
        type: Schema.Types.ObjectId,
        refs: 'users'
    }
});

module.exports = mongoose.model('pictures', PitcureSchema);