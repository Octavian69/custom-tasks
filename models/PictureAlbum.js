const mongoose = require('mongoose');
const { Schema } = mongoose;

const PictureAlbumSchema = new Schema({
    Title: {
        type: String,
        required: true
    },
    Type: {
        type: String,
        default: 'Created'
    },
    Logo: {
        type: String,
        default: null
    },
    Description: {
        type: String,
        default: null
    },
    Created: {
        type: Date,
        default: Date.now
    },
    ListPaths: {
        type: [String],
        default: []
    },
    TotalCount: {
        type: Number,
        default: 0
    },
    Updated: {
        type: Date,
        default: Date.now
    },
    User: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
})

module.exports = mongoose.model('picture-albums', PictureAlbumSchema);