const mongoose = require('mongoose');
const { Schema } = mongoose;

const APlaylistsSchema = new Schema({
    User: {
        type: Schema.Types.ObjectId,
        refs: 'users',
    },
    CollectionID: {
        type: [String],
        default: []
    }
})


module.exports = mongoose.model('audio-playlists-collections', APlaylistsSchema);