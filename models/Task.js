const mongoose = require('mongoose');
const { Schema } = mongoose;

const TaskSchema = new Schema({
    StartDate: {
        type: Date,
        default: Date.now
    },
    EndDate: {
        type: Date,
        default: Date.now
    },
    Description: {
        type: String,
        default: null
    },
    Title: {
        type: String,
        required: true
    },
    Completed: {
        type: Boolean,
        default: false
    },
    User: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
})

module.exports = mongoose.model('tasks', TaskSchema);