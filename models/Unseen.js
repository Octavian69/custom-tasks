const mongoose = require('mongoose');
const { Schema } = mongoose;

const UnseenSchema = new Schema({
    keys: {
        dialogues: {
            type: Object,
            default: {}
        },
        friendsRequests: {
            type: Object,
            default: {}
        }
    },
    User: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
}, { minimize: false });

module.exports = mongoose.model('unseens', UnseenSchema);