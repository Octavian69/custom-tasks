const mongoose = require('mongoose');
const { Schema } = mongoose;

const MesageSchema = new Schema({
    Text: {
        type: String,
        required: true
    },
    Created: {
        type: Date,
        default: Date.now
    },
    Dialogue: {
        refs: 'dialogues',
        type: Schema.Types.ObjectId
    },
    User: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
});

module.exports = mongoose.model('messages', MesageSchema);