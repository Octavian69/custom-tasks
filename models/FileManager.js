const mongoose = require('mongoose');
const { Schema } = mongoose;

const FileManagerSchema = new Schema({
    Path: {
        type: String,
        required: true
    },
    AmountAdded: {
        type: Number,
        default: 1
    }
})

module.exports = mongoose.model('file-managers', FileManagerSchema);