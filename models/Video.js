const mongoose = require('mongoose');
const { Schema } = mongoose;


const VideoSchema = new Schema({
    Title: {
        type: String,
        required: true
    },
    Description: {
        type: String,
        default: null
    },
    Path: {
        type: String,
        required: true
    },
    Poster: {
        type: String,
        required: true
    },
    Likes: {
        type: [String],
        default: []
    },
    AmountAdditions: {
        type: Number,
        default: 1
    },
    Duration: {
        type: Number,
        required: true
    },
    CreatorName: {
        type: String,
        required: true
    },
    Created: {
        type: Date,
        default: Date.now
    },
    Updated: {
        type: Date,
        default: Date.now
    },
    Playlist: {
        type: String,
        required: true
    },
    User: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
});


module.exports = mongoose.model('videos', VideoSchema);