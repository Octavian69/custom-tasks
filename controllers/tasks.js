const Task = require('../models/Task');
const { completeTaskFilter } = require('../handlers/handlers'); 
const errorHandler = require('../handlers/errorHandler');
const { mongoObjectId, paginationAggregate } = require('../handlers/handlers');

async function fetch(req, res) {
    try {
       
       const { skip, limit } = req.query;
       const { _id } = req.user;

       const filters = completeTaskFilter(['taskMongoKeys'], req.body);

       const [result] = await Task.aggregate([
           {$match: { User: mongoObjectId(_id) }},
           {$match: filters},
           ...paginationAggregate(skip, limit),
       ]);

       const rows = result ? result.rows : [];
       const totalCount = result ? result.totalCount : 0;

       res.status(200).json({ rows, totalCount })

    } catch (e) {
        errorHandler(e, res)
    }
}


async function fetchById(req, res) {
    try {
        const { taskId } = req.params;
        const task = await Task.findById(taskId);

        res.status(200).json(task);

    } catch (e) {
        errorHandler(e, res)
    }
}


async function create(req, res) {
    try {

        const User = req.user._id;

        const candidate = Object.assign({}, req.body, { User });
        const task = await new Task(candidate).save();


        res.status(200).json(task);

    } catch (e) {
        errorHandler(e, res)
    }
}


async function update(req, res) {
    try {

        const { taskId } = req.params;

        const update = await Task.findByIdAndUpdate(taskId, req.body, { new: true });

        res.status(200).json(update);

    } catch (e) {
        errorHandler(e, res)
    }
}

async function remove(req, res) {
    try {

        await Task.deleteMany({ _id: { $in: req.body } });

        res.status(200).json({ message: 'Задачи удалены.' });

    } catch (e) {
        errorHandler(e, res)
    }
}



module.exports = {
    fetch, 
    fetchById, 
    create, 
    update, 
    remove
}