const del = require('del');
const errorHandler  = require('../handlers/errorHandler');
const Task = require('../models/Task');
const User = require('../models/User');
const Friends = require('../models/Friends');
const Info = require('../models/Info');
const Picture = require('../models/Picture');
const TotalStats = require('../models/TotalStats');
const Token = require('../models/Token');
const AudioPlaylist = require('../models/AudioPlaylist');
const APCollection = require('../models/APlaylists.collection');
const Audio = require('../models/Audio');
const Video = require('../models/Video');
const VideoPlaylist = require('../models/VideoPlaylist');
const VPCollection = require('../models/VPlaylists.collection');
const Comment = require('../models/Comment');
const Unseen = require('../models/Unseen');
const Message = require('../models/Message');
const Dialogue = require('../models/Dialogue');

async function fetchInfo(req, res) {

    try {
        const { id } = req.params;
        const info = await Info.findOne({ User: id });

        res.status(200).json(info);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchInfoPart(req, res) {

    try {
        const { _id } = req.user;
        const { part } = req.query;
        const info = await Info.findOne({ User: _id }, {}, { projection: { [part]: true } });


        res.status(200).json(info[part]);
    } catch(e) {
        errorHandler(e, res);
    }
}

async function remove(req, res) {

    try {

        const { _id } = req.user;

        const project = { User: _id };

        await User.findByIdAndRemove(_id);
        await Token.findOneAndDelete(project);
        await Unseen.findOneAndDelete(project);
        await Info.findOneAndDelete(project);
        await Friends.findOneAndDelete(project);
        await TotalStats.findOneAndDelete(project);
        await APCollection.findOneAndDelete(project);
        await VPCollection.findOneAndDelete(project);
        await AudioPlaylist.deleteMany(project);
        await Task.deleteMany(project);
        await Picture.deleteMany(project);
        await Comment.deleteMany(project);
        await Audio.deleteMany(project);
        await Video.deleteMany(project);
        await VideoPlaylist.deleteMany(project);
        await Message.deleteMany({});
        await Dialogue.deleteMany({});

        del.sync([`uploads/${_id}`]);
        
        res.status(200).json({ message: 'Аккаунт удален.' })

    } catch(e) {
        errorHandler(e, res);
    }
}

module.exports = {
    fetchInfo,
    fetchInfoPart,
    remove
}