const errorHandler = require('../handlers/errorHandler');
const TotalStats = require('../models/TotalStats');

async function fetch(req, res) {
    try {
        const { id: User } = req.params;

        const stats = await TotalStats.findOne({ User });
    
        res.status(200).json(stats);

    } catch(e) {
        errorHandler(e, res);
    }
}

module.exports = {
    fetch
}