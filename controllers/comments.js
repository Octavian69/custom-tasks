const errorHandler = require('../handlers/errorHandler');
const { getDataByArrayKeys } = require('../handlers/db');
const { mongoObjectId, paginationAggregate } = require('../handlers/handlers');
const Comment = require('../models/Comment');
const UserSchema = require('../models/User');
const IComment = require('../classes/IComment');


async function fetch(req, res) {

    try {

        const { _id: User } = req.user;
        const { refId: RefersID } = req.params;
        const { page: { skip, limit }, Type } = req.body;

        // const project = 

        // numberOfColors: { $cond: { if: { $isArray: "$colors" }, then: { $size: "$colors" }, else: "NA"} }

        // "has bananas" : {
        //     $in: [ "bananas", "$in_stock" ]   }

        const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator'])
        

        const [result] = await Comment.aggregate([
            { $match: { RefersID: mongoObjectId(RefersID), Type } },
            { $sort: { Created: -1 } },
            { $lookup },
            {
               $project: {
                  Text: 1,
                  RefersID: 1,
                  Avatar: 1,
                  User: 1,
                  Created: 1,
                  Avatar: { $arrayElemAt: ['$CreatorObject.Avatar', 0] },
                  CreatorName: { $arrayElemAt: ['$CreatorObject.Name', 0] },
                  OwnLike: { $in: [User, '$Likes'] },
                  LikesTotal: { $size: "$Likes" }
               }

            },
            ...paginationAggregate(skip, limit)
         ]);

         const rows = result ? result.rows : [];
         const totalCount = result ? result.totalCount : 0;
         
         res.status(200).json({ rows, totalCount });
    } catch(e) {
        errorHandler(e, res);
    }
}

async function create(req, res) {

    try {
        const candidate = Object.assign(req.body);

        const create = await new Comment(candidate).save();
        const user = await UserSchema.findById(req.user._id);

        const comment = create.toObject();

        comment.Avatar = user.Avatar;
        comment.CreatorName = user.Name;

        const iComment = new IComment(comment);

        res.status(200).json(iComment);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function edit(req, res) {

    try {

        const { id } = req.params;
        
        const comment = await Comment.findByIdAndUpdate(id, { $set: req.body });

        res.status(200).json(comment);

    } catch(e) {
        errorHandler(e, res);
    }
}



module.exports = {
    fetch,
    create,
    edit
}