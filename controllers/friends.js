const errorHandler = require('../handlers/errorHandler');
const { getDataByArrayKeys } = require('../handlers/db');
const { mongoObjectId, paginationAggregate, createFilters, mapMongoIds } = require('../handlers/handlers');
const UserSchema = require('../models/User');
const TotalStats = require('../models/TotalStats')
const Friends = require('../models/Friends');

async function fetch(req, res) {
    try {
        const { id } = req.params;
        const { _id: User } = req.user;
        const { state: {listType}, page: { skip, limit }, filters, sorted } = req.body;
        const { Friends: friends } = await Friends.findOne({User: id});
        const ownFriends = await Friends.findOne({User});
        const mongoFriendIds = mapMongoIds(friends);
        const matchFilters = createFilters(filters, ['filters', 'friends']);

        const searchKey = listType === 'main' ? '$in' : '$nin';

        const $project = getDataByArrayKeys(['aggregate', 'user', 'project']);
        const totalLookup = getDataByArrayKeys(['aggregate', 'lookup', 'totalStats']);
        const infoLookup = getDataByArrayKeys(['aggregate', 'lookup', 'info']);

        const $match =  { $and: [{_id: {[searchKey]:  mongoFriendIds}}, {_id: {$ne: mongoObjectId(id)}}], ...matchFilters };
        const $sort = Object.keys(sorted).length ? sorted : { Name: 1 };

        $project.IsHave = { $in: ['$_id', mapMongoIds(ownFriends.Friends)] };
        $project.IsIncomingRequest = { $in: ['$_id', mapMongoIds(ownFriends.IncomingRequestIds)] };
        $project.IsOutgoingRequest = { $in: ['$_id', mapMongoIds(ownFriends.OutgoingRequestIds)] };

        const aggregate = [
            { $match },
            { $lookup: totalLookup },
            { $lookup: infoLookup },
            { $sort },
            { $project },
            ...paginationAggregate(skip, limit)
        ];

        const [result] = await UserSchema.aggregate(aggregate);

        const rows = result ? result.rows : [];
        const totalCount = result ? result.totalCount : 0;

        res.status(200).json({ rows, totalCount });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchMutual(req, res) {
    try {
        const { _id: User } = req.user;
        const { id } = req.params;
        const { page: {skip, limit} } = req.body;

        const { Friends: ownFriends, IncomingRequestIds, OutgoingRequestIds } = await Friends.findOne({ User });
        const { Friends: currentFriends } = await Friends.findOne({ User: id });

        const start = skip * limit;
        const end = start + limit;
        const mutualIds = currentFriends.filter(id => ownFriends.includes(id));
        const reqIds = mutualIds.slice(start, end)

        const rows = [];
        const totalCount = mutualIds.length;

        if(reqIds.length) {
            const $project = getDataByArrayKeys(['aggregate', 'user', 'project']);
            const totalLookup = getDataByArrayKeys(['aggregate', 'lookup', 'totalStats']);
            const infoLookup = getDataByArrayKeys(['aggregate', 'lookup', 'info']);

            $project.IsHave = { $in: ['$_id', mapMongoIds(ownFriends)] };
            $project.IsIncomingRequest = { $in: ['$_id', mapMongoIds(IncomingRequestIds)] };
            $project.IsOutgoingRequest = { $in: ['$_id', mapMongoIds(OutgoingRequestIds)] };

            const asyncArray = reqIds.map(id => {
                const $match = { _id: mongoObjectId(id) };
    
                return UserSchema.aggregate([{ $match }])
                .lookup(totalLookup)
                .lookup(infoLookup)
                .project($project)
                .limit(1);
            });

            const users = await Promise.all(asyncArray);
            const response = users.map(user => user[0]);

            rows.push(...response);
        }

        res.status(200).json({ rows, totalCount });


    } catch(e) {
        errorHandler(e, res)
    }
}

async function fetchIncomplete(req, res) {
    try {

        const { id: User } = req.params;
        const { page: { skip, limit }, filters } = req.body;
        const { Friends: friends } = await Friends.findOne({User});
        const mongoFriendIds = mapMongoIds(friends);
        const matchFilters = createFilters(filters, ['filters', 'friends']);
        const $match = {
            _id: { $in: mongoFriendIds },
            ...matchFilters
        };

        const $project = {
            Name: 1,
            Avatar: 1
        }

        const $sort = {Name: -1}

        const aggregate = [
            { $match },
            { $sort },
            { $project },
            ...paginationAggregate(skip, limit)
        ];

        const [result] = await UserSchema.aggregate(aggregate);

        const rows = result ? result.rows : [];
        const totalCount = result ? result.totalCount : 0;

        res.status(200).json({ rows, totalCount });



    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchRequests(req, res) {
    try {

        const { _id: User } = req.user;
        const { page: { skip, limit }, state: { type } } = req.body;

        const start = skip * limit;
        const end = start + limit;

        const {IncomingRequestIds, OutgoingRequestIds, Friends: friends} = await Friends.findOne({ User });
        const arrayByType = type === 'incoming' ? IncomingRequestIds : OutgoingRequestIds;
        const reqIds = arrayByType.concat().slice(start, end);
        const $project = getDataByArrayKeys(['aggregate', 'user', 'project']);
        const totalLookup = getDataByArrayKeys(['aggregate', 'lookup', 'totalStats']);
        const infoLookup = getDataByArrayKeys(['aggregate', 'lookup', 'info']);

        $project.IsHave = { $in: ['$_id', mapMongoIds(friends)] };
        $project.IsIncomingRequest = { $in: ['$_id', mapMongoIds(IncomingRequestIds)] };
        $project.IsOutgoingRequest = { $in: ['$_id', mapMongoIds(OutgoingRequestIds)] };

        const asyncArray = reqIds.map(id => {
            const $match = { _id: mongoObjectId(id) };

            return UserSchema.aggregate([{ $match }])
            .lookup(totalLookup)
            .lookup(infoLookup)
            .project($project)
            .limit(1);
        });

        const users = await Promise.all(asyncArray);
        const rows = users.map(user => user[0]);
        const totalCount = arrayByType.length;

        res.status(200).json({ rows, totalCount });


    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchOnline(req, res) {
    try {

    } catch(e) {
        errorHandler(e, res);
    }
}

async function add(req, res) {
    try {
        const { _id: User } = req.user;
        const { id } = req.params;
        const $inc = { Friends: 1 };

        await Friends.findOneAndUpdate({User: id}, { $pull: { OutgoingRequestIds: User }, $push: { Friends: { $each: [User], $position: 0 } } }, {new: true});
        await Friends.findOneAndUpdate({ User }, { $pull: { IncomingRequestIds: id }, $push: { Friends: { $each: [id], $position: 0 } } }, {new: true});

        await TotalStats.findOneAndUpdate({User: id}, { $inc });
        await TotalStats.findOneAndUpdate({User}, { $inc });


        res.status(200).json({ message: 'Пользователь добавлен в друзья' });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function remove(req, res) {
    try {
        const { _id: User } = req.user;
        const { id } = req.params;
        const $inc = { Friends: -1 };

        await Friends.findOneAndUpdate({User: id}, { $pull: { Friends: User } });
        await Friends.findOneAndUpdate({User}, { $pull: { Friends: id } });

        await TotalStats.findOneAndUpdate({User: id}, { $inc });
        await TotalStats.findOneAndUpdate({User}, { $inc });
        
        
        res.status(200).json({ message: 'Пользователь удален из списка друзей' });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function requestToAdd(req, res) {
    try {
        const { _id: User } = req.user;
        const { id } = req.params;
        const { action } = req.body;
        const message = `Заявка ${action === 'add' ? 'отправлена' : 'отменена'}.`;

        const own = action === 'add'
            ? { $push: { OutgoingRequestIds: { $each: [id], $position: 0 } } }
            : { $pull: { OutgoingRequestIds: id } };

        const other = action === 'add'
        ? { $push: { IncomingRequestIds: { $each: [User], $position: 0 } } }
        : { $pull: { IncomingRequestIds: User } };

        const o = await Friends.findOneAndUpdate({ User }, own, { new: true });
        const f = await Friends.findOneAndUpdate({ User: id }, other, { new: true });

        res.status(200).json({ message });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function rejectToAdd(req, res) {
    try {

        const { id } = req.params;
        const { _id: User } = req.user;

        await Friends.findOneAndUpdate({User: id}, { $pull: { OutgoingRequestIds: User } }, { new: true });
        await Friends.findOneAndUpdate({ User }, { $pull: { IncomingRequestIds: id } }, { new: true });

        res.status(200).json({ message: 'Завка отклонена' });

    } catch(e) {
        errorHandler(e, res);
    }
}



module.exports = {
    fetch,
    fetchRequests,
    fetchOnline,
    fetchMutual,
    fetchIncomplete,
    add,
    remove,
    requestToAdd,
    rejectToAdd
}