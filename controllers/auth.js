const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { jwtKey, refreshJwtKey } = require('../keys/keys');
const errorHandler = require('../handlers/errorHandler');
const { createAccountFolders } = require('../handlers/handlers');
const User = require('../models/User');
const Friends = require('../models/Friends');
const Info = require('../models/Info');
const Token = require('../models/Token');
const TotalStats = require('../models/TotalStats');
const PictureAlbum = require('../models/PictureAlbum');
const PicturesCollection = require('../models/Pictures.collection');
const AudioPlaylist = require('../models/AudioPlaylist');
const APlaylistsCollection = require('../models/APlaylists.collection');
const VideoPlaylist = require('../models/VideoPlaylist');
const VPlaylistsCollection = require('../models/VPlaylists.collection');
const Unseen = require('../models/Unseen');


async function login(req, res) {

    try {

        const { Login, Password } = req.body;

        const candidate = await User.findOne({ Login });

        if(candidate) {
            const comparePassword = bcrypt.compareSync(Password, candidate.Password);

            if(!comparePassword) {
              return res.status(402).json({ message: 'Неверный пароль, попробуйте еще раз' });
            }
            
            const access_token = jwt.sign({ _id: candidate._id, Name: candidate.Name  }, jwtKey, { expiresIn: '1h' });
            const refresh_token = jwt.sign({ _id: candidate._id, Name: candidate.Name }, refreshJwtKey, { expiresIn: '1d' });

            await Token.findOneAndUpdate({ User: candidate._id }, { $set: { access_token, refresh_token} });

            return res.status(200).json({ access_token, refresh_token });

        }

        return res.status(404).json({ message: 'Пользователь с таким логином не зарегистрирован' });
        

    } catch (e) {
        console.log(e)
        errorHandler(e, res)
    }
}


async function registration(req, res) {
    try {

        const salt = bcrypt.genSaltSync(10);
        const Password = bcrypt.hashSync(req.body.Password, salt);
        const candidate = Object.assign({}, req.body, { Password });
        
        const create = await new User(candidate).save();

        const { _id } = create.toJSON(); 
        
        await new Info({User: _id, Name: create.Name}).save();
        await new Friends({ User: _id }).save();
        await new Unseen({ User: _id }).save();
        await new PictureAlbum({User: _id, Type: 'Saved', Title: 'Сохраненные фотографии'}).save();
        await new PictureAlbum({_id, User: _id, Type: 'Avatars', Title: 'Фотографии профиля'}).save();
        await new PicturesCollection({User: _id}).save();
        await new AudioPlaylist({User: _id, Creator: _id, CreatorName: create.Name,  Type: 'Default', Title: 'default', _id: _id}).save();
        await new APlaylistsCollection({User: _id}).save();
        await new VideoPlaylist({User: _id, CreatorName: create.Name, Type: 'Default', Title: 'default', _id: _id}).save();
        await new VPlaylistsCollection({User: _id}).save();
        await new TotalStats({ User: _id }).save();
        await new Token({User: _id}).save();

        createAccountFolders(_id);

        
        res.status(200).json({ message: 'Используйте свои данные для входа в кабинет.' });

    } catch (e) {
        errorHandler(e, res)
    }
}

async function refreshToken(req, res) {
    
    try {

        const { token: refreshToken } = req.body;
        const { _id, Name } = jwt.decode(refreshToken);
        
        const token = await Token.findOne({ User: _id });

        jwt.verify(refreshToken, refreshJwtKey, async (err, decoded) => {

            const message = 'Сбой авторизации.Используйте повторно свои учетные данные для входа в личный кабинет';

            if(err) return res.status(401).json({ message });


            const isEqual = Object.is(refreshToken, token.refresh_token);

            if(!isEqual) return res.status(401).json({ message })


            const access_token = jwt.sign({ _id, Name  }, jwtKey, { expiresIn: '1h' });
            const refresh_token = jwt.sign({ _id, Name  }, refreshJwtKey, { expiresIn: '1d' });

            token.access_token = access_token;
            token.refresh_token = refresh_token;

            await token.save();

            res.status(200).json({access_token, refresh_token});
        })

    } catch(e) {
        errorHandler(e, res);
    }
}


async function getByLogin(req, res) {

    try {
        const { Login } = req.body
        const user = await User.findOne({ Login }, { Login: 1, Name: 1, _id: 0 });

        res.status(200).json(user);

    } catch (e) {
        errorHandler(e, res)
    }
}




module.exports =  { 
    login, 
    registration, 
    getByLogin,
    refreshToken
}