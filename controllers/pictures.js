const del = require('del');
const errorHandler = require('../handlers/errorHandler');
const { paginationAggregate, mongoObjectId } = require('../handlers/handlers');
const { getDataByArrayKeys } = require('../handlers/db');
const IPicture = require('../classes/IPicture');
const Picture = require('../models/Picture');
const PictureAlbum = require('../models/PictureAlbum');
const IPictureAlbum = require('../classes/IPictureAlbum');
const UserSchema = require('../models/User');
const TotalStats = require('../models/TotalStats');

async function fetch(req, res) {
    try {
       
        const { _id: User } = req.user;
        const { skip, limit } = req.body;
        const { id: Album } = req.params;
        const savedAlbum = await PictureAlbum.findOne({ User, Type: 'Saved' });

        const $match = { Album };
        const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
        const $project = getDataByArrayKeys(['aggregate', 'picture', 'project']);
        const $sort = { Created: -1 }

        $project.OwnLike = { $in: [User, '$Likes'] };
        $project.IsHaveSaved = { $in: ['$Path', savedAlbum.ListPaths] };

        const aggregate = [
            { $match }, 
            { $lookup }, 
            { $project }, 
            { $sort },
            ...paginationAggregate(skip, limit)
        ]

        const [result] = await Picture.aggregate(aggregate);

        const rows = result ? result.rows : [];
        const totalCount = result ? result.totalCount : [];

        res.status(200).json({ rows, totalCount });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchByType(req, res) {
    const { _id: OwnId } = req.user;
    const { id: User } = req.params;
    const { Type } = req.query;
    const { skip, limit } = req.body;
    const savedAlbum = await PictureAlbum.findOne({ User: OwnId, Type: 'Saved' });

    const $match = { User: mongoObjectId(User), Type };
    const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
    const $project = getDataByArrayKeys(['aggregate', 'picture', 'project']);
    const $sort = { Created: -1 }

    $project.OwnLike = { $in: [OwnId, '$Likes'] };
    $project.IsHaveSaved = { $in: ['$Path', savedAlbum.ListPaths] };

    const aggregate = [
        { $match }, 
        { $lookup }, 
        { $project }, 
        { $sort },
        ...paginationAggregate(skip, limit)
    ]

    const [result] = await Picture.aggregate(aggregate);

    const rows = result ? result.rows : [];
    const totalCount = result ? result.totalCount : [];

    res.status(200).json({ rows, totalCount });
}


async function fetchBySlider(req, res) {
    try {

        const { page: { skip, limit }, albumType } = req.body;

        const { _id: OwnId } = req.user;
        const { id } = req.params;
        const savedAlbum = await PictureAlbum.findOne({ User: OwnId, Type: 'Saved' });

        
        const $match = albumType === 'Main' ? { User: mongoObjectId(id), Type: 'Created' } : { Album: id };
        const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
        const $project = getDataByArrayKeys(['aggregate', 'picture', 'project']);
        const $sort = { Created: -1 };

        $project.OwnLike = { $in: [OwnId, '$Likes'] };
        $project.IsHaveSaved = { $in: ['$Path', savedAlbum.ListPaths] };
    
        const aggregate = [
            { $match }, 
            { $sort },
            { $lookup }, 
            { $project }, 
            { $skip: (skip * limit)},
            { $limit: +limit }
        ]

        const pictures = await Picture.aggregate(aggregate);
    
        res.status(200).json(pictures);

    } catch(e) {
        errorHandler(e, res);
    }
}


async function fetchById(req, res) {
    try {
        const { id: pictureId } = req.params;
        const { _id: OwnId } = req.user;

        const savedAlbum = await PictureAlbum.findOne({ User: OwnId, Type: 'Saved' });
    
        const $match = {_id: mongoObjectId(pictureId)};

        const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
        const $project = getDataByArrayKeys(['aggregate', 'picture', 'project']);
        const $sort = { Created: -1 }
    
        $project.OwnLike = { $in: [OwnId, '$Likes'] };
        $project.IsHaveSaved = { $in: ['$Path', savedAlbum.ListPaths] };
    
        const [picture] = await Picture.aggregate([{ $match }])
                                        .lookup($lookup)
                                        .project($project)
                                        .sort($sort)
                                        .skip(0)
                                        .limit(1);
    
        res.status(200).json(picture);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function create(req, res) {
    try {
        const { id: Album } = req.params;
        const { _id: User } = req.user;
        const { ListPaths } = await PictureAlbum.findOne({ User, Type: 'Saved' });

        const album = await PictureAlbum.findById(Album);

        const Type = album.Type;

        const candidates = req.files.map(file => {
            const Path = file.path;
            const candidate = {Path, Album, User, Type};

            return candidate;
        });

        const created = await Picture.insertMany(candidates);

        album.TotalCount += candidates.length;

        const paths = req.files.map(file => file.path);

        if(Type !== 'Created') {
            const Logo = req.files[req.files.length - 1].path;
            album.Logo = Logo;
        } 

        album.ListPaths.push(...paths)

        await album.save();

        const pictures = created.map(p => new IPicture(p, User, ListPaths));

        await TotalStats.findOneAndUpdate({ User }, { $inc: { Pictures: paths.length } });


        res.status(200).json(pictures);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function save(req, res) {
    try {
        const { _id: User } = req.user;
        const { Path, IsHaveSaved, _id: PictureId} = req.body;
        const album = await PictureAlbum.findOne({ User, Type: 'Saved' });
        const Album = album.toObject()._id;
        const Type = 'Saved';

        if(!IsHaveSaved) {
    
            await new Picture({ Album, Type, Path, User }).save();
            
            album.TotalCount++;
            album.ListPaths.push(Path);
            album.Logo = Path;
        
        } else {
            const idx = album.ListPaths.indexOf(Path);

            await Picture.findOneAndDelete({ Path, Type, User, Album });

            album.TotalCount--;
            album.ListPaths.splice(idx, 1);
            
            const isLogo = Object.is(album.Logo, Path);

            if(isLogo) {
                const firstPicture = await Picture.findOne({ Album }).sort('-Created');

                album.Logo = firstPicture ? firstPicture.Logo : null;
            }
        }

        await album.save();

        const action = IsHaveSaved ? 'удалена' : 'сохранена';

        res.status(200).json({ message: `Фотография ${action}.` });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function remove(req, res) {
    try {

        const { _id: User } = req.user;
        const { id: albumId } = req.params;
        const  { pictureIds, picturePaths } = req.body;
        const count = pictureIds.length;

        const album = await PictureAlbum.findById(albumId);

        const isSavedAlbum = album.Type === 'Saved';

        await Picture.deleteMany({ _id: { $in: pictureIds } });

        if(isSavedAlbum) {
            const picture = await Picture.findOne({ Album: albumId }).sort({Created: -1});

            const Logo = picture ? picture.Path : null;

            album.Logo = Logo;
        }

        if(!isSavedAlbum) {
            TotalStats.findOneAndUpdate({ User }, { $inc: { Pictures: -pictureIds.length } }).then()
        }

        picturePaths.forEach(path => {
            const idx = album.ListPaths.indexOf(path);

            if(~idx) album.ListPaths.splice(idx, 1);
        })

        album.TotalCount -= count;

        await album.save();

        res.status(200).json({ message: 'Фотографии удалены!' });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function replace(req, res) {
    try {

        const { fromAlbumId } = req.query;
        const { id: toAlbumId } = req.params;
        const { _id: User } = req.user;
        const {pictureIds, picturePaths} = req.body;
        const count = pictureIds.length;
        const Updated = new Date();

        const toAlbum = await PictureAlbum.findById(toAlbumId);
        const fromAlbum = await PictureAlbum.findById(fromAlbumId);

        await Picture.updateMany({ _id: { $in: pictureIds } }, { $set: { Album: toAlbumId, Type: toAlbum.Type, Updated, Created: Updated } });

        if(toAlbum.Type === 'Saved') {
            const lastPictureId = pictureIds[pictureIds.length - 1];
            const { Path } = await Picture.findById(lastPictureId);

            TotalStats.findOneAndUpdate({ User }, { $inc: { Pictures: -pictureIds.length } }).then()

            toAlbum.Logo = Path
        }

        if(fromAlbum.Type === 'Saved') {
            const picture = await Picture.findOne({ Album: fromAlbum._id }).sort({ Created: -1 });
            fromAlbum.Logo = picture ? picture.Path : null
        }

        picturePaths.forEach(path => {
            const idx = fromAlbum.ListPaths.indexOf(path);

            if(~idx) fromAlbum.ListPaths.splice(idx, 1);
        });

        toAlbum.ListPaths.push(...picturePaths);

        toAlbum.TotalCount += count;
        fromAlbum.TotalCount -= count;

        await toAlbum.save();
        await fromAlbum.save();

        res.status(200).json({ message: 'Фотографии перемещены' });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function edit(req, res) {
    try {
        const { id: pictureId } = req.params;

        await Picture.findByIdAndUpdate(pictureId, { $set: req.body });

        res.status(200).json({ message: 'Фотография отредактированна' });

    } catch(e) {
        errorHandler(e, res);
    }
}


async function fetchAlbums(req, res) {
    try {
        const { id: User } = req.params;
        const { skip, limit } = req.body;

        const $match = { User: mongoObjectId(User) };
        const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
        const $project = getDataByArrayKeys(['aggregate', 'picture-album', 'project']);
        const $sort = { Created: -1 }
        
        const aggregate = [
            { $match },
            { $lookup },
            { $project },
            { $sort },
            ...paginationAggregate(skip, limit)
        ];
        
        const [result] = await PictureAlbum.aggregate(aggregate);


        const rows = result ? result.rows : [];
        const totalCount = result ? result.totalCount : 0;
    
        res.status(200).json({ rows, totalCount });

    } catch(e) {
        errorHandler(e, res)
    }
}

async function fetchReplaceAlbums(req, res) {
    try {

        const { _id: User } = req.user;
        const { id: ExcludeAlbum } = req.params;
        const { skip, limit } = req.body;

        const $match = { User: mongoObjectId(User), _id: { $ne: mongoObjectId(ExcludeAlbum) }, Type: { $ne: 'Avatars' } }
        const $project = getDataByArrayKeys(['aggregate', 'picture-replace-album', 'project']);
        const $sort = { Created: -1 }
        
        const aggregate = [
            { $match },
            { $project },
            { $sort },
            ...paginationAggregate(skip, limit)
        ];
        
        const [result] = await PictureAlbum.aggregate(aggregate);


        const rows = result ? result.rows : [];
        const totalCount = result ? result.totalCount : 0;
    
        res.status(200).json({ rows, totalCount }); 

    } catch(e) {

    }
}

async function fetchAlbumById(req, res) {
    
    try {

        const { id } = req.params;

        const $match = { _id: mongoObjectId(id) };
        const $lookup  = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
        const $project = getDataByArrayKeys(['aggregate', 'picture-album', 'project']);
        
        const result = await PictureAlbum.aggregate([{$match}]).lookup($lookup).project($project).limit(1);

        res.status(200).json(result[0]);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function createAlbum(req, res) {
    try {

        const candidate = req.file ? JSON.parse(req.body.body) : req.body;
        const { _id: User } = req.user;
        
        if(req.file) {
            candidate.Logo = req.file.path;
        }

        const create = await new PictureAlbum(candidate).save();
        const user = await UserSchema.findById(User);

        const album = new IPictureAlbum(create, user);

        res.status(200).json(album);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function editAlbum(req, res) {
    try {

        const candidate = req.file ? JSON.parse(req.body.body) : req.body;
        const { Title, Description, Logo } = candidate;
        const { id } = req.params;

        const $set = { Title, Description }; 
        
        if(req.file) {
            if(Logo) del(Logo).then(_ => 'Logo album remove');
            $set.Logo = req.file.path;
        }

        await PictureAlbum.findByIdAndUpdate(id, { $set });

        const album = Object.assign({}, candidate, $set);

        res.status(200).json(album);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function removeAlbum(req, res) {
    try {

        const { _id: User } = req.user;
        const { id: Album } = req.params;
        const album = req.body;

        if(album.type === 'Created') {
            TotalStats.findOneAndUpdate({User}, { $inc: { Pictures: -album.TotalCount } })
        }

        await PictureAlbum.findByIdAndDelete(Album);
        Picture.deleteMany({Album}).then(_ => 'Фотографии альбома удалены');

        res.status(200).json({ message: 'Альбом удален' })

    } catch(e) {
        errorHandler(e, res);
    }
}




module.exports = {
    fetch, 
    fetchById,
    fetchByType,
    fetchBySlider,
    create, 
    save,
    remove,
    replace,
    edit,
    fetchAlbums,
    fetchReplaceAlbums,
    fetchAlbumById,
    createAlbum, 
    removeAlbum,
    editAlbum 
}