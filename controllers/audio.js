const del = require('del');
const { getAudioDurationInSeconds } = require('get-audio-duration');
const errorHandler = require('../handlers/errorHandler');
const { getMediaTags, paginationAggregate, mongoObjectId } = require('../handlers/handlers');
const User = require('../models/User');
const TotalStats = require('../models/TotalStats');
const IAudio = require('../classes/IAudio');
const Audio = require('../models/Audio');
const AudioPlaylist = require('../models/AudioPlaylist');
const APCollection = require('../models/APlaylists.collection');


async function fetch(req, res) {

    try {
        const { id: Playlist } = req.params;
        const { page: { skip, limit }, searchString, listType, state: { watchId: candidateWatchID } } = req.body;
        const watchId = candidateWatchID || req.user._id;
        const watchPlaylist = await AudioPlaylist.findById(watchId);
        const match = {};

        match.Playlist = listType === 'main' ? Playlist : { $ne: Playlist };
        listType === 'search' ? match.Type = 'Created' : null;
        

        const regex = { $regex: new RegExp(searchString, 'gi') };
        const sort = { Created: -1 };
        const filters = { $or : [ { Artist: regex }, { Title: regex } ] };

        const [result] = await Audio.aggregate([
            { $match: match },
            { $match: filters },
            { $sort: sort },
            { $project: {
                    Genre: 1,
                    Title: 1,
                    Artist: 1,
                    Path: 1,
                    Playlist: 1,
                    Duration: 1,
                    Logo: 1,
                    Created: 1,
                    Type: 1,
                    User: 1,
                    IsHave: { $in: ['$Path', watchPlaylist.ListPaths] }
                }
            },
            ...paginationAggregate(skip, limit)
        ]);

        
        const rows = result ? result.rows : [];
        const totalCount = result ? result.totalCount : 0;

        res.status(200).json({ rows, totalCount });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchByString(req, res) {

    try {

        const { searchString, page: { skip, limit } } = req.body;
        const watchPlaylist = await AudioPlaylist.findById(req.user._id);

        const regex = { $regex: new RegExp(searchString, 'gi') };
        const $match = { $or : [ { Artist: regex }, { Title: regex } ]  };
        const $sort = { Created: -1 };
        const $project = {
            Genre: 1,
            Title: 1,
            Artist: 1,
            Path: 1,
            Playlist: 1,
            Duration: 1,
            Logo: 1,
            Created: 1,
            Type: 1,
            User: 1,
            IsHave: { $in: ['$Path', watchPlaylist.ListPaths] },
        }

        const [result] = await Audio.aggregate([
            { $match },
            { $sort },
            { $project },
            { $group: {
                _id: {
                    Title: '$Title',
                },
                results: { $push: "$$ROOT" }
            } },
            { $group: {
                _id: null,
                results: { $push: {$arrayElemAt: ['$results', 0]} },
                totalCount: { $sum: 1 }
            } },
            
            { $project: {
                totalCount: 1,
                rows: { $slice: ['$results', skip * limit, +limit] }
            } }
        ]);

        const rows = result ? result.rows : [];
        const totalCount = result ? result.totalCount : 0;

        res.status(200).json({ rows, totalCount });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function getAudioById(req, res) {

    try {

        const { id } = req.params;

        const audio = await Audio.findById(id);

        res.status(200).json(audio)

    } catch(e) {
        errorHandler(e, res);
    }
}

async function getPlaylistById(req, res) {

    try {

        const { id } = req.params;
        const { _id: User } = req.user;

        const { CollectionID } = await APCollection.findOne({ User });

        const playlist = await AudioPlaylist.findById(id);

        const toObject = playlist.toObject();

        toObject.IsHave = CollectionID.includes(id);

        res.status(200).json(toObject);

    } catch(e) {
        errorHandler(e, res);
    }
}


async function create(req, res) {

    try {
        const { _id: User } = req.user;
        const Path = req.file.path;
        const Genre = JSON.parse(req.body.Genre);
        const Type = 'Created';
        const Duration = await getAudioDurationInSeconds(Path);
        const { _id: Playlist } = await AudioPlaylist.findOne({ User, Type: 'Default' });
        const mediaTags = await getMediaTags(req.file, User);

        const candidate = Object.assign({Path, Duration, Playlist, Genre, User, Type}, mediaTags);
        const create = await new Audio(candidate).save();
        const audio = new IAudio(create, [create.Path]);

        await TotalStats.findOneAndUpdate({ User }, { $inc: { Audio: 1 } });
        await AudioPlaylist.findByIdAndUpdate(Playlist, { $push: { ListPaths: audio.Path } });
        
        res.status(200).json(audio);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function edit(req, res) {

    try {
        const candidate = req.body;
        const update = await Audio.findByIdAndUpdate(candidate._id, { $set: candidate }, { new: true });
        const media = update.toObject();

        res.status(200).json(media); 


    } catch(e) {
        errorHandler(e, res);
    }
}

async function addToPlaylist(req, res) {

    try {
        const { Path } = req.body;
        const { id: Playlist } = req.params;

        const Created = new Date();
        const Type = 'Added';
        const isDefault = Object.is(Playlist, req.user._id);

        const candidate = Object.assign({}, req.body, {Created, Type, Playlist});

        delete candidate._id;

        const create = await new Audio(candidate).save();

        await AudioPlaylist.findByIdAndUpdate(Playlist, { $push: { ListPaths: Path }, $inc: { TotalCount: 1 } });

        if(isDefault) {
            await TotalStats.findOneAndUpdate({ User: req.user._id }, { $inc: { Audio: 1 } });
        }

        const media = create.toObject();

        media.IsHave = true;

        res.status(200).json(media);
        

    } catch(e) {
        errorHandler(e, res);
    }
}

async function removeFromPlaylist(req, res) {

    try {

        const { Path } = req.body;
        const { id: Playlist } = req.params;
        const isDefault = Object.is(Playlist, req.user._id);

        await Audio.findOneAndDelete({ Path, Playlist });
        await AudioPlaylist.findByIdAndUpdate(Playlist, { $pull: { ListPaths: Path }, $inc: { TotalCount: -1 } });

        if(isDefault) {
            await TotalStats.findOneAndUpdate({ User: req.user._id }, { $inc: { Audio: -1 } });
        }
        
    
        res.status(200).json({ message: 'Песня удалена' });


    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchPlaylists(req, res) {

    try {

        const { id } = req.params;
        const { skip, limit } = req.body;
        
        const {CollectionID: watchCollection} = await APCollection.findOne({ User: req.user._id });
        const { CollectionID } = await APCollection.findOne({ User: id });
        const start = skip * limit;
        const end = start + limit;


        const ids = CollectionID.slice(start, end);

        const asyncPlaylists = ids.map((id) => AudioPlaylist.findById(id))

        const playlists = await Promise.all(asyncPlaylists);

        const rows = playlists.map(p => {

            const playlist = p.toObject();
            delete playlist.ListPaths;
            playlist.IsHave = watchCollection.includes(playlist._id);

            return playlist;
        })

        const totalCount = CollectionID.length;

        res.status(200).json({ rows, totalCount }); 

    } catch(e) {
        errorHandler(e, res);
    }
}

async function getCreatedByUser(req, res) {
    try {

        const { _id: User } = req.user;
        const { audio: { Path }, page: { skip, limit } } = req.body;

        const [result] = await AudioPlaylist.aggregate([
            { $match: { Creator: User, Type: { $ne: 'Default' } } },
            { $sort: { Updated: -1 } },
            { $project: {
                    _id: 1,
                    Title: 1,
                    IsHave: { $in: [Path, '$ListPaths'] }
                }
            },
            ...paginationAggregate(skip, limit)
        ])

        const rows = result ? result.rows : [];
        const totalCount = result ? result.totalCount : 0;

        res.status(200).json({ rows, totalCount });

    } catch(e) {
        errorHandler(e, res)
    }
}

async function createPlaylist(req, res) {
 
    try {

        const { audios, playlist: candidate } = req.file ? JSON.parse(req.body.body) : req.body;

        const ListPaths = audios.map(a => a.Path);
        const Logo = req.file ? req.file.path : null;
        
        const { _id: UserId } = req.user;
        const { Name } = await User.findById(UserId, { Name: 1 });

        const playlist = Object.assign({}, candidate, { User: UserId, Creator: UserId, CreatorName: Name, ListPaths, Logo, TotalCount: ListPaths.length });

        const create = await new AudioPlaylist(playlist).save();

        await APCollection.findOneAndUpdate({ User: req.user._id }, {$push: { CollectionID: { $each: [ create._id ], $position: 0 } } });
        await TotalStats.findOneAndUpdate({ User: UserId }, { $inc: { AudioPlaylists: 1 }  });

        if(audios.length) {
            const { _id: Playlist } = create;
            const candidateAudios = audios.map(audio => {

                const Type = 'Added';
                const Created = new Date();
                
                delete audio._id;

                return Object.assign({}, audio, { Playlist, Type, Created });
            })

            await Audio.insertMany(candidateAudios);
        }

        res.status(200).json(create);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function editPlaylist(req, res) {
    
    try {
        const candidate = req.file ? JSON.parse(req.body.body) : req.body;
        
        if(req.file) {
            
            if(candidate.Logo) {
                del.sync(candidate.Logo);
            }

            candidate.Logo = req.file.path;
        }

        const edit = await AudioPlaylist.findByIdAndUpdate(candidate._id, { $set: candidate }, { new: true });
        
        res.status(200).json(edit);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function addPlaylist(req, res) {
 
    try {
        const { _id: User } = req.user;

        await APCollection.findOneAndUpdate({ User: req.user._id }, { $push: { CollectionID: { $each: [ req.params.id ], $position: 0 } } })

        await TotalStats.findOneAndUpdate({ User }, { $inc: { AudioPlaylists: 1 } });

        res.status(200).json({ media: create, IsHave: false });


    } catch(e) {
        errorHandler(e, res);
    }
}



async function removePlaylist(req, res) {

    try {

        const { _id: User } = req.user;
        const playlist = req.body;

        const isOwnList = playlist.Creator === User;

        if(isOwnList) {
            if(playlist.Logo) {
                del(playlist.Logo).then(_ => {})
            }

            await AudioPlaylist.findByIdAndDelete(req.params.id);
            Audio.deleteMany({ Playlist: req.params.id }).then(_=> console.log('Аудиозаписи, приндалежащие плейлисту удалены'));

            APCollection.find({ CollectionID: { $in: [req.params.id] } }, (err, res) => {
                if(err) throw err;

                res.forEach(item => {
                    const CollectionID = item.CollectionID.filter(id => String(id) !== req.params.id);

                    item.updateOne({ CollectionID }, () => {
                        console.log('Плейлист Успешно удален!');
                    }).exec();
                })

                
                const UserIds = res.map(obj => obj.User);
                
                TotalStats.updateMany({ User: { $in: UserIds } }, { $inc: {AudioPlaylists: -1} }).then(_ => {});
            })
    
        } else {
            await APCollection.findOneAndUpdate({ User: req.user._id }, { $pull: { CollectionID: req.params.id} });
            await TotalStats.findOneAndUpdate({ User }, { $inc: { AudioPlaylists: -1 } });
        }

        res.status(200).json({ message: "Плейлист удален." });

    } catch(e) {
        errorHandler(e, res);
    }
}


module.exports = {
    create,
    edit,
    fetch,
    fetchByString,
    getAudioById,
    getPlaylistById,
    addToPlaylist,
    removeFromPlaylist,
    fetchPlaylists,
    getCreatedByUser,
    createPlaylist,
    editPlaylist,
    addPlaylist,
    removePlaylist
}