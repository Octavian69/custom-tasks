const errorHandler = require('../handlers/errorHandler');
const Comment = require('../models/Comment');
const Picture = require('../models/Picture');
const Video = require('../models/Video');
const VideoPlaylist = require('../models/VideoPlaylist');

class LikeModels  {
    constructor() {
        this.Comment = Comment;
        this.Picture = Picture;
        this.Video = Video;
        this.VideoPlaylist = VideoPlaylist;
    }
}

const Models = new LikeModels();

async function like(req, res) {

    try {

        const { _id, Type, Checked } = req.body;
        const { _id: User } = req.user;
        
        const method = Checked ? '$pull' : '$push';

        await Models[Type].findByIdAndUpdate(_id, { [method]: { Likes: User } });

        res.status(200).json(null);

    } catch(e) {
        errorHandler(e, res);
    }
}

module.exports = {
    like
}