const errorHandler = require('../handlers/errorHandler');
const { mapMongoIds, mongoObjectId, createFilters, paginationAggregate } = require('../handlers/handlers');
const { getDataByArrayKeys } = require('../handlers/db');
const UserSchema = require('../models/User');
const Dialogue = require('../models/Dialogue');
const Message = require('../models/Message');
const Unseen = require('../models/Unseen');

 
async function singleMessage(req, res) {
    try {
        const { id } = req.params;
        const { _id: User } = req.user;
        const candidate = req.body;
        const { Name: UserName, Avatar: UserAvatar } = await UserSchema.findById(User);

        const $match = {Participants: { $all: [User, id] }, Type: 'dialogue' }

        const dialogue = await Dialogue.findOne($match);

        let message;
        let dialogueType;

        if(dialogue) {
            candidate.Dialogue = dialogue._id;
            dialogueType = dialogue.Type;

            const created = await new Message(candidate).save();

            dialogue.TotalMessages++;
            dialogue.Updated = Date.now();
            await dialogue.save();

            message = created.toObject();

        } else {
            const candidateDialogue = { Participants: [ User, id ], User, Type: 'dialogue' };
            const createdDialogue = await new Dialogue(candidateDialogue).save();

            dialogueType = createdDialogue.Type;

            candidate.Dialogue = createdDialogue._id;

            const created = await new Message(candidate).save();

            message = created.toObject();
        }

        message.UserName = UserName;
        message.Avatar = UserAvatar;
        message.DialogueType = dialogueType;

        res.status(200).json(message)
        
    }catch(e) {
        errorHandler(e, res);
    }
}

async function fetchMessages(req, res) {
    try {
        const { id: dialogueId } = req.params;
        const { page: { skip, limit } } = req.body;

        const dialogue = await Dialogue.findById(dialogueId);
        
        const $match = { Dialogue: mongoObjectId(dialogueId) };
        const $sort = { Created: -1 };
        const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
        const $project = getDataByArrayKeys(['aggregate', 'message', 'project'], true)(dialogue.Type);
        const $group = {
            _id: null,
            totalCount: { $sum: 1 },
            results: { $push: '$$ROOT' }
        }
        const $groupProject = {
            totalCount: 1,
            rows: { $slice: ['$results', +skip , +limit] }
        }

        const aggregate = [
            { $sort }, 
            { $match }, 
            { $lookup }, 
            { $project }, 
            { $group },
            { $project: $groupProject } 
        ]

        const [result] = await Message.aggregate(aggregate);

        const rows = result ? result.rows.sort((a, b) =>  +new Date(a.Created) - new Date(b.Created)) : [];
        const totalCount = result ? result.totalCount : 0;

        res.status(200).json({ rows, totalCount });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchDialogues(req, res) {
    try {
        const { _id: User } = req.user;
        const unseen = await Unseen.findOne({ User });

        const { filters, page: { skip, limit } } = req.body;
        const { dialogues: unreadDeialogues } = unseen.keys;

        const unread = Object.keys(unreadDeialogues || {});
        const unreadMongoIds = mapMongoIds(unread);
        const matchFilters = createFilters(filters, ['filters', 'dialogues']);

        const $match = { Participants: { $in: [mongoObjectId(User)] }, TotalMessages: { $gt: 0 }, ...matchFilters  };
        const $sort = { Updated: -1 };
        const $messageLookup = getDataByArrayKeys(['aggregate', 'lookup', 'lastMessage']);
        const $participantsLookup = getDataByArrayKeys(['aggregate', 'lookup', 'participants'], true)(mongoObjectId(User));
        const $project= getDataByArrayKeys(['aggregate', 'dialogue', 'project'], true)(mongoObjectId(User), unreadMongoIds);

        const aggreagate = [
            { $match },
            { $sort },
            { $lookup: $messageLookup },
            { $lookup: $participantsLookup },
            { $project },
            ...paginationAggregate(skip, limit)
        ]

        const [result] = await Dialogue.aggregate(aggreagate);

        const aggreagateRows = result ? result.rows : [];
        const totalCount = result ? result.totalCount : 0;

        const rows = aggreagateRows.map(dialogue => {
            dialogue.UnreadMessages = unreadDeialogues[dialogue._id] || 0;
            
            return dialogue;
        });

        res.status(200).json({rows, totalCount});

    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchDialogueById(req, res) {
    try {
        const { _id: User } = req.user;
        const { id } = req.params;
        const unseen = await Unseen.findOne({ User });

        const { dialogues: unreadDeialogues } = unseen.keys;
        const unread = Object.keys(unreadDeialogues || {});
        const unreadMongoIds = mapMongoIds(unread);

        const $match = { _id: mongoObjectId(id) };
        const $messageLookup = getDataByArrayKeys(['aggregate', 'lookup', 'lastMessage']);
        const $participantsLookup = getDataByArrayKeys(['aggregate', 'lookup', 'participants'], true)(mongoObjectId(User));
        const $project = getDataByArrayKeys(['aggregate', 'dialogue', 'project'], true)(mongoObjectId(User), unreadMongoIds);

        const aggreagate = [
            { $match },
            { $limit: 1 },
            { $lookup: $messageLookup },
            { $lookup: $participantsLookup },
            { $project }
        ]

        const [dialogue] = await Dialogue.aggregate(aggreagate);

        if(dialogue) {
            dialogue.UnreadMessages = unreadDeialogues[ String(dialogue._id)] || 0;
        }

        res.status(200).json(dialogue);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchByParticipants(req, res) {
    try {
        const { _id: User } = req.user;
        const participants = req.body;
        const { Type } = req.query;
        const unseen = await Unseen.findOne({ User });

        const { dialogues: unreadDeialogues } = unseen.keys;
        const unread = Object.keys(unreadDeialogues || {});
        const unreadMongoIds = mapMongoIds(unread);
        const $match = { Participants: { $all: mapMongoIds(participants) } , Type };
        const $messageLookup = getDataByArrayKeys(['aggregate', 'lookup', 'lastMessage']);
        const $participantsLookup = getDataByArrayKeys(['aggregate', 'lookup', 'participants'], true)(mongoObjectId(User));
        const $project = getDataByArrayKeys(['aggregate', 'dialogue', 'project'], true)(mongoObjectId(User), unreadMongoIds);

        const aggreagate = [
            { $match },
            { $limit: 1},
            { $lookup: $messageLookup },
            { $lookup: $participantsLookup },
            { $project }
        ]

        const [dialogue] = await Dialogue.aggregate(aggreagate);

        if(dialogue) {
            dialogue.UnreadMessages = unreadDeialogues[ String(dialogue._id) ] || 0;
        }

        res.status(200).json(dialogue);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function createMessage(req, res) {
    try {
        const candidate = req.body;
        const { _id: User } = req.user;
        const { Name: UserName, Avatar: UserAvatar } = await UserSchema.findById(User);
        const { Dialogue: DialogueId } = candidate;
        const dialogue = await Dialogue.findById(DialogueId);
        const created = await new Message(candidate).save();

        await Dialogue.findByIdAndUpdate(DialogueId, { $inc: { TotalMessages: 1 }, $set: { Updated: new Date() } });

        dialogue.TotalMessages++;
        dialogue.Updated = new Date();

        await dialogue.save();


        const message = created.toObject();

        message.IsOwn = true;
        message.UserName = UserName;
        message.UserAvatar = UserAvatar;
        message.DialogueType = dialogue.Type;

        res.status(200).json(message);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function editMessage(req, res) {
    try {
        const { Text, _id } = req.body;

        await Message.findByIdAndUpdate(_id, { $set: { Text } });

        res.status(200).json(req.body);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function removeMessages(req, res) {

    try {

        const messagesIds = req.body;

        await Message.deleteMany({ _id: { $in: messagesIds } });

        res.status(200).json({ message: 'Сообщения успешно удалены' });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function createDialogue(req, res) {
    try {
        const { id } = req.params;
        const { Name, Avatar, _id} = await UserSchema.findById(id);
        const candidate = req.body;
        candidate.TotalMessages = 0;
        const createdDialogue = await new Dialogue(candidate).save();
        const dialogue = createdDialogue.toJSON();

        dialogue.Participants = [ { Name, Avatar, _id } ];
        dialogue.Title = Name;
        dialogue.Logo = Avatar;
        dialogue.LastMessage = null;
        dialogue.Unread = false;
        dialogue.UnreadMessages = 0;
        dialogue.IsOwn = true;

        res.status(200).json(dialogue);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function createDiscussion(req, res) {
    try {
        const { _id: User } = req.user;
        const { dialogue: candidateDialogue, message: candidateMessage } = req.file ? JSON.parse(req.body.body) : req.body;
        const { Name: UserName, Avatar: UserAvatar } = await UserSchema.findById(User);
        
        const $messageLookup = getDataByArrayKeys(['aggregate', 'lookup', 'lastMessage']);
        const $participantsLookup = getDataByArrayKeys(['aggregate', 'lookup', 'participants'], true)(mongoObjectId(User));
        const $project = getDataByArrayKeys(['aggregate', 'dialogue', 'project'], true)(mongoObjectId(User), []);



        if(req.file) {
            candidateDialogue.Logo = req.file.path;
        }

        const createdDialogue = await new Dialogue(candidateDialogue).save();
        candidateMessage.Dialogue = createdDialogue._id;

        const createdMessage = await new Message(candidateMessage).save();
        const message = createdMessage.toObject();

        const $match = { _id: mongoObjectId(createdDialogue._id) };
        const aggreagate = [
            { $match },
            { $limit: 1 },
            { $lookup: $messageLookup },
            { $lookup: $participantsLookup },
            { $project }
        ]
        const [dialogue] = await Dialogue.aggregate(aggreagate);

        dialogue.UnreadMessages = 0;
        dialogue.IsOwn = true;
        dialogue.LastMessage = {
            User,
            Text: message.Text,
            Created: message.Created,
            _id: message._id
        }

        message.UserName = UserName;
        message.UserAvatar = UserAvatar;
        message.DialogueType = 'discussion';

        res.status(200).json({ dialogue, message });

    } catch(e) {
        errorHandler(e, res);
    }
}


module.exports = {
    singleMessage,
    fetchMessages,
    fetchDialogueById,
    fetchDialogues,
    fetchByParticipants,
    createMessage,
    editMessage,
    removeMessages,
    createDialogue,
    createDiscussion
}