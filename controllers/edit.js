const errorHandler = require('../handlers/errorHandler');
const Info = require('../models/Info');

async function update(req, res) {

    try {

        const { part } = req.params;
        const { type } = req.query;
        const { _id: User } = req.user;

        const info = await Info.findOne({ User });

        switch(type) {
            
            case 'Simple': {
                info[part] = req.body[part];
                break;
            }
            
            case 'Single': {
                Object.assign(info[part], req.body);
                break;
            }

            case 'Array': {

                req.body.forEach(item => {
                    const idx = info[part].findIndex(partItem => partItem._id === item._id);

                    if(~idx) info[part][idx] = item;
                    else  info[part].push(item);
                })
                
                break;
            }
        }

        await info.save();

        res.status(200).json({ message: `Информация обновлена` });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function remove(req, res) {

    try {
        const { _id: User } = req.user;
        const { id } = req.params;
        const { part } = req.query;
        const info = await Info.findOne({ User });

        const idx = info[part].findIndex(item => item._id === id);

        info[part].splice(idx, 1);

        await info.save();

        res.status(200).json({ message: 'Раздел формы удален' });

    } catch(e) {
        errorHandler(e, res);
    }
}




module.exports = {
    update,
    remove
}