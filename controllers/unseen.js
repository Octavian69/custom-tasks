const errorHandler = require('../handlers/errorHandler');
const Unseen = require('../models/Unseen');


async function fetch(req, res) {
    try {
        const { _id: User } = req.user;

        const unseen = await Unseen.findOne({ User });

        res.status(200).json(unseen);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function update(req, res) {
    try {
        const { _id: User } = req.user;
        const unseen = await Unseen.findOne({ User });

        const [key] = Object.keys(req.body);

        unseen.keys[key] = req.body[key];
        
        await unseen.save();

        res.status(200).json(unseen);


    } catch(e) {
        errorHandler(e, res);
    }
}

async function reset(req, res) {
    try {
        const { _id: User } = req.user;
        const { key } = req.body;
        const unseen = await Unseen.findOne({ User });

        unseen.keys[key] = {};


        await unseen.save();

        res.status(200).json(unseen);


    } catch(e) {
        errorHandler(e, res);
    }
}

module.exports = {
    fetch,
    update,
    reset
}