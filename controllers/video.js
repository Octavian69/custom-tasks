const del = require('del');
const errorHandler = require('../handlers/errorHandler');
const { getDataByArrayKeys } = require('../handlers/db');
const { paginationAggregate, detectedFilters, mapMongoIds, createFilters, mongoObjectId } = require('../handlers/handlers');
const { videoMediaTags } = require('../handlers/handlers');
const UserSchema = require('../models/User');
const TotalStats = require('../models/TotalStats');
const IVideo = require('../classes/IVideo');
const IVideoPlaylist = require('../classes/IVideoPlaylist');
const Video = require('../models/Video');
const VideoPlaylist = require('../models/VideoPlaylist');
const VPCollection = require('../models/VPlaylists.collection');


async function fetch(req, res) {
    try {
        const { filters, sorted } = req.body;
        const isFilters = detectedFilters({ ...filters, ...sorted });

        const response = await (isFilters.length ? fetchWithFilters : fetchWithoutFilters)(req);

        res.status(200).json(response);
        
    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchWithFilters(req) {
    const { _id: User } = req.user;
    const { id: Playlist } = req.params;
    const { page: { skip, limit }, filters, sorted, listType } = req.body;

    const { ListIds: OwnList } = await VideoPlaylist.findById(User);
    const { ListIds: CurrentList } = Playlist === User ? { ListIds: OwnList } : await VideoPlaylist.findById(Playlist);
    const currentMongoIds = mapMongoIds(CurrentList);

    const $match = createFilters(filters, ['video', 'filterKeys']);
    const $sort = sorted;
    
    const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
    const $project = getDataByArrayKeys(['aggregate', 'video', 'project']);

    $project.IsHave = { $in: ['$_id', currentMongoIds] };
    $project.IsHaveOwn = { $in: ['$_id', mapMongoIds(OwnList)] };
    $project.OwnLike = { $in: [User, '$Likes'] };

    const searchIdKey = listType === 'search' ? '$nin' : '$in';
    $match._id = { [searchIdKey]: currentMongoIds }

    const aggregate = [
        { $match },
        { $lookup },
        { $project },
        ...paginationAggregate(skip, limit)
    ];
    
    if(Object.keys(sorted).length) {
        aggregate.splice(3, 0, { $sort });
    } 

    const [result] = await Video.aggregate(aggregate);

    const rows = result ? result.rows : [];
    const totalCount = result ? result.totalCount : 0;

    return { rows, totalCount };
}


async function fetchWithoutFilters(req) {
    const { _id: User } = req.user;
    const { id: Playlist } = req.params;
    const { page: { skip, limit } } = req.body;

    const start = skip * limit;
    const end = start + limit;

    const { ListIds: CurrentList } = await VideoPlaylist.findById(Playlist);
    const { ListIds: OwnList } = User !== Playlist ? await VideoPlaylist.findById(User, { ListIds: 1 }) : { ListIds: CurrentList };

    const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
    const $project = getDataByArrayKeys(['aggregate', 'video', 'project']);


    $project.IsHave = { $in: ['$_id', mapMongoIds(CurrentList)] };
    $project.IsHaveOwn = { $in: ['$_id', mapMongoIds(OwnList)] };
    $project.OwnLike = { $in: [User, '$Likes'] };

    const ids = CurrentList.slice(start, end);
    const promises = ids.map(id => Video.aggregate([{ $match: { _id: mongoObjectId(id) } }]).lookup($lookup).project($project).limit(1));

    const videos = await Promise.all(promises);
    const rows = videos.map(current => current[0]);

    const totalCount = CurrentList.length;

    return { rows, totalCount };
}

async function getVideoById(req, res) {
    try {
        
        const { id } = req.params;
        const { _id: User }= req.user;
        const { detail_playlist_id } = req.query;

        const { ListIds: OwnCollection } = await VideoPlaylist.findById(User); 
        const { ListIds: CurrentCollection } = detail_playlist_id ? await VideoPlaylist.findById(detail_playlist_id) : { ListIds: OwnCollection };

        const $match = { $match: { _id:  mongoObjectId(id)} };
        const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
        const $project = getDataByArrayKeys(['aggregate', 'video', 'project']);
    
        $project.IsHaveOwn = { $in: ['$_id', mapMongoIds(OwnCollection)] };
        $project.IsHave = { $in: ['$_id', mapMongoIds(CurrentCollection)] };
        $project.OwnLike = { $in: [User, '$Likes'] };

        const result = await Video.aggregate([$match]).lookup($lookup).project($project).limit(1);

        res.status(200).json(result[0]);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function create(req, res) {
    try {
        const { _id: User } = req.user;
        const body = JSON.parse(req.body.candidate);
        const tags = await videoMediaTags(req.file, User);
        const { Name: CreatorName } = await UserSchema.findById(User);
        const candidate = Object.assign({}, body, { CreatorName }, tags);

        const create = await new Video(candidate).save();
        const video = new IVideo(create, User, [String(create._id)]);

        await VideoPlaylist.findOneAndUpdate({User}, { $push: { ListIds: { $each: [create._id] , $position: 0 } } });
        await TotalStats.findOneAndUpdate({ User }, { $inc: { Video: 1 } });


        res.status(200).json(video);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function edit(req, res) {
    try {
        const { id } = req.params;

        await Video.findByIdAndUpdate(id, { $set: { ...req.body, Updated: new Date() } }, { new: true } );

        res.status(200).json({ message: 'Видео обновлено' });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function add(req, res) {
    try {
        const { _id: User } = req.user;
        const { id: Playlist } = req.params;
        const candidate = req.body;
        const isOwnList = Object.is(User, Playlist);

       await VideoPlaylist.findByIdAndUpdate(Playlist, { $push: { ListIds: { $each: [candidate._id] , $position: 0 } } });


        if(isOwnList) {
            await TotalStats.findOneAndUpdate({ User }, { $inc: { Video: 1 } });
        }

        Video.findByIdAndUpdate(candidate._id, { $inc: { AmountAdditions: 1 } }).then(_ => {});

        res.status(200).json({ message: 'Видео добавлено' });
        
    } catch(e) {
        errorHandler(e, res);
    }
}

async function remove(req, res) {
    try {
        const { _id: User } = req.user;
        const { id: Playlist } = req.params;
        const candidate = req.body;
        const isOwnPlaylist = Playlist === User;

        await VideoPlaylist.findByIdAndUpdate(Playlist, { $pull: { ListIds: candidate._id } });
        Video.findByIdAndUpdate(candidate._id, { $inc: { AmountAdditions: -1 } }).then(_ => {})

        if(isOwnPlaylist) {
            await TotalStats.findOneAndUpdate({ User }, { $inc: { Video: -1 } });
        }

        res.status(200).json({ message: 'Видео удалено' });
        
    } catch(e) {
        errorHandler(e, res);
    }
}

async function fetchPlaylists(req, res) {
    try {
       
        const { page: { skip, limit }, searchString } = req.body;
        
        const response = await (searchString ? fetchListWithFilters : fetchListsWithoutFilters)(req, skip, limit);


        res.status(200).json(response);

    } catch(e) {
        errorHandler(e, res);
    }
}


async function fetchListWithFilters(req, skip, limit) {
    const { _id: User } = req.user;
    const { searchString, listType } = req.body;
    const { id } = req.params;

    const { CollectionID: OwnCollection } = await VPCollection.findOne({ User });
    const { CollectionID: CurrentCollection } = id === User ? { CollectionID: OwnCollection } : await VPCollection.findOne({ User: id });

    const idQuery = listType === 'search' ? '$nin' : '$in';

    const $regex = {$regex: new RegExp(searchString, 'gi')};

    const $match = {
        _id: { [idQuery]: mapMongoIds(CurrentCollection) },
        Type: 'Created',
        $or: [ 
        { Title: $regex},
        { Description: $regex}
     ]
    };
    const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
    const $project = getDataByArrayKeys(['aggregate', 'video-playlist', 'project']);

    $project.IsHave = { $in: ['$_id', mapMongoIds(OwnCollection)] };
    $project.OwnLike = { $in: [User, '$Likes']};

    const aggregate = [
        {$match},
        { $lookup },
        { $project },
        ...paginationAggregate(skip, limit)
    ];

    const [result] = await VideoPlaylist.aggregate(aggregate);

    const rows = result ? result.rows : [];
    const totalCount = result ? result.totalCount : 0;

    return { rows, totalCount };
}

async function fetchListsWithoutFilters(req, skip, limit) {
    const { _id: User } = req.user;
    const { id } = req.params;

    const { CollectionID } = await VPCollection.findOne({ User: id });

    const isOwnLists = id === User;
    const { CollectionID: OwnCollection } = isOwnLists ? { CollectionID } : await VPCollection.findOne({ User });

    const start = skip * limit;
    const end = start + limit;

    const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
    const $project = getDataByArrayKeys(['aggregate', 'video-playlist', 'project']);

    $project.IsHave = { $in: ['$_id', mapMongoIds(OwnCollection)] };
    $project.OwnLike = { $in: [User, '$Likes']};

    const ids  = CollectionID.slice(start, end);
    
    const promises = ids.map(id => {
        const $match = { $match: { _id: mongoObjectId(id) } };

        return VideoPlaylist
                .aggregate([$match])
                .lookup($lookup)
                .project($project)
                .limit(1)
    });

    const playlists = await Promise.all(promises);

    const rows = playlists.map(pl => pl[0]);
    
    const totalCount = CollectionID.length;

    return { rows, totalCount };
}

async function getPlaylistsByCreator(req, res) {

    try {
        const { _id: User } = req.user;
        const {skip, limit} = req.body;

        const { video_id } = req.query; 

        const { CollectionID } = await VPCollection.findOne({ User });

        const $match = {
            _id: { $in: mapMongoIds(CollectionID) },
            Type: 'Created',
            User: mongoObjectId(User)
        };
        const $project = {
            _id: 1,
            Title: 1,
            IsHave: {$in: [video_id, '$ListIds']},
        };
        const aggregate = [ 
            {$match},
            {$project},
            ...paginationAggregate(skip, limit)
         ]

         const [result] = await VideoPlaylist.aggregate(aggregate);

         const rows = result ? result.rows : [];
         const totalCount = result ? result.totalCount : 0;

         res.status(200).json({ rows, totalCount });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function getPlaylistById(req, res) {

    try {
        const { _id: User } = req.user;
        const { id: Playlist } = req.params;

        const { CollectionID } = await VPCollection.findOne({ User });

        const $match = { _id: mongoObjectId(Playlist) }
        const $lookup = getDataByArrayKeys(['aggregate', 'lookup', 'creator']);
        const $project = getDataByArrayKeys(['aggregate', 'video-playlist', 'project']);
    
        $project.IsHave = { $in: ['$_id', mapMongoIds(CollectionID)] };
        $project.OwnLike = { $in: [User, '$Likes']};
    
        const [playlist] = await VideoPlaylist.aggregate([{$match}]).lookup($lookup).project($project).limit(1);

        res.status(200).json(playlist);


    } catch(e) {
        errorHandler(e)
    }
}


async function createPlaylist(req, res) {
    try {

        const { _id: User } = req.user;
        const { Name } = await UserSchema.findById(User);
        const candidate = req.file ? JSON.parse(req.body.body) : req.body;

        if(req.file) {
            candidate.Logo = req.file.path;
        }

        const create = await new VideoPlaylist(candidate).save();

        await VPCollection.findOneAndUpdate({ User }, {$push: { CollectionID: { $each: [ create._id ], $position: 0 } } });
        await TotalStats.findOneAndUpdate({ User }, { $inc: { VideoPlaylists: 1 } });

        const playlist = new IVideoPlaylist(create, User, [String(create._id)]);

        playlist.CreatorName = Name;
   
        res.status(200).json(playlist);

        
    } catch(e) {
        errorHandler(e, res);
    }
}

async function editPlaylist(req, res) {
    try {

        const { id: Playlist } = req.params;
        const candidate = req.body;

        await VideoPlaylist.findByIdAndUpdate(Playlist, { $set: candidate });

        candidate.Updated = new Date();

        res.status(200).json({message: 'Плейлист обновлен'});
        
        
    } catch(e) {
        errorHandler(e, res);
    }
}

async function addPlaylist(req, res) {
    try {
        const {id: PlaylistId} = req.params;
        const { _id: User } = req.user;
        const { ListIds } = await VideoPlaylist.findByIdAndUpdate(PlaylistId, { $inc: { AmountAdditions: 1 } }, { new: true });

        Video.updateMany({ _id: { $in: ListIds } }, { $inc: { AmountAdditions: 1 } }).then(_ => {});
        
        await VPCollection.findOneAndUpdate({ User }, { $push: { CollectionID: {$each: [PlaylistId] , $position: 0 } } });
        await TotalStats.findOneAndUpdate({ User }, { $inc: { VideoPlaylists: 1 } });

        res.status(200).json({ message: 'Плейлист добавлен' });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function removePlaylist(req, res) {
    try {

        const { _id: User } = req.user;
        const { id: Playlist } = req.params;

        const { ListIds } = await VideoPlaylist.findByIdAndUpdate(Playlist, { $inc: { AmountAdditions: -1 } }, { new: true });
        await TotalStats.findOneAndUpdate({ User }, { $inc: { VideoPlaylists: -1 } });
        await VPCollection.findOneAndUpdate({ User }, {$pull: { CollectionID: Playlist }});

        Video.updateMany({ _id: { $in: ListIds } }, { $inc: { AmountAdditions: -1 } }).then(_ => {});

        res.status(200).json({ message: 'Плейлист удален' });


        
    } catch(e) {
        errorHandler(e, res);
    }
}

async function updatePlaylistLogo(req, res) {

    try {

        const playlist = await VideoPlaylist.findById(req.params.id);

        if(playlist.Logo) del(playlist.Logo).then(_ => {});

        playlist.Logo = req.file.path;

        await playlist.save();

        res.status(200).json({ Path: req.file.path });

    } catch(e) {
        errorHandler(e);
    }
}

module.exports = {
    fetch,
    getVideoById,
    create,
    edit,
    add,
    remove,
    fetchPlaylists,
    getPlaylistsByCreator,
    getPlaylistById,
    createPlaylist,
    editPlaylist,
    addPlaylist,
    removePlaylist,
    updatePlaylistLogo
}