const errorHandler = require('../handlers/errorHandler');
const { getDataByArrayKeys } = require('../handlers/db');
const { mapMongoIds, mongoObjectId } = require('../handlers/handlers');
const UserSchema = require('../models/User');
const FriendsSchema = require('../models/Friends');
const TotalStats = require('../models/TotalStats');
const IPicture = require('../classes/IPicture');
const Picture = require('../models/Picture');
const PictureAlbum = require('../models/PictureAlbum');

async function fetchById(req, res) {
    try {
        const { _id: User } = req.user;
        const { id } = req.params;
        const { Friends, IncomingRequestIds, OutgoingRequestIds } = await FriendsSchema.findOne({User});

        const $project = getDataByArrayKeys(['aggregate', 'user', 'project']);
        const $match = { _id: mongoObjectId(id) };
        const totalLookup = getDataByArrayKeys(['aggregate', 'lookup', 'totalStats']);
        const infoLookup = getDataByArrayKeys(['aggregate', 'lookup', 'info']);

        $project.IsHave = { $in: ['$_id', mapMongoIds(Friends)] };
        $project.IsIncomingRequest = { $in: ['$_id', mapMongoIds(IncomingRequestIds)] };
        $project.IsOutgoingRequest = { $in: ['$_id', mapMongoIds(OutgoingRequestIds)] };

        const [user] = await UserSchema.aggregate([{$match}])
            .lookup(totalLookup)
            .lookup(infoLookup)
            .project($project)
            .limit(1);

        res.status(200).json(user);

    } catch(e) {
        errorHandler(e ,res);
    }
}

async function createAvatar(req, res) {
    try {
        const { _id: User } = req.user;
        const { path: Path } = req.file;
        const Album = User;
        const Type = 'Avatars';

        const candidate = { User, Path, Album, Type };
        
        const create = await new Picture(candidate).save();

        await UserSchema.findByIdAndUpdate(User, {$set: { Avatar: Path } });
        await TotalStats.findOneAndUpdate({ User }, { $inc: { Pictures: 1 } });
        await PictureAlbum.findByIdAndUpdate(User, { $inc: { TotalCount: 1 }, $push: { ListPaths: Path }, $set: { Logo: Path } });

        const picture = new IPicture(create, User, []);

        res.status(200).json(picture);

    } catch(e) {
        errorHandler(e, res);
    }
}

async function updateAvatar(req, res) {
    try {

        const { _id: User } = req.user;
        const { _id, Path, User: PictureUser, Type: PictureType } = req.body;

        const existsPicture = await Picture.findOne({ $or: [ {_id}, {Path} ], Album: User, Type: 'Avatars' });

        let picture;

        if(existsPicture) {
            picture = existsPicture;

            existsPicture.Created = new Date();
            existsPicture.Updated = new Date();

            await PictureAlbum.findByIdAndUpdate(User, { $set: { Logo: existsPicture.Path } });

            await existsPicture.save();

        } else {
            const candidate = { User, Path, Album: User, Type: 'Avatars'};

            picture = await new Picture(candidate).save();

            const isUpdateStats = PictureUser === User || PictureType === 'Saved';
            
            if(!isUpdateStats) {
                await TotalStats.findOneAndUpdate({ User }, { $inc: { Pictures: 1 } });
            }

            await PictureAlbum.findByIdAndUpdate(User, { $inc: { TotalCount: 1 }, $push: { ListPaths: Path }, $set: { Logo: Path }});
        }

        await UserSchema.findByIdAndUpdate(User, { $set: { Avatar: picture.Path } });

        res.status(200).json({ message: 'Аватар обновлен' });

    } catch(e) {
        errorHandler(e, res);
    }
}

async function edit(req, res) {
    try {

        const { _id: User } = req.user;

        await UserSchema.findByIdAndUpdate(User, { $set: req.body });

        res.status(200).json({ message: 'Пользователь отредактирован' });

    } catch(e) {
        errorHandler(e, res);
    }
}

module.exports = {
    fetchById,
    createAvatar,
    updateAvatar,
    edit
}