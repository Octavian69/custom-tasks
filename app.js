// App Utils
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const mongoose = require('mongoose');
const morgan = require('morgan');
const FFMPEG_PATH = require('@ffmpeg-installer/ffmpeg').path;
const FFPROBE_PATH = require('@ffprobe-installer/ffprobe').path;
const ffmpeg = require('fluent-ffmpeg');

ffmpeg.setFfmpegPath(FFMPEG_PATH);
ffmpeg.setFfprobePath(FFPROBE_PATH);

// App Settings
const keys = require('./keys/keys');


// App
const app = express();

// Auth

const authenticate = require('./middlewares/authenticate');


// App routes

const userRoutes = require('./routes/user');
const authRoutes = require('./routes/auth');
const taskRoutes = require('./routes/tasks');
const accountRoutes = require('./routes/account');
const editRoutes = require('./routes/edit');
const totalStatRoutes = require('./routes/total-stats');
const pictureRoutes = require('./routes/pictures');
const audioRoutes = require('./routes/audio');
const videoRoutes = require('./routes/video');
const commentRoutes = require('./routes/comments');
const likeRoutes = require('./routes/likes');
const friendsRoutes = require('./routes/friends');
const messagesRoutes = require('./routes/messages');
const unseenRoutes = require('./routes/unseen');

mongoose.Promise = global.Promise;

mongoose.connect(keys.mongoURI, {
    useNewUrlParser: true,
    useFindAndModify: false,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000
})
.then(_ => console.log(`MongoDB get started.`))
.catch(e => console.log(e));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(cors({origin: '*', allowedHeaders: '*'}));

app.use(authenticate());

app.use('/uploads' , express.static('./uploads'));
app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/tasks', taskRoutes);
app.use('/api/account', accountRoutes);
app.use('/api/edit', editRoutes);
app.use('/api/stats', totalStatRoutes);
app.use('/api/pictures', pictureRoutes);
app.use('/api/audio', audioRoutes);
app.use('/api/video', videoRoutes);
app.use('/api/comments', commentRoutes);
app.use('/api/likes', likeRoutes);
app.use('/api/friends', friendsRoutes);
app.use('/api/messages', messagesRoutes);
app.use('/api/unseen', unseenRoutes);

if(process.env.NODE_ENV === 'production') {
    app.use(express.static('client/dist/client'));

    app.get('*', (req, res) => {
        path.join()
        res.sendFile(
            path.resolve(__dirname, 'client', 'dist', 'client', 'index.html')
        )
    })
}

module.exports = app;