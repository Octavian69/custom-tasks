const SocketWorker = require('../SocketWorker');
const Unseen = require('../../models/Unseen');
const { getDataByArrayKeys } = require('../../handlers/db');
const SOCEKT_EVENTS = require('../utils/socket-events');


class UnseenManager extends SocketWorker {
    constructor(io, socket, info) {
        super(io, socket, info)
    }

    init() {
        const subscribers = getDataByArrayKeys(['sockets', 'subscribers', 'unseen']);

        subscribers.forEach(({ event, method }) => {
            this.socket.on(event, this[method].bind(this));
        })
    }

    async updateUnseen(event) {
        // action: string,
        // payload: T,
        // state?: any
    }
}

module.exports = UnseenManager;