const SocketWorker = require('../SocketWorker');
const Dialogue = require('../../models/Dialogue');
const { getDataByArrayKeys } = require('../../handlers/db');
const { createUnseenEvent } = require('../../handlers/handlers');
const SOCKET_EVENTS = require('../utils/socket-events');


class MessageManager extends SocketWorker {
    constructor(io, socket, info) {
        super(io, socket, info)
    }

    init() {
        const subscribers = getDataByArrayKeys(['sockets', 'subscribers', 'messages']);

        subscribers.forEach(({ event, method }) => {
            this.socket.on(event, this[method].bind(this));
        })
    }

    async sendNewMessage(message) {
        const dialogue = await Dialogue.findById(message.Dialogue);
        const { Participants } = dialogue;

        const onlineUsers = await this.getOnlineUsers(Participants);
        const offlineUsers = await this.getOfflineUsers(Participants);
        
        const offlineUsersIds = offlineUsers.map(u => u.toObject()._id);
        const unseenEvent = createUnseenEvent('add', 'dialogues', 1, dialogue._id);

        onlineUsers.forEach(user => {

            const userSocket = this.getSocket(user._id);

            userSocket.emit(SOCKET_EVENTS.MESSAGE_NEW, message)
        })
        

        this.updateUsersUnseen(offlineUsersIds, unseenEvent);
    }

    async createNewDiscussion(payload) {
        const participantsIds = payload.dialogue.Participants.map(d => d._id);
        
        const onlineUsers = await this.getOnlineUsers(participantsIds);
        const offlineUsers = await this.getOfflineUsers(participantsIds);
        const offlineUsersIds = offlineUsers.map(u => u.toObject()._id);
        const unseenEvent = createUnseenEvent('add', 'dialogues', 1, payload.dialogue._id);

        payload.dialogue.Unread = true;
        payload.dialogue.UnreadMessages = 1;
        payload.dialogue.IsOwn = false;

        onlineUsers.forEach(user => {
            const userSocket = this.getSocket(user._id);

            userSocket.emit(SOCKET_EVENTS.MESSAGE_CREATE_DISCUSSION, payload);
        });

        this.updateUsersUnseen(offlineUsersIds, unseenEvent);
    }

    async switchRoom(payload) {
        const { leaveRoom, joinRoom } = payload;

        if(leaveRoom) this.socket.leave(leaveRoom);
        if(joinRoom) this.socket.join(joinRoom);
    }

    async typingMessage(payload) {
        const { DialogueId, UserName } =  payload;
        this.socket.broadcast.to(DialogueId).emit(SOCKET_EVENTS.MESSAGE_TYPING, { UserName });
    }

    async editMessage(message) {
        this.socket.broadcast.to(message.Dialogue).emit(SOCKET_EVENTS.MESSAGE_EDIT, message)
    }

    async removeMessages(payload) {
         // console.log(this.io.sockets.adapter.rooms[payload.Dialogue].sockets)
        this.socket.broadcast.to(payload.Dialogue).emit(SOCKET_EVENTS.MESSAGE_REMOVE, payload)
    }
}

module.exports = MessageManager;