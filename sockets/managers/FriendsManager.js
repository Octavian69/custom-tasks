const SocketWorker = require('../SocketWorker');
const UserSchema = require('../../models/User');
const Friends = require('../../models/Friends');
const Unseen = require('../../models/Unseen');
const { getDataByArrayKeys } = require('../../handlers/db');
const { createUnseenEvent } = require('../../handlers/handlers');
const SOCKET_EVENTS = require('../utils/socket-events');


class FriendsManager extends SocketWorker {
    constructor(io, socket, info) {
        super(io, socket, info)
    }

    init() {
        const subscribers = getDataByArrayKeys(['sockets', 'subscribers', 'friends']);

        subscribers.forEach(({ event, method }) => {
            this.socket.on(event, this[method].bind(this));
        })
    }

    async changeFriendsState(payload) {
        const { referenceId, action, state, User } = payload;
        const onlineUsers = await this.getOnlineUsers([referenceId]);
        const offlineUsers = await this.getOfflineUsers([referenceId]);
        const offlineIds = offlineUsers.map(u => u.toObject()._id);
        const isUpdateUnseen = action === 'requestToAddFriend';

        if(onlineUsers.length) {
            const friendSocket = this.getSocket(referenceId);

            friendSocket.emit(SOCKET_EVENTS.FRIENDS_CHANGE_STATE, payload);
        }

        if(isUpdateUnseen) {
            const unseenEvent = createUnseenEvent(state.status, 'friendsRequests', 1, User);

            this.updateUsersUnseen(offlineIds, unseenEvent)
        }
        
    }


}

module.exports = FriendsManager;