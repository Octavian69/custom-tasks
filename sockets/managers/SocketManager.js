const UserSchema = require('../../models/User')
const SocketWorker = require('../SocketWorker');
const MessageManager = require('./MessageManager');
const FriendsManager = require('./FriendsManager');

class SocketManager extends SocketWorker {
    constructor(io, socket) {
        super(io, socket, socket.decoded_token);
    }

    async init() {
        await UserSchema.findByIdAndUpdate(this.info._id, {$set: { Online: true }});

        new MessageManager(this.io, this.socket, this.info)
        new FriendsManager(this.io, this.socket, this.info)
    }
}

module.exports = SocketManager;