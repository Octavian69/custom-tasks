const UserSchema = require('../models/User');

class Users {
    constructor() {
        this.users = {};
        this.disconnectedTimeouts = {};
    };

    get(id) {
        return this.users[id];
    }

    add(id, manager) {
        this.users[id] = manager;
    }

    remove(id) {
        const user = this.users[id];

        delete this.users[id];

        return user;
    }

    getDisconnectTimeout(userId) {
        return this.disconnectedTimeouts[userId];
    }

    addDisconnectTimeout(userId) {
        this.removeDisconnectTimeout(userId);

        this.disconnectedTimeouts[userId] = setTimeout(_ => {
            UserSchema.findByIdAndUpdate({ _id: userId }, { $set: { Online: false } }).then(() => {
                console.log(`${userId} disconnect`)
            });
        }, 5000);
    }

    removeDisconnectTimeout(userId) {
        const timeout = this.getDisconnectTimeout(userId);

        if(timeout) {
            clearTimeout(timeout);
            delete this.disconnectedTimeouts[userId];
        }
    }
}

const users = new Users;

module.exports = _ => Object.freeze(users);