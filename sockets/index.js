const ioJWT = require('socketio-jwt');
const jwt = require('jsonwebtoken');
const keys = require('../keys/keys');
const SocketManager = require('./managers/SocketManager');
const Users = require('./Users')();
const { removeListeners } = require('../handlers/handlers');


module.exports = function(io) {
    // io.use(ioJWT.authorize({
    //     secret: keys.jwtKey,
    //     handshake: true
    // }))
    
    io.on('connection', (socket) => {
        // const { _id: User } = socket.decoded_token;

        const decoded = jwt.decode(socket.handshake.query.token);

        socket.decoded_token = decoded;
        socket.User = decoded._id;

        Users.add(socket.id, new SocketManager(io, socket));
        Users.removeDisconnectTimeout(decoded._id)
       
        socket.on('disconnect', async () => {
            removeListeners(socket);
            Users.addDisconnectTimeout(decoded._id)
            Users.remove(socket.id);
        })
    }) 
}