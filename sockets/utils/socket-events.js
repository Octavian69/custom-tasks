const SOKET_EVENTS = {
    //MESAGES
    MESSAGE_NEW: '[MESSAGES]newMessage',
    MESSAGE_CREATE_DISCUSSION: '[MESSAGES]createDiscussion',
    MESSAGE_SWITCH_ROOM: '[MESSAGES]switchRoom',
    MESSAGE_EDIT: '[MESSAGES]edit',
    MESSAGE_TYPING: '[MESSAGES]typing',
    MESSAGE_REMOVE: '[MESSAGES]remove',
    //FRIENDS
    FRIENDS_CHANGE_STATE: '[FRIENDS]changeFriendsState',
    //UNSEEN
    UNSEEN_UPDATE: '[UNSEEN]updateUnseen'
}

module.exports = Object.freeze(SOKET_EVENTS)