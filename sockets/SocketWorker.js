const UserSchema = require('../models/User');
const Unseen = require('../models/Unseen');

class SocketWorker {
    constructor(io, socket, info) {
        this.io = io;
        this.socket = socket;
        this.info = info;

        this.init();
    }

    init(){}

    getSocket(id) {
        const sockets = this.io.sockets.sockets;
        const socketId = Object.keys(sockets).find(key => sockets[key].User === String(id));

        return sockets[socketId]
    }

    async getOnlineUsers(userIds) {
        const { _id: User } = this.info;

        const onlineUsers = await UserSchema.find({ $and: [{_id: { $in: userIds }}, { _id: {$ne: User} }] , Online: true }).limit(userIds.length);

        return onlineUsers;
    }

    async getOfflineUsers(userIds) {
        const offlineUsers = await UserSchema.find({ _id: { $in: userIds }, Online: false }).limit(userIds.length);

        return offlineUsers;
    }

    async updateUsersUnseen(users, unseenEvent) {
        const unseens = await Unseen.find({ User: { $in: users } }).limit(users.length);

        unseens.forEach(unseen => {

            const { keys } = unseen;
            const { action, key, value, referenceId } = unseenEvent;

            if(action === 'add') {
                keys[key][referenceId] ? keys[key][referenceId] += value : keys[key][referenceId] = value;
            } else {
                keys[key][referenceId] -= value;
                if(keys[key][referenceId] === 0) delete keys[key][referenceId];
            }
                
            unseen.updateOne({ keys }).exec();
        });
    }
}

module.exports = SocketWorker;