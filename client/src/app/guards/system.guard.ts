import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class SystemGuard implements CanActivate  {

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private loginService: LoginService
  ) {}
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const isLogged: boolean = this.loginService.getLogged();

    if(!isLogged) {
      this.toastr.warning('Используйте учетную запись для входа в систему', 'Внимание.');
      this.router.navigate(['/login']);
      return false;
    }

    return true;
    
  }
}
