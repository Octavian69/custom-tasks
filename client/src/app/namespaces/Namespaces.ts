import { Length } from '../types/validation.types';

export namespace SHARED {

  export const DATE_FORMATS = {
      parse: {
        dateInput: 'LL',
      },
      display: {
        dateInput: 'LL',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
      },
  };

  export const NANOID_SALT: number = 10;
}

export namespace LIMIT {
    export const USERS = 15;
    export const USERS_INCOMPLETE = 30;
    export const USERS_PREVIEW = 6;
    export const VIDEO = 15;
    export const VIDEO_PLAYLISTS = 15;
    export const COMMENTS: number = 30;
    export const AUDIO: number = 15;
    export const AUDIO_PLAYLISTS: number = 15;
    export const AUDIO_SELECTED: number = 10;
    export const PICTURE: number = 15;
    export const PICTURE_ALBUMS: number = 20;
    export const PICTURE_REPLACE_ALBUMS: number = 15;
    export const PICTURE_SELECTED: number = 15;
    export const PICTURE_CREATED: number = 5;
    export const MESSAGE: number = 700;
    export const DIALOGUES: number = 30;
    export const MESSAGES: number = 15;
}

export namespace FILE {
  export const TITLE_LENGTH = {
    AUDIO: 40,
    PICTURE: 35,
    VIDEO: 50
  };

  export const SIZE = {
    AUDIO: 10,
    PICTURE: 5,
    VIDEO: 20
  }

  export const FORMATS = {
    AUDIO: ['audio/mpeg', 'audio/mp3'],
    PICTURE: ['image/jpeg', 'image/png'],
    VIDEO: ['video/mp4', 'video/webm', 'video/ogg']
  }
}

export namespace VALIDATOR_LENGTH {

  export const TASKS: Length = {
    MIN: {},
    MAX: {
      Title: 70,
      Description: 350
    }
  }

  export const LOGIN: Length = {
    MIN: {
      Login: 7,
      Password: 7
    },
    MAX: {
      Login: 40,
      Password: 30
    }
  }

  export const REGISTRATION: Length = {
    MIN: {
      Login: 7,
      Password: 7,
      Name: 4
    },
    MAX: {
      Login: 40,
      Password: 30,
      Name: 40
    }
  }

  export const AUDIO: Length = {
    MIN: {},
    MAX: {
      Title: 60,
      Artist: 80
    }
  }

  export const PLAYLIST: Length = {
      MIN: {},
      MAX: {
        Title: 70,
        Description: 200
      }
  }

  export const VIDEO: Length = {
    MIN: {},
    MAX: {
      Title: 120,
      Description: 400
    }
  }

  export const VIDEO_PLAYLIST: Length = {
    MIN: {},
    MAX: {
      Title: 150,
      Description: 500
    }
  }

  export const PICTURE: Length = {
    MIN: {},
    MAX: {
      Description: 350
    }
  }

  export const PICTURE_ALBUM: Length = {
    MIN: {},
    MAX: {
      Title: 130,
      Description: 380
    }
  }

  export const MESSAGE: Length = {
    MIN: {
      Text: 1
    },
    MAX: {
      Text: 400
    }
  }

  export const DIALOGUE: Length = {
    MIN: {},
    MAX: {
      Title: 60
    }
  }
}