export namespace SOKET_EVENTS {
    //MESAGES
    export const MESSAGE_NEW = '[MESSAGES]newMessage';
    export const MESSAGE_CREATE_DISCUSSION = '[MESSAGES]createDiscussion';
    export const MESSAGE_EDIT = '[MESSAGES]edit';
    export const MESSAGE_REMOVE = '[MESSAGES]remove';
    export const MESSAGE_SWITCH_ROOM = '[MESSAGES]switchRoom';
    export const MESSAGE_TYPING = '[MESSAGES]typing';

    //FRIENDS
    export const FRIENDS_CHANGE_STATE = '[FRIENDS]changeFriendsState';

    //UNSEEN
    export const UNSEEN_UPDATE = '[UNSEEN]updateUnseen'
}