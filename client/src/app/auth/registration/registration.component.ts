import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/User';
import { AuthFlags } from 'src/app/models/flags/AuthFlags';
import { Length, FormObjectErrors, ValidationError } from 'src/app/types/validation.types';
import { TUserLogin } from 'src/app/types/auth.types';
import { ValidationMananger } from 'src/app/shared/managers/Validation.manager';

@Component({
  selector: 'cst-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;
  isShowPassword: boolean = false;
  isShowConfirm: boolean = false;
  flags: AuthFlags = this.viewService.getFlagObject();
  lengths: Length = this.viewService.getValidatorLength('REGISTRATION');
  errors: FormObjectErrors = this.viewService.completeDbErrors(['auth', 'formErrors', 'Registration'], 'Registration');

  constructor(
    private viewService: LoginService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    const {
      MIN: { Login: LoginMin, Password: PasswordMin, Name: NameMin }, 
      MAX: { Login: LoginMax, Password: PasswordMax, Name: NameMax } } = this.lengths;

    this.form = new FormGroup({
      firstStep: new FormGroup({
        Login: new FormControl(null,  [...this.getDefaultValidators(LoginMin, LoginMax), Validators.email], [ValidationMananger.asyncValidator(...this.existsLogin())]),
        Name: new FormControl(null, [...this.getDefaultValidators(NameMin, NameMax), Validators.pattern('[a-zA-Zа-яА-ЯёЁ ]+')]),
      }),
      secondStep: new FormGroup({
        Password: new FormControl(null, this.getDefaultValidators(PasswordMin, PasswordMax)),
        ConfirmPassword: new FormControl(null, [...this.getDefaultValidators(PasswordMin, PasswordMax), this.confirmValidator])
      })
    })
  }
  
  getDefaultValidators(min: number, max: number): ValidatorFn[] {
    const validators: ValidatorFn[] = [
      Validators.required, 
      Validators.minLength(min), 
      Validators.maxLength(max)
    ];

    return validators;
  }

  getDisabledFirstStep(): boolean {
     return this.form.get("firstStep").invalid || this.form.get("firstStep.Login").pending;
  }

  confirmValidator = (control: FormControl): ValidationError => {
    const password = this.form ? this.form.get('secondStep.Password').value : null;

    return password && password !== control.value ? { confirm: true } : null;
  }
 
  existsLogin = () => {
    const obsCb: (v: string) => Observable<TUserLogin> = (value: string) => this.authService.getByLogin(value);
    const validationCb: (u: TUserLogin) => ValidationError = (user: TUserLogin) => {
      return user ? { exists: true } : null;
    };
    const debounceTime: number = 400;

    return [obsCb, validationCb, debounceTime] as const;
  }

  onSubmit(): void {
    const { firstStep: { Login, Name }, secondStep: { Password } } = this.form.value;
    const user: User = new User(Login, Name, Password);

    this.viewService.registration(user);
  }

}
