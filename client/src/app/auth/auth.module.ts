import { NgModule } from '@angular/core';
import { AuthPageComponent } from './auth-page/auth-page.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { AuthRoutingModule } from '../routes/auth.routing.module';
import { AuthService } from '../services/auth.service';
import { AuthGuard } from '../guards/auth.guard';
import { SharedModule } from '../shared/modules/shared.module';
import { LoginService } from '../services/login.service';

@NgModule({
  declarations: [
    AuthPageComponent,
    LoginComponent, 
    RegistrationComponent
  ],
  imports: [
    SharedModule,
    AuthRoutingModule
  ],
  providers: [AuthService, AuthGuard, LoginService]
})
export class AuthModule { }
