import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { AuthFlags } from 'src/app/models/flags/AuthFlags';
import { Length, FormObjectErrors } from 'src/app/types/validation.types';

@Component({
  selector: 'cst-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  hidden: boolean = true;
  flags: AuthFlags = this.viewService.getFlagObject();
  lengths: Length = this.viewService.getValidatorLength('LOGIN');
  errors: FormObjectErrors = this.viewService.completeDbErrors(['auth', 'formErrors', 'SignIn'], 'Login');

  constructor(
    private fb: FormBuilder,
    private viewService: LoginService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    const {
       MIN: { Login: LoginMin, Password: PasswordMin }, 
       MAX: { Login: LoginMax, Password: PasswordMax } } = this.lengths;

    this.form = this.fb.group({
      Login: [null, [Validators.required, Validators.minLength(LoginMin), Validators.maxLength(LoginMax)], []],
      Password: [null, [Validators.required, Validators.minLength(PasswordMin), Validators.maxLength(PasswordMax)], []]
    })
  }


  onSubmit(): void {
    const { value } = this.form;
    
    this.viewService.login(value);
  }
}