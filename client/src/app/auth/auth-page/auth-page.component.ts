import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'cst-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss'],
})
export class AuthPageComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {}

  getTitle(): string {
    return this.router.url.includes('login') ? 'Авторизация' : 'Регистрация';
  }

}
