import { MatInput } from '@angular/material/input';
import { MatSelect } from '@angular/material/select';
import { MatDatepicker } from '@angular/material/datepicker';
import { ElementRef } from '@angular/core';

export type FormControlType = 'input' | 'select' | 'textarea' | 'datepicker';
export type MaterialControlRef = MatInput | MatSelect | MatDatepicker<any> | ElementRef;