export type SocketListener = {
    method: string,
    event: string
}
