export type CheckedFile = {
    isValid: boolean,
    errors: string[]
}

export type FormObjectErrors = {
    [key: string]: ControlError[]
}

export type ControlError = {
    errorName: string;
    message: string
}


export type FormErrorSettings = {
    errorName: string, 
    type: 'default' | 'custom', 
    replace:  boolean, 
    replaceValue: any
}


export type FormErrors = {
    [key: string]: FormErrorSettings[]
}

export type ValidationError = {
    [key: string]: boolean
}

export type Length = {
    
    MIN: {
        [key: string]: number;
    }

    MAX: {
        [key: string]: number;
    }
}
