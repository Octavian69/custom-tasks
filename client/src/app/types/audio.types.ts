import { IAudio } from '../interfaces/IAudio'
import { IAudioPlaylist } from '../interfaces/IAudioPlaylist'

export type AddAudioState = {
    DefaultList: {
        value: boolean,
        'true': string,
        'false': string
    },
    CreatedList: {
        value: boolean,
        'true': string,
        'false': string
    },
}

export type AddAudioCandidate = {
    audio: IAudio,
    listType: 'Default' | 'Created'
}

export type CreateAudioCandidate = {
    audio: { Genre: number },
    file: File
}



export type CreateAudioPlaylist = {
    audios: IAudio[],
    playlist: IAudioPlaylist
}

export type AudioNavIcons = {
    main: string[],
    search: string[]
}
