import { Simple } from './shared.types';
import { IVkResponse } from '../interfaces/IVkResponse';

export type TEditPageName = 'Main' | 'Contacts' | 'Education' | 'HigherEducation' | 'Career' | 'Military' | 'LifePosition';

export type EditCache = {
    [prop: string]: IVkResponse
}

export type ControlSettings = {
    requestSettings: Simple<any>,
    dependentControls: string[]
}

export type EditVkReqSettings = {
    method: string;
    params: {need_all?: number, count?: number}
    parents: string[]
    child: string
}

export type EditLink = {
    path: TEditPageName;
    title: string;
    id?: number;
}