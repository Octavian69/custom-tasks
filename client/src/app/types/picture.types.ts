import { IPicture } from '../interfaces/IPicture';

export type PictureAlbumType = 'Created' | 'Saved' | 'Avatars' | 'Main';
export type PictureType = 'Created' | 'Saved' | 'Avatars' | 'Post';
export type Side = 'previous' | 'next';

export type AvatarCandidate = {
    type: 'Add' | 'Update',
    data: File | IPicture
}

export type SlideEvent = {
    side: 'prev' | 'next'
}


export type PictureReplaceAlbum = {
    Title: string,
    Type: PictureAlbumType,
    TotalCount: number;
    Logo: string
    _id: string,
}