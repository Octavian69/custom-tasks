export type CssSize = 'height' | 'width';
export type TPosition = 'top' | 'bottom' | 'left' | 'right';
export type TPageNames = 'friendsPage' | 'audioPage' | 'videoPage' | 'picturePage' | 'editPage' | 'openChatPage';

export type BooleanValues<T> = {
    true: T,
    false: T
}

export type SipmleAction<T> = {
    action: string,
    payload: T,
    state?: any
}

export type Simple<T> = {
    key: string,
    value: T
}

export type RegexPatterns = {
    [key: string]: RegExp
}

export type LineSettings = {
    [key: string]: {
        sizeProp: 'height' | 'width',
        offsetProp: 'top' | 'left'
    }
}

export type FilterSettings = {
    filters: {[key: string]: any}
    sorted: {[key: string]: any},
}

export type BorderStyles = {
    height: string,
    width: string,
    borderWidth: string,
    borderStyle: string,
    borderColor: string
}


export type ListOption = {
    id: string | number | boolean,
    title: string
}

export type AnyOption = {
    [key: string]: any
}

export type ListLink = {
    path: string,
    title: string
}

export type Coordinate = {
    latitude: number,
    longitude: number
}

export type BufferType = {
    type: "Buffer",
    data: ArrayBuffer
}
