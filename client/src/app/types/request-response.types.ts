import { IAudio } from '../interfaces/IAudio';
import { AnyOption } from './shared.types';
import { Params } from '@angular/router';

export type ResponseAction = 'replace' | 'expand';

export type RequestHandler = {
    next: (response: any) => void,
    error: (e: Error) => void,
    complete: () => void
}

export type RequestSettings = {
    filters: AnyOption,
    sorted: AnyOption
}

export type LoginResponse = {
    user: string 
}

export type MessageResponse = {
    message: string
}

export type PaginationResponse<T> = {
    rows: T[],
    totalCount: number
}

export type MediaSearchResponse = {
    mainList: PaginationResponse<IAudio>,
    findList: PaginationResponse<IAudio>
}

export type WeatherResponse = {
    text: string, 
    icon: string,
    temp_c: number
}

export type PageLink = {
    page: string;
    title: string;
    id: number,
    total?: number,
    query?: Params
}