import { MediaRequest } from '../models/MediaRequest'
import { PaginationResponse } from './request-response.types'
import { ListOption } from './shared.types'

export type AddMedia<T> = {
    media: T,
    IsHave: boolean
}

export type UpdateMediaList<T> = {
    action: 'remove' | 'add' | 'edit' | 'replace',
    media: T
}


export type UpdateMedia<T> = {
    mediaRequest: MediaRequest,
    response: PaginationResponse<T>
}

export type FilterOptions = {
    [key: string]: ListOption[]
}

export type FilterControls = {
    filter: string[],
    sorted: string[]
}

export type MediaNavigate<T> = {
    navigateTo: string;
    payload: T
}