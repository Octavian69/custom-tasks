import { IVideo } from '../interfaces/IVideo'
import { Video } from '../models/Video'

export type MutateVideoPlaylist = {
    logo: [File],
    video: File[],
    added: IVideo[],
}

export type CreateVideoCandidate = {
    video: Video,
    file: File
}

export type RewindType = 'back' | 'forward';