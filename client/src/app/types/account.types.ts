import { Params } from '@angular/router';
import { Simple } from './shared.types';
import { TEditPageName } from './edit.types';



export type AccountNavLink = {
    path: string,
    title: string,
    icon: string,
    status: 'private' | 'public',
    query?: Params,
    unseen?: Simple<number>
}

export type InfoPart = {
    path: TEditPageName,
    title: string,
    type: 'Array' | 'Single',
    rows: InfoRow[] | InfoRow[][],
    id: number
}

export type InfoRow = {
    title: string,
    value: any,
    type: string
}