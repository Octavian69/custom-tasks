import { TPosition } from './shared.types';

export type LineOffsetSide = Exclude<TPosition, 'bottom' | 'right'>;
export type LineAction = 'click' | 'move' | 'leave';
export type LineOffsetProp = 'offsetLeft' | 'offsetTop';