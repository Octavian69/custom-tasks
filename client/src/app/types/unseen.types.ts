export type UnseenKey = 'dialogues' | 'friendsRequests';
export type UnseenAction = 'add' | 'remove';

export type UnseenState = {
    referenceId: string;
}

export type UnseenEvent = {
    action: UnseenAction,
    referenceId: string,
    key: UnseenKey,
    value: number
}
            