import { HttpHeaders } from '@angular/common/http'
import { User } from '../models/User'

export type Authenticate = {
    access_token: string,
    refresh_token: string
}

export type Login = {
    Login: string,
    Password: string
}

export type InteceptOptions = {
    url: string,
    headers?: HttpHeaders
}

export type TUserLogin = Exclude<User, 'Password' | '_id'>; 