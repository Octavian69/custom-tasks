import { Dialogue } from '../models/Dialogue';
import { Message } from '../models/Message';
import { IDialogue } from '../interfaces/IDialogue';
import { IMessage } from '../interfaces/IMessage';

export type MessagePage = 'dialogue' | 'discussion';

export type TypingEvent = {
    UserName: string,
    DialogueId: string
}

export type SwitchRoom = {
    leaveRoom: string;
    joinRoom: string;
}

export type DialogueType = 'discussion' | 'dialogue';

export type CreateDiscussion = {
    dialogue: Dialogue;
    message: Message;
}

export type NewDiscussion = {
    dialogue: IDialogue,
    message: IMessage,
    state: { CreatorName: string }
}

export type LastMessage = {
    Text: string,
    User: string,
    Created: Date,
    _id: string
}


