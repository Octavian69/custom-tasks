import { TaskFilter } from '../models/TaskFilter'

export type TaskFilterOptions = {
    filter: TaskFilter,
    isActive: boolean
}

export type Crumb = {
    Title: string
}

export type TaskIndicator = {
    className: string,
    title: string
}
