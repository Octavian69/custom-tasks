import { AnyOption, TPageNames } from './shared.types';
import { Params } from '@angular/router';

export type PreviewFriends = 'all' | 'online';

export type FriendsStateAction = 
'addFriend' | 'removeFriend' | 
'requestToAddFriend' | 'rejectToAddFriend';

export type FriendState = {
    UserName: string,
    UserAvatar: string,
    User: string,
    referenceId: string,
    action: FriendsStateAction
    state?: AnyOption
}

export type FriendsFetch = 'fetch' | 'fetchRequests' | 'fetchMutual' | 'fetchOnline';
export type FriendsRequest = 'incoming' | 'outgoing';

export type StatsOption = {
    path: string,
    icon: string,
    tooltip: string,
    pageName: TPageNames,
    value?: any
    query?: Params
}
