import { SOKET_EVENTS } from '../namespaces/socket-events.namespaces';


export default {
    sockets: {
        events: [
            SOKET_EVENTS.MESSAGE_NEW,
            SOKET_EVENTS.MESSAGE_CREATE_DISCUSSION,
            SOKET_EVENTS.MESSAGE_TYPING,
            SOKET_EVENTS.FRIENDS_CHANGE_STATE,
            SOKET_EVENTS.UNSEEN_UPDATE
        ]
    }
}