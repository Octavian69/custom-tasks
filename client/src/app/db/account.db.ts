import { VALIDATOR_LENGTH } from '../namespaces/Namespaces';

export default {
    account: { 
        navList: [
            {path: 'account', title: 'Моя страница', icon: 'account_circle', status: 'public', id: 1},
            {path: 'messages', title: 'Мои сообщения', icon: 'textsms', status: 'private', unseen: {key: 'dialogues',value: 0}, query: {page: 'dialogue'}, id: 2,},
            {path: 'friends', title: 'Мои друзья', icon: 'accessibility_new', unseen: {key: 'friendsRequests',value: 0}, status: 'public', id: 3},
            {path: 'tasks', title: 'Мои задачи', icon: 'work', status: 'private', id: 4},
            {path: 'audio', title: 'Мои аудиозаписи', icon: 'audiotrack', status: 'public', id: 5},
            {path: 'video', title: 'Мои видеозаписи', icon: 'video_library', status: 'public', id: 6},
            {path: 'picture', title: 'Мои фотографии', icon: 'photo_library', status: 'public', query: {page: 'main'}, id: 7}
        ],
        fieldsToDraw: [
            'Contacts',
            'Interesting',
            'Education',
            'HigherEducation',
            'Career',
            'Military',
            'LifePosition'
        ],
        formErrors: {
            User: {
                Name: [
                    { 
                        errorName: 'required', 
                        type: 'default', 
                        replace:  false, 
                        replaceValue: null
                    },
                    { 
                        errorName: 'pattern', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: 'содержать: только буквы'
                    },
                    { 
                        errorName: 'maxlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.REGISTRATION.MAX.Name
                    },
                    { 
                        errorName: 'minlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.REGISTRATION.MIN.Name
                    }
                ]
            }
        },
        arrayForms: ['Education', 'HigherEducation', 'Career', 'Military']
    },
}