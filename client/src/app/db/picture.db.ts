export default {
    picture: {
        navList: [
            {id: 1, page: 'main', title: 'Фотографии'},
            {id: 2, page: 'albums', title: 'Альбомы'}
        ],
        formErrors: {
            Title: [
                {
                    errorName: 'required', 
                    type: 'default', 
                    replace:  false, 
                    replaceValue: null
                }
            ]
        }
    }
}