export default {
    messages: {
        navList: [
            {id: 1, page: 'dialogue', title: 'Диалоги'},
            {id: 2, page: 'discussion', title: 'Дискуссии'}
        ],
        formErrors: {
            dialogues: {
                Title: [
                    {
                        errorName: 'required', 
                        type: 'default', 
                        replace:  false, 
                        replaceValue: null
                    }
                ]
            }
        }
    }
}