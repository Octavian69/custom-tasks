import { InfoEducation } from '../models/info/InfoEducation';
import { InfoHigherEducation } from '../models/info/InfoHigherEducation';
import { InfoCareer } from '../models/info/InfoCareer';
import { InfoMilitary } from '../models/info/InfoMilitary';

export default {
    edit: {
            
        asyncForms: ['Contacts', 'Education', 'HigherEducation', 'Career', 'Military'],
        stateTypes: {
            Main: 'Single',
            Contacts: 'Single',
            Interesting: 'Single',
            Education: 'Array',
            HigherEducation: 'Array',
            Career: 'Array',
            Military: 'Array',
            LifePosition: 'Single'
        },
        vkParentParams: {
            Country: 'country_id',
            City: 'city_id',
            University: 'university_id'
        },
        requestSettings: {
            Contacts: {
                Country: {
                    method: 'getCountries',
                    params: {
                        need_all: 1,
                        count: 256
                    },
                    parents: null,
                    child: 'City'
                },
                City: {
                    method: 'getCities',
                    params: {
                        count: 100
                    },
                    parents: ['Country'],
                    child: null
                }
            },

            Career: {
                Country: {
                    method: 'getCountries',
                    params: {
                        need_all: 1,
                        count: 256
                    },
                    parents: null,
                    child: 'City'
                },
                City: {
                    method: 'getCities',
                    params: {
                        count: 100
                    },
                    parents: ['Country'],
                    child: null
                }
            },

            Military: {
                Country: {
                    method: 'getCountries',
                    params: {
                        need_all: 1,
                        count: 256
                    },
                    parents: null,
                    child: null
                }
            },
            Education: {
                Country: {
                    method: 'getCountries',
                    params: {
                        need_all: 1,
                        count: 256
                    },
                    parents: null,
                    child: 'City'
                },
                City: {
                    method: 'getCities',
                    params: {
                        count: 100,
                    },
                    parents: ['Country'],
                    child: 'School'
                },
                School: {
                    method: 'getSchools',
                    params: {
                        count: 100
                    },
                    parents: ['City'],
                    child: null
                }
            },
            HigherEducation: {
                Country: {
                    method: 'getCountries',
                    params: {
                        need_all: 1,
                        count: 256
                    },
                    parents: null,
                    child: 'City'
                },
                City: {
                    method: 'getCities',
                    params: {
                        count: 100
                    },
                    parents: ['Country'],
                    child: 'University'
                },
                University: {
                    method: 'getUniversities',
                    params: {
                        count: 100
                    },
                    parents: ['Country', 'City'],
                    child: 'Faculty'
                },
                Faculty: {
                    method: 'getFaculties',
                    params: {
                        count: 100,
                        need_all: 1
                    },
                    parents: ['University'],
                    child: null
                },
            }
        },
        dependendentControls: {
            Contacts: {
                Country: ['City'],
                City: []
            },
            Military: {
                Country: ['City']
            },
            Career: {
                Country: ['City'],
                City: []
            },
            Education: {
                Country: ['City', 'School'],
                City: ['School'],
                School: []
            },
            HigherEducation: {
                Country: ['City', 'University', 'Faculty'],
                City: ['University', 'Faculty'],
                University: ['Faculty'],
                Faculty: []
            }
        },
        defaultForms: {
            Education: _ => new InfoEducation(),
            HigherEducation: _ => new InfoHigherEducation(),
            Career: _ => new InfoCareer(),
            Military: _ => new InfoMilitary()
        },
        addFormTitles: {
            Education: 'одну школу',
            HigherEducation: 'один университет',
            Career: 'одно рабочее место',
            Military: 'одну войсковую часть'
        },
        removeFormTitles: {
            Education: 'школу',
            HigherEducation: 'университет',
            Career: 'рабочее место',
            Military: 'войсковую часть'
        },
        list: [
            {
                path: 'Main',
                title: 'Основное',
                id: 0
            },
            {
                path: 'Contacts',
                title: 'Контакты',
                id: 1
            },
            {
                path: 'Interesting',
                title: 'Интересы',
                id: 2
            },
            {
                path: 'Education',
                title: 'Образование',
                id: 3
            },
            {
                path: 'HigherEducation',
                title: 'Высшее образование',
                id: 3
            },
            {
                path: 'Career',
                title: 'Карьера',
                id: 4
            },
            {
                path: 'Military',
                title: 'Военная служба',
                id: 5
            },
            {
                path: 'LifePosition',
                title: 'Жизненная позиция',
                id: 6
            }
        ],
        forms: {

            Main: {
                Gender: {
                    type: 'select',
                    title: 'Пол',
                    controlName: 'Gender',
                    multiple: false,
                    async: false,
                    selectData: [
                        { id: 1, title: 'Мужской' },
                        { id: 2, title: 'Женский' },
                    ]
                },
                Birthday: {
                    type: 'datepicker',
                    title: 'Дата рождения',
                    controlName: 'Birthday'
                },
                City: {
                    type: 'input',
                    title: 'Родной город',
                    controlName: 'City'
                },
                Language: {
                    type: 'select',
                    title: 'Языки',
                    controlName: 'Language',
                    multiple: true,
                    async: false,
                    selectData: [
                        {
                            "id": "en",
                            "title": "Английский"
                        },
                        {
                            "id": "ar",
                            "title": "Арабский"
                        },
                        {
                            "id": "es",
                            "title": "Испанский"
                        },
                        {
                            "id": "kk",
                            "title": "Казахский"
                        },
                        {
                            "id": "zh",
                            "title": "Китайский"
                        },
                        {
                            "id": "ko",
                            "title": "Корейский"
                        },
                        {
                            "id": "de",
                            "title": "Немецкий"
                        },
                        {
                            "id": "fa",
                            "title": "Персидский"
                        },
                        {
                            "id": "pl",
                            "title": "Польский"
                        },
                        {
                            "id": "pt",
                            "title": "Португальский"
                        },
                        {
                            "id": "ru",
                            "title": "Русский"
                        },
                        {
                            "id": "tr",
                            "title": "Турецкий"
                        },
                        {
                            "id": "uk",
                            "title": "Украинский"
                        },
                        {
                            "id": "fr",
                            "title": "Французский"
                        },
                        {
                            "id": "hi",
                            "title": "Хинди"
                        },
                        {
                            "id": "ja",
                            "title": "Японский"
                        }
                    ]
                }
            },
            Contacts: {
                Country: {
                    type: 'select',
                    title: 'Страна',
                    controlName: 'Country',
                    multiple: false,
                    isCanRequest: true,
                    async: true,
                    selectData: []
                },
                City: {
                    type: 'select',
                    title: 'Город',
                    controlName: 'City',
                    multiple: false,
                    isCanRequest: true,
                    async: true,
                    search: true,
                    selectData: []
                },
                Address: {
                    type: 'input',
                    title: 'Адрес',
                    controlName: 'Address'
                },
                Phone: {
                    type: 'input',
                    title: 'Мобильный телефон',
                    controlName: 'Phone'
                },
                AdditionalPhone: {
                    type: 'input',
                    title: 'Доп. телефон',
                    controlName: 'AdditionalPhone'
                },
                Skype: {
                    type: 'input',
                    title: 'Скайп',
                    controlName: 'Skype'
                },
                WebSite: {
                    type: 'input',
                    title: 'Личный сайт',
                    controlName: 'WebSite'
                }
            },
            Interesting: {
                Activity: {
                    type: 'textarea',
                    title: 'Деятельность',
                    controlName: 'Activity' 
                },
                Interesting: {
                    type: 'textarea',
                    title: 'Интересы',
                    controlName: 'Interesting' 
                },
                Music: {
                    type: 'textarea',
                    title: 'Любимая музыка',
                    controlName: 'Music' 
                },
                Films: {
                    type: 'textarea',
                    title: 'Любимые фильмы',
                    controlName: 'Films' 
                },
                Games: {
                    type: 'textarea',
                    title: 'Любимые игры',
                    controlName: 'Games' 
                },
                Quotes: {
                    type: 'textarea',
                    title: 'Любимые цитаты',
                    controlName: 'Quotes' 
                },
                About: {
                    type: 'textarea',
                    title: 'О себе',
                    controlName: 'About' 
                }
            },
            Education: {
                Country: {
                    type: 'select',
                    title: 'Страна',
                    controlName: 'Country',
                    isCanRequest: true,
                    multiple: false,
                    async: true,
                    selectData: []
                },
                City: {
                    type: 'select',
                    title: 'Город',
                    controlName: 'City',
                    multiple: false,
                    isCanRequest: true,
                    async: true,
                    search: true,
                    selectData: []
                },
                School: {
                    type: 'select',
                    title: 'Школа',
                    controlName: 'School',
                    multiple: false,
                    isCanRequest: true,
                    async: true,
                    search: true,
                    selectData: []
                },
                StartDate: {
                    type: 'datepicker',
                    title: 'Дата начала обучения',
                    controlName: 'StartDate'
                },
                EndDate: {
                    type: 'datepicker',
                    title: 'Дата окончания обучения',
                    controlName: 'EndDate',
                },
            },
            HigherEducation: {
                Country: {
                    type: 'select',
                    title: 'Страна',
                    controlName: 'Country',
                    multiple: false,
                    isCanRequest: true,
                    async: true,
                    selectData: []
                },
                City: {
                    type: 'select',
                    title: 'Город',
                    controlName: 'City',
                    multiple: false,
                    isCanRequest: true,
                    async: true,
                    search: true,
                    selectData: []
                },
                University: {
                    type: 'select',
                    title: 'Университет',
                    controlName: 'University',
                    multiple: false,
                    async: true,
                    isCanRequest: true,
                    search: true,
                    selectData: []
                },
                Faculty: {
                    type: 'select',
                    title: 'Факультет',
                    controlName: 'Faculty',
                    multiple: false,
                    isCanRequest: true,
                    async: true,
                    selectData: []
                },
                Status: {
                    type: 'select',
                    title: 'Статус',
                    controlName: 'Status',
                    multiple: false,
                    async: false,
                    selectData: [
                        {id: 1, title: 'Абитуриент'},
                        {id: 2, title: 'Студент (специалист)'},
                        {id: 3, title: 'Студент (бакалавр)'},
                        {id: 4, title: 'Студент (магистр)'},
                        {id: 5, title: 'Выпускник (специалист)'},
                        {id: 6, title: 'Выпускник (специалист)'},
                        {id: 7, title: 'Выпускник (специалист)'},
                        {id: 8, title: 'Аспирант'},
                        {id: 9, title: 'Кандидат наук'},
                        {id: 10, title: 'Доктор наук'},
                        {id: 11, title: 'Интерн'},
                        {id: 12, title: 'Клинический ординатор'},
                        {id: 13, title: 'Соискатель'},
                        {id: 14, title: 'Ассистент-стажёр'},
                        {id: 15, title: 'Докторант'},
                        {id: 16, title: 'Адъюнкт'},
                    ]
                },
                FormOfEducation: {
                    type: 'select',
                    title: 'Форма обучения',
                    controlName: 'FormOfEducation',
                    multiple: false,
                    async: false,
                    selectData: [
                        {id: 1, title: 'Очная'},
                        {id: 2, title: 'Очно-заочная'},
                        {id: 3, title: 'Заочная'},
                        {id: 4, title: 'Экстернат'},
                        {id: 5, title: 'Дистанционная'}
                    ]
                },
                StartDate: {
                    type: 'datepicker',
                    title: 'Дата начала обучения',
                    controlName: 'StartDate'
                },
                EndDate: {
                    type: 'datepicker',
                    title: 'Дата окончания обучения',
                    controlName: 'EndDate',
                }
            },
            Career: {
                CompanyName: {
                    type: 'input',
                    title: 'Название компании',
                    controlName: 'CompanyName', 
                },
                Country: {
                    type: 'select',
                    title: 'Страна',
                    controlName: 'Country',
                    multiple: false,
                    isCanRequest: true,
                    async: true,
                    selectData: []
                },
                City: {
                    type: 'select',
                    title: 'Город',
                    controlName: 'City',
                    multiple: false,
                    isCanRequest: true,
                    async: true,
                    search: true,
                    selectData: []
                },
                StartDate: {
                    type: 'datepicker',
                    title: 'Дата начала работы',
                    controlName: 'StartDate'
                },
                EndDate: {
                    type: 'datepicker',
                    title: 'Дата окончания работы',
                    controlName: 'EndDate',
                },
                Position: {
                    type: 'input',
                    title: 'Должность',
                    controlName: 'Position',
                }
            },
            Military: {
                Country: {
                    type: 'select',
                    title: 'Страна',
                    controlName: 'Country',
                    multiple: false,
                    isCanRequest: true,
                    async: true,
                    selectData: []
                },
                ArmyPart: {
                    type: 'text',
                    title: 'Войсковая часть',
                    controlName: 'ArmyPart'
                },
                StartDate: {
                    type: 'datepicker',
                    title: 'Дата начала службы',
                    controlName: 'StartDate'
                },
                EndDate: {
                    type: 'datepicker',
                    title: 'Дата окончания службы',
                    controlName: 'EndDate',
                },
            },
            LifePosition: {
                PoliticalPreference: {
                    type: 'select',
                    title: 'Политические предпочтения',
                    controlName: 'PoliticalPreference',
                    multiple: false,
                    async: false,
                    selectData: [
                        {id: 1, title: 'Индефернтные'},
                        {id: 2, title: 'Коммунистические'},
                        {id: 3, title: 'Социалистические'},
                        {id: 4, title: 'Умеренные'},
                        {id: 5, title: 'Либеральные'},
                        {id: 6, title: 'Консервативные'},
                        {id: 7, title: 'Монархические'},
                        {id: 8, title: 'Ультраконсервативные'},
                        {id: 9, title: 'Либертарианские'}
                    ]
                    
                },
                Outlook: {
                    type: 'input',
                    title: 'Мировоззрение',
                    controlName: 'Outlook'
                },
                MainInLife: {
                    type: 'select',
                    title: 'Главное в жизни',
                    controlName: 'MainInLife',
                    multiple: false,
                    async: false,
                    selectData: [
                        {id: 1, title: 'Семья и дети'},
                        {id: 2, title: 'Карьера и деньги'},
                        {id: 3, title: 'Развлечения и отдых'},
                        {id: 4, title: 'Наука и исследования'},
                        {id: 5, title: 'Совершенствование мира'},
                        {id: 6, title: 'Саморазвитие'},
                        {id: 7, title: 'Красота и искусство'},
                        {id: 8, title: 'Слава и влияние'}
                    ]
                },
                MainInPeople: {
                    type: 'select',
                    title: 'Главное в людях',
                    controlName: 'MainInPeople',
                    multiple: false,
                    async: false,
                    selectData: [
                        {id: 1, title: 'Ум и креативность'},
                        {id: 2, title: 'Доброта и честность'},
                        {id: 3, title: 'Красота и здоровье'},
                        {id: 4, title: 'Власть и богаство'},
                        {id: 5, title: 'Смелость и упорство'},
                        {id: 6, title: 'Юмор и жизнелюбие'}
                    ]
                },
                AttitudeToSmoking: {
                    type: 'select',
                    title: 'Отношение к курению',
                    controlName: 'AttitudeToSmoking',
                    multiple: false,
                    async: false,
                    selectData: [
                        {id: 1, title: 'Резко негативное'},
                        {id: 2, title: 'Негативное'},
                        {id: 3, title: 'Компромиссное'},
                        {id: 4, title: 'Нейтральное'},
                        {id: 5, title: 'Положительное'}
                    ]
                },
                AttitudeToAlcohol: {
                    type: 'select',
                    title: 'Отношение к алкоголю',
                    controlName: 'AttitudeToAlcohol',
                    multiple: false,
                    async: false,
                    selectData: [
                        {id: 1, title: 'Резко негативное'},
                        {id: 2, title: 'Негативное'},
                        {id: 3, title: 'Компромиссное'},
                        {id: 4, title: 'Нейтральное'},
                        {id: 5, title: 'Положительное'}
                    ]
                },
                Inspiration: {
                    type: 'input',
                    title: 'Вдохновляют',
                    controlName: 'Inspiration'
                }
            }
        }
     }
}