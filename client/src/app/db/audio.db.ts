export default {
    audio: {
        navList: [
            {id: 1, page: 'own', title: 'Музыка'},
            {id: 2, page: 'playlists', title: 'Плейлисты'}
        ],
        genres: [
            {id: 1, title: 'Электронная'},
            {id: 2, title: 'Поп'},
            {id: 3, title: 'Рок'},
            {id: 4, title: 'Rap'},
            {id: 5, title: 'Классическая'},
            {id: 6, title: 'Другое'}
        ],
        addAudioState: {
            DefaultList: {
                value: false,
                'true': 'Удалить из моих аудиозаписей',
                'false': 'Добавить в мои аудиозаписи'
            },
           CreatedList: {
                value: false,
                'true': 'Удалить из плейлиста',
                'false': 'Добавить в плейлист'
            }
        },
        navIcons: {
            own: {
                default: {
                    main: ['add', 'edit', 'add-multi'],
                    search: ['add', 'add-multi']
                },
                'playlist-create': {
                    main: ['checkbox'],
                    search: ['checkbox']
                },
                'playlist-edit': {
                    main: ['add'],
                    search: ['add']
                }
            },

            other: {
                default: {
                    main: ['add', 'add-multi'],
                    search: ['add', 'add-multi']
                }
            }

        },
        formErrors: {
            Audio: {
                Artist: [
                    {
                        errorName: 'required', 
                        type: 'default', 
                        replace:  false, 
                        replaceValue: null
                    }
                ],
                Title: [
                    {
                        errorName: 'required', 
                        type: 'default', 
                        replace:  false, 
                        replaceValue: null
                    }
                ]
            },
            Playlist: {
                Title: [
                    { 
                        errorName: 'required', 
                        type: 'default', 
                        replace:  false, 
                        replaceValue: null
                    }
                ]
            }
        }
    },
}