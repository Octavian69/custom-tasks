export default {
    video: {
        navList: [
            {id: 1, page: 'own', title: 'Видео'},
            {id: 2, page: 'playlists', title: 'Плейлисты'}
        ],
        formErrors: {
            Title: [
                {
                    errorName: 'required', 
                    type: 'default', 
                    replace:  false, 
                    replaceValue: null
                }
            ]
        },
        filterControls: {
            filter: ['Duration'],
            sorted: ['Created', 'LikesTotal']
        },
        filterOptions: {
            Duration: [
                {id: 'short', title: 'Короткие'},
                {id: 'long', title: 'Длинные'}
            ]
        },
        sortedOptions: {
            LikesTotal: [
                    {id: -1, title: 'Популярные'},
                    {id: 1, title: 'Не популярные'},
            ],
            Created: [
                {id: -1, title: 'Новые'},
                {id: 1, title: 'Старые'}
            ]
        }
    }
}