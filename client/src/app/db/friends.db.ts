export default {
    friends: {
        navList: [
            {id: 1, page: 'main', title: 'Друзья'},
            {id: 2, page: 'requests', title: 'Заявки в друзья', query: { type: 'incoming' }},
            {id: 2, page: 'mutual', title: 'Общие друзья'}
        ],
        statsList: {
            FriendsTotal: {
                path: '/friends',
                icon: 'accessibility_new',
                tooltip: 'Друзья',
                pageName: 'friendsPage',
                query: {
                    page: 'main'
                }
            },
            AudioTotal: {
                path: '/audio',
                icon: 'audiotrack',
                tooltip: 'Аудозаписи',
                pageName: 'audioPage'
            },
            VideoTotal: {
                path: '/video',
                icon: 'video_library',
                pageName: 'videoPage',
                tooltip: 'Видеозаписи'
            },
            PicturesTotal: {
                path: '/picture',
                icon: 'photo_library',
                tooltip: 'Фотографии',
                pageName: 'picturePage',
                query: { page: 'main' }
            }
        },
        totalFields: [
            'FriendsTotal',
            'AudioTotal',
            'VideoTotal',
            'PicturesTotal'
        ]
    }
}