import { VALIDATOR_LENGTH } from '../namespaces/Namespaces';

export default {
    tasks: {

        statusList: [
            {
                value: false,
                title: 'Не завершена'
            },
            {
                value: true,
                title: 'Завершена'
            },
        ],

        columns: [
            'Выбор', 
            'Название', 
            'Описание', 
            'Статус', 
            'Дата начала', 
            'Дата окончания', 
            'Детали'
        ],
        indicators: [
            {
                className: 'indicators-box_before',
                title: 'Истек срок выполнения'
            },
            {
                className: 'indicators-box_same',
                title: 'Остался 1 день'
            },
            {
                className: 'indicators-box_complete',
                title: 'Выполнена в срок'
            },
            {
                className: 'indicators-box_after',
                title: 'Ожидает выполнения'
            },
        ],

        formErrors: {

            Title: [
                { 
                  errorName: 'required', 
                  type: 'default', 
                  replace:  false, 
                  replaceValue: null
                },
                { 
                  errorName: 'maxlength', 
                  type: 'default', 
                  replace:  true, 
                  replaceValue: VALIDATOR_LENGTH.TASKS.MAX.Title 
                }

            ],
            Description: [
                { 
                    errorName: 'required', 
                    type: 'default', 
                    replace:  false, 
                    replaceValue: null
                },
                { 
                    errorName: 'maxlength', 
                    type: 'default', 
                    replace:  true, 
                    replaceValue: VALIDATOR_LENGTH.TASKS.MAX.Description
                }
            ],
            StartDate: [
                { 
                    errorName: 'required', 
                    type: 'default', 
                    replace:  false, 
                    replaceValue: null
                },
                { 
                    errorName: 'matDatepickerMin', 
                    type: 'default', 
                    replace:  false, 
                    replaceValue: null
                },
                { 
                    errorName: 'invalidDate', 
                    type: 'custom', 
                    replace:  false, 
                    replaceValue: null
                }
            ],
            EndDate: [
                { 
                    errorName: 'required', 
                    type: 'default', 
                    replace:  false, 
                    replaceValue: null
                },
                { 
                    errorName: 'matDatepickerMin', 
                    type: 'custom', 
                    replace:  false, 
                    replaceValue: null
                },
            ]
        },
        ContainerStyles: 'position: absolute; top: 100%; display: flex; align-items: flex-start; flex-direction: column; width: 500px;',
        PrintStyles: 'display: flex; flex-direction: column; width: 100%;',
    },

}