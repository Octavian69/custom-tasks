export default {
    shared: {
        DATE_FORMATS: {
            parse: {
              dateInput: 'LL',
            },
            display: {
              dateInput: 'LL',
              monthYearLabel: 'MMM YYYY',
              dateA11yLabel: 'LL',
              monthYearA11yLabel: 'MMMM YYYY',
            },
        },

        quill: {
            config: [
                ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                ['blockquote', 'code-block'],
            
                [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
                [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
                [{ 'direction': 'rtl' }],                         // text direction
            
                [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
            
                [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                [{ 'font': [] }],
                [{ 'align': [] }],
            
                ['clean'],  
            ]
        },
        defaultFormErrors: {
            'required': 'Это поле обязательно для заполнения',
            'maxlength': 'Максимальное количество символов:{{replace}}',
            'minlength': 'Минимальное количество символов:{{replace}}',
            "pattern": "Поле должно {{replace}}",
            "email": "Поле должено быть типа email: ivan@email.ru",
            "matDatepickerMin": 'Дата начала выполнения не может быть меньше текущей даты.'
        },
        customFormErrors: {
            Tasks: {
                StartDate: {
                    invalidDate: 'Дата начала выполнения не может быть меньше даты окончания выполнения.' 
                },
                EndDate: {
                    matDatepickerMin: 'Дата окончания выполнения не может быть меньше текущей даты и даты начала выполнения.' 
                } 
            },
            TaskFilter: {

            },
            Registration: {
                Login: {
                    exists: 'Пользователь с таким логином уже имеется.'
                },
                ConfirmPassword: {
                    confirm: 'Пароли не совпадают.'
                }
            }
        },

        lineSettings: {
            'left-top': { 
                sizeProp: 'height',
                offsetProp: 'top'
                // катается с левой сторноны по вертикали
             },
             'rigth-top': { 
                sizeProp: 'height',
                offsetProp: 'top'
                // катается с правой сторноны по вертикали
             },
             'top-left': { 
                sizeProp: 'width',
                offsetProp: 'left'
                // катается сверху по горизонтали
             },
             'bottom-left': { 
                sizeProp: 'width',
                offsetProp: 'left'
                // катается снизу по горизонтали
             }
          },

          borderStyles: {
            height: '20px',
            width: '30px',
            borderWidth: ' 5px',
            borderStyle: 'solid',
            borderColor: '#3f51b5'
          },

          addMediaTooltipTexts: {
            videoUnit: {
                true: 'Убрать из списка',
                false: 'Добавить в список'
            }
          }
          
    }
}