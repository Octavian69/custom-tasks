import { SOKET_EVENTS } from '../namespaces/socket-events.namespaces';

export default {
    popups: {
        events: [
            {method: 'message', event: SOKET_EVENTS.MESSAGE_NEW},
            {method: 'createDiscussion', event: SOKET_EVENTS.MESSAGE_CREATE_DISCUSSION},
            {method: 'changeFriendsState', event: SOKET_EVENTS.FRIENDS_CHANGE_STATE},
        ]
    }
}