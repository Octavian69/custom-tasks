import { VALIDATOR_LENGTH } from '../namespaces/Namespaces';

export default {
    auth: {
        formErrors: {

            SignIn: {

                Login: [
                    { 
                        errorName: 'required', 
                        type: 'default', 
                        replace:  false, 
                        replaceValue: null
                    },
                    { 
                        errorName: 'minlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.LOGIN.MIN.Login
                    },
                    { 
                        errorName: 'maxlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.LOGIN.MAX.Login
                    },
                ],

                Password: [
                    { 
                        errorName: 'required', 
                        type: 'default', 
                        replace:  false, 
                        replaceValue: null
                    },
                    { 
                        errorName: 'minlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.LOGIN.MIN.Password
                    },
                    { 
                        errorName: 'maxlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.LOGIN.MAX.Password
                    },
                ]
            },

            Registration: {
                Login: [
                    { 
                        errorName: 'required', 
                        type: 'default', 
                        replace:  false, 
                        replaceValue: null
                    },
                    { 
                        errorName: 'minlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.LOGIN.MIN.Login
                    },
                    { 
                        errorName: 'maxlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.LOGIN.MAX.Login
                    },
                    { 
                        errorName: 'email', 
                        type: 'default', 
                        replace:  false,
                        replaceValue: null
                    },
                    { 
                        errorName: 'exists', 
                        type: 'custom', 
                        replace:  false, 
                        replaceValue: false
                    },
                ],

                Password: [
                    { 
                        errorName: 'required', 
                        type: 'default', 
                        replace:  false, 
                        replaceValue: null
                    },
                    { 
                        errorName: 'minlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.LOGIN.MIN.Password
                    },
                    { 
                        errorName: 'pattern', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: 'содержать: цифры и буквы не используя пробелы'
                    },
                    { 
                        errorName: 'maxlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.LOGIN.MAX.Password
                    },
                ],
                ConfirmPassword: [
                    { 
                        errorName: 'required', 
                        type: 'default', 
                        replace:  false, 
                        replaceValue: null
                    },
                    { 
                        errorName: 'confirm', 
                        type: 'custom', 
                        replace:  false, 
                        replaceValue: null
                    }
                ],
                Name: [
                    { 
                        errorName: 'required', 
                        type: 'default', 
                        replace:  false, 
                        replaceValue: null
                    },
                    { 
                        errorName: 'pattern', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: 'содержать: только буквы'
                    },
                    { 
                        errorName: 'maxlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.REGISTRATION.MAX.Name
                    },
                    { 
                        errorName: 'minlength', 
                        type: 'default', 
                        replace:  true, 
                        replaceValue: VALIDATOR_LENGTH.REGISTRATION.MIN.Name
                    }
                ]
            }
        }
    },
}