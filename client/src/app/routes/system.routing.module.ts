import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { SystemPageComponent } from '../system/system-page/system-page.component';
import { SystemGuard } from '../guards/system.guard';

const routes: Routes = [
    {path: '', component: SystemPageComponent, canActivate: [SystemGuard], children: [
        {path: 'tasks', loadChildren: () => import('../system/task-page/task.module').then(({ TaskModule }) => TaskModule)},
        {path: 'edit', loadChildren: () => import('../system/edit-page/edit.module').then(({ EditModule }) => EditModule)},
        {path: 'messages', loadChildren: () => import('../system/message-page/message.module').then(({ MessageModule }) => MessageModule )},
        {path: 'picture/:id', loadChildren: () => import('../system/picture-page/picture.module').then(({ PictureModule }) => PictureModule)}, 
        {path: 'account/:id', loadChildren: () => import('../system/account-page/account.module').then(({ AccountModule }) => AccountModule)},
        {path: 'audio/:id', loadChildren: () => import('../system/audio-page/audio.module').then(({ AudioModule }) => AudioModule)},
        {path: 'video/:id', loadChildren: () => import('../system/video-page/video.module').then(({ VideoModule }) => VideoModule)},
        {path: 'friends/:id', loadChildren: () => import('../system/friends-page/friends.module').then(({ FriendsModule }) => FriendsModule)},
    ]},
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SystemRoutingModule {}
