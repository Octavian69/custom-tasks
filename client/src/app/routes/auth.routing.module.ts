import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { AuthPageComponent } from '../auth/auth-page/auth-page.component';
import { LoginComponent } from '../auth/login/login.component';
import { RegistrationComponent } from '../auth/registration/registration.component';
import { AuthGuard } from '../guards/auth.guard';

const routes: Routes = [
    {path: '', component: AuthPageComponent, canActivate: [AuthGuard], children: [
        {path: 'login', component: LoginComponent},
        {path: 'registration', component: RegistrationComponent}
    ]}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule {}