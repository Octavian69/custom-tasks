import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, HostBinding } from '@angular/core';
import { Popup } from 'src/app/models/Popup';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { scaleShow } from '../../animations/animation';

@Component({
  selector: 'cst-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
  animations: [scaleShow]
})
export class PopupComponent implements OnInit, OnDestroy {
  
  time: number = 4000;
  timeout$: any;
  interval$: any;
  
  @HostBinding('@scaleShow') scaleShow = true;
  @Input('popup') popup: Popup;
  @Output('action') _action = new EventEmitter<SimpleAction<Popup>>();
  @Output('close') _close = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.clear();
  }

  init(): void {
    if(this.timeout$ && this.interval$) this.clear();

    this.timeout$ = setTimeout(_ => this.close(), this.time);
    this.interval$ = setInterval(_ => {
      this.time -= 1000;
      if(this.time === 0) clearInterval(this.interval$);
    } , 1000);
  }

  clear(): void {
    clearTimeout(this.timeout$);
    clearInterval(this.interval$);

    this.interval$ = null;
    this.timeout$ = null;
  }

  close(): void {
    this._close.emit(this.popup._id);
  }

  action(action: string): void {
    const event: SimpleAction<Popup> = new SimpleAction(action, this.popup);
    this._action.emit(event);
  }

}
