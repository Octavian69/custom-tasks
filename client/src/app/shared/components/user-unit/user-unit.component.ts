import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IUser } from 'src/app/interfaces/IUser';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { fadeEvent } from '../../animations/animation';
import { IIncompleteUser } from 'src/app/interfaces/IIncompleteUser';

@Component({
  selector: 'cst-user-unit',
  templateUrl: './user-unit.component.html',
  styleUrls: ['./user-unit.component.scss'],
  animations: [fadeEvent]
})
export class UserUnitComponent {

  @Input('user') user: IUser | IIncompleteUser;
  @Input('direction') direction: 'row' | 'column' = 'row';
  @Input('height') height: string = '50px';
  @Input('width') width: string = '50px';
  @Input('start') start: number = 0;
  @Input('end') end: number = 40;

  @Output('action') _action = new EventEmitter<IUser | IIncompleteUser>();

  getMainStyles(): ICssStyles {

    return {
      'flex-direction': this.direction,
      'margin-top': '0.5rem'
    }
  }

  getPreviewStyle(): ICssStyles {
    const margin: string = this.direction === 'row' ? 'margin-right' : 'margin-bottom';

    return {
      height: this.height,
      width: this.width,
      [margin]: '1rem'
    }
  }

  action(): void {
    this._action.emit(this.user);
  }

}
