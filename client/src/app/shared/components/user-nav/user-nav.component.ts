import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { IUser } from 'src/app/interfaces/IUser';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { Router } from '@angular/router';
import { UserViewService } from 'src/app/services/user.view.service';
import { filter, takeUntil } from 'rxjs/operators';
import { translateAxis } from '../../animations/animation';
import { getFilledStats, completeStats } from '../../handlers/friends.handlers';
import { StatsOption } from 'src/app/types/friends.types';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'cst-user-nav',
  templateUrl: './user-nav.component.html',
  styleUrls: ['./user-nav.component.scss'],
  animations: [translateAxis('-50%', '-100%', '-50%', '0%')]
})
export class UserNavComponent implements OnInit {
  users: IUser[] = [];
  userId: string;
  stats: StatsOption[] = [];
  unsubscriber$: Subject<boolean> = new Subject()
  
  @Input('userId') set _userId(id: string) {
    if(id) {
      this.unsubscriber$.next(true);
      this.userId = id;
      this.initUser(id);
    }
  };

  @Output('navigateStats') _navigateStats = new EventEmitter<StatsOption>();

  constructor(
    private router: Router,
    private userService: UserService,
    private userViewService: UserViewService,
    private storage: StorageService
  ) { }

  ngOnInit() {
    this.initOwnUser();
  }

  getUsersStyles(): ICssStyles {
    const height: string = this.users.length > 1 ? '100px' : '54px';

    return { height }
  }

  trackByFn(index: number, user: IUser): string {
    return user._id;
  }

  initUser(userId: string): void {
     const ownId: string = this.userService.getOwnId();
     const isOwn: boolean = this.userService.isOwn(userId);

     if(!isOwn) {
         const next = (user: IUser) => {
           this.users = this.users.filter(u => u._id === ownId);
           this.users.push(user);
           this.initStats(user);
         }
  
        this.userService.fetch(userId)
        .pipe(takeUntil(this.unsubscriber$))
        .subscribe({ next });
     } else {
        this.users = this.users.filter(u => u._id === ownId);
        this.stats = [];
     }
  }

  initOwnUser(): void {
    const next = (user: IUser) => {
      this.users.unshift(user);
    }

     this.userViewService.getStream('user$')
    .pipe(filter((user: IUser) => {
      return user !== null;
    }))
    .subscribe({ next });
  }

  initStats(user: IUser): void {
    const filled: string[] = getFilledStats(user);

    if(filled.length) {
      this.stats = completeStats(filled, user);
    } 
  }

  navigate(user: IUser): void {
    this.router.navigate(['/account', user._id]);
  }

  navigateStats(user: IUser, option: StatsOption): void {
    const { path, query: queryParams, pageName } = option;
    this.storage.setItem('sessionStorage', pageName, 0);
    this.router.navigate([path, user._id], { queryParams });
    this._navigateStats.emit(option);
  }

}
