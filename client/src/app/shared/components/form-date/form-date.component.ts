import { Component, OnInit, Provider, forwardRef, Injector, Input, ViewChild, EventEmitter, Output, AfterViewInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { ErrorMatcher } from '../../classes/error.matcher';
import { IControlDate } from 'src/app/interfaces/IControls/IControlDate';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { SHARED } from 'src/app/namespaces/Namespaces';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { ControlError } from 'src/app/types/validation.types';
import { MatDatepicker } from '@angular/material/datepicker';


const VALUE_ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FormDateComponent),
  multi: true
}

const DATE_ADAPTER = {
  provide: DateAdapter,
  useClass: MomentDateAdapter,
  deps: [MAT_DATE_LOCALE]
}

const DATE_FORMATS = {
  provide: MAT_DATE_FORMATS,
  useValue: SHARED.DATE_FORMATS
}

@Component({
  selector: 'cst-form-date',
  templateUrl: './form-date.component.html',
  styleUrls: ['./form-date.component.scss'],
  providers: [VALUE_ACCESSOR, DATE_ADAPTER, DATE_FORMATS]
})
export class FormDateComponent implements OnInit, AfterViewInit, ControlValueAccessor, IControlDate {
  
  @ViewChild('Date', { static: false}) controlRef: MatDatepicker<any>;

  @Input('placeholder') placeholder: string;
  @Input('fontSize') fontSize: string = '16px';
  @Input('isResetIcon') isResetIcon: boolean = true;
  @Input('errorCondition') errorCondition: 'standard' | 'date' = 'standard';
  @Input('errors') errors: ControlError[] = [];
  @Input('min') min: Date = new Date();
  @Input('max') max: Date;

  @Output('initialize') _initialize: EventEmitter<MatDatepicker<any>> = new EventEmitter<MatDatepicker<any>>()

  value: Date;
  formControl: NgControl;
  errorMatcher: ErrorMatcher;
  disabled: boolean = false;

  onChange: (v: Date) => void = (value: Date) => {};
  onTouched: (e: Event) => void = (e: Event) => {};
  
  constructor(
    private injector: Injector,
    private adapter: DateAdapter<any>,
    ) { }

  ngOnInit() {
    this.formControl = this.injector.get(NgControl);
    this.errorMatcher = new ErrorMatcher(this.formControl, this.errorCondition);
    
    this.initDateLocale();
  }

  ngAfterViewInit() {
    this.initialize();
  }

  initialize(): void {
    this._initialize.emit(this.controlRef);
  }

  initDateLocale(): void {
    this.adapter.setLocale('ru');
  }

  setValue(value: Date) {
    this.value = value;
    this.onChange(value);
  }

  reset(e: Event): void {
    e.stopPropagation();
    this.setValue(null);
    this.onTouched(e);
  }

  getErrorStyle(error: ControlError): ICssStyles {
    const { errorName } = error;
    const isError: boolean = this.formControl.hasError(errorName);

    return {
      display: isError ? '' : 'none'
    }
  }

  writeValue(value: Date): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

}
