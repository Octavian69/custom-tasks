import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { PageLink } from 'src/app/types/request-response.types';
import { LineActiveDirective } from '../../directives/line-active.directive';
import { TPageNames } from 'src/app/types/shared.types';

@Component({
  selector: 'cst-media-nav',
  templateUrl: './media-nav.component.html',
  styleUrls: ['./media-nav.component.scss']
})
export class MediaNavComponent {

  @ViewChild('lineActiveRef', { read: LineActiveDirective, static: false }) lineRef$: LineActiveDirective;
  @Input('pageName') pageName: TPageNames;
  @Input('options') options: PageLink[];
  @Input('isInitializeEmit') isInitializeEmit: boolean = true;
  @Output('page') _page = new EventEmitter<PageLink>();

  setPage(optionIdx: number): void {
    const option: PageLink = this.options[optionIdx];
    
    this._page.emit(option);
  }
}
