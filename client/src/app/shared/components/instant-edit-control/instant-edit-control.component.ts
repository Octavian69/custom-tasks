import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { FormControl, ValidatorFn } from '@angular/forms';
import { ControlError } from 'src/app/types/validation.types';

@Component({
  selector: 'cst-instant-edit-control',
  templateUrl: './instant-edit-control.component.html',
  styleUrls: ['./instant-edit-control.component.scss']
})
export class InstantEditControlComponent implements OnInit {

  formControl: FormControl;
  value: any;

  showMode: 'input' | 'textarea' = null; 

  
  @Input('value') set _value(val: any) {
    this.value = val;
  }
  @Input('request') request: boolean = false;
  @Input('emptyText') emptyText: string;
  @Input('modeType') modeType: 'input' | 'textarea' = 'input';
  @Input('editStatus') editStatus: boolean = true;
  @Input('validators') validators: ValidatorFn[] = [];
  @Input('minLength') minLength: number = 0;
  @Input('maxLength') maxLength: number;
  @Input('placeholder') placeholder: string;
  @Input('cssClasses') cssClasses: ICssStyles = null;
  @Input('errors') errors: ControlError[] = [];

  @Output('submit') _submit = new EventEmitter<any>();

  ngOnInit() {
    this.initControl();
  };

  getValue(): void {
    return this.value || this.emptyText;
  }

  initControl(): void {
    this.formControl = new FormControl(this.value, this.validators);
  }

  getCssClasses(): ICssStyles {
    const hoverClass: ICssStyles = {
      'default-hover': this.editStatus
    };

    const isExists: boolean = this.cssClasses !== null;

    if(isExists) {
      return { ...this.cssClasses, ...hoverClass };
    }

    return { 'default': true, ...hoverClass };
  }

  inside(flag: boolean): void {
    const isHide: boolean = this.showMode && !flag;

    if(isHide) this.showMode = null;
  }

  showTrigger(): void {
    if(this.editStatus) {
      this.showMode = this.showMode ? null : this.modeType;
      this.formControl.patchValue(this.value);
    }
  }

  submit(): void {
    const { value, dirty, invalid } = this.formControl;

    switch(true) {
      case invalid || this.request: {
        return;
      }
      case !dirty: {
        break;
      }
      default: {
        const isEqual: boolean = Object.is(this.value, value);

        if(isEqual) break;

        this._submit.emit(value);
      
      }
    }

    this.showTrigger();
  }
}
