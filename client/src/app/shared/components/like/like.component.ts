import { Component, Input, EventEmitter, Output } from '@angular/core';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { changeTop } from '../../animations/animation';

@Component({
  selector: 'cst-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.scss'],
  animations: [changeTop(300, 15)]
})
export class LikeComponent {
  
  @Output('action') _action = new EventEmitter<void>()
  @Input('like') like: boolean = false;
  @Input('showTotal') showTotal: boolean = true;
  @Input('totalLikes') totalLikes: number;
  @Input('cssStyles') cssStyles: ICssStyles = {};


  getStyles(): ICssStyles {
    return {
      activated: this.like,
      deactivated: !this.like
    }
  }

  action(): void {
    this._action.emit();
  }
}
