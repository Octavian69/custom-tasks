import { Component, Output, EventEmitter, Input} from '@angular/core';

@Component({
  selector: 'cst-intersection-observer',
  templateUrl: './intersection-observer.component.html',
  styleUrls: ['./intersection-observer.component.scss']
})
export class IntersectionObserverComponent {

  emitCounter: number = 0;

  @Input('detectedCount') detectedCount: number;
  @Output('intersection') intersection = new EventEmitter<void>();

  action(): void {
    
    if(this.detectedCount >= this.emitCounter) {
      this.intersection.emit();
    }
    
    this.emitCounter++;
    
  }

}
