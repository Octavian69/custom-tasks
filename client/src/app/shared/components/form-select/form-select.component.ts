import { Component, OnInit, Input, Injector, Provider, forwardRef, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { ControlValueAccessor, NgControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ErrorMatcher } from '../../classes/error.matcher';
import { IControlSelect } from 'src/app/interfaces/IControls/IControlSelect';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { ControlError } from 'src/app/types/validation.types';
import { ListOption, AnyOption } from 'src/app/types/shared.types';
import { MatSelect } from '@angular/material/select';

const VALUE_ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FormSelectComponent),
  multi: true
}

@Component({
  selector: 'cst-form-select',
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.scss'],
  providers: [VALUE_ACCESSOR]
})
export class FormSelectComponent implements OnInit, AfterViewInit, ControlValueAccessor, IControlSelect {

  @ViewChild('controlRef', { static: false }) controlRef: MatSelect;

  @Input('options') options: (ListOption | AnyOption)[] = [];
  @Input('placeholder') placeholder: string;
  @Input('fontSize') fontSize: string = '16px';
  @Input('bindLabel') bindLabel: string = 'title';
  @Input('bindValue') bindValue: string = 'id';
  @Input('multiple') multiple: boolean = false;
  @Input('isResetIcon') isResetIcon: boolean = true;
  @Input('errorCondition') errorCondition: 'standard' | 'date' = 'standard';
  @Input('errors') errors: ControlError[] = [];
  @Input('typeofValue') typeofValue: 'primitive' | 'reference' = 'primitive';
  @Input('disabledOptFn') disabledOptFn: (opt: any) => boolean = (option: any) => false;

  @Output('focus') focus: EventEmitter<Event> = new EventEmitter();
  @Output('blur') blur: EventEmitter<Event> = new EventEmitter();
  @Output('initialize') _initialize = new EventEmitter<MatSelect>();
  
  value: any;
  formControl: NgControl;
  errorMatcher: ErrorMatcher;
  disabled: boolean = false;

  onChange: (v: any) => void = (value: any) => {};
  onTouched: (e: Event) => void = (e: Event) => {};

  constructor(private injector: Injector) { }
  setInputType?: () => void;

  ngOnInit() {
    this.formControl = this.injector.get(NgControl);
    this.errorMatcher = new ErrorMatcher(this.formControl, this.errorCondition);
  }

  ngAfterViewInit() {
    this.initialize();
  }

  initialize(): void {
    this._initialize.emit(this.controlRef);
  }

  setValue(value: any) {
    this.value = value;
    this.onChange(value);
  }

  reset(e: Event) {
    e.stopPropagation();
    this.setValue(null);
    this.onTouched(e);
  }

  getValue(opt: AnyOption): any {
    return this.typeofValue === 'primitive' ? opt[this.bindValue] : opt;
  }

  getErrorStyle(error: ControlError): ICssStyles {
    const { errorName } = error;
    const isError: boolean = this.formControl.hasError(errorName);

    return {
      display: isError ? '' : 'none'
    }
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onFocus(e: Event): void {
    this.onTouched(e);
    this.focus.emit(e);
  }

  onBlur(e: Event): void {
    this.blur.emit(e);
  }

}
