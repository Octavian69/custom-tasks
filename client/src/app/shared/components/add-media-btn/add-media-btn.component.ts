import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cst-add-media-btn',
  templateUrl: './add-media-btn.component.html',
  styleUrls: ['./add-media-btn.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddMediaBtnComponent {

  @Input('status') status: boolean = false;
  @Input('activeIcon') activeIcon: string;
  @Input('deactiveIcon') deactiveIcon: string;
  @Input('activeText') activeText: string;
  @Input('deactiveText') deactiveText: string;

  @Output('action') _action = new EventEmitter<boolean>();

  getContent(type: 'Icon' | 'Text'): string {
    const action = this.status ? `deactive${type}` : `active${type}`

    return this[action];
  }

  action(): void {
    this._action.emit(this.status);
  }

}
