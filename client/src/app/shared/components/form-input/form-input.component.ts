import { Component, Input, forwardRef, Provider, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output, AfterViewInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { ErrorMatcher } from '../../classes/error.matcher';
import { IControlInput } from 'src/app/interfaces/IControls/IControlInput';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { trim } from '../../handlers/handlers';
import { ControlError } from 'src/app/types/validation.types';
import { MatInput } from '@angular/material/input';

const VALUE_ACCESSOR: Provider = {
  provide:  NG_VALUE_ACCESSOR,
  useExisting:  forwardRef(() => FormInputComponent),
  multi: true 
}

@Component({
  selector: 'cst-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
  providers: [VALUE_ACCESSOR]
})
export class FormInputComponent implements  OnInit, AfterViewInit, ControlValueAccessor, IControlInput {

  @ViewChild('inputRef', {static: false}) controlRef: MatInput;
  
  @Input('type') type: string = 'text';
  @Input('minLength') minLength: number;
  @Input('maxLength') maxLength: number;
  @Input('placeholder') placeholder: string;
  @Input('fontSize') fontSize: string = '16px';
  @Input('isResetIcon') isResetIcon: boolean = true;
  @Input('isHiddenIcon') isHiddenIcon: boolean = false;
  @Input('trimPattern') trimPattern: string = ' ';
  @Input('errorCondition') errorCondition: 'standard' | 'date' = 'standard';
  @Input('errors') errors: ControlError[] = [];

  @Output('initialize') _initialize: EventEmitter<MatInput> = new EventEmitter<MatInput>();
  

  value: string;
  formControl: NgControl;
  errorMatcher: ErrorMatcher;
  disabled: boolean = false;
  hidden: boolean = true;

  onChange: (v: string) => void = (value: string) => {};
  onTouched: (e: Event) => void = (e: Event) => {};

  constructor(private injector: Injector) {}

  ngOnInit() {
    this.formControl = this.injector.get(NgControl);
    this.errorMatcher = new ErrorMatcher(this.formControl, this.errorCondition);
  }

  ngAfterViewInit() {
    this.initialize();
  }

  initialize(): void {
    this._initialize.emit(this.controlRef);
  }

  setValue(value: string) {
    this.value = value;
    this.onChange(trim(value, this.trimPattern));
  }

  reset(e: Event) {
    e.stopPropagation();
    this.setValue(null);
    this.onTouched(e);
  }

  setInputType(): void {
      this.hidden = !this.hidden;
      this.type = this.type === 'text' ? 'password' : 'text';
  }

  getErrorStyle(error: ControlError): ICssStyles {
    const { errorName } = error;
    const isError: boolean = this.formControl.hasError(errorName);

    return {
      display: isError ? '' : 'none',
      paddingBottom: '.5rem'
    }
  }

  writeValue(newValue: string): void {
    this.value = newValue;
  }
  
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
