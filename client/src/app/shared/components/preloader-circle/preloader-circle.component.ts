import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cst-preloader-circle',
  templateUrl: './preloader-circle.component.html',
  styleUrls: ['./preloader-circle.component.scss']
})
export class PreloaderCircleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
