import { Component, Input, Output, EventEmitter, HostBinding } from '@angular/core';
import { bottomShow, fade } from '../../animations/animation';

@Component({
  selector: 'cst-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
  animations: [ fade, bottomShow ]
})
export class ConfirmComponent {

  @HostBinding('@fade')
  @Input('text') text: string;
  @Output('confirm') _confirm = new EventEmitter<boolean>();

  confirm(value: boolean): void {
    this._confirm.emit(value)
  }
}
