import { Component, ElementRef, ViewChild, Output, EventEmitter, HostBinding } from '@angular/core';
import { fade } from '../../animations/animation';

@Component({
  selector: 'cst-shadow',
  templateUrl: './shadow.component.html',
  styleUrls: ['./shadow.component.scss'],
  animations: [fade]
})
export class ShadowComponent {
  
  @HostBinding('@fade')

  @Output('action') _action = new EventEmitter<boolean>();
  @ViewChild('shadowRef', { static: true }) shadowRef: ElementRef;

  constructor() { }

  action(e: Event) {
    const { nativeElement } = this.shadowRef;

    if(e.target === nativeElement) this._action.emit(true);
  }

}
