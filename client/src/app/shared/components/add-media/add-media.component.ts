import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import SharedDB from '../../../db/shared.db'
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { changeTop } from '../../animations/animation';
import { getDataByArrayKeys } from '../../handlers/handlers';
import { BooleanValues } from 'src/app/types/shared.types';

@Component({
  selector: 'cst-add-media',
  templateUrl: './add-media.component.html',
  styleUrls: ['./add-media.component.scss'],
  animations: [changeTop(300, 15)]
})
export class AddMediaComponent implements OnInit  {

  tooltipTexts: BooleanValues<string>;

  @Input('tooltips') tooltips: string;
  @Input('cssStyles') cssStyles: ICssStyles = {};
  @Input('have') have: boolean = false;
  @Input('loading') loading: boolean = false;
  @Output('action') _action = new EventEmitter<void>();

  ngOnInit() {
    this.initTooltips();
  }

  initTooltips(): void {
    this.tooltipTexts = getDataByArrayKeys(['shared', 'addMediaTooltipTexts', this.tooltips], SharedDB)
  }

  getCssClass(): ICssStyles {
    return {
      activated: this.have,
      deactivated: !this.have
    }
  }

  getTooltipText(): string {
    const key: string = String(this.have);

    return this.tooltipTexts[key];
  }

  action(): void {
    this._action.emit();
  }

}
