import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ArrowType } from 'src/app/models/ArrowType';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';

@Component({
  selector: 'cst-slider-arrow',
  templateUrl: './slider-arrow.component.html',
  styleUrls: ['./slider-arrow.component.scss']
})
export class SliderArrowComponent  {
  
  tooltip: ArrowType = new ArrowType('Предыдущая', 'Следующая');
  tooltipPosisiton: ArrowType = new ArrowType('before', 'after');
  icon: ArrowType = new ArrowType('arrow_back_ios', 'arrow_forward_ios');
  margin: ArrowType = new ArrowType('margin-right: 1rem', 'margin-left: 1rem');
  
  
  @Input('type') type: 'prev' | 'next';
  @Output('action') _action = new EventEmitter<'prev' | 'next'>();
  

  getMargin(): ICssStyles {
    const property: string = this.type === 'prev' ? 'margin-right' : 'margin-left';

    return {
        [property]: '1rem'
    }
  }

  action(): void {
    this._action.emit(this.type);
  }

}
