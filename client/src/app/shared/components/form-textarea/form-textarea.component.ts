import { Component, Provider, forwardRef, Injector, OnInit, Input, ViewChild, ElementRef, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { ErrorMatcher } from '../../classes/error.matcher';
import { IControlTextArea } from 'src/app/interfaces/IControls/IControlTextArea';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { trim } from '../../handlers/handlers';
import { ControlError } from 'src/app/types/validation.types';

const VALUE_ACCESSOR: Provider = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => FormTextareaComponent),
    multi: true
}

@Component({
  selector: 'cst-form-textarea',
  templateUrl: './form-textarea.component.html',
  styleUrls: ['./form-textarea.component.scss'],
  providers: [VALUE_ACCESSOR]
})
export class FormTextareaComponent implements OnInit, AfterViewInit, ControlValueAccessor, IControlTextArea {
  
  @ViewChild('textAreaRef', { static: false }) controlRef: ElementRef

  @Input('maxLength') maxLength: number;
  @Input('minLength') minLength: number;
  @Input('placeholder') placeholder: string;
  @Input('fontSize') fontSize: string = '16px';
  @Input('rows') rows: number;
  @Input('isResetIcon') isResetIcon: boolean = true;
  @Input('errorCondition') errorCondition: 'standard' | 'date' = 'standard';
  @Input('errors') errors: ControlError[] = [];

  @Output('initialize') _initialize: EventEmitter<ElementRef> = new EventEmitter<ElementRef>()

  value: string;
  formControl: NgControl;
  errorMatcher: ErrorMatcher;
  disabled: boolean = false;

  onChange: (v: string) => void = (value: string) => {};
  onTouched: (e: Event) => void = (e: Event) => {};

  constructor(
    private injector: Injector
  ) { }

  ngOnInit() {
    this.formControl = this.injector.get(NgControl);
    this.errorMatcher = new ErrorMatcher(this.formControl, this.errorCondition);
  }

  ngAfterViewInit() {
    this.initialize();
  }

  initialize() {
    this._initialize.emit(this.controlRef)
  }

  setValue(value: string): void {
    this.value = value;
    this.onChange(trim(value));
  }

  reset(e: Event): void {
    e.stopPropagation();
    this.setValue(null);
    this.onTouched(e);
  }

  getErrorStyle(error: ControlError): ICssStyles {
    const { errorName } = error;
    const isError: boolean = this.formControl.hasError(errorName);

    return {
      display: isError ? '' : 'none'
    }
  }

  writeValue(value: string): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
