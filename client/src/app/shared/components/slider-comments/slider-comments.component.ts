import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { trim, unsubscriber, isExpand } from '../../handlers/handlers';
import { CommentViewService } from 'src/app/services/comment.view.service';
import { Comment } from 'src/app/models/Comment';
import { IComment } from 'src/app/interfaces/IComment';
import { Observable, Subscription } from 'rxjs';
import { MediaList } from 'src/app/models/MediaList';
import { CommentFlags } from 'src/app/models/flags/CommentFlags';
import { CommentRequest } from 'src/app/models/CommentRequest';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { IUser } from 'src/app/interfaces/IUser';
import { UserViewService } from 'src/app/services/user.view.service';
import { CommentRefType } from 'src/app/types/comment.types';

@Component({
  selector: 'cst-slider-comments',
  templateUrl: './slider-comments.component.html',
  styleUrls: ['./slider-comments.component.scss']
})
export class SliderCommentsComponent implements OnInit, OnDestroy {
  
  _refId: string;
  comments$: Observable<MediaList<IComment>> = this.viewService.getStream('comments$');
  flags: CommentFlags = this.viewService.getFlagObject();
  subs$: Subscription[] = [];
  request: CommentRequest;
  
  @Input('maxHeight') maxHeight: string;
  @Input('minHeight') minHeight: string;
  @Input('refType') refType: CommentRefType; 
  @Input('refId') set refId(value) {
    if(value) {
      this._refId = value;
      this.request = new CommentRequest(value, this.refType);
      this.viewService.fetch('replace', this.request);
    }
  };
  
  commentControl: FormControl = new FormControl('', [], []);

  constructor(
    public viewService: CommentViewService,
    private userViewService: UserViewService
  ) { }

  ngOnInit() {
    this.initUserMutateSub();
  }

  ngOnDestroy() {
    this.viewService.destroy();
    unsubscriber(this.subs$);
    
  }

  disabled(): boolean {
    return <boolean>this.viewService.getFlag('isLoading') || <boolean>this.viewService.getFlag('isExpand');
  }

  initUserMutateSub(): void {
    const mutate$: Observable<SimpleAction<IUser>> = this.userViewService.getStream('mutate$');

    const sub$: Subscription = mutate$.subscribe((event: SimpleAction<IUser>) => {
      this.viewService.mutateDetail(event);
    });  

    this.subs$.push(sub$);
  }

  trackByFn(index: number, comment: IComment) {
    return comment._id;
  }

  addComment(): void {
    const { value } = this.commentControl;

    const Text: string = trim(value);

    if(Text) {
      const { _id: User } = this.viewService.getDecodeToken();
      const comment: Comment = new Comment(Text, this.refType, this._refId, User);
      this.viewService.create(comment);
    }

    this.commentControl.reset();
  }

  expand() {
    const comments: MediaList<IComment> = this.viewService.getStreamValue('comments$');

    const expand: boolean = isExpand(comments);

    if(expand) {
      this.request.page.skip++;
      this.viewService.fetch('expand')
    }
  }

}
