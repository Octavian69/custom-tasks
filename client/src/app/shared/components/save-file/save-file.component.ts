import { Component, Input } from '@angular/core';
import { DownloadService } from 'src/app/services/download.service';

@Component({
  selector: 'cst-save-file',
  templateUrl: './save-file.component.html',
  styleUrls: ['./save-file.component.scss']
})
export class SaveFileComponent {

  @Input() path: string;
  @Input() tooltip: string = '';

  constructor(
    private downloadService: DownloadService
  ) { }

  getTooltip(): string {
    return `Скачать ${this.tooltip}`;
  }

  download() {
    const parse: string[] = String(this.path).split('\\');
    const isDownload = this.downloadService.getFlag('isDownload') as boolean;

    const filename: string = parse[parse.length - 1];
    // const extensions: string = last.match(/[.]\w+$/)[0];
    // const filename: string = last.replace(extensions, '');

    if(this.path && !isDownload) {
      this.downloadService.download(this.path, filename);
    }
  }
}
