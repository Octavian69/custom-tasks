import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { AudioWorkService } from 'src/app/services/audio.work.service';
import { IAudio } from 'src/app/interfaces/IAudio';

@Component({
  selector: 'cst-headboard',
  templateUrl: './headboard.component.html',
  styleUrls: ['./headboard.component.scss']
})
export class HeadboardComponent  {

  get isLogged(): boolean {
    return this.loginService.getLogged();
  }

  constructor(
    private router: Router,
    private loginService: LoginService,
    public audioWorker: AudioWorkService
  ) { }

  navigate(path: string) {
    this.router.navigateByUrl(`/${path}`);
  }

  home(): void {
    const path: string = this.isLogged ? `/account/${this.loginService.getOwnId()}` : '/login';

    this.router.navigateByUrl(path);

  }

  isShowAudio(): boolean {
    const audio: IAudio = this.audioWorker.currentSong$.getValue();

    return !!audio && !this.router.url.includes('audio');
  }

}
