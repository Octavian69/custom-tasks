import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import SharedDB from '../../../db/shared.db';
import { getDataByArrayKeys } from '../../handlers/handlers';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { BorderStyles } from 'src/app/types/shared.types';

@Component({
  selector: 'cst-borders',
  templateUrl: './borders.component.html',
  styleUrls: ['./borders.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BordersComponent  {

  @Input('borderStyles') BS: BorderStyles = getDataByArrayKeys(['shared', 'borderStyles'], SharedDB);
  @Input('position') position: 'top' | 'bottom';

  getItemsStyles(child: 'left' | 'right'): ICssStyles {
    const LRBorderProp: string = `border-${child}`;
    const UDBorderProp: string = this.position === 'top' ? 'border-top' : 'border-bottom';

    return {
      width: this.BS.width,
      height: this.BS.height,
      [LRBorderProp]: `${this.BS.borderWidth} ${this.BS.borderStyle} ${this.BS.borderColor}`,
      [UDBorderProp]: `${this.BS.borderWidth} ${this.BS.borderStyle} ${this.BS.borderColor}`,
    }
  }

}
