import { Component, OnInit, Input, OnDestroy, HostBinding } from '@angular/core';
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { IAudio } from 'src/app/interfaces/IAudio';
import { deepCopy } from '../../handlers/handlers';
import { ICurrentMediaList } from 'src/app/interfaces/ICurrentMediaList';
import { MediaRequest } from 'src/app/models/MediaRequest';
import { MediaList } from 'src/app/models/MediaList';
import { AudioWorkService } from 'src/app/services/audio.work.service';
import { topShow, fade } from '../../animations/animation';
import { filterUnique } from '../../handlers/media.handlers';
import { ResponseAction } from 'src/app/types/request-response.types';
import { untilDestroyed } from 'ngx-take-until-destroy';

@AutoUnsubscribe({arrayName: 'subs$'})
@Component({
  selector: 'cst-headboard-audio',
  templateUrl: './headboard-audio.component.html',
  styleUrls: ['./headboard-audio.component.scss'],
  animations: [topShow, fade]
})
export class HeadboardAudioComponent implements OnInit, OnDestroy {

  searchControl: FormControl;

  loading: boolean = false;
  isShowList: boolean = false;
  
  mediaRequest: MediaRequest;
  mediaList: MediaList<IAudio>;
  subs$: Subscription[] = [];
  
  @HostBinding('@topShow')
  @Input('audio') audio: IAudio;
  @Input('currentList') currentList: ICurrentMediaList<IAudio>;

  constructor(
    public audioWorker: AudioWorkService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() { }

  init(): void {
    this.initSearch();
  }

  initMedia(flag: boolean): void {

    if(flag) {
      const { mediaRequest, mediaList: { rows, totalCount } } = this.currentList;
      this.mediaRequest = deepCopy(mediaRequest);
      this.mediaList = new MediaList(rows.concat(), totalCount);
       
      this.isShowList = true;

    } else {
      this.isShowList = false;
      this.reset();
      this.searchControl.patchValue(null, {emitEvent: false});
    }

  }

  initSearch(): void {
    this.searchControl = new FormControl('', [], []);

    const sub$: Subscription = this.searchControl.valueChanges.pipe(debounceTime(400)).subscribe((value: string) => {
     
        if(value && this.mediaRequest) {
          this.mediaRequest.searchString = value;
          this.mediaRequest.state = {methodType: 'by-string'};
          this.mediaRequest.page.skip = 0;

          this.fetch();
        } else {
          this.initMedia(true);
        }
    });
    
    this.subs$.push(sub$);
  }

  fetch(action: ResponseAction = 'replace'): void {
    this.loading = true;

    const next = (response: MediaList<IAudio>) => {
        const { rows, totalCount } = response;

        this.mediaList.rows = action === 'replace' ? rows : this.mediaList.rows.concat(rows);
        this.mediaList.totalCount = totalCount;

        this.loading = false;
    }

    this.audioWorker
    .getMediaMethod(this.mediaRequest)
    .pipe(
        map((response: MediaList<IAudio>) => {
          return filterUnique(this.mediaList, response, action)
      }),
      untilDestroyed(this)
    )
    .subscribe({ next });
  }

  page(): void {
    const { rows: { length }, totalCount } = this.mediaList;
    const isExpand: boolean = length < totalCount;

    if(isExpand) {
      this.mediaRequest.page.skip++;
      this.fetch('expand');
    }
  }

  changeSong(audio: IAudio): void {
    const { playlistId } = this.mediaRequest;
    const list: MediaList<IAudio> = deepCopy(this.mediaList);
    const req: MediaRequest = deepCopy(this.mediaRequest);

    const audioList: ICurrentMediaList<IAudio> = this.audioWorker.createList(list, req, playlistId);

    this.audioWorker.changeCurrentList(audioList);
    this.audioWorker.playSong(audio);
  }

  reset(): void {
    this.mediaList = null;
    this.mediaRequest = null;
  }
  
  isPaused() {
    return this.audioWorker.getPlayStatus(this.audio);
  }

  getPlayIcon(): string {
    return this.isPaused() ? 'play_arrow' : 'pause';
  }

  trackByFn(index: number, audio: IAudio): string {
    return audio._id;
  }
}
