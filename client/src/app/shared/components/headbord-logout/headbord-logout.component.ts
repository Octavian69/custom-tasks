import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { scaleShow, fade } from '../../animations/animation';
import { SocketService } from 'src/app/services/socket.service';
import { Coordinate } from 'src/app/types/shared.types';

@Component({
  selector: 'cst-headbord-logout',
  templateUrl: './headbord-logout.component.html',
  styleUrls: ['./headbord-logout.component.scss'],
  animations: [scaleShow, fade]
})
export class HeadbordLogoutComponent implements OnInit, OnDestroy {

  time: Date = new Date();
  interval$: number;
  isShowRemove: boolean = false;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private socketService: SocketService
  ) { }

  ngOnInit() {
    // this.getWeather();
  }

  ngOnDestroy() {
    clearInterval(this.interval$);
  }

  getCoordinate(): Promise<Coordinate> {
    return new Promise((res) => {
      window.navigator.geolocation.getCurrentPosition((cb: Position) => {
        const { latitude, longitude } = cb.coords;
        res( {latitude, longitude} );
      })
    })
  }

  async getWeather() {
    const coordinate: Coordinate = await this.getCoordinate();

    if(coordinate) {
      this.loginService.getWeather(coordinate);
    }
  }

  navigate(): void {
    const { _id } = this.loginService.getDecodeToken();

    this.router.navigate(['/account', _id]);
  }

  remove(confirm: boolean) {
    if(confirm) {
      this.loginService.remove();
    }

    this.isShowRemove = false;
  }

  logout() {
    this.socketService.reset();
    this.loginService.logOut();
  }
}
