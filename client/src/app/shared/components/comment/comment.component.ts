import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IComment } from 'src/app/interfaces/IComment';
import { Router } from '@angular/router';
import { fadeEvent, changeTop } from '../../animations/animation';

@Component({
  selector: 'cst-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss'],
  animations: [fadeEvent, changeTop(300, 15)]
})
export class CommentComponent {
  @Input('comment') comment: IComment;
  @Input('odd') odd: boolean;
  @Output('like') _like = new EventEmitter<IComment>();

  constructor(
    private router: Router
  ) { }

  getLikeIcon(): string {
    return this.comment.OwnLike ? 'favorite' : 'favorite_border';
  }

  getShowClass() {
      return {
        'right-show': this.odd,
        'left-show': !this.odd
      }
  }

  navigate(): void {
    this.router.navigate(["/account", this.comment.User], { queryParams: {} });
  }

  like(): void {
    this._like.emit(this.comment);
  }
}
