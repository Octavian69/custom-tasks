import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sliceString'
})
export class SliceStringPipe implements PipeTransform {

  transform(value: string, start: number = 0, end: number = 32): string {
    if(value) {
      const result: string = value.substr(start, end);

      return value.length > end ?  `${result}...` : result;
    }

    return value

  }

}
