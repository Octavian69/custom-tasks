import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'moment'
})
export class MomentPipe implements PipeTransform {

  transform(value: Date, format: string = 'L - HH : mm : ss'): string {
    return moment(value).locale('ru').format(format);
  }

}
