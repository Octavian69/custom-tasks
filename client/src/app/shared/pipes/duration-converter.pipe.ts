import { Pipe, PipeTransform } from '@angular/core';
import { durationConverterFn } from '../handlers/handlers';

@Pipe({
  name: 'durationConverter'
})
export class DurationConverterPipe implements PipeTransform {

  transform(value: number): string {
    const time: string = durationConverterFn(value);

    return time;
  }

}
