import { Pipe, PipeTransform } from '@angular/core';
import { pathManager } from '../handlers/handlers';

@Pipe({
  name: 'pathManager'
})
export class PathManagerPipe implements PipeTransform {

  transform(path: string): string {
  
    return pathManager(path);
  }

}
