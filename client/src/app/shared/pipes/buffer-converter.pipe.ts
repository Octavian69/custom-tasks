import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { arrayBufferToBase64 } from '../handlers/handlers';
import { BufferType } from 'src/app/types/shared.types';

@Pipe({
  name: 'bufferConverter'
})
export class BufferConverterPipe implements PipeTransform {

  constructor(
    private sanitaizer: DomSanitizer
  ) {}

  transform(value: BufferType): SafeStyle {
    const base64: string = arrayBufferToBase64(value.data);
    const img: SafeStyle = this.sanitaizer.bypassSecurityTrustStyle(`url(${base64})`);

    return img;
  }

}
