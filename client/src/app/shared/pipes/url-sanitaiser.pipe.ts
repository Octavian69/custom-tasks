import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'urlSanitaiser'
})
export class UrlSanitaiserPipe implements PipeTransform {

  constructor(
    private sanitaizer: DomSanitizer
  ) {}

  transform(url: string): any {
    return this.sanitaizer.bypassSecurityTrustResourceUrl(url);
  }

}
