import { trigger, transition, animate, style, state, AnimationTriggerMetadata } from '@angular/animations';

export const topShow = trigger('topShow', [
    transition(':enter', [
        style({ 
            opacity: '0',
            transform: 'translateY(-100%)'
        }),
        animate(400, style({
            opacity: '1',
            transform: 'translateY(0%)'
        }))
    ]),
    transition(':leave', [
        animate(400, style({
            opacity: '0',
            transform: 'translateY(-100%)'
        }))
    ])
]);

export const translateAxis = (fromX: string, fromY: string, toX: string, toY: string) =>  {
   
    return trigger('topShow', [
        transition(':enter', [
            style({ 
                opacity: '0',
                transform: `translateX(${fromX}) translateY(${fromY})`
            }),
            animate(400, style({
                opacity: '1',
                transform: `translateX(${toX}) translateY(${toY})`
            }))
        ]),
        transition(':leave', [
            animate(400, style({
                opacity: '0',
                transform: `translateX(${fromX}) translateY(${fromY})`
            }))
        ])
    ])
}

export const parentAnimation = trigger('parentAnimation', [
    transition(':enter', [
        // query('@*', [animateChild()], { optional: true })
    ]),
    transition(':leave', [
        // query('@*', [animateChild()], { optional: true })
    ]),
])

export const changeTop = (ms: number, offset: number) =>  trigger('changeTop', [
    transition('* <=> *', [
        style({ 
            opacity: 0,
            transform: `translateY(-${offset}px)`
         }),
         animate(ms, style({
            opacity: 1,
            transform: `translateY(0px)`
         }))
    ])
])

export const animateHeight = (height: string) => trigger('animateHeight', [
    state('a', style({
        height: '*'
    })),
    state('b', style({
        height 
    })),
    transition('a <=> b', animate(300))
]);

export const bottomShow = trigger('bottomShow', [
    transition(':enter', [
        style({
            transform: 'translate(-50%, 100%)'
        }),
        animate(300, style({transform: 'translate(-50%, 0%)'}))
        
    ]),
    transition(':leave',  animate(300, style({
        transform: 'translate(-50%, 100%)'
    }))),
]);


export const scaleShow = trigger('scaleShow', [
    transition(':enter', [
        style({
            transform: 'scale(0)'
        }),
        animate(300, style({ transform: 'scale(1)' }))
    ]),
    transition(':leave', [
        style({transform: 'scale(1)'}),
        animate(300, style({ transform: 'scale(0)' }))
    ])
]);

export const transitionWidth = (ms: number) => trigger('transitionWidth', [
    transition(':enter', [
        style({ width: '0px' }),
        animate(ms, style({ width: '*' }))
    ]),
    transition(':leave', animate( ms , style( { width: '0px' } )))
])


export const transitionHeight = trigger('transitionHeight', [
    transition(':enter', [
        style({ height: '0' }),
        animate(100, style({ height: '*' }))
    ]),
    transition(':leave', [
        style({ height: '*' }),
        animate(400, style({ height: '0px' }))
    ])
]);

export const defaultSizeAnim = (field: string, ms: string) =>  trigger('defaultSizeAnim', [
    transition(':enter', [
        style({
            [field]: '0'
        }),
        animate(ms, style({ [field]: '*' }))
    ]),
    transition(':leave', [
        animate(ms, style({ [field]: '0' }))
    ]), 
    transition('*=>*', [
        style({ [field]: '0' }),
        animate(ms, style({ [field]: '*' }))
    ]) 
])

export const fade: AnimationTriggerMetadata = trigger('fade', [
    transition(':enter', [
        style({
            opacity: '0'
        }),
        animate(300, style({ opacity: '1' })),
        // query('@*', [
        //     animateChild()
        // ], {optional: true})
    ]),
    transition(':leave', [
        // query('@*', [
        //     animateChild()
        // ], {optional: true}),
        animate(300, style({ opacity: 0 }))
    ])
]);

export const fadeEvent = trigger('fadeEvent', [
    transition('* <=> *', [
        style({ opacity: 0 }),
        animate(300, style({ opacity: 1 }))
    ])
])

export const ANToggleMenu = trigger('ANToggleMenu', [
    transition('*=>*', [
        style({
            opacity: '0',
            transform: 'scale(0)'
        }),
        animate(300, style({
            opacity: 1,
            transform: 'scale(1)'
        }))
    ])
])