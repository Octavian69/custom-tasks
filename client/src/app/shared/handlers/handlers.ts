import { Subscription } from 'rxjs';
import { FILE } from 'src/app/namespaces/Namespaces';
import * as moment from 'moment';
import { environment } from '@env/environment';
import { MediaList } from 'src/app/models/MediaList';
import { IUser } from 'src/app/interfaces/IUser';
import { CheckedFile } from 'src/app/types/validation.types';
import { ListOption } from 'src/app/types/shared.types';
import { InfoRow } from 'src/app/types/account.types';


///////Auth

export function tokenGetter(): string {
    return window.sessionStorage.getItem('user');
}
// : MediaList<T>

export function isOwn(id: string): boolean {
    const user: IUser = JSON.parse(window.sessionStorage.getItem('ownUser'));

    return Object.is(user._id, id);
}

export function mapIsOwnItem<R extends { IsOwn: boolean }>(item: R, key: string = 'User'): R {
    item.IsOwn = isOwn(item[key]);

    return item;
}

export function mapIsOwnRows<T extends { IsOwn: boolean }>(mediaList: MediaList<T>, key: string = 'User') {
    const rows: T[] = mediaList.rows.map(item => {
        item.IsOwn = isOwn(item[key]);

        return item;
    });
    const { totalCount } = mediaList;
    
    return { rows, totalCount };
}

export function isRefreshTime(exp: number): boolean {
    const expDate: Date = new Date(0);
    const now: number = new Date().valueOf();

    expDate.setUTCSeconds(exp);

    const minutes: number = ( (expDate.valueOf() - now) / 1000 / 60 );

    return +minutes.toFixed(0) < 5;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function unsubscriber(subs: Subscription[]): void {
    subs.forEach(sub => { if(sub) sub.unsubscribe() });
    subs.splice(0);
}

export function sortBy<T>(array: T[], field: string, sort: 'asc' | 'desc', isDate: boolean = false): void {
    array.sort((a: T, b: T) => {

        const val1: any =  isDate ? +new Date(a[field]) : a[field];
        const val2: any =  isDate ? +new Date(b[field]) : b[field];

        return sort === 'asc' ? val1 - val2 : val2 - val1;
    })
}

export function trim(value: string, trimPattern: string = ' '): string {
    const result: string = value ? value.trim().replace(/[ ]+/gi, trimPattern) : value;

    return result;
}

export function completeInfoRows(part: any, configuration: any) {
    const fieldNames: any = Object.keys(configuration);

    const rows: InfoRow[] = fieldNames.reduce((array: any[], current: string) => {
      const { title, async, type, multiple, selectData } = configuration[current];

      let value: any = null;

      switch(true) {

        case async: {
          
          value = part[current] ? part[current].title : null;
          break;
        }


        case type === 'datepicker': {
            value = part[current] ? moment(part[current]).locale('ru').format('LL') : null;
            break;
        }

        case type !== 'select': {
          value = part[current];
          break;
        }

        case multiple: {
          const values: any[] = [];

          if(part[current]) {
            part[current].forEach(item => {
              const option: ListOption = selectData.find((opt: ListOption) => opt.id === item);
              values.push(option.title);
            });
          }

          value = values.length ? values : null;

          break;
        }

        case !multiple: {
          const option: ListOption = selectData.find((opt: ListOption) => opt.id === part[current]);

          if(option) value = option.title;
          
          break;
        }

      }

      if(value) {
        const row: InfoRow = { title, value, type }
        array.push(row);
      }

      return array;
    }, []);


    return rows;
}

export function removeExtName(filename: string) {
    return filename.replace(/[.]\w+\d?$/, '');
}

export function getUrlPart(start: string, url: string) {
    // const regex = new RegExp(`/${start}[/]{1}[a-zA-Z0-9]+[/?]?`);
    // .match(/video-playlist[/]{1}[a-zA-Z0-9]+/)[0].split('/')[1]

}

export function isExpand(mediaList: MediaList<any>): boolean {
    if(!mediaList) return false;

    const { rows: { length }, totalCount } = mediaList;

    const isExpand: boolean = length < totalCount;

    return isExpand;
}

export function containCssClass(elem$: HTMLElement, classNames: string[]): boolean {

    return classNames.some(item => elem$.classList.contains(item));
}

export function checkedFile(TYPE: 'PICTURE' | 'AUDIO' | 'VIDEO', file: File): CheckedFile {
    const { size, type, name} = file;

    const isValidFormat: boolean = FILE.FORMATS[TYPE].includes(type);
    const isValidSize: boolean = (FILE.SIZE[TYPE] * Math.pow(1024, 2)) > size;

    const isValid: boolean = isValidFormat && isValidSize;
    // const filename: string = name.replace(/[.]\w+$/, '').substr(0, FILE.TITLE_LENGTH[TYPE]);
    const errors: string[] = [];

    if(!isValidFormat) errors.push(`Допустимые форматы файла: ${FILE.FORMATS[TYPE]}`)
    if(!isValidSize) errors.push(`Максимальный размер файла: ${FILE.SIZE[TYPE]} Mb.`);


    return { isValid, errors };
}

export function createFormData(file: File, tagName: string, candidate?: any): FormData {
    const formData: FormData = new FormData();

    formData.append(tagName, file, file.name);
    
    if(candidate) {
        formData.append('body', JSON.stringify(candidate));
    }

    return formData;
}

export function completeArrayFormData(files: File[], tagName: string) {
    const formData: FormData = new FormData();

    return files.reduce((accum: FormData, file: File) => {
        accum.append(tagName, file, file.name);

        return accum;
    }, formData)
}


export function completeFormData(file: File, tagName: string, obj: any = null): FormData {
    const formData: FormData = new FormData();

    formData.append(tagName, file, file.name);
    
    if(obj) {
        Object.keys(obj).forEach((key: string) => formData.append(key, obj[key]));
    }

    
    return formData;
}

export function pathManager(path: string): string {
    return `${environment.FILE_URL}${path}`;
}

export function arrayBufferToBase64(buffer: ArrayBuffer): string {
    let binary = '';
    const bytes = new Uint8Array( buffer );

    bytes.forEach(byte => binary += String.fromCharCode(byte));

    return 'data:image/jpeg;base64,' +  window.btoa( binary );
}


export function resetHoursDate(date: Date): moment.Moment {
    return moment(date).set('hours', 0).set('minutes', 0);
}


export function durationConverterFn(duration) {
    
    duration = +duration.toFixed(0);

    function completeTime(time) {
        return time < 10 ? '0' + time : String(time); 
    }
    
    switch(true) {

        case duration < 60: {
            return `00 : ${completeTime(duration)}`;
        }

        case duration < 3600: {
            const minutes = Math.floor(duration / 60);
            const seconds = Math.floor(duration - (minutes * 60));

            return `${completeTime(minutes)} : ${completeTime(seconds)}`
        }

        default: {
            const hours = Math.floor(duration / 60 / 60);
            const minutes = Math.floor((duration - (hours * 60 * 60)) / 60);
            const seconds = duration - ((hours * 60 * 60) + (minutes * 60));

            return `${completeTime(hours)} : ${completeTime(minutes)} : ${completeTime(seconds)}`;
        }
    }
}

export function deepCopy<T>(obj: T): T {
    return JSON.parse(JSON.stringify(obj)); 
}


export function capitalLetter(str: string) {
    return `${str[0].toUpperCase()}${str.slice(1)}`;
}

export function isEnd(elem: HTMLElement): boolean {
    const { scrollTop, scrollHeight, clientHeight } = elem;
    
    const offset: number = scrollHeight - clientHeight;

    return scrollTop >= offset;
}


export function getDataByArrayKeys(keys: string[] = [], database?: any) {

    const db = database || this.database;

    let counter = 1;

    let data = db[keys[0]];
    
    
    while(counter < keys.length) {
        
        data = data[keys[counter]];
        
        counter++;
    }

    if(typeof data === 'function') data = data();
    
    if(data) {
        return JSON.parse( ( JSON.stringify(data) ) );
    }

    return null;
}

export function JSONEqual(val1: any, val2: any) {
    const v1 = JSON.stringify(val1);
    const v2 = JSON.stringify(val2);

    return Object.is(v1, v2);
}