import { UnseenEvent, UnseenAction, UnseenKey } from 'src/app/types/unseen.types';

export function createUnseenEvent(action: UnseenAction, key: UnseenKey, value: number, referenceId: string): UnseenEvent {
    return { action, key, value, referenceId };
}