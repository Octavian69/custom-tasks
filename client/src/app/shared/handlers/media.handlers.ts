import { MediaList } from 'src/app/models/MediaList';
import { ResponseAction } from 'src/app/types/request-response.types';

export function filterUnique<T extends Partial<{ _id: string }>>(current: MediaList<T>, response: MediaList<T>, action: ResponseAction): MediaList<T> {
    
    if(!current || action === 'replace') return response;

    const isResetTotalCount: boolean = response.rows.length === 0;

    const currentRows = current.rows;
    const currentIds: string[] = currentRows.map(item => item._id);
    const unique = response.rows.filter(item => !currentIds.includes(item._id));
    const totalCount = isResetTotalCount ? current.rows.length : response.totalCount;

    return { rows: unique, totalCount }
}