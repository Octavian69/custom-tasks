import FriendsDB from '../../db/friends.db';
import { getDataByArrayKeys } from './handlers';
import { IUser } from 'src/app/interfaces/IUser';
import { StatsOption } from 'src/app/types/friends.types';


export function getStatsList(): { [key: string]: StatsOption } {
    return getDataByArrayKeys(['friends', 'statsList'], FriendsDB);
  }

export function getFilledStats(user: IUser): string[] {
    const totalFields: string[] = getDataByArrayKeys(['friends', 'totalFields'], FriendsDB);

    const filled: string[] = totalFields.filter(key => user[key]);

    return filled;
}

export function completeStats(filled: string[], user: IUser): StatsOption[] {
    const statsList: { [key: string]: StatsOption } = getStatsList();

    return filled.map((key: string) => {
        
      const statsUnit: StatsOption = Object.assign({}, statsList[key]);

      statsUnit.value = user[key];

      return statsUnit;
    })
  }