import { Directive, ElementRef, Input } from '@angular/core';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Directive({
  selector: '[cstPrint]',
  exportAs: 'printRef'
})
export class PrintDirective {

  @Input('Title') Title: string = 'document';
  @Input('ContainerStyles') ContainerStyles: string = '';
  @Input('PrintStyles') PrintStyles: string = '';

  constructor(
    private elem$: ElementRef
  ) { }

  async print(): Promise<any> {
    
    const doc = new jsPDF();

    const container = this.completePrint();

    const canvas = await html2canvas(container);

    document.querySelector('#print').remove();

    doc.addImage(canvas, 'JPEG', 50, 30);
    doc.save(`${this.Title}.pdf`);
    
  }

  completePrint() {
    const { nativeElement } = this.elem$;
    const container: HTMLDivElement = document.createElement('div');
    container.id = 'print'
    const printElement: HTMLDivElement = nativeElement.cloneNode(true);

    printElement.style.cssText = this.PrintStyles;
    container.style.cssText = this.ContainerStyles;

    // printElement
    // .querySelector('li')
    // .querySelector('span')
    // .querySelector('i')
    // .style.display = 'none'

    container.appendChild(printElement);
    document.body.appendChild(container);

    return container;

  }

}
