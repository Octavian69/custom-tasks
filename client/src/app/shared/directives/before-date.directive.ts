import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import * as moment from 'moment';

@Directive({
  selector: '[ifBefore]'
})
export class BeforeDateDirective {
  date: Date
  
  @Input('ifBefore') set ifBefore(value: Date) {
    if(value && +new Date(value) !== +new Date(this.date)) this.show(value);
  }

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef
  ) { }

  show(value: Date) {
    this.date = value;
    
    const isBefore: boolean = moment(value).isBefore(new Date(), 'days');

    if(isBefore) {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainerRef.clear();
    }
  }
}
