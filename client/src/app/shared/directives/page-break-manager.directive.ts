import { Directive, Output, EventEmitter, HostListener, Input } from '@angular/core';
import { isEnd } from '../handlers/handlers';

@Directive({
  selector: '[cstPageBreakManager]'
})
export class PageBreakManagerDirective {
  
  @Input('disable') disable: boolean;
  @Output('ending') ending = new EventEmitter<void>();

  @HostListener('window:scroll', ['$event'])
    scroll(e: Event): void {

      if(this.disable) return;

      const isEndPage: boolean = isEnd(document.documentElement);
  
      if(isEndPage) {
        this.ending.emit()
      }
    }
}
