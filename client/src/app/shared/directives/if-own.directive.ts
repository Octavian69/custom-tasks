import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';
import { ViewWorker } from '../classes/view.worker';

@Directive({
  selector: '[ifOwn]',
  exportAs: 'own'
})
export class IfOwnDirective extends ViewWorker<null> {

  ownId: string = this.getOwnId();

  @Input('ifOwn') set ifOwn(userId: string) {
    const isOwn: boolean = this.isOwn(userId);

    if(isOwn) {
      this.viewContainer.createEmbeddedView(this.templaterRef);
    } else {
      this.viewContainer.clear();
    }
  }

  constructor(
    private templaterRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {
    super(null);
  }

}
