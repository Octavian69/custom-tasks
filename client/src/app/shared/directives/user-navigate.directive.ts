import { Directive, HostListener, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';

@Directive({
  selector: '[cstUserNavigate]'
})
export class UserNavigateDirective {

  @Input('cstUserNavigate') cstUserNavigate: string; //userId
  @Output('navigate') _navigate = new EventEmitter<string>();

  @HostListener('click', ['$event'])
    navigate(): void {
      if(this.cstUserNavigate) {
        this._navigate.emit(this.cstUserNavigate);
        this.router.navigate(['/account', this.cstUserNavigate]);
      }
    }

  constructor(
    private router: Router
  ) { }

}
