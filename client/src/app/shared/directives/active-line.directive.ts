import { Directive, ElementRef, Input, Renderer2, HostBinding, AfterViewInit } from '@angular/core';
import SharedDB from '../../db/shared.db'; 
import { ViewWorker } from '../classes/view.worker';
import { LineSettings, TPosition } from 'src/app/types/shared.types';

@Directive({
  selector: '[cstActiveLine]'
})
export class ActiveLineDirective extends ViewWorker<null> implements AfterViewInit {
  styleProps: LineSettings = this.getDataByArrayKeys(['shared', 'lineSettings']);
  parentRef: HTMLElement = <HTMLElement>this.elem$.nativeElement.parentElement;
  _currentIndex: number;
  sizeProp: 'height' | 'width';
  offsetProp: 'top' | 'left';

  @HostBinding('style.height') height: string;
  @HostBinding('style.width') width: string;
  @HostBinding('style.left') left: string;
  @HostBinding('style.top') top: string;
  
  @Input('position') position: TPosition;

  @Input('currentIndex') set currentIndex(value: number) {
      if(typeof value === 'number') {
        this._currentIndex = value;
        this.setLineState(value);
      }
  };

  constructor(
    private elem$: ElementRef,
    private renderer: Renderer2,
    
  ) { 
    super(null, SharedDB)
   }

  ngAfterViewInit() {
    this.initProps();
    this.initLineState();
  }

  initProps(): void {
    const isXAxios: boolean = this.position === 'bottom' || this.position === 'top';

    this.sizeProp = isXAxios ? 'width' : 'height';
    this.offsetProp = isXAxios ? 'left' : 'top'
  }

  initLineState(): void {
    const { nativeElement } = this.elem$;

    this.renderer.setStyle(this.parentRef, 'position', 'relative');
    this.renderer.setStyle(nativeElement, 'position', 'absolute');
    this.renderer.setStyle(nativeElement, this.position, '0');
    this.renderer.setStyle(nativeElement, this.offsetProp, '0');
    this.renderer.setStyle(nativeElement, 'transition', '.3s all ease');
  }

  setLineState(index: number): void {
      const offset: number = this.getOffset(index);
      const size: number = this.parentRef.children[index][ this.getCompletePropName(this.sizeProp) ];

      this[this.offsetProp] = offset + 'px';
      this[this.sizeProp] = size + 'px';
  }

  getOffset(index: number): number {

   return Array.from(this.parentRef.children).slice(0, index).reduce((accum: number, current: HTMLElement) => {
        const propOffsetName = this.getCompletePropName(this.offsetProp === 'left' ? 'width' : 'height' ); 
  
        accum += current[propOffsetName];
  
        return accum;

    }, 0);
  }

  getCompletePropName(prop: 'height' | 'width'): 'offsetHeight' | 'offsetWidth' {
    return `offset${prop[0].toUpperCase() + prop.substr(1)}` as 'offsetHeight' | 'offsetWidth';
  }
}
