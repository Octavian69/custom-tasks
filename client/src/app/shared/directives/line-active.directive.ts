import { Directive, ElementRef, Renderer2, OnInit, AfterViewInit, Input, Output, EventEmitter, HostListener, OnDestroy } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { TPageNames, TPosition, CssSize } from 'src/app/types/shared.types';
import { LineOffsetSide, LineOffsetProp, LineAction } from 'src/app/types/line.directive.types';
import { TEditPageName } from 'src/app/types/edit.types';

@Directive({
  selector: '[cstRowActive]',
  exportAs: 'lineActiveRef'
})
export class LineActiveDirective implements OnInit, AfterViewInit, OnDestroy {
  
  currentIdx: number;
  hoverIdx: number;
  line$: HTMLDivElement;
  parent$: HTMLElement;
  childrens$: Element[];
  mutation$: MutationObserver;
  offsetPosition: LineOffsetSide;
  offsetProp: LineOffsetProp;
  sizeProp: CssSize;
  
  @Input() pageName: TPageNames | TEditPageName;
  @Input() position: TPosition = 'bottom';
  @Input() isInitializeEmit: boolean = true;
  @Input() mutationObserve: boolean = false;
  @Input() lineClass: string = 'line-active-directive-page-menu';
  @Output('page') _page: EventEmitter<number> = new EventEmitter();

  @HostListener('click', ['$event'])
    click(e: MouseEvent): void {
      this.parentEvent(e, 'click');
    }

  @HostListener('mousemove', ['$event'])
    move(e: MouseEvent): void {
      this.parentEvent(e, 'move');
    }

  @HostListener('mouseleave')
      leave(): void {
        this.hoverIdx = this.currentIdx;
        this.setOffset(this.currentIdx);
      }
  

  constructor(
    private elem$: ElementRef,
    private renderer: Renderer2,
    private storage: StorageService
  ) { }
  

  ngOnInit(): void {
    this.initBeforeView();
  }

  ngAfterViewInit() {
    this.initAfterView();
    this.initMutationObserver();
  }

  ngOnDestroy() {
    this.destroy();
  }

  initBeforeView(): void {
    this.initProps();
    this.createLine();
    this.currentIdx = this.hoverIdx = this.storage.getItem('sessionStorage', this.pageName) || 0;
    this.storage.setItem('sessionStorage', this.pageName, this.currentIdx);
    
    if(this.isInitializeEmit) {
      this._page.emit(this.currentIdx);
    }
  }

  initAfterView(): void {
    this.parent$ = this.elem$.nativeElement;
    this.childrens$ = this.getChildrens();
    this.renderer.appendChild(this.parent$, this.line$);
    this.renderer.setStyle(this.parent$, 'position', 'relative');

    setTimeout(_ => this.setOffset(this.currentIdx), 0);
  }

  initMutationObserver(): void {
    if(this.mutationObserve) {
      this.mutation$ = new MutationObserver((...args) => {
        this.setChildrens();
      });

      this.mutation$.observe(this.parent$, { childList: true })
    }
  }

  initProps(): void {
    const isUpDown: boolean = this.position === 'top' || this.position === 'bottom';

    this.offsetPosition = isUpDown ? 'left' : 'top';
    this.sizeProp = isUpDown ? 'width' : 'height';
    this.offsetProp = isUpDown ? 'offsetLeft' : 'offsetTop';
  }

  getChildrens(): Element[] {
    return Array.from(this.parent$.children).filter(el$ => el$ !== this.line$);
  }

  setChildrens(): void {
    this.childrens$ = this.getChildrens();
  }

  createLine(): void {
    this.line$ = document.createElement('div');
    this.renderer.addClass(this.line$, 'line-active-directive');
    this.renderer.addClass(this.line$, this.lineClass);
    this.renderer.setStyle(this.line$, this.position, '0');
  }

  getChildIdx(e: MouseEvent): number {
    const childIdx: number = this.childrens$.findIndex(child => {
      return (e as any).path.includes(child);
    });

    return childIdx;
  }


  parentEvent(e: MouseEvent, action: LineAction) {
    const idx: number = this.getChildIdx(e);

    if(~idx) {

      switch(action) {
        case 'click': {
          if(this.currentIdx !== idx) {
            this.setCurrentIdx(idx);
          } 
          
          this.page(this.currentIdx);

          break;
        }

        case 'move': {
          const isCurrent = this.hoverIdx == idx;
          
          if(!isCurrent) {
            this.hoverIdx = idx;
            this.setOffset(idx);
          }

          break;
        }
      }
    }
  }

  setCurrentIdx(idx: number): void {
    if(~idx) {
      this.currentIdx = this.hoverIdx = idx;
      this.setOffset(idx)
    }
  }

  getCurrentIdx(): number {
    return this.currentIdx;
  }

  isCurrent(idx: number): boolean {
    return this.currentIdx === idx;
  }

  setOffset(childIdx: number): void {
    const elem$: Element = this.childrens$[childIdx];
    const position: string = elem$[this.offsetProp] + 'px';
    const size: string = getComputedStyle(elem$).getPropertyValue(this.sizeProp);

    this.renderer.setStyle(this.line$, this.offsetPosition, position);
    this.renderer.setStyle(this.line$, this.sizeProp, size);
  }

  page(childIdx: number): void {
    this._page.emit(childIdx);
    this.storage.setItem('sessionStorage', this.pageName, childIdx);
  }

  destroy(): void {
    if(this.mutationObserve) {
      this.mutation$.disconnect();
    }

    this.storage.removeItem('sessionStorage', this.pageName);
  }
}
