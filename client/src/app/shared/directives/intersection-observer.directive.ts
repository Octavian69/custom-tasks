import { Directive, EventEmitter, Output, ElementRef, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[cstIntersectionObserver]'
})
export class IntersectionObserverDirective implements OnInit, OnDestroy {

  observer: IntersectionObserver;

  @Output('action') action = new EventEmitter<void>();

  constructor(
    private elem$: ElementRef
  ) { }

  ngOnInit() {
    this.initObserver();
  }

  ngOnDestroy() {
    this.observer.disconnect();
  }

  initObserver(): void {
    const { nativeElement } = this.elem$;
    
    this.observer = new IntersectionObserver(this.observerCb, { threshold: 0.1 });

    this.observer.observe(nativeElement);
  }

  observerCb = (entries: IntersectionObserverEntry[]): void => {
    const { intersectionRatio }  = entries[0];

    if(intersectionRatio > 0) {
      this.action.emit();
    }
  }

}
