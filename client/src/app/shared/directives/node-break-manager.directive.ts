import { Directive, Output, EventEmitter, HostListener, ElementRef, Input } from '@angular/core';
import { isEnd } from '../handlers/handlers';

@Directive({
  selector: '[cstNodeBreakManager]'
})
export class NodeBreakManagerDirective {

  @Input('disable') disable: boolean;
  @Input('threshold') threshold: 'start' | 'end' = 'end';
  @Output('ending') ending = new EventEmitter<void>();

  constructor(
    private elem$: ElementRef
  ) { }


  @HostListener('scroll', ['$event'])
  scroll(e: Event): void {

    if(this.disable) return;

    const { nativeElement } = this.elem$;

    if(this.threshold === 'end') {
      const isEndPage: boolean = isEnd(nativeElement);

      if(isEndPage) {
        this.ending.emit()
      }
    } else {
      
      const isStartPage: boolean = nativeElement.scrollTop === 0;

      if(isStartPage) {
        this.ending.emit()
      }
    }
  }

}
