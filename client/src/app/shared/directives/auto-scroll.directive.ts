import { Directive, ElementRef, AfterViewInit } from '@angular/core';
import ResizeObserver from 'resize-observer-polyfill';

@Directive({
  selector: '[cstAutoScroll]'
})
export class AutoScrollDirective implements AfterViewInit {

  observer: MutationObserver;
  resizeObserver: ResizeObserver

  constructor(
    private elem$: ElementRef
  ) { }

  ngOnDestroy() {
    // this.observer.disconnect();
    this.resizeObserver.disconnect()
  }

  ngAfterViewInit() {
    // this.initObserver();
    this.initResizeObserver()
  }

  initResizeObserver(): void {
    this.resizeObserver = new ResizeObserver(this.resize)
  }

  initObserver(): void {
    const { nativeElement } = this.elem$;

    this.observer = new MutationObserver(this.observerCb);
    this.observer.observe(nativeElement, { childList: true });
  }

  resize = (entries: ResizeObserverEntry[]) => {
    const { nativeElement } = this.elem$;

    nativeElement.scrollTop = nativeElement.scrollHeight;
  
  }

  observerCb = (mutations: MutationRecord[]) => {
      const { nativeElement } = this.elem$;
      const { scrollTop, scrollHeight, clientHeight } = nativeElement;

      const offset: number = scrollHeight - clientHeight;

      if(scrollTop < offset) {
        nativeElement.scrollTop = offset;
      }
      
  }

}
