import { Directive, ElementRef, OnInit, OnDestroy, Output, EventEmitter, HostListener } from '@angular/core';
import ResizeObserver from 'resize-observer-polyfill';

@Directive({
  selector: '[cstResizeObserver]'
})
export class ResizeObserverDirective implements OnInit, OnDestroy {
  
  @HostListener('window:resize', ['$event']) windowResize(e) { }
  @Output('resize') _resize = new EventEmitter<DOMRectReadOnly>()

  observer: ResizeObserver;

  constructor(
    private elem$: ElementRef
  ) { }

  ngOnInit() {
    const { nativeElement } = this.elem$;
    this.observer = new ResizeObserver(this.resize.bind(this));

    this.observer.observe(nativeElement);
  }

  ngOnDestroy() {
    this.observer.disconnect();
  }

  resize(entries: ResizeObserverEntry[]) {
    const [ params ] = entries;
    const { contentRect } = params;
    
    this._resize.emit(contentRect as DOMRectReadOnly);
  }

}
