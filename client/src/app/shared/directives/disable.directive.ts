import { Directive, Optional, Self, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[disableControl]'
})
export class DisableDirective {

  @Input('disableControl') set disable(value: boolean) {
    const action: string = value ? 'disable' : 'enable';
    const { control } = this.control;

    control[action]();
  }  

  constructor(
    @Optional()
    @Self()
    private control: NgControl
  ) { }

}
