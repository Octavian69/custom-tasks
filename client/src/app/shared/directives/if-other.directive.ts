import { Directive, TemplateRef, Input, ViewContainerRef } from '@angular/core';
import { ViewWorker } from '../classes/view.worker';

@Directive({
  selector: '[ifOther]'
})
export class IfOtherDirective extends ViewWorker<null> {

  @Input('ifOther') set ifOther(userId: string) {
      if(userId) this.initContainer(userId)
  }

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) { 
    super(null);
  }
  
  initContainer(userId: string) {
    const isOwn: boolean = this.isOwn(userId);

    if(!isOwn) {
      this.viewContainer.createEmbeddedView(this.templateRef)
    } else {
      this.viewContainer.clear();
    }
  }
}
