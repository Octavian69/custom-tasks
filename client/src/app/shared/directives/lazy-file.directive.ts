import { Directive, ElementRef, Input, Renderer2, OnInit, OnDestroy, HostBinding } from '@angular/core';
import { environment } from '@env/environment';

@Directive({
  selector: '[cstLazyFile]'
})
export class LazyFileDirective implements OnInit, OnDestroy {

  observer: IntersectionObserver;
  filepath: string

  @Input('cstLazyFile') set _filepath(value: string) {
    
    const isExistsPath: boolean = !!this.filepath;
    this.filepath = value;

    if(isExistsPath) this.setSource();

  };
  @HostBinding('style.visibility') visiblility: 'hidden' | 'visible'  = 'hidden'; 

  constructor(
    private elem$: ElementRef,
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    this.initObserver();
  }

  ngOnDestroy() {
    this.observer.disconnect();
  }

  initObserver(): void {
    const { nativeElement } = this.elem$;

    this.observer = new IntersectionObserver(this.observeCb, { threshold: 0.1 });

    this.observer.observe(nativeElement);
  }

  setSource(): void {
    const { nativeElement } = this.elem$;
    const path: string = `${environment.FILE_URL}${this.filepath}`;

    this.renderer.setAttribute(nativeElement, 'src', path);
    this.visiblility = 'visible';
  }
  
  observeCb = (entries: IntersectionObserverEntry[]): void => {
      const { intersectionRatio } = entries[0];
      const { nativeElement } = this.elem$;

      if(intersectionRatio > 0 && !nativeElement.src) {
        this.setSource();
      }
  }

}
