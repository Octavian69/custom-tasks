import { Directive, ElementRef, HostListener, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[cstInsideClick]'
})
export class InsideClickDirective {

  @HostListener('window:click', ['$event'])
    click(e: any) {
      const { nativeElement } = this.elem$;
      const isInside: boolean = e.path.includes(nativeElement);
      
      this.inside.emit(isInside);
    }

  @Output('inside') inside = new EventEmitter<boolean>()

  constructor(
    private elem$: ElementRef
  ) { }

}
