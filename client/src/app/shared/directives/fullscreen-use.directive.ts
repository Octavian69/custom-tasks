import { Directive, ElementRef, Output, EventEmitter, OnInit, OnDestroy, HostListener } from '@angular/core';

@Directive({
  selector: '[cstFullscreenUse]',
  exportAs: 'fullScreenRef'
})
export class FullscreenUseDirective implements OnInit, OnDestroy {

  onfullscreenchange: () => void = this.onFullScreenChange.bind(this);

  @HostListener('window:keyup.tab', ['$event'])                
    set(e: KeyboardEvent) {
      this.setFullscreen();
    }

  @Output('fullscreen') fullscreen = new EventEmitter<boolean>();

  constructor(
    private elem$: ElementRef
  ) { }

  ngOnInit() {
    this.fullscreen.emit(this.getFullscreenStatus());
    this.listen('init');
  }

  ngOnDestroy() {
    this.listen('destroy');
  }

  listen(action: 'init' | 'destroy') {
    const { nativeElement } = this.elem$;

    const method = action === 'init' ? 'addEventListener' : 'removeEventListener';

    nativeElement[method]("webkitfullscreenchange", this.onfullscreenchange);
    nativeElement[method]("mozfullscreenchange",    this.onfullscreenchange);
    nativeElement[method]("fullscreenchange",       this.onfullscreenchange);
  }

  getFullscreenStatus(): boolean {
    const docElem$: any = document;

    const status = 
    docElem$.fullscreenElement || 
    docElem$.mozFullscreenElement || 
    docElem$.webkitFullscreenElement;


    return Boolean(status);
  }

  onFullScreenChange(e: Event): void {
    const isFullscreen: any = this.getFullscreenStatus();
    
    this.fullscreen.emit(isFullscreen);
  }

  setFullscreen(): void {
    const isFullscreen: boolean = this.getFullscreenStatus();

    const method = isFullscreen ? 'cancelFullScreen' : 'requestFullScreen';

    this[method]();
  }

  requestFullScreen(): void {
    const { nativeElement } = this.elem$;

    if(nativeElement.requestFullScreen) {
      nativeElement.requestFullScreen();
    } else if(nativeElement.mozRequestFullScreen) {
      nativeElement.mozRequestFullScreen();
    } else if(nativeElement.webkitRequestFullScreen) {
      nativeElement.webkitRequestFullScreen();
    }

  }

  cancelFullScreen(): void {
    const docElem$: any = document;

    if(docElem$.cancelFullScreen) {
      docElem$.cancelFullScreen();
    } else if(docElem$.mozCancelFullScreen) {
      docElem$.mozCancelFullScreen();
    } else if(docElem$.webkitCancelFullScreen) {
      docElem$.webkitCancelFullScreen();
    }
  }

}
