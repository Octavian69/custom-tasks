import { AsyncValidatorFn, AbstractControl } from '@angular/forms';
import { Observable, BehaviorSubject} from 'rxjs';
import { switchMap, debounceTime, map, first } from 'rxjs/operators';
import { ValidationError } from 'src/app/types/validation.types';

export class ValidationMananger {
    static asyncValidator<T>(obsCb: (v: any) => Observable<T>, validationCb: (value: T) => ValidationError, time: number = 0): AsyncValidatorFn  {
        const subject$: BehaviorSubject<any> = new BehaviorSubject(null);
        const validator$ = subject$.asObservable().pipe(
            debounceTime(time),
            switchMap((value: any) => {
                return obsCb(value)
            }),
            map((value: T) => {
                return validationCb(value)
            }),
            first()
        )

        return (formControl: AbstractControl) => {
            const { value } = formControl;
            subject$.next(value);

            return validator$;
        }
    }
}