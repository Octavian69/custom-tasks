import { JwtHelperService } from '@auth0/angular-jwt';

export class AuthenticateWorker {
    jwtService: JwtHelperService = new JwtHelperService();

    updateTokens(access_token: string, refresh_token: string): void {
        window.sessionStorage.setItem('user', access_token);
        window.sessionStorage.setItem('refresh_token', refresh_token);
    }

    getDecodeToken(): any {
        const token = window.sessionStorage.getItem('user');

        return this.jwtService.decodeToken(token);
    }

    isOwn(id: string): boolean {
        const { _id: OwnId } = this.getDecodeToken();

        return Object.is(OwnId, id);
    }

    getOwnId(): string {
        const { _id } = this.getDecodeToken();
        
        return _id;
    }

    expiredToken(): boolean {
        const token: string = window.sessionStorage.getItem('user');

        return !!token && this.jwtService.isTokenExpired(token);
    }

}