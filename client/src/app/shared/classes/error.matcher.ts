import { ErrorStateMatcher } from '@angular/material/core';
import { FormControl, FormGroupDirective, NgForm, NgControl } from '@angular/forms';

export class ErrorMatcher implements ErrorStateMatcher {
    
 
    constructor(
        private control: NgControl,
        private condition: 'standard' | 'date'
    ) {}

    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {

        switch(this.condition) {
            case 'date': {
                return this.control.invalid;
            }
            default: {
                return this.control.invalid && this.control.dirty;
            }
        } 
    }
}