import { Observable, Subject, BehaviorSubject, ReplaySubject } from 'rxjs';
import { AuthenticateWorker } from './authenticate.worker';
import { VALIDATOR_LENGTH } from 'src/app/namespaces/Namespaces';
import SharedDb from 'src/app/db/shared.db';
import { getDataByArrayKeys, mapIsOwnItem, mapIsOwnRows } from '../handlers/handlers';
import { MediaList } from 'src/app/models/MediaList';
import { Length, FormErrorSettings, FormObjectErrors, ControlError } from 'src/app/types/validation.types';



export class ViewWorker<T> extends AuthenticateWorker {
    unsubscriber$: Subject<boolean> = new Subject();

    constructor(
        protected flags: T,
        protected database?: any
    ) {
        super();
    }

    getField<T>(key: string): T {
        return this[key];
    }

    getFlag(key: string): any {
        return this.flags[key];
    }

    getFlagObject(): T {
        return this.flags;
    }

    getStream<T>(name: string): Observable<T> {
        return (<Subject<T> | BehaviorSubject<T> | ReplaySubject<T>>this[name]).asObservable();
    }

    getStreamValue<T>(streamName: string): T {

        return (this[streamName] as BehaviorSubject<T>).getValue();
    }

    defaultFlagObject(flags: T): void {
        Object.entries(flags).forEach(([key, value]) => this.flags[key] = value);
    }

    setField<T>(key: string, value: T): void {
        this[key] = value;
    }

    setFlag(key: string, value: boolean | string | string[]): void {
        this.flags[key] = value;
    }

    emitStream<T>(streamName: string, payload: T): void {
        this[streamName].next(payload);
    }

    mapIsOwnItem<R extends { IsOwn: boolean }>(item: R, key: string = 'User'): R {
        return mapIsOwnItem(item, key);
    }

    mapIsOwnRows<T extends { IsOwn: boolean }>(mediaList: MediaList<T>, key: string = 'User') {
        return mapIsOwnRows(mediaList, key);
    }

    getValidatorLength(formName: string): Length {
        return Object.assign({}, VALIDATOR_LENGTH[formName]);
    }
    
    getDataByArrayKeys(keys: string[] = [], database?: any) {
        return getDataByArrayKeys.apply(this, arguments);
    }

    completeDbErrors(path: string[], formName?: string): FormObjectErrors {
        const formErrors: FormErrorSettings = this.getDataByArrayKeys(path);

        const defaultFormErrors = this.getDataByArrayKeys(['shared', 'defaultFormErrors'], SharedDb);
        const customFormErrors = this.getDataByArrayKeys(['shared', 'customFormErrors'], SharedDb);

        return Object.keys(formErrors).reduce((accumulator: FormObjectErrors, key: string) => {
            
            const errors: ControlError[] = formErrors[key].map((error: FormErrorSettings) => {
                const { type, errorName, replace, replaceValue } = error;
                
                let message: string = type === 'default' ? defaultFormErrors[errorName] : customFormErrors[formName][key][errorName];
                
                if(replace) message = message.replace('{{replace}}', replaceValue);

                return { errorName, message };
            })

            accumulator[key] = errors;

            return accumulator;
        }, {});
    }

    destroySubjects(subNames: string[]): void {
        subNames.forEach(sub => this[sub].next(null))
    }

    destroyFields(fileds: string[]): void {
        fileds.forEach(key => this[key] = null);
    }

    unsubscribe(): void {
        this.unsubscriber$.next(true);
    }

}