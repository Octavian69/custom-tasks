// Modules
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { QuillModule, QuillConfig } from 'ngx-quill';

//db
import SharedDB from '../../db/shared.db';

//handlers 
import { getDataByArrayKeys } from '../handlers/handlers';

//Services
import { CommentViewService } from 'src/app/services/comment.view.service';
import { CommentService } from 'src/app/services/comment.service'; 

//Components
import { AddMediaComponent } from '../components/add-media/add-media.component';
import { AddMediaBtnComponent } from '../components/add-media-btn/add-media-btn.component';
import { AudioSongComponent } from '../../system/audio-page/audio-song/audio-song.component';
import { BordersComponent } from '../components/borders/borders.component';
import { ConfirmComponent } from '../components/confirm/confirm.component';
import { CommentComponent } from '../components/comment/comment.component';
import { HeadboardComponent } from '../components/headboard/headboard.component';
import { HeadbordLogoutComponent } from '../components/headbord-logout/headbord-logout.component';
import { HeadboardAudioComponent } from '../components/headboard-audio/headboard-audio.component';
import { UserNavComponent } from '../components/user-nav/user-nav.component';
import { UserUnitComponent } from '../components/user-unit/user-unit.component';
import { PictureSliderComponent } from '../../system/picture-page/picture-slider/picture-slider.component';
import { PictureUnitComponent } from 'src/app/system/picture-page/picture-unit/picture-unit.component';
import { PictureDetailComponent } from 'src/app/system/picture-page/picture-detail/picture-detail.component';
import { PictureDetailMenuComponent } from 'src/app/system/picture-page/picture-detail-menu/picture-detail-menu.component';
import { PreloaderCircleComponent } from '../components/preloader-circle/preloader-circle.component';
import { PictureAvatarCreateComponent } from 'src/app/system/picture-page/picture-avatar-create/picture-avatar-create.component';
import { PopupComponent } from '../components/popup/popup.component';
import { PreloaderComponent } from '../components/preloader/preloader.component';
import { LikeComponent } from '../components/like/like.component';
import { MediaNavComponent } from '../components/media-nav/media-nav.component';
import { MessageModalComponent } from '../../system/message-page/message-modal/message-modal.component';
import { MessagePopupsComponent } from 'src/app/system/message-page/message-popups/message-popups.component';
import { SliderArrowComponent } from '../components/slider-arrow/slider-arrow.component';
import { SliderCommentsComponent } from '../components/slider-comments/slider-comments.component';
import { ShadowComponent } from '../components/shadow/shadow.component';
import { SaveFileComponent } from '../components/save-file/save-file.component';
import { SliderStatsComponent } from '../components/slider-stats/slider-stats.component';
import { FormDateComponent } from '../components/form-date/form-date.component';
import { FormTextareaComponent } from '../components/form-textarea/form-textarea.component';
import { FormInputComponent } from '../components/form-input/form-input.component';
import { FormSelectComponent } from '../components/form-select/form-select.component';
import { VideoSliderMultiAdditionUnitComponent } from 'src/app/system/video-page/video-slider-multi-addition-unit/video-slider-multi-addition-unit.component';
import { VideoSliderMultiAdditionComponent } from 'src/app/system/video-page/video-slider-multi-addition/video-slider-multi-addition.component';
import { VideoSliderComponent } from 'src/app/system/video-page/video-slider/video-slider.component';
import { VideoSliderInfoComponent } from 'src/app/system/video-page/video-slider-info/video-slider-info.component';
import { VideoSliderUnitComponent } from 'src/app/system/video-page/video-slider-unit/video-slider-unit.component';
import { VideoSliderPlaybackComponent } from 'src/app/system/video-page/video-slider-playback/video-slider-playback.component';
import { IntersectionObserverComponent } from '../components/intersection-observer/intersection-observer.component';
import { InstantEditControlComponent } from '../components/instant-edit-control/instant-edit-control.component';


//Directives
import { IfOwnDirective } from '../directives/if-own.directive';
import { IfOtherDirective } from '../directives/if-other.directive';
import { PrintDirective } from '../directives/print.directive';
import { DisableDirective } from '../directives/disable.directive';
import { LazyFileDirective } from '../directives/lazy-file.directive';
import { ActiveLineDirective } from '../directives/active-line.directive';
import { AutoScrollDirective } from '../directives/auto-scroll.directive';
import { PageBreakManagerDirective } from '../directives/page-break-manager.directive';
import { NodeBreakManagerDirective } from '../directives/node-break-manager.directive';
import { IntersectionObserverDirective } from '../directives/intersection-observer.directive';
import { ResizeObserverDirective } from '../directives/resize-observer.directive';
import { FullscreenUseDirective } from '../directives/fullscreen-use.directive';
import { UserNavigateDirective } from '../directives/user-navigate.directive';
import { InsideClickDirective } from '../directives/inside-click.directive';
import { BeforeDateDirective } from '../directives/before-date.directive';
import { LineActiveDirective } from '../directives/line-active.directive';

//Pipes
import { MomentPipe } from '../pipes/moment.pipe';
import { UrlSanitaiserPipe } from '../pipes/url-sanitaiser.pipe';
import { PathManagerPipe } from '../pipes/path-manager.pipe';
import { BufferConverterPipe } from '../pipes/buffer-converter.pipe';
import { DurationConverterPipe } from '../pipes/duration-converter.pipe';
import { SliceStringPipe } from '../pipes/slice-string.pipe';
import { HtmlSanitizerPipe } from '../pipes/html-sanitizer.pipe';




const declarations = [
    AudioSongComponent,
    VideoSliderComponent,
    VideoSliderInfoComponent,
    VideoSliderPlaybackComponent,
    VideoSliderUnitComponent,
    VideoSliderMultiAdditionComponent, 
    VideoSliderMultiAdditionUnitComponent,
    PictureSliderComponent,
    PictureUnitComponent,
    PictureDetailComponent,
    PictureDetailMenuComponent,
    PictureAvatarCreateComponent,
    HeadboardComponent,
    HeadbordLogoutComponent,
    HeadboardAudioComponent,
    MediaNavComponent,
    MessageModalComponent,
    MessagePopupsComponent,
    UserNavComponent,
    UserUnitComponent,
    PreloaderComponent,
    PreloaderCircleComponent,
    PopupComponent,
    ConfirmComponent,
    ShadowComponent,
    CommentComponent,
    SliderArrowComponent,
    SliderCommentsComponent,
    SliderStatsComponent,
    SaveFileComponent,
    FormSelectComponent,
    FormInputComponent,
    FormTextareaComponent,
    FormDateComponent,
    IntersectionObserverComponent,
    InstantEditControlComponent,
    LikeComponent,
    AddMediaComponent,
    AddMediaBtnComponent,
    BordersComponent,
    IfOwnDirective,
    IfOtherDirective,
    PrintDirective,
    DisableDirective,
    LazyFileDirective,
    ActiveLineDirective,
    AutoScrollDirective,
    PageBreakManagerDirective,
    NodeBreakManagerDirective,
    IntersectionObserverDirective,
    ResizeObserverDirective,
    FullscreenUseDirective,
    UserNavigateDirective,
    InsideClickDirective,
    BeforeDateDirective,
    LineActiveDirective,
    MomentPipe,
    UrlSanitaiserPipe,
    PathManagerPipe,
    BufferConverterPipe,
    DurationConverterPipe,
    SliceStringPipe,
    HtmlSanitizerPipe
];

const imports = [
    CommonModule,
    MaterialModule,
    FormsModule,
    QuillModule
]

const providers = [
    // CommentService,
    // CommentViewService
]

const exports = [
    ...declarations,
    ...imports,
    ReactiveFormsModule
]

const quillConfig: QuillConfig = {
    modules: {
        toolbar: getDataByArrayKeys(['shared', 'quill', 'config'], SharedDB)
    }
};

@NgModule({
    declarations,
    imports: [
        ...imports,
         ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}), 
         QuillModule.forRoot(quillConfig)
    ],
    exports,
    providers
})
export class SharedModule {}