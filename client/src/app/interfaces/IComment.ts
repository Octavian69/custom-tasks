export interface IComment {
    RefersID: string;
    Created: Date;
    Text: string;
    User?:string; 
    CreatorName?: string;
    LikesTotal?: number;
    OwnLike?: boolean;
    Avatar?: string;
    _id?: string;
}