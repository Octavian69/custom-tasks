export interface ITask {
    StartDate: Date;
    EndDate: Date;
    Description: string;
    Title: string;
    Completed: boolean;
    User: string;
    _id?: string;
}