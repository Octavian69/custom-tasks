export interface IVideoPlaylist {
    Title: string;
    Description: string;
    User: string;
    Type: 'Default' | 'Created' | 'Added';
    Logo: string;
    CreatorName: string;
    Created: Date;
    Updated: Date;
    VideoTotal?: number;
    LikesTotal?: number;
    IsHave?: boolean;
    OwnLike?: boolean;
    _id?: string;
}