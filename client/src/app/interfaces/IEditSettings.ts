import { InfoControl } from '../models/info/InfoControl';

export interface IEditSettings<T> {
    info: T | T[];
    configuration: InfoControl[];
    formGroup: any;
}