export interface ICreatedPlaylist {
    Title: string;
    IsHave: boolean;
    _id: string;
}