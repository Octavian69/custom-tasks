import { PictureType } from '../types/picture.types';

export interface IPicture {
    Description: string;
    Created: Date;
    Updated: Date;
    Type: PictureType;
    Album?: string;
    User?: string;
    Path?: string;
    LikesTotal?: number;
    IsHaveSaved?: boolean;
    IsHave?: boolean;
    OwnLike?: boolean;
    CreatorName?: string,
    CreatorAvatar?: string,
    CreatorId?: string,
    _id?: string;
}