import { InfoMain } from '../models/info/InfoMain';

export interface IUser {
    Name: string;
    Avatar: string;
    Online?: boolean;
    MainInfo?: InfoMain;
    FriendsTotal?: number;
    AudioTotal?: number;
    VideoTotal?: number;
    PicturesTotal?: number;
    Status?: string;
    IsIncomingRequest?: boolean; // входящая заявка
    IsOutgoingRequest?: boolean; // исходящая заявка
    IsHave?: boolean;
    _id?: string;
}