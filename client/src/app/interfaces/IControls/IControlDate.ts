import { IControlWorkManager } from './IControlWorkManager';
import { IControlKeysManager } from './IControlKeysManager';
import { MatDatepicker } from '@angular/material/datepicker';

export interface IControlDate extends IControlWorkManager, IControlKeysManager<MatDatepicker<any>> {
    min: Date;
    max: Date;
    initDateLocale: () => void;
}