import { IControlWorkManager } from './IControlWorkManager';
import { IControlKeysManager } from './IControlKeysManager';
import { ListOption, AnyOption } from 'src/app/types/shared.types';
import { MatSelect } from '@angular/material/select';

export interface IControlSelect extends IControlWorkManager, IControlKeysManager<MatSelect> {
    options: (ListOption | AnyOption)[];
    placeholder: string;
    bindLabel: string;
    bindValue: string;
    multiple: boolean;
    typeofValue: 'primitive' | 'reference';
    disabledOptFn: (opt: ListOption) => boolean;
}