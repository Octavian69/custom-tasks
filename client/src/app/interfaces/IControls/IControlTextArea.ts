import { IControlWorkManager } from './IControlWorkManager';
import { IControlKeysManager } from './IControlKeysManager';
import { ElementRef } from '@angular/core';

export interface IControlTextArea extends IControlWorkManager, IControlKeysManager<ElementRef> {
    minLength: number;
    maxLength: number;
    rows: number;
}