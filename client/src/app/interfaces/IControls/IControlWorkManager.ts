import { ICssStyles } from '../ICssStyles';
import { ControlError } from 'src/app/types/validation.types';

export interface IControlWorkManager {
    onChange: (v: any) => void;
    onTouched: (e: Event) => void;
    setValue: (value: any) =>  void;
    reset: (e: Event) => void;
    getErrorStyle: (error: ControlError) => ICssStyles
    setInputType?: () => void;
    initialize(): void;
}