import { IControlWorkManager } from './IControlWorkManager';
import { IControlKeysManager } from './IControlKeysManager';
import { MatInput } from '@angular/material/input';

export interface IControlInput extends IControlWorkManager, IControlKeysManager<MatInput> {
    type: string;
    isHiddenIcon: boolean;
    maxLength: number;
    minLength: number;
    trimPattern: string;
}