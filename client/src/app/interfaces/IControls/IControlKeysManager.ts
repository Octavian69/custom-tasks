import { NgControl } from '@angular/forms';
import { ErrorMatcher } from '../../shared/classes/error.matcher';
import { ControlError } from 'src/app/types/validation.types';
import { EventEmitter } from '@angular/core';

export interface IControlKeysManager<T> {
    value: any;
    formControl: NgControl;
    controlRef: T
    errorMatcher: ErrorMatcher;
    disabled: boolean;
    placeholder: string;
    isResetIcon: boolean;
    errorCondition: 'standard' | 'date';
    fontSize: string;
    errors: ControlError[];
    _initialize: EventEmitter<T>
}