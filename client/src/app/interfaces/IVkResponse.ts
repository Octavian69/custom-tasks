import { ListOption } from '../types/shared.types';

export interface IVkResponse {
    response: {
        count?: number,
        items: ListOption[]
    }
}