import { AnyOption } from '../types/shared.types';

export interface IUnseen {
    keys: {
        dialogues: AnyOption
        friendsRequests: AnyOption
    };
    User: string;
}