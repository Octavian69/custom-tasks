export interface IIncompleteUser {
    Name: string;
    Avatar: string;
    _id: string;
}