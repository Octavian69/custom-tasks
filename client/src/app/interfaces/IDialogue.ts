import { IIncompleteUser } from './IIncompleteUser';
import { LastMessage, DialogueType } from '../types/messages.types';

export interface IDialogue {
    Title: string;
    Participants: IIncompleteUser[];
    LastMessage: LastMessage;
    Logo: string;
    Unread: boolean;
    UnreadMessages: number;
    Type: DialogueType;
    Created: Date;
    Updated: Date;
    IsOwn: boolean;
    TotalMessages: number;
    User: string;
    _id: string;
}


