export interface IMultiVideoList {
    _id: string;
    Title: string;
    IsHave: boolean;
}