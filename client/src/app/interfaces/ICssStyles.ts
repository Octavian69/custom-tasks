export interface ICssStyles {
    [key: string]: string | boolean;
}