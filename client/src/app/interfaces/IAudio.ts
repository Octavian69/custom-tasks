export interface IAudio {
    Genre: number;
    Title: string;
    Artist: string;
    Path: string;
    Playlist: string;
    Duration: number;
    Logo: string;
    Created: Date;
    Type: 'Created' | 'Added';
    IsHave?: boolean;
    User?: string;
    _id?: string;
}