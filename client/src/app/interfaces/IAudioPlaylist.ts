export interface IAudioPlaylist {
    Title: string;
    Description: string;
    Logo: string;
    CreatorName: string;
    Created: Date;
    Updated: Date;
    Type: 'Default' | 'Created' | 'Added';
    TotalCount: number;
    OriginalPlaylist: string;
    ListPaths?: string[];
    Creator: string;
    IsHave?: boolean;
    User?: string;
    _id?: string;
}