import { DialogueType } from '../types/messages.types';

export interface IMessage {
    Text: string;
    User: string;
    Dialogue: string;
    Created: Date;
    UserName: string;
    UserAvatar: string;
    DialogueType: DialogueType;
    IsOwn: boolean;
    _id?: string;
}