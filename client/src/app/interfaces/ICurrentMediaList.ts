import { MediaRequest } from '../models/MediaRequest';
import { MediaList } from '../models/MediaList';

export interface ICurrentMediaList<T> {
    _id: string;
    mediaList: MediaList<T>;
    mediaRequest: MediaRequest;
}