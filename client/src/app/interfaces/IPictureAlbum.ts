import { PictureAlbumType } from '../types/picture.types';

export interface IPictureAlbum {
    User: string;
    Type: PictureAlbumType;
    Title: string;
    Description?: string;
    Logo?: string;
    Created?: Date;
    Updated?: Date;
    CreatorName?: string;
    UserAvatar?: string;
    TotalCount?: number;
    _id?: string
}