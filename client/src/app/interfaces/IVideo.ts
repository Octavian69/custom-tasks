export interface IVideo {
    Title: string;
    Description: string;
    Playlist: string;
    User: string;
    Path: string;
    Poster: string;
    Duration: number;
    CreatorName: string;
    Created: Date;
    Updated: Date;
    UserAvatar?: string;
    IsHave?: boolean;
    IsHaveOwn?: boolean;
    LikesTotal?: number;
    OwnLike?: boolean;
    _id?: string;
}