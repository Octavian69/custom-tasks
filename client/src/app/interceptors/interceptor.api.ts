import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, Subject } from 'rxjs';
import { catchError, tap, mergeMap } from 'rxjs/operators';
import { LoginService } from '../services/login.service';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../services/auth.service';
import { SocketService } from '../services/socket.service';
import { Authenticate } from '../types/auth.types';
import { environment } from '@env/environment';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
    
    refreshSub$: Subject<string> = new Subject();
    isRefresh: boolean = false;

    constructor(
        private toastr: ToastrService,
        private loginService: LoginService,
        private authService: AuthService,
        private socketService: SocketService
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const url: string =  req.url.replace(/@/, environment.API_URL + '/api');

        const clonedRequest = req.clone({ url });

        return next.handle(clonedRequest).pipe(
            catchError((err: HttpErrorResponse) => {
            
            let message: string;
            
            if(err.error) message = err.error.message;

            if(err.status === 400) {

                if(!this.isRefresh) {

                    this.isRefresh = true;

                    return this.authService.refreshTokens().pipe(
                        tap( ({ access_token, refresh_token }: Authenticate) => this.authService.updateTokens(access_token, refresh_token) ),
                        mergeMap(( { access_token } ) => {

                            const headers: HttpHeaders = new HttpHeaders({'Authorization':  access_token});

                            this.socketService.setToken(access_token);
                            
                            const refreshRequest = req.clone({ url, headers });
  
                            this.refreshSub$.next(access_token);
                            
                            this.isRefresh = false;

                            return next.handle(refreshRequest);
                        })
                    )

                }

                return this.refreshSub$.pipe(
                    mergeMap((access_token: string) => {
                        const headers: HttpHeaders = new HttpHeaders({'Authorization':  access_token});
                        const refreshRequest = req.clone({ url, headers });

                        return next.handle(refreshRequest);
                    })
                )
            } 

            if(err.status === 401) {
                this.loginService.logOut();
            }

            this.toastr.error(message || String(err), 'Внимание');

            return throwError(err);
        }))
        
    }
}
