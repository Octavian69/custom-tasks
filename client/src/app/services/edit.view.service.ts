import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription, Observable, of } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { ViewWorker } from '../shared/classes/view.worker';
import EditDB from '../db/edit.db';
import { EditService } from './edit.service';
import { InfoControl } from '../models/info/InfoControl';
import { EditVkReqSettings, EditCache } from '../types/edit.types';
import { AccountService } from './account.service';
import { EditSingleState } from '../models/EditSingleState';
import { EditArrayState } from '../models/EditArrayState';
import { VkRequest } from '../models/info/VkRequest';
import { IVkResponse } from '../interfaces/IVkResponse';
import { EditFlags } from '../models/flags/EditFlags';
import { ListOption, AnyOption } from '../types/shared.types';
import { deepCopy } from '../shared/handlers/handlers';
@Injectable()
export class EditViewService extends ViewWorker<EditFlags> {
    
    optionsCache: EditCache = {};
    info: any;
    part: string;
    type: 'Single' | 'Array';
    subs$: Subscription[] = [];
    SingleState: EditSingleState;
    ArrayState: EditArrayState;
    vkParentParams: any = this.getDataByArrayKeys(['edit', 'vkParentParams']);
    stateTypes: any = this.getDataByArrayKeys(['edit', 'stateTypes']);

    removeId: string = null;

    constructor(
        private fb: FormBuilder,
        private toastr: ToastrService,
        private editService: EditService,
        private accountService: AccountService
    ) {
        super(new EditFlags, EditDB);
    }

    async initState<T>(part: string) {
        this.destroy();

        const type: 'Single' | 'Array' = this.stateTypes[part];
        const method: string = `${type}FormInit`;

        this.accountService.fetchPart<T>(part)
        .pipe(takeUntil(this.unsubscriber$))
        .subscribe((info:T) => {
            const options: any = this.getDataByArrayKeys(['edit', 'forms', part]);
        
            this.info = info;
            this.part = part;
            this.type = type;
    
            this[method](info, options);
        });
    }

    SingleFormInit<T>(info: T, options: any): void {
        
        const formControlNames: string[] = Object.keys(options);
        const formGroup: FormGroup = this.completeFormGroup(formControlNames);
        const configuration: any = this.completeConfiguration(options, formControlNames);

        formGroup.patchValue(info);

        this.SingleState = new EditSingleState(formGroup, configuration, this.part);
        this.setFlag('isLoad', true);
    }

    ArrayFormInit<T>(info: T[], options: any): void {
        
        const defaultForm = this.getDataByArrayKeys(['edit', 'defaultForms', this.part]);
        const controlNames: string[] = Object.keys(defaultForm);
        const forms: FormGroup[] = [];
        

        if(info.length > 0) {

            info.forEach((part: T) => {
                const group: FormGroup = this.completeFormGroup(controlNames);
                group.patchValue(part);
                forms.push(group);
            })

        } else {
            const form: FormGroup = this.completeDefaultForm();
            forms.push(form);
        }

        const configurations: InfoControl[][] = forms.map(_ => this.completeConfiguration(options, Object.keys(options)));

        this.ArrayState = new EditArrayState(forms, configurations, this.part);
        this.setFlag('isLoad', true);
    }

    getControlDeps(controlName: string): string[] {
        
        const dependentControls: string[] = this.getDataByArrayKeys(['edit', 'dependendentControls', this.part, controlName]);

        return dependentControls;
    }

    getRequestSettings(controlName: string): EditVkReqSettings {
        const requestSettings = this.getDataByArrayKeys(['edit', 'requestSettings', this.part, controlName]);

        return requestSettings;
    }

    completeVkQuery(form: FormGroup, config: InfoControl): AnyOption {
        const { params, method, parents } = this.getDataByArrayKeys(['edit', 'requestSettings', this.part, config.controlName]);

        let requestStatus: boolean = true;
        
        if(parents) {
            const dependentParams = parents.reduce((params, current) => {
                const key: string = this.vkParentParams[current];
                const {value} = form.get(current);

                if(value) params[key] = value.id;
                else requestStatus = false;
                
                return params;
    
            }, {});

            const query = Object.assign({}, params, dependentParams)

            return { method, params: query, requestStatus };
        }


        return { method, params, requestStatus };
    }

    getOptionsRequest(method, params): Observable<IVkResponse> {
        const url: string = this.completeUrl(method, params);

        const options: IVkResponse = this.optionsCache[url];

        if(options) {
            return of(deepCopy(options));
        }

        return this.editService
        .getSelectOptions(url)
        .pipe(tap((response: IVkResponse) => {
            this.optionsCache[url] = response;
        }))
    }

    getSearchOptions(form: FormGroup, config: InfoControl, value: string): Observable<IVkResponse> {
        const { method, params, requestStatus } = this.completeVkQuery(form, config);
        params.q = value;

        return requestStatus ? this.getOptionsRequest(method, params) : of({ response: { items: [] }});
    }

    addOption(form: FormGroup, config: InfoControl) {
        const value = form.value[config.controlName];

        if(value) {
            const idx: number = config.selectData.findIndex((item: ListOption) => item.id === value.id);

            if(~idx) config.selectData[idx] = value;
            else config.selectData.push(value);
        }
    }

    resetControls(form: FormGroup, controlNames: string[], configuration: InfoControl[]): void {
        controlNames.forEach((name: string) => {
            const config: InfoControl = configuration.find(cfg => cfg.controlName === name);
            config.selectData = [];
            form.get(name).reset();
        })
    }

    completeUrl(method: string, params: any): string {
        const { url } = new VkRequest(method, params);
        return url;
    }

    completeConfiguration(options: any, formControlNames: string[]): InfoControl[] {
       return formControlNames.map((control: string) => {
            const settings: InfoControl = new InfoControl( options[control] );
            
            return settings;
        })
    }

    completeFormGroup(formControlNames: string[]): FormGroup {
       const group: any =  formControlNames.reduce((formGroup: any, controlName: string) => {
            formGroup[controlName] = [null, [], []];

            return formGroup;
        }, {});

        return this.fb.group(group);
    }

    completeDefaultForm(): FormGroup {
        const defaultForm = this.getDataByArrayKeys(['edit', 'defaultForms', this.part]);
        const form: FormGroup = this.completeFormGroup(Object.keys(defaultForm));
        form.patchValue(defaultForm);

        return form;
    }


    addDefaultForm(): void {
        const form: FormGroup = this.completeDefaultForm();
        const options:any = this.getDataByArrayKeys(['edit', 'forms', this.part]);
        const configuration = this.completeConfiguration(options, Object.keys(options));

        this.ArrayState.forms.push(form);
        this.ArrayState.configuration.push(configuration);
    }
    
    showConfirm(removeId: string) {
        this.setField('removeId', removeId);
        this.setFlag('isConfirm', true);
    }
    
    confirmRemove(confirm: boolean) {
        if(confirm) {
            this.remove();
        } else {
            this.resetRemovedFields();
        }
    }

    resetRemovedFields(index?: number): void {
        this.setField('removeId', null)
        this.setFlag('isConfirm', false);

        if(index) {
            this.ArrayState.forms.splice(index, 1);
        }
        
    }

    remove(): void {
        const idxInfo: number = this.info.findIndex(item => item._id === this.removeId);
        const idxForm: number = this.ArrayState.forms.findIndex(item => item.value._id === this.removeId);

        if(~idxInfo) {

            this.setFlag('isRequest', true);

            const next = ({ message }) => {
                
                this.info.splice(idxInfo, 1);
                this.resetRemovedFields(idxForm);
                this.toastr.success(message, 'Успешно!');
                this.setFlag('isRequest', false);
            };

            const errorFn = () => this.setFlag('isRequest', false);

           this.editService.remove(this.removeId, this.part).subscribe({ next })

        } else {
            this.resetRemovedFields(idxForm);
        }
    }

    submit(): void {
        const method: string = `update${this.type}`;

        this[method]();
    }

    updateArray<T>(): void {
        const updateForms: FormGroup[] = this.ArrayState.forms.filter((form: FormGroup) => form.touched);

        if(updateForms.length) {
            this.setFlag('isRequest', true);
            const values: T[] = updateForms.map((form: FormGroup) => form.value);

            const next = ({ message }) => {
                
                updateForms.forEach(form => form.markAsUntouched());

                this.setFlag('isRequest', false);
                this.toastr.success(message, 'Успешно!');
            };

            const error = () => this.setFlag('isRequest', false);

            this.editService.update(values, this.part, this.type).subscribe({ next, error });

        } else {
            this.toastr.info('Значения форм не изменялись.', 'Информация.');
        }
    }

    updateSingle(): void {
        
        if(this.SingleState.form.touched) {

            this.setFlag('isRequest', true);

            const { value } = this.SingleState.form;

            const next = ({ message }) => {
                
                this.toastr.success(message, 'Успешно!');
                this.SingleState.form.markAsUntouched();
                this.setFlag('isRequest', false);
            };

            const error = () => this.setFlag('isRequest', false);
    
            this.editService.update(value, this.part, this.type).subscribe({ next, error });

        } else {
            this.toastr.info('Значения форма не изменялись.', 'Информация.');
        }
    }

    destroy(): void {
        this.setFlag('isLoad', false);
        this.setField('info', null);
        this.unsubscribe();

        if(this.type === 'Single') {
            this.SingleState = new EditSingleState();
        } else {
            this.ArrayState = new EditArrayState();
        };
    }
    
}