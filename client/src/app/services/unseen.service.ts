import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IUnseen } from '../interfaces/IUnseen';
import { Observable } from 'rxjs';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { UnseenKey } from '../types/unseen.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class UnseenService {

    constructor(
        private http: HttpClient
    ){}

    fetch(): Observable<IUnseen> {
        return this.http.get<IUnseen>('@/unseen/fetch');
    }

    update(body: object): Observable<IUnseen> {
        return this.http.patch<IUnseen>('@/unseen/update', body);
    }

    reset(body: { key: UnseenKey }): Observable<IUnseen> {
        return this.http.patch<IUnseen>('@/unseen/reset', body);
    }
}