import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ViewWorker } from '../shared/classes/view.worker';
import FriendsDB from '../db/friends.db';
import { FriendsFlags } from '../models/flags/FriendsFlags';
import { FriendsService } from './friends.service';
import { MediaList } from '../models/MediaList';
import { PaginationRequest } from '../models/PaginationRequest';
import { capitalLetter, deepCopy } from '../shared/handlers/handlers';
import { IUser } from '../interfaces/IUser';
import { tap, map, takeUntil } from 'rxjs/operators';
import { FriendState, FriendsStateAction, FriendsFetch } from '../types/friends.types';
import { SocketService } from './socket.service';
import { SOKET_EVENTS } from '../namespaces/socket-events.namespaces';
import { UserService } from './user.service';
import { filterUnique } from '../shared/handlers/media.handlers';
import { ResponseAction } from '../types/request-response.types';
import { AnyOption } from '../types/shared.types';

@Injectable()
export class FriendsViewService extends ViewWorker<FriendsFlags> {
    
    main$: BehaviorSubject<MediaList<IUser>> = new BehaviorSubject(null);
    search$: BehaviorSubject<MediaList<IUser>> = new BehaviorSubject(null);

    requestArray: string[] = [];

    constructor(
        private friendsService: FriendsService,
        private userService: UserService,
        private socketService: SocketService
    ){
        super(new FriendsFlags, FriendsDB)
    }

    fetch(method: FriendsFetch, request: PaginationRequest, action: ResponseAction): void {
        const { state: { listType } } = request;
        const flagName = `is${capitalLetter(listType)}Request`;
        const listName = `${listType}$` as 'main$' | 'search$';
        const friends: MediaList<IUser> = this.getStreamValue(listName);
        
        this.setFlag(flagName, true);

        const next = (response: MediaList<IUser>) => {

            if(action === 'replace') {
                this[listName].next(response);
            } else {
                const { rows, totalCount } = response;

                friends.rows.push(...rows);
                friends.totalCount = totalCount;

                this[listName].next(friends);
                
            }

            this.setFlag(flagName, false);
        }
        
        this.friendsService[method](request)
            .pipe(
                map((response: MediaList<IUser>) => {
                    return filterUnique(friends, response, action)
                }),
                takeUntil(this.unsubscriber$)
            )
            .subscribe({ next });
    }

    add(userId: string): void {
        this.requestArray.push(userId);

        const next = () => this.filter(userId);

        this.friendsService.add(userId)
        .pipe(
            tap(() => {
                this.emitSocketEvent('addFriend', userId)
            })
        )
        .subscribe({ next });
    }

    remove(userId: string): void {
        this.requestArray.push(userId);

        const next = () => this.filter(userId);

        this.friendsService
        .remove(userId)
        .pipe(
            tap(() => {
                this.emitSocketEvent('removeFriend', userId)
            })
        )
        .subscribe({ next });
    }

    requestToAdd(userId: string): void {
        this.requestArray.push(userId);

        const next = () => this.filter(userId);

        this.friendsService
        .requestToAdd(userId, 'remove')
        .pipe(
            tap(() => {
                this.emitSocketEvent('requestToAddFriend', userId, { status: 'remove' })
            })
        )
        .subscribe({ next });
    }

    rejectToAdd(userId: string): void {
        this.requestArray.push(userId);

        const next = () => this.filter(userId);

        this.friendsService
        .rejectToAdd(userId)
        .pipe(
            tap(() => {
                this.emitSocketEvent('rejectToAddFriend', userId, { status: 'remove' })
            })
        )
        .subscribe({ next });
    }

    emitSocketEvent(action: FriendsStateAction, referenceId: string, state: AnyOption = {}): void {
        const { _id: User, Avatar: UserAvatar, Name: UserName} = this.getDecodeToken();
        const payload: FriendState = { User, UserAvatar, UserName, referenceId, action, state};
        
        this.socketService.emitEvent(SOKET_EVENTS.FRIENDS_CHANGE_STATE, payload);
    }

    getNewFriend(userId: string): void {
        const next = (user: IUser) => {
            const mediaList: MediaList<IUser> = this.getStreamValue('main$');
            mediaList.rows.unshift(user)
            mediaList.totalCount++;

            this.main$.next(mediaList);
        }
        
        this.userService.fetch(userId).subscribe({ next });
    }

    mutateMainDetail(event: FriendState): void {
        const { action, User } = event;
        const mediaList: MediaList<IUser> = this.getStreamValue('main$');
        const idx: number = mediaList.rows.findIndex(u => u._id === User);


        switch(action) {
            case 'addFriend': {
                this.getNewFriend(User)
                return;
            }
            case 'removeFriend': {
                if(~idx) {
                    mediaList.rows.splice(idx, 1);
                }
                mediaList.totalCount--;
                break;
            }
        }

        this.main$.next(mediaList);
    }

    getUserById(userId: string, listName: 'main$' | 'search$' = 'main$'): IUser {
        const mediaList: MediaList<IUser> =  this.getStreamValue(listName);

        if(mediaList) {
            const user: IUser = mediaList.rows.find(u => u._id === userId);

            return user ? deepCopy(user) : null
        }

        return null;
    }

    filter(userId: string) {
        const friends: MediaList<IUser> = this.getStreamValue('main$');
        const userIdx: number = friends.rows.findIndex(u => u._id === userId);
        const reqIdx: number = this.requestArray.indexOf(userId);

        if(~reqIdx) {
            this.requestArray.splice(reqIdx, 1);
        }
        
        if(~userIdx) {
            friends.rows.splice(userIdx, 1);
        }

        friends.totalCount--;

        this.main$.next(friends);
    }

    reset() {
        this.destroySubjects(['main$', 'search$']);
        this.defaultFlagObject(new FriendsFlags);
        this.unsubscribe();
    }
}