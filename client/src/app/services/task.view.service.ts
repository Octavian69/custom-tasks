import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { ViewWorker } from '../shared/classes/view.worker';
import TasksDB from '../db/tasks.db';
import { ITask } from '../interfaces/ITask';
import { Page } from '../models/Page';
import { TaskService } from './task.service';
import { ToastrService } from 'ngx-toastr';
import { Task } from '../models/Task';
import { TaskFilter } from '../models/TaskFilter';
import { TaskFlags } from '../models/flags/TaskFlags';
import { Location } from '@angular/common';
import { Crumb } from '../types/task.types';
import { MessageResponse } from '../types/request-response.types';
import { FormObjectErrors, Length } from '../types/validation.types';
import { AnyOption } from '../types/shared.types';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class TaskViewService extends ViewWorker<TaskFlags> {

    tasks$: BehaviorSubject<ITask[]> = new BehaviorSubject<ITask[]>(null);
    task$: BehaviorSubject<ITask> = new BehaviorSubject<ITask>(null);
    crumbs$: BehaviorSubject<Crumb[]> = new BehaviorSubject<Crumb[]>( [ { Title: 'Список задач' } ]);

    statusList: AnyOption[] = this.getDataByArrayKeys(['tasks', 'statusList']);
    errors: FormObjectErrors = this.completeDbErrors(['tasks', 'formErrors'], 'Tasks');
    lengths: Length = this.getValidatorLength('TASKS');

    page: Page = new Page();
    selected: string[] = [];
    totalCount: number = 0;
    filters: TaskFilter = new TaskFilter();

    constructor(
        private taskService: TaskService,
        private toastr: ToastrService,
        private router: Router,
        private location: Location
    ) {
        super(new TaskFlags, TasksDB);
    }

    fetch(): void {
        const next = ( { rows, totalCount } ) => {
            this.totalCount = totalCount;
            this.tasks$.next(rows);
        }

       this.taskService
           .fetch(this.page, this.filters)
           .pipe(takeUntil(this.unsubscriber$))
           .subscribe({ next });

    }

    fetchById(id: string): void {

        if(id !== '0') {
            const next = (task: ITask) => {
                const { Title } = task;
                this.setCrumbs({Title});
                this.task$.next(task);
            }

            this.taskService
                .fetchById(id)
                .pipe(takeUntil(this.unsubscriber$))
                .subscribe({ next })

        } else {

            const { _id } = this.getDecodeToken();
            const task = new Task(_id);
            const { Title } = task;
            this.setCrumbs({ Title });

            this.task$.next(task);
        }
    }

    create(task: ITask): void {
        
        this.setFlag('isRequest', true);

        const next = (createdTask: ITask) => {

            this.toastr.success(`Задача ${createdTask.Title} создана.`, 'Успешно!');

            this.router.navigate(['/tasks', 'detail', createdTask._id]);
            
            this.setFlag('isRequest', false);
        }

        this.taskService.create(task).subscribe({ next });
    }

    update(task: ITask): void {
        this.setFlag('isRequest', true);

        const next = (updatedTask: ITask) => {

            const { Title } = updatedTask;

            this.toastr.success(`Задача ${Title} отредактированна.`, 'Успешно!');
            this.setCrumbs({ Title }, true);

            this.task$.next(updatedTask);

            this.setFlag('isRequest', false);
        }

        this.taskService.update(task).subscribe({ next });
    }

    remove(): void {
        this.setFlag('isRequest', true);

        const next = (response: MessageResponse) => {

            const titles = [];
            const { message } = response;

            this.tasks$.value.forEach((task: ITask) => {
                const isSelected = this.selected.includes(task._id);

                if(isSelected) titles.push(task.Title);
            });

            this.toastr.success(`Задачи: ${titles} ${message}`, 'Успешно!');

            this.selected = [];

            this.setPage(0, this.page.limit);

            this.setFlag('isRequest', false);
        }

        this.taskService.remove(this.selected).subscribe({ next });

    }

    setPage(skip: number, limit: number): void {

        this.page.skip = skip;
        this.page.limit = limit;
        
        this.fetch();
        this.resetSelected();
    }

    resetSelected() {
        if(this.selected.length) this.selected = [];
    }

    filter(filters: TaskFilter): void {
        this.filters = filters;
        this.setPage(0, this.page.limit);
    }

    setCrumbs(crumb?: Crumb, isUpdate: boolean = false): void {

        const crumbs: Crumb[] = this.crumbs$.getValue();

        switch(true) {
            case isUpdate: {
                crumbs[1] = crumb;
                break;
            }
            case !!crumb: {
                crumbs.push(crumb)
                break;
            }

            default: {
                crumbs.splice(1, 1);
            }
        }

        this.crumbs$.next(crumbs);
    }

    select(id: string): void {

        const idx: number = this.selected.indexOf(id);
        
        if(~idx) {
            this.selected.splice(idx, 1);
        } else {
            this.selected.push(id);
        }
    }

    selectAll(): void {
        const isAll: boolean = this.selected.length === this.tasks$.value.length;

        if(isAll) {
            this.selected = [];
        } else {
            this.selected = this.tasks$.value.map((task: ITask) => task._id);
        }
    }

    destroy(): void {
        const subNames: string[] = ['tasks$'];

        this.selected = [];
        this.filters = new TaskFilter;
        this.flags = new TaskFlags;
        this.destroySubjects(subNames);
        this.unsubscribe();
    }
}