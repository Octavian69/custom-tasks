import { Socket } from 'ngx-socket-io';
import { Injectable } from '@angular/core';
import { Observable, Subscription, Subject } from 'rxjs';
import { ViewWorker } from '../shared/classes/view.worker';
import SocketDB from '../db/socket.db';
import { UserService } from './user.service';
import { switchMap, takeUntil } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SocketService extends ViewWorker<null> {
  
    status$: Subject<boolean> = new Subject();
    subs$: Subscription[] = [];

    constructor(
        private socket: Socket,
        private userService: UserService
    ) {
        super(null, SocketDB)
        this.initStatusSub();
    }


    setToken(token: string) {
        this.socket.ioSocket.io.opts.query = `token=${token}`;
        // this.socket.ioSocket.io.uri = "http://localhost:3001" //new uri
        
        // window.addEventListener('beforeunload', () => {
        //     console.log('beforeunload')
        //   })
    }

    initStatusSub(): void {
        this.status$.pipe(
            switchMap((status: boolean) => this.userService.edit({ Online: status }))
        ).subscribe()
    }

    init() {
        this.reset();
        this.setToken(sessionStorage.getItem('user'));
        this.socket.connect();
        this.status$.next(true);
    }

    reset() {
        if(this.socket && this.socket.ioSocket.connected) {
            this.removeListeners();
            this.socket.disconnect();
            this.status$.next(false);
            this.unsubscribe();
        }
    }

    setOnlineStatus(status: boolean): void {
        const next = () => {};

        this.userService
            .edit({ Online: status })
            .pipe(takeUntil(this.unsubscriber$))
            .subscribe({ next })
    }

    getSocket(): Socket {
        return this.socket;
    }

    getListener(event: string): Observable<any> {
        return this.socket.fromEvent(event);
    }

    emitEvent<T>(event: string, payload: T): any {
        return this.socket.emit(event, payload)
    }

    removeListeners(): void {
        const events: string[] = this.getDataByArrayKeys(['sockets', 'events']);
        events.forEach((event: string) => this.socket.removeAllListeners(event));
    }

}