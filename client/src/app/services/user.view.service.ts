import { ViewWorker } from '../shared/classes/view.worker';
import { Injectable } from '@angular/core';
import { IUser } from '../interfaces/IUser';
import { BehaviorSubject, Subject } from 'rxjs';
import { UserService } from './user.service';
import { IPicture } from '../interfaces/IPicture';
import { createFormData } from '../shared/handlers/handlers';
import { UserFlags } from '../models/flags/UserFlags';
import { SimpleAction } from '../models/SimpleAction';
import { StorageService } from './storage.service';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { AnyOption } from '../types/shared.types';
import { takeUntil } from 'rxjs/operators';

@Injectable({
    providedIn: SystemServicesModule
})
export class UserViewService extends ViewWorker<UserFlags> {

    user$: BehaviorSubject<IUser> = new BehaviorSubject(null);
    mutate$: Subject<SimpleAction<IUser>> = new Subject();

    constructor(
        private storage: StorageService,
        private userService: UserService
    ){ 
        super(new UserFlags);
     }

     init() {
        const ownId: string = this.getOwnId();
        this.setFlag('isFetch', true);

        const next = (user: IUser) => {
            this.storage.setItem('sessionStorage', 'ownUser', user);
            this.user$.next(user);

            this.setFlag('isFetch', false);
        }

        this.userService
                .fetch(ownId)
                .pipe(takeUntil(this.unsubscriber$))
                .subscribe({ next });
     }

     createAvatar(file: File): void {
        this.setFlag('isCreateAvatar', true);

        const formData: FormData = createFormData(file, 'avatar');

        const next = (picture: IPicture) => {
            const user: IUser = this.getStreamValue('user$');
            const { Path } = picture;
            
            user.Avatar = Path;

            const event: SimpleAction<IUser> = new SimpleAction('create-avatar', user);

            this.user$.next(user);
            this.mutate$.next(event);
            this.storage.setItem('sessionStorage', 'ownUser', user);

            this.setFlag('isCreateAvatar', false);
            this.setFlag('isShowCreateAvatar', false);
        }

        this.userService
            .createAvatar(formData)
            .subscribe({ next })
     }

     updateAvatar(picture: IPicture): void {
        this.setFlag('isUpdateAvatar', true);
        
        const next = () => {
            const user: IUser = this.getStreamValue('user$');

            user.Avatar = picture.Path;

            const event: SimpleAction<IUser> = new SimpleAction('update-avatar', user);

            this.user$.next(user);
            this.mutate$.next(event);

            this.storage.setItem('sessionStorage', 'ownUser', user);

            this.setFlag('isUpdateAvatar', false);
        }

        this.userService
                .updateAvatar(picture)
                .subscribe({ next });
     }

     edit(payload: AnyOption): void {
        this.setFlag('isEdit', true);
        
        const next = () => {
            const user: IUser = this.getStreamValue('user$');
            const updateUser: IUser = Object.assign({}, user, payload);
            const event: SimpleAction<IUser> = new SimpleAction('edit', updateUser);

            this.user$.next(updateUser);
            this.mutate$.next(event);

            this.setFlag('isEdit', false);
        };

        this.userService.edit(payload).subscribe({ next });
    }

    destroy(): void {
        this.unsubscribe()
    }
}