import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { ViewWorker } from '../shared/classes/view.worker';
import VideoDB from '../db/video.db';
import { VideoMainFlags } from '../models/flags/VideoMainFlags';
import { IVideo } from '../interfaces/IVideo';
import { MediaList } from '../models/MediaList';
import { VideoService } from './video.service';
import { MediaRequest } from '../models/MediaRequest';
import { StorageService } from './storage.service';
import { SimpleAction } from '../models/SimpleAction';
import { map, takeUntil } from 'rxjs/operators';
import { filterUnique } from '../shared/handlers/media.handlers';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { CreateVideoCandidate } from '../types/video.types';
import { PageLink, ResponseAction } from '../types/request-response.types';
import { Router } from '@angular/router';



@Injectable({
    providedIn: SystemServicesModule
})
export class VideoViewService extends ViewWorker<VideoMainFlags> {
 
    main$: BehaviorSubject<MediaList<IVideo>> = new BehaviorSubject(null);
    search$: BehaviorSubject<MediaList<IVideo>> = new BehaviorSubject(null);

    constructor(
        private videoService: VideoService,
        private storage: StorageService,
        private router: Router
    ) {
        super(new VideoMainFlags, VideoDB)
    }

    fetch(mediaRequest: MediaRequest, action: ResponseAction, reqFlag: string) {
        this.setFlag(reqFlag, true);
        
        const { listType } = mediaRequest;
        const listName = listType === 'main' ? 'main$' : 'search$';
        const list: MediaList<IVideo> = this.getStreamValue(listName);

        const next = (response: MediaList<IVideo>) => {

            if(action === 'replace') {
                this[listName].next(response);
            } else {
                const { rows, totalCount } = response;

                list.rows = list.rows.concat(rows);
                list.totalCount = totalCount;
                
                this[listName].next(list);
            }

            this.setFlag(reqFlag, false);
        }

        this.videoService
            .fetch(mediaRequest)
            .pipe(
                map((response: MediaList<IVideo>) => {
                    return filterUnique(list, response, action)
                }),
                takeUntil(this.unsubscriber$)
            )
            .subscribe({ next });
    }

    create(payload: CreateVideoCandidate): void {
        
        this.setFlag('isCreate', true);

        const { video, file } = payload;
        const formData = new FormData();

        formData.append('candidate', JSON.stringify(video));
        formData.append('video', file, file.name);

        const next = (create: IVideo) => {
            const ownId: string = this.getOwnId();
            const isOwn: boolean = this.router.url.includes(ownId)
            
            if(isOwn) {
                const currentList: MediaList<IVideo> = this.getStreamValue('main$');
                
                currentList.rows.unshift(create);
                currentList.totalCount++;

                this.main$.next(currentList);
            }

            this.setFlag('isCreateVideoModal', false);
            this.setFlag('isCreate', false);
        }

        this.videoService
            .create(formData)
            .subscribe({ next });

    }
    
    add(playlistId: string, video: IVideo, field: 'IsHaveOwn' | 'IsHave' = 'IsHave'): void {
        const next = _ => {
            const update: IVideo = Object.assign({}, video, { [field]: true });
            const event: SimpleAction<IVideo> = new SimpleAction('add', update);

            this.mutate(event);
        }

        this.videoService
            .add(playlistId, video)
            .subscribe({ next });
    }

    remove(playlistId: string, video: IVideo, field: 'IsHaveOwn' | 'IsHave' = 'IsHave'): void {

        const next = _ => {
            const update: IVideo = Object.assign({}, video, { [field]: false });
            const event: SimpleAction<IVideo> = new SimpleAction('remove', update);

            this.mutate(event);
        }

        this.videoService
            .remove(playlistId, video)
            .subscribe({ next });
    }

    isCanMutate(creatorId: string): void {
        const isOwn: boolean = this.isOwn(creatorId);

        this.setFlag('isCanMuatate', isOwn);
    }

    mutate(event: SimpleAction<IVideo>) {

        const { action, payload } = event;

        const isMutate: boolean = this.getFlag('isCanMutate') as boolean;
        const loadingIdx: number = this.flags.mutateRequestArray.findIndex(id => id === payload._id);

        if(~loadingIdx) this.flags.mutateRequestArray.splice(loadingIdx, 1);

        if(isMutate) {

            const currentList: MediaList<IVideo> = this.getStreamValue('main$');

            switch(action) {
                
                case 'add': {
                    currentList.rows.unshift(payload);
                    currentList.totalCount++;

                    break;
                }
    
                case 'remove': {
                    const videoIdx: number = currentList.rows.findIndex(v => v._id === payload._id);
                    currentList.rows.splice(videoIdx, 1);
                    currentList.totalCount--;
                    
                    break;
                }
            }
    
            this.main$.next(currentList);
        }
 
        this.mutateDetail(event, 'main');
        this.mutateDetail(event, 'search');

    }

    mutateDetail(event: SimpleAction<IVideo>, listType: 'main' | 'search'): void {

        const { payload } = event;

        const listName = `${listType}$` as 'main$' | 'search$';
        const mediaList: MediaList<IVideo> = this.getStreamValue(listName);

        if(mediaList) {

            const idx: number = mediaList.rows.findIndex(v => v._id === payload._id);
            
            if(~idx) {
                mediaList.rows[idx] = Object.assign({}, payload);
                this[listName].next(mediaList);
            }       
        }
    }

    
    reset() {
        this.destroySubjects(['main$', 'search$']);
        this.setFlag('isCanMutate', false);
        this.unsubscribe();
    }

    destroy() {
        this.reset();
        this.defaultFlagObject(new VideoMainFlags);
    }

}