import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticateWorker } from '../shared/classes/authenticate.worker';
import { User } from '../models/User';
import { Authenticate, Login, TUserLogin } from '../types/auth.types';
import { MessageResponse } from '../types/request-response.types';

@Injectable()
export class AuthService extends AuthenticateWorker {

    random: number = Math.random();

    constructor(
        private http: HttpClient
    ) {
        super()
    }

    login(login: Login): Observable<Authenticate> {
        return this.http.post<Authenticate>('@/auth/login', login);
    }

    registration(user: User): Observable<MessageResponse> {
       return this.http.post<MessageResponse>('@/auth/registration', user)
    }

    refreshTokens(): Observable<Authenticate> {
        const token = window.sessionStorage.getItem('refresh_token');

        return this.http.post<Authenticate>('@/auth/refresh_token', { token })
    }
 
    getByLogin(Login: string): Observable<TUserLogin> {
        return this.http.post<TUserLogin>('@/auth/user', { Login })
    }  
}