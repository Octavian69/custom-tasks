import { Injectable } from '@angular/core';
import { ViewWorker } from '../shared/classes/view.worker';
import PictureDB from '../db/picture.db';
import { PictureAlbumsFlags } from '../models/flags/PictureAlbumsFlags';
import { BehaviorSubject, Subscription, Subject } from 'rxjs';
import { PictureAlbumsService } from './picture-albums.service';
import { IPictureAlbum } from '../interfaces/IPictureAlbum';
import { PictureAlbum } from '../models/PictureAlbum';
import { createFormData } from '../shared/handlers/handlers';
import { SimpleAction } from '../models/SimpleAction';
import { MediaList } from '../models/MediaList';
import { Page } from '../models/Page';
import { StorageService } from './storage.service';
import { IPicture } from '../interfaces/IPicture';
import { map, takeUntil } from 'rxjs/operators';
import { filterUnique } from '../shared/handlers/media.handlers';
import { ResponseAction } from '../types/request-response.types';


@Injectable()
export class PictureAlbumsViewService extends ViewWorker<PictureAlbumsFlags> {

    album$: BehaviorSubject<IPictureAlbum> = new BehaviorSubject(null);
    albums$: BehaviorSubject<MediaList<IPictureAlbum>> = new BehaviorSubject(null);
    detail$: BehaviorSubject<IPictureAlbum> = new BehaviorSubject(null);

    mutate$: Subject<SimpleAction<IPictureAlbum>> = new Subject();
    
    constructor(
        private albumService: PictureAlbumsService,
        private storage: StorageService
    ) {
        super(new PictureAlbumsFlags, PictureDB)
    }

    fetch(id: string, page: Page, action: ResponseAction): void {
        this.setFlag('isFetch', true);
        const albums: MediaList<IPictureAlbum> = this.getStreamValue('albums$');

        const next = (response: MediaList<IPictureAlbum>) => {

            if(action === 'replace') {
                this.albums$.next(response);
            } else {
                

                const rows: IPictureAlbum[] = albums.rows.concat(response.rows);
                const totalCount: number = response.totalCount;

                this.albums$.next({rows, totalCount});
            }

            this.setFlag('isFetch', false);
        }
        
        this.albumService
                .fetch(id, page)
                .pipe(
                    map((response: MediaList<IPictureAlbum>) => {
                        return filterUnique(albums, response, action)
                    }),
                    takeUntil(this.unsubscriber$)
                )
                .subscribe({ next });

    }

    fetchAlbum(id: string): void {
        if(id === '0') {
            const ownId: string = this.getOwnId();
            const album: IPictureAlbum = new PictureAlbum(ownId);

            this.album$.next(album);
        } else {
            
            const next = (album: IPictureAlbum) => {
                this.album$.next(album);
            }

            this.albumService
                .fetchById(id)
                .pipe(takeUntil(this.unsubscriber$))
                .subscribe({ next });
        }
    }

    initDetail(albumId: string): void {
        const next = (album: IPictureAlbum) => {
            this.detail$.next(album);
        }

        this.albumService
            .fetchById(albumId)
            .pipe(takeUntil(this.unsubscriber$))
            .subscribe({ next });
    }

    create(album: IPictureAlbum, file: File): void {
        this.setFlag('isRequest', true);

        const data: FormData | IPictureAlbum = file ? createFormData(file, 'logo', album) : album;

        const next = (create: IPictureAlbum) => {
            const event: SimpleAction<IPictureAlbum> = new SimpleAction('create', create);
            this.mutate(event)
            this.mutate$.next(event);
            this.setFlag('isRequest', false);
        }

        this.albumService
            .create(data)
            .subscribe({ next })

    }

    edit(album: IPictureAlbum, file: File): void {
        this.setFlag('isRequest', true);

        const data: FormData | IPictureAlbum = file ? createFormData(file, 'logo', album) : album;

        const next = (edit: IPictureAlbum) => {
            const event: SimpleAction<IPictureAlbum> = new SimpleAction('edit', edit);

            this.mutate(event)
            this.mutate$.next(event);
            this.setFlag('isRequest', false);
        }

        this.albumService
            .edit(album._id, data)
            .subscribe({ next })
    }

    remove(): void {
        this.setFlag('isRequest', true);

        const album = this.getStreamValue('album$') as IPictureAlbum;

        const next = () => {
            const event: SimpleAction<IPictureAlbum> = new SimpleAction('remove', album);

            this.mutate(event);
            this.mutate$.next(event);

            this.setFlag('isRequest', false);
        }

        this.albumService
            .remove(album._id, album)
            .subscribe({ next });
    }

    mutate(event: SimpleAction<IPictureAlbum>): void {
        const isCanMutate: boolean = this.getFlag('isCanMutate') as boolean;

        if(isCanMutate) {
            const { action, payload } = event;
            const albums: MediaList<IPictureAlbum> = this.getStreamValue('albums$');

            switch(action) {
                case 'create': {
                    albums.rows.unshift(payload);
                    albums.totalCount++;
                    break;
                }

                case 'edit': {
                    const idx: number = albums.rows.findIndex(album => album._id === payload._id);

                    if(~idx) albums.rows[idx] = payload;
                    break;
                }

                case 'remove': {
                    const idx: number = albums.rows.findIndex(album => album._id === payload._id);

                    if(~idx) {
                        albums.rows.splice(idx, 1);
                        albums.totalCount--;
                    }    

                    this.album$.next(null);

                    break;
                }
            }

            this.albums$.next(albums);
        }
    }

    mutateDetail(event: SimpleAction<IPicture | IPicture[] | string[]>): void {
        
        const mediaList: MediaList<IPictureAlbum> = this.getStreamValue('albums$');
        const detail: IPictureAlbum = this.getStreamValue('detail$');

        const { action, payload } = event;

        let update: IPictureAlbum;

        switch(action) {

            case 'create': {
                const pictures: IPicture[] = payload as IPicture[];
                const TotalCount = detail.TotalCount + pictures.length;
                update = Object.assign({}, detail, { TotalCount });

                break;
            }

            case 'replace': {
                const pictureIds: string[] = payload as string[];
                const TotalCount = detail.TotalCount - pictureIds.length
                update = Object.assign({}, detail, { TotalCount });

                break;
            }

            case 'remove': {
                const pictureIds: string[] = payload as string[];
                const TotalCount = detail.TotalCount - pictureIds.length
                update = Object.assign({}, detail, { TotalCount });
        

                break;
            }
        }
        

        this.detail$.next(update);
        
        this.storage.setItem('sessionStorage', 'picture-detail-album', update);

        if(mediaList) {
            const idx: number = mediaList.rows.findIndex(al => al._id === detail._id);

            if(~idx) mediaList.rows[idx] = Object.assign({}, update);

            this.albums$.next(mediaList);
        }
        
    }

    destroy(): void {
        this.destroySubjects(['album$', 'albums$']);
        this.defaultFlagObject(new PictureAlbumsFlags);
        this.unsubscribe();
    }
}