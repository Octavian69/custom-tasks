import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TotalStats } from '../models/TotalStats';
import { Observable } from 'rxjs';
import { SystemServicesModule } from '../shared/modules/system.services.module';

@Injectable({
    providedIn: SystemServicesModule
})
export class TotalStatsService {
    
    constructor(
        private http: HttpClient
    ) {}

    fetch(id: string): Observable<TotalStats> {
       return this.http.get<TotalStats>(`@/stats/${id}`);
    }
}