import { Like } from '../models/Like';
import { Injectable } from '@angular/core';
import { LikeService } from './like.service';
import { ViewWorker } from '../shared/classes/view.worker';
import { LikeFlags } from '../models/flags/LikeFlags';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { LikeType } from '../types/like.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class LikeViewService extends ViewWorker<LikeFlags> {
    constructor(
        private likeService: LikeService
    ) {
        super(new LikeFlags);
    }

    mutate(payload: any): any {
        const update: any = Object.assign({}, payload);
        const { OwnLike, LikesTotal } = update;

        (update as any).OwnLike = !OwnLike;
        (update as any).LikesTotal = OwnLike ? LikesTotal - 1 : LikesTotal + 1;

        return update;
    }


    like<T>(payload: T, type: LikeType): void {
        this.setFlag('isRequest', true);

        const { _id, OwnLike } = payload as any;

        const like: Like = new Like(_id, type, OwnLike);

        const next = _ => this.setFlag('isRequest', false);

        this.likeService.like(like).subscribe({ next });
    }
} 