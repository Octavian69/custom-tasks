import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IPicture } from '../interfaces/IPicture';
import { Page } from '../models/Page';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { PaginationResponse, MessageResponse } from '../types/request-response.types';
import { AnyOption } from '../types/shared.types';
import { PictureType, PictureAlbumType } from '../types/picture.types';


@Injectable({
    providedIn: SystemServicesModule
})
export class PictureService {

    constructor(
        private http: HttpClient
    ){}

    fetch(albumId: string, page: Page): Observable<PaginationResponse<IPicture>> {
        return this.http.post<PaginationResponse<IPicture>>(`@/pictures/fetch/${albumId}`, page);
    }

    fetchByType(userId: string, page: Page, Type: PictureType = 'Created') {
        const fromObject: AnyOption = { Type };
        const params: HttpParams = new HttpParams({fromObject});

        return this.http.post(`@/pictures/fetch-by-type/${userId}`, page, {params});
    }

    fetchBySlider(albumId: string, page: Page, albumType: PictureAlbumType): Observable<IPicture[]> {
        const body = { page, albumType };

        return this.http.post<IPicture[]>(`@/pictures/fetch-by-slider/${albumId}`, body);
    }

    fetchSlide(pictureId: string): Observable<IPicture> {
        return this.http.get<IPicture>(`@/pictures/fetch-slide/${pictureId}`);
    }

    create(albumId: string, formData: FormData): Observable<IPicture[]> {
        return this.http.post<IPicture[]>(`@/pictures/create-picture/${albumId}`, formData);
    }

    save(picture: IPicture): Observable<MessageResponse> {
        return this.http.post<MessageResponse>(`@/pictures/save-picture`, picture);
    }

    edit(pictureId: string, update: AnyOption): Observable<MessageResponse> {
        return this.http.patch<MessageResponse>(`@/pictures/edit-picture/${pictureId}`, update);
    }

    replace(toAlbumId: string, fromAlbumId: string, pictureIds: string[], picturePaths: string[]): Observable<MessageResponse> {
        const fromObject = { fromAlbumId };    
        const params: HttpParams = new HttpParams({ fromObject });

        return this.http.patch<MessageResponse>(`@/pictures/replace-picture/${toAlbumId}`, { picturePaths, pictureIds }, { params });
    }

    remove(albumId: string, pictureIds: string[], picturePaths: string[]): Observable<MessageResponse> {
        const headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
        const httpOptions = {
            headers,
            body: {pictureIds, picturePaths}
        };

        return this.http.delete<MessageResponse>(`@/pictures/remove-picture/${albumId}`, httpOptions);
    }
}