import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IPictureAlbum } from '../interfaces/IPictureAlbum';
import { Page } from '../models/Page';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { PaginationResponse, MessageResponse } from '../types/request-response.types';
import { PictureReplaceAlbum } from '../types/picture.types';
import { MediaList } from '../models/MediaList';

@Injectable({
    providedIn: SystemServicesModule
})
export class PictureAlbumsService {
    
    constructor(
        private http: HttpClient
    ) {}

    fetch(id: string, page: Page): Observable<PaginationResponse<IPictureAlbum>> {
        return this.http.post<PaginationResponse<IPictureAlbum>>(`@/pictures/fetch-albums/${id}`, page);
    }

    fetchReplaceAlbums(excludeAlbumId: string, page: Page): Observable<MediaList<PictureReplaceAlbum>> {
        return this.http.post<MediaList<PictureReplaceAlbum>>(`@/pictures/fetch-replace-albums/${excludeAlbumId}`, page);
    }

    fetchById(id: string): Observable<IPictureAlbum> {
        return this.http.get<IPictureAlbum>(`@/pictures/get-album-by-id/${id}`);
    }

    create(body: FormData | IPictureAlbum): Observable<IPictureAlbum> {
        return this.http.post<IPictureAlbum>(`@/pictures/create-album`, body)
    }

    edit(id: string, body: FormData | IPictureAlbum): Observable<IPictureAlbum> {
       return this.http.patch<IPictureAlbum>(`@/pictures/edit-album/${id}`, body)
    }

    remove(albumId: string, album: IPictureAlbum): Observable<MessageResponse> {
        const headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
        const httpOptions = { headers, body: album };

        return this.http.delete<MessageResponse>(`@/pictures/remove-album/${albumId}`, httpOptions);
    }
}