import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IDialogue } from '../interfaces/IDialogue';
import { Observable } from 'rxjs';
import { PaginationRequest } from '../models/PaginationRequest';
import { Dialogue } from '../models/Dialogue';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { PaginationResponse } from '../types/request-response.types';
import { CreateDiscussion, NewDiscussion, DialogueType } from '../types/messages.types';
import { AnyOption } from '../types/shared.types';
@Injectable({
    providedIn: SystemServicesModule
})
export class DialoguesService {

    constructor(
        private http: HttpClient
    ){}

    fetch(request: PaginationRequest): Observable<PaginationResponse<IDialogue>> {
        return this.http.post<PaginationResponse<IDialogue>>('@/messages/fetch-dialogues', request)
    }

    fetchByParticipants(usersIds: string[], Type: DialogueType): Observable<IDialogue> {
        const fromObject: AnyOption = { Type };
        const params: HttpParams = new HttpParams({ fromObject });

        return this.http.post<IDialogue>(`@/messages/fetch-by-participants`, usersIds, { params });
    }

    fecthById(dialogueId: string): Observable<IDialogue> {
        return this.http.get<IDialogue>(`@/messages/get-dialogue-by-id/${dialogueId}`);
    }

    createDiscussion(body: FormData | CreateDiscussion): Observable<NewDiscussion> {
        return this.http.post<NewDiscussion>(`@/messages/create-discussion`, body);
    }

    createDialogue(dialogue: Dialogue, userId: string): Observable<IDialogue> {
        return this.http.post<IDialogue>(`@/messages/create-dialogue/${userId}`, dialogue);
    }

}