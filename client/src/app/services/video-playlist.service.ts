import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs'; 
import { MediaRequest } from '../models/MediaRequest';
import { IVideoPlaylist } from '../interfaces/IVideoPlaylist';
import { Page } from '../models/Page';
import { IMultiVideoList } from '../interfaces/IMultiVideoList';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { AnyOption } from '../types/shared.types';
import { MessageResponse, PaginationResponse } from '../types/request-response.types';


@Injectable({
    providedIn: SystemServicesModule
})
export class VideoPlaylistService {
    
    constructor(
        private http: HttpClient
    ) {}
    
    fetch(mediaRequest: MediaRequest): Observable<PaginationResponse<IVideoPlaylist>>  {
        const { playlistId: UserId } = mediaRequest;

        return this.http.post<PaginationResponse<IVideoPlaylist>>(`@/video/fetch-playlists/${UserId}`, mediaRequest);
    }

    fetchByCreator(body: Page, video_id: string): Observable<PaginationResponse<IMultiVideoList>> {
        const fromObject: AnyOption = {video_id};
        const params = new HttpParams({ fromObject });
        
        return this.http.post<PaginationResponse<IMultiVideoList>>(`@/video/get-playlists-by-creator`, body, {params});
    }

    fetchById(playlistId: string): Observable<IVideoPlaylist> {
        return this.http.get<IVideoPlaylist>(`@/video/get-playlist-by-id/${playlistId}`);
    }

    create(data: FormData | IVideoPlaylist): Observable<IVideoPlaylist> {
        return this.http.post<IVideoPlaylist>(`@/video/create-playlist`, data);
    }

    edit(playlistId: string, payload: AnyOption): Observable<IVideoPlaylist> {
        return this.http.patch<IVideoPlaylist>(`@/video/edit-playlist/${playlistId}`, payload);
    }

    add(playlistId: string): Observable<MessageResponse> {
        return this.http.patch<MessageResponse>(`@/video/add-playlist/${playlistId}`, {});
    }

    remove(playlistId: string): Observable<MessageResponse> {
        return this.http.delete<MessageResponse>(`@/video/remove-playlist/${playlistId}`);
    }

    updateLogo(palylistId: string, formData: FormData): Observable<AnyOption> {
        return this.http.patch(`@/video/update-playlist-logo/${palylistId}`, formData);
    }
}