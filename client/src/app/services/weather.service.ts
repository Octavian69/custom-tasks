import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Coordinate } from '../types/shared.types';


@Injectable({
    providedIn: 'root'
})
export class WeatherService {
    constructor(
        private http: HttpClient
    ){}

    fetch(coordinate: Coordinate): Observable<any> {
        
        const { latitude, longitude } = coordinate;
        // const fromObject = {
        //     key: 'c5d6924c93b04fb595b173552191107',
        //     q: `${latitude},${longitude}`,
        //     lang: 'ru'
        // }
        const fromObject = {
            access_key: 'd439caf4f2dcdef3eda34509b6a4ec44',
            query: `${latitude},${longitude}`,
            language: 'ru'
        }
        const params: HttpParams = new HttpParams({ fromObject });

        // return this.http.get(`${environment.HEROKU_CORS_URL}http://api.apixu.com/v1/current.json`, { params });
        return this.http.get(`http://api.weatherstack.com/current`, { params });
    }
}