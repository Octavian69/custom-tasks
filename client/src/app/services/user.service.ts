import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ViewWorker } from '../shared/classes/view.worker';
import { IUser } from '../interfaces/IUser';
import { IPicture } from '../interfaces/IPicture';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { MessageResponse } from '../types/request-response.types';
import { AnyOption } from '../types/shared.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class UserService extends ViewWorker<null> {
    
    constructor(
        private http: HttpClient
    ){
        super(null);
    }

    fetch(id: string): Observable<IUser> {
        return this.http.get<IUser>(`@/users/get-by-id/${id}`);
    }

    createAvatar(formData: FormData): Observable<IPicture> {
        return this.http.post<IPicture>(`@/users/create-avatar`, formData);
    }

    updateAvatar(picture: IPicture): Observable<MessageResponse> {
        return this.http.patch<MessageResponse>(`@/users/update-avatar`, picture);
    }

    edit(payload: AnyOption): Observable<MessageResponse> {
        return this.http.patch<MessageResponse>(`@/users/edit`, payload);
    }
}