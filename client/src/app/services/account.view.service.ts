import { Injectable } from '@angular/core';
import { ViewWorker } from '../shared/classes/view.worker';
import AccountDB from '../db/account.db';
import { AccountService } from './account.service';
import { Info } from '../models/Info';
import { Subscription, BehaviorSubject } from 'rxjs';
import { IPicture } from '../interfaces/IPicture';
import { PicturePage } from '../models/PicturePage';
import { AccountFlags } from '../models/flags/AccountFlags';
import { IUser } from '../interfaces/IUser';
import { UserService } from './user.service';
import { FriendsService } from './friends.service';
import { FriendState, FriendsStateAction, PreviewFriends } from '../types/friends.types';
import { SocketService } from './socket.service';
import { tap, takeUntil } from 'rxjs/operators';
import { SOKET_EVENTS } from '../namespaces/socket-events.namespaces';
import { MediaList } from '../models/MediaList';
import { IIncompleteUser } from '../interfaces/IIncompleteUser';
import { SimpleAction } from '../models/SimpleAction';
import { Page } from '../models/Page';
import { PaginationRequest } from '../models/PaginationRequest';
import { LIMIT } from '../namespaces/Namespaces';
import { AnyOption } from '../types/shared.types';

@Injectable()
export class AccountViewService extends ViewWorker<AccountFlags> {
    
    picturePage: PicturePage = new PicturePage('Avatar');
    slides: IPicture[] = [];
    slideIndex: number = 0;
    currentSlideCount: number = 1;

    info$: BehaviorSubject<Info> = new BehaviorSubject<Info>(null);
    user$: BehaviorSubject<IUser> = new BehaviorSubject<IUser>(null);
    previewFriends$: BehaviorSubject<MediaList<IIncompleteUser>> = new BehaviorSubject<MediaList<IIncompleteUser>>(null);
    previewOnlineFriends$: BehaviorSubject<MediaList<IIncompleteUser>> = new BehaviorSubject<MediaList<IIncompleteUser>>(null);
    onlineFriends$: BehaviorSubject<MediaList<IUser>> = new BehaviorSubject<MediaList<IUser>>(null);
    subs$: Subscription[] = [];

    constructor(
        private accountService: AccountService,
        private userService: UserService,
        private friendsService: FriendsService,
        private socketService: SocketService
    ){
        super( new AccountFlags(), AccountDB);
    }

    fetch(id: string): void {
        this.fetchInfo(id);
        this.fetchUser(id);
        this.fetchIncompleteUsers(id, 'all');
        this.fetchIncompleteUsers(id, 'online');
    }

    fetchInfo(id: string): void {
        const next = (info: Info) => {
            this.info$.next(info);
        }

       this.accountService
        .fetch(id)
        .pipe(takeUntil(this.unsubscriber$))
        .subscribe({ next });
    }

    fetchUser(userId: string): void {
        this.setFlag('isFetchUser', true);

        const next = (user: IUser) => {
            this.user$.next(user);

            this.setFlag('isFetchUser', false);
        }

        this.userService.fetch(userId)
        .pipe(takeUntil(this.unsubscriber$))
        .subscribe({ next });
    }

    fetchIncompleteUsers(id: string, listType: PreviewFriends) {
        const page: Page = new Page(0, LIMIT.USERS_PREVIEW);
        const request: PaginationRequest = new PaginationRequest(id, page);

        if(listType === 'online') {
            request.filters.Online = true;
        }

        const next = (response: MediaList<IIncompleteUser>) => {
            const listName = listType === 'all' ? 'previewFriends$' : 'previewOnlineFriends$';

            this[listName].next(response);

        }

        this.friendsService
        .fetchIncomplete(request)
        .pipe(takeUntil(this.unsubscriber$))
        .subscribe({ next });
    }

    fetchOnlineUsers(event: SimpleAction<PaginationRequest>) {
        this.setFlag('isFetchOnlineUsers', true);

        const { action, payload } = event;

        const next = (response: MediaList<IUser>) => {

            if(action === 'replace') {
                this.onlineFriends$.next(response);
            } else {
                const friends: MediaList<IUser> = this.getStreamValue('onlineFriends$');
                const rows: IUser[] = friends.rows.concat(response.rows);
                const {totalCount} = response;

                this.onlineFriends$.next({ rows, totalCount });
            }

            this.setFlag('isFetchOnlineUsers', false);
        }

        this.friendsService
        .fetch(payload)
        .pipe(takeUntil(this.unsubscriber$))
        .subscribe({ next });
    }

    requestToAdd(): void {
        this.setFlag('isFriendsRequest', true);

        const user: IUser = this.getStreamValue('user$');
        const action: 'add' | 'remove' = user.IsOutgoingRequest ? 'remove' : 'add';

        const next = () => {
            user.IsOutgoingRequest = !user.IsOutgoingRequest;

            this.user$.next(user);

            this.setFlag('isFriendsRequest', false);
        };

        this.friendsService
            .requestToAdd(user._id, action)
            .pipe(
                tap(() => {
                    this.emitSocketEvent('requestToAddFriend', user._id, { status: action })
                }),
                takeUntil(this.unsubscriber$)
            )
            .subscribe({ next });
    }

    rejectFriend(): void {
        this.setFlag('isFriendsRequest', true);

        const user: IUser = this.getStreamValue('user$');

        const next = () => {
            user.IsIncomingRequest = false;

            this.user$.next(user);

            this.setFlag('isFriendsRequest', false);
        };

        const sub$: Subscription = this.friendsService
            .rejectToAdd(user._id)
            .pipe(
                tap(() => {
                    this.emitSocketEvent('rejectToAddFriend', user._id)
                }),
                takeUntil(this.unsubscriber$)
            )
            .subscribe({ next });
    }

    removeFriend(): void {
        this.setFlag('isFriendsRequest', true);

        const user: IUser = this.getStreamValue('user$');

        const next = () => {
            user.IsHave = false;
            user.FriendsTotal--;
            user.IsOutgoingRequest = false;

            this.user$.next(user);

            this.setFlag('isFriendsRequest', false);
        };

        const sub$: Subscription = this.friendsService
            .remove(user._id)
            .pipe(
                tap(() => {
                    this.emitSocketEvent('removeFriend', user._id)
                }),
                takeUntil(this.unsubscriber$)
            )
            .subscribe({ next });
    }

    addFriend(): void {
        this.setFlag('isFriendsRequest', true);

        const user: IUser = this.getStreamValue('user$');

        const next = () => {
            user.IsHave = true;
            user.FriendsTotal++;
            user.IsIncomingRequest = false;

            this.user$.next(user);

            this.setFlag('isFriendsRequest', false);
        };

        const sub$: Subscription = this.friendsService
            .add(user._id)
            .pipe(
                tap(() => {
                    this.emitSocketEvent('addFriend', user._id)
                }),
                takeUntil(this.unsubscriber$)
            )
            .subscribe({ next });
    }

    emitSocketEvent(action: FriendsStateAction, referenceId: string, state: AnyOption = {}): void {
        const { _id: User, Avatar: UserAvatar, Name: UserName} = this.getDecodeToken();
        const payload: FriendState = { User, UserAvatar, UserName, referenceId, action, state};
        
        this.socketService.emitEvent(SOKET_EVENTS.FRIENDS_CHANGE_STATE, payload);
    }

    mutateDetail(event: FriendState): void {
        const { action } = event;

        const user: IUser = this.getStreamValue('user$');

        switch(action) {
            case 'addFriend': {
                user.IsHave = true;
                user.FriendsTotal++;
                user.IsIncomingRequest = false;
                break;
            }
            case 'removeFriend': {
                user.IsHave = false;
                user.FriendsTotal--;
                user.IsIncomingRequest = false;
                user.IsOutgoingRequest = false;
                break;
            }
            case 'requestToAddFriend': {
                user.IsIncomingRequest = !user.IsIncomingRequest;
                break;
            }
            case 'rejectToAddFriend': {
                user.IsOutgoingRequest = false;
                break;
            }
        }

        this.user$.next(user);
    }

    destroy() {
        const subNames: string[] = [
            'info$', 
            'user$',
            'previewFriends$',
            'previewOnlineFriends$',
            'onlineFriends$'
        ];

        this.destroySubjects(subNames);
        this.defaultFlagObject(new AccountFlags);
        this.unsubscribe();
    }
}