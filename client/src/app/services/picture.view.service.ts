                                        import { Injectable } from '@angular/core';
import { ViewWorker } from '../shared/classes/view.worker';
import PictureDB from '../db/picture.db'
import { PictureFlags } from '../models/flags/PictureFlags';
import { BehaviorSubject, Subscription, Subject } from 'rxjs';
import { MediaList } from '../models/MediaList';
import { IPicture } from '../interfaces/IPicture';
import { Page } from '../models/Page';
import { PictureService } from './picture.service';
import { completeArrayFormData } from '../shared/handlers/handlers';
import { SimpleAction } from '../models/SimpleAction';
import { map, takeUntil } from 'rxjs/operators';
import { filterUnique } from '../shared/handlers/media.handlers';
import { PictureType } from '../types/picture.types';
import { ResponseAction } from '../types/request-response.types';



@Injectable()
export class PictureViewService extends ViewWorker<PictureFlags> {
    
    pictures$: BehaviorSubject<MediaList<IPicture>> = new BehaviorSubject(null);
    mutate$: Subject<SimpleAction<IPicture | IPicture[] | string[]>> = new Subject();
   
    constructor(
        private pictureService: PictureService
    ) {
        super(new PictureFlags, PictureDB)
    }

    fetch(id: string, page: Page, action: ResponseAction, type?: PictureType) {
        this.setFlag('isFetch', true);

        const pictures: MediaList<IPicture> = this.getStreamValue('pictures$');

        const next = (response: MediaList<IPicture>) => {
            
            if(action === 'replace') {
                this.pictures$.next(response);
            } else {
                const rows: IPicture[] = pictures.rows.concat(response.rows);
                const totalCount = response.totalCount;

                this.pictures$.next({ rows, totalCount });
            }

            this.setFlag('isFetch', false);
        }
        
        const method: 'fetch' | 'fetchByType' = type ? 'fetchByType' : 'fetch';

        this.pictureService
                [method](id, page, type)
                .pipe(
                    map((response: MediaList<IPicture>) => {
                        return filterUnique(pictures, response, action)
                    }),
                    takeUntil(this.unsubscriber$)
                )
                .subscribe({ next });
    }

    create(albumId: string, files: File[]) {
        this.setFlag('isMutateRequest', true);

        const formData: FormData = completeArrayFormData(files, 'pictures');

        const next = (pictures: IPicture[]) => {
            const event: SimpleAction<IPicture[]>= new SimpleAction('create', pictures);

            this.mutate(event);
            this.mutate$.next(event);

            this.setFlag('isShowCreateModal', false);
            this.setFlag('isMutateRequest', false);
        }

        this.pictureService
            .create(albumId, formData)
            .subscribe({ next });
    }

    replace(toAlbumId: string, fromAlbumId: string, pictureIds: string[], picturePaths: string[]): void {
        this.setFlag('isMutateRequest', true);

        const next = _ => {
            const event: SimpleAction<string[]>= new SimpleAction('replace', pictureIds);

            
            this.mutate(event);
            this.mutate$.next(event);
            
            this.setFlag('multiRequestArray', []);
            this.setFlag('isShowReplaceModal', false);
            this.setFlag('isMutateRequest', false);
        }

        this.pictureService
            .replace(toAlbumId, fromAlbumId, pictureIds, picturePaths)
            .subscribe({ next });
    }

    remove(albumId: string, pictureIds: string[], picturePaths: string[]): void {
        this.setFlag('isMutateRequest', true);

        const next = _ => {
            const event: SimpleAction<string[]>= new SimpleAction('remove', pictureIds);

            this.mutate(event);
            this.mutate$.next(event);
            
            this.setFlag('multiRequestArray', []);
            this.setFlag('isMutateRequest', false);
        }

        this.pictureService
            .remove(albumId, pictureIds, picturePaths)
            .subscribe({ next });
    }

    mutate(event: SimpleAction<IPicture | IPicture[] | string[]>): void {
        const { action, payload } = event;

        const mediaList: MediaList<IPicture> = this.getStreamValue('pictures$');

        if(mediaList) {
            
            switch(action) {

                case 'create': {
                    const pictures: IPicture[] = payload as IPicture[];

                    mediaList.rows.unshift(...pictures);
                    mediaList.totalCount += pictures.length;

                    break;
                }
                case 'save': {
                    const picture: IPicture = payload as IPicture;
                    const idx: number = mediaList.rows.findIndex(p => p._id === picture._id);
                    
                    if(~idx) {
                        mediaList.rows[idx] = Object.assign({}, picture);
                    }

                    break;
                }
                
                case 'replace': {
                    const pictureIds: string[] = payload as string[];
                    mediaList.rows = mediaList.rows.filter(p => !pictureIds.includes(p._id) );
                    mediaList.totalCount -= pictureIds.length;

                    break;
                }

                case 'remove': {
                    const pictureIds: string[] = payload as string[];
                    mediaList.rows = mediaList.rows.filter(p => !pictureIds.includes(p._id) );
                    mediaList.totalCount -= pictureIds.length;

                    break;
                }

                case 'edit': {
                    const picture: IPicture = payload as IPicture;
                    const idx: number = mediaList.rows.findIndex(p => p._id === picture._id);
                    
                    if(~idx) {
                        mediaList.rows[idx] = Object.assign({}, picture);
                    }

                    break;
                }

                case 'like': {
                    const picture: IPicture = payload as IPicture;
                    const idx: number = mediaList.rows.findIndex(p => p._id === picture._id);
                    
                    if(~idx) {
                        mediaList.rows[idx] = Object.assign({}, picture);
                    }

                    break;
                }
            }

            this.pictures$.next(mediaList);
        }
    }

    destroy(): void {
        this.destroySubjects(['pictures$']);
        this.defaultFlagObject(new PictureFlags);
        this.unsubscribe()
    }
}