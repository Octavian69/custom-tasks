import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IAudioPlaylist } from '../interfaces/IAudioPlaylist';
import { Observable } from 'rxjs';
import { Page } from '../models/Page';
import { IAudio } from '../interfaces/IAudio';
import { ICreatedPlaylist } from '../interfaces/ICreatorAudioList';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { Injectable } from '@angular/core';
import { PaginationResponse, MessageResponse } from '../types/request-response.types';
import { CreateAudioPlaylist } from '../types/audio.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class AudioPlaylistsService {
    constructor(
        private http: HttpClient
    ) {}

    fetch(userId: string, page: Page): Observable<PaginationResponse<IAudioPlaylist>> {
        return this.http.post<PaginationResponse<IAudioPlaylist>>(`@/audio/fetch-playlists/${userId}`, page);
    }

    create(create: CreateAudioPlaylist | FormData): Observable<IAudioPlaylist> {
        return this.http.post<IAudioPlaylist>('@/audio/create-playlist', create);
    }

    getById(id: string): Observable<IAudioPlaylist>{
        return this.http.get<IAudioPlaylist>(`@/audio/get-by-playlist/${id}`);
    }

    getCreatedByUser(audio: IAudio, page: Page): Observable<PaginationResponse<ICreatedPlaylist>> {
        const body = { audio, page };

        return this.http.post<PaginationResponse<ICreatedPlaylist>>(`@/audio/get-created-by-user`, body);
    }

    edit(body: IAudioPlaylist | FormData): Observable<IAudioPlaylist> {
        return this.http.patch<IAudioPlaylist>('@/audio/edit-playlist', body);
    }

    add(playlistId: string): Observable<MessageResponse> {
        return this.http.patch<MessageResponse>(`@/audio/add-playlist/${playlistId}`, null);
    }

    remove(playlist: IAudioPlaylist): Observable<MessageResponse> {
        const headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
        const httpOptions = { headers, body: playlist };

        return this.http.delete<MessageResponse>(`@/audio/remove-playlist/${playlist._id}`, httpOptions);
    }
}