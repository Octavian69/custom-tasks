import { BehaviorSubject, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { ViewWorker } from '../shared/classes/view.worker';
import AudioDB from '../db/audio.db';
import { AudioService } from './audio.service';
import { Audio } from '../models/Audio';
import { completeFormData } from '../shared/handlers/handlers';
import { AudioFlags } from '../models/flags/AudioFlags';
import { IAudio } from '../interfaces/IAudio';
import { AudioWorkService } from './audio.work.service';
import { Location } from '@angular/common';
import { MessageResponse } from '../types/request-response.types';
import { UpdateMediaList } from '../types/media.types';
import { CreateAudioCandidate } from '../types/audio.types';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { StorageService } from './storage.service';

export class AudioViewService extends ViewWorker<AudioFlags> {
    
    ownList$: Subject<UpdateMediaList<IAudio>> = new Subject<UpdateMediaList<IAudio>>();
    create$: Subject<IAudio> = new Subject<IAudio>();
    audio$: BehaviorSubject<IAudio> = new BehaviorSubject<IAudio>(null);
    activeAudio: IAudio;

    constructor(
        private toastr: ToastrService,
        private router: Router,
        private location: Location,
        private audioService: AudioService,
        private audioWorker: AudioWorkService,
        private storage: StorageService
    ) {
        super(new AudioFlags, AudioDB);
    }

    init(audioId: string): void {
        if(audioId !== '0') {

            const next = (audio: IAudio) => {
                this.audio$.next(audio);
            }

            this.audioService.getById(audioId)
            .pipe(takeUntil(this.unsubscriber$))
            .subscribe({ next });

        } else {
            const audio: IAudio = new Audio();
            this.audio$.next(audio);
            
        }
    }

    isCanMuatate(): boolean {
        const ownId: string = this.getOwnId();
        const audioPageIdx: number = this.storage.getItem('sessionStorage', 'audioPage');
        return this.router.url.includes(ownId) && audioPageIdx === 0;
    }

    createAudio(candidate: CreateAudioCandidate): void {
        
        this.setFlag('isRequest', true);

        const { audio, file } = candidate;

        const formData: FormData = completeFormData(file, 'audio', audio);

        const error = () => this.setFlag('isRequest', false);

        const next = (audio: Audio) => {
            const isCanMuatate: boolean = this.isCanMuatate();
            

            if(isCanMuatate) {
                this.create$.next(audio);
            }

            const { Artist, Title } = audio;
            
            this.location.back();
            this.audio$.next(null);
            this.toastr.success(`Композиция: ${Artist}-${Title} добавлена.`, 'Успешно!');
            this.setFlag('isRequest', false);
        }

        this.audioService.create(formData).subscribe({ next, error });
    }

    editAudio(value: IAudio): void {
        this.setFlag('isRequset', true);
        
        const current: IAudio = this.audio$.getValue();
        const update: IAudio = Object.assign({}, current, value);

        const next = _ => {

            const isCanMuatate: boolean = this.isCanMuatate();

            if(isCanMuatate) {
                this.ownList$.next({ action: 'edit', media: update });
             }
            
            const { Artist, Title } = update;

            this.toastr.success(`Композиция: ${Artist} - ${Title} отредактированна`, 'Успешно');
            this.location.back();
            this.audio$.next(null);
            this.setFlag('isRequset', false);
        }

        this.audioService.edit(update).subscribe({ next });
    }

    addAudioInPlaylist(audio: IAudio, isOwn: boolean): void {
        this.audioWorker.setFlag('requestAudioId', audio._id);
        
        const ownId: string = this.getOwnId();

        const next = (media: IAudio) => {
            if(isOwn) {
                this.ownList$.next({ action: 'add', media });
            }

            this.audioWorker.setFlag('requestAudioId', null);
        }

        this.audioService.addAudioInPlaylist(ownId, audio).subscribe({ next });
    }

    removeAudioFromPlaylist(audio: IAudio, isOwn: boolean): void {
        this.audioWorker.setFlag('requestAudioId', audio._id);

        const ownId: string = this.getOwnId();

        const next = (response: MessageResponse) => {

            if(isOwn) {
                this.ownList$.next({ action: 'remove', media: audio });
            }
            
            this.audioWorker.setFlag('requestAudioId', null);
        }

        this.audioService.removeAudioFromPlaylist(ownId, audio).subscribe({ next })
    }


    destroy(): void {
        this.defaultFlagObject(new AudioFlags);
    }

    destroyOwn(): void {
        this.activeAudio = null;
        this.setFlag('isShowAddAudioList', false);
        this.unsubscribe();
    }
}