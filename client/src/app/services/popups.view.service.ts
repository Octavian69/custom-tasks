import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import * as nanoid from 'nanoid';
import { ViewWorker } from '../shared/classes/view.worker';
import PopupsDB from '../db/popups.db'
import { PopupsFlags } from '../models/flags/PopupsFlags';
import { Popup } from '../models/Popup';
import { SocketService } from './socket.service';
import { unsubscriber } from '../shared/handlers/handlers';
import { SimpleAction } from '../models/SimpleAction';
import { Router } from '@angular/router';
import { IMessage } from '../interfaces/IMessage';
import { UnseenViewService } from './unseen.view.service';
import { FriendState } from '../types/friends.types';
import { UnseenEvent } from '../types/unseen.types';
import { createUnseenEvent } from '../shared/handlers/unseen.handlers';
import { StorageService } from './storage.service';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { SocketListener } from '../types/socket.types';
import { NewDiscussion } from '../types/messages.types';
import { PageLink } from '../types/request-response.types';
import { NoticeService } from './notice.service';

@Injectable({
    providedIn: SystemServicesModule
})
export class PopupsViewService extends ViewWorker<PopupsFlags> {

    popups$: BehaviorSubject<Popup[]> = new BehaviorSubject(null);
    subs$: Subscription[] = [];
    
    constructor(
        private router: Router,
        private socketService: SocketService,
        private unseenViewService: UnseenViewService,
        private noticeSerice: NoticeService,
        private storage: StorageService
    ){
        super(new PopupsFlags, PopupsDB)
    }

    init() {
        const events: SocketListener[] = this.getDataByArrayKeys(['popups', 'events']);

        events.forEach(({ method, event }) => {
            const sub$: Subscription = this.socketService.getListener(event).subscribe(this[method]);

            this.subs$.push(sub$);
        })
    }

    message = (message: IMessage): void => {
        const { Dialogue } = message;
        const { url } = this.router;
        const isSkip: boolean = url.includes('messages');
        const isNotice: boolean = !url.includes(Dialogue)

        if(!isSkip) {
            const event: UnseenEvent = createUnseenEvent('add', 'dialogues', 1, Dialogue)

            this.unseenViewService.update(event)

            const popup: Popup = new Popup(
                'newMessage',
                'Сообщение',
                'Перейти в диалог',
                message.Text,
                nanoid(),
                message
            );

            this.add(popup);
            
        }

        if(isNotice) {
            this.noticeSerice.play('message');
        }
    }

    createDiscussion = (payload: NewDiscussion) => {
        const { message, dialogue, state: { CreatorName } } = payload;
        const event: UnseenEvent = createUnseenEvent('add', 'dialogues', 1, dialogue._id)

        this.unseenViewService.update(event);

        const popup: Popup = new Popup(
            'createDiscussion',
            'Новая беседа',
            'Перейти к беседе',
            `Тема: ${dialogue.Title}`,
            nanoid(),
            { 
                UserName: CreatorName, 
                Dialogue: dialogue._id, 
                User: dialogue.User 
            }
        )

        this.add(popup);
        this.noticeSerice.play('message');
    }

    changeFriendsState = (payload: FriendState): void => {
        const { state, action, User } = payload;

        let actionTitle: string = 'На страницу пользователя';
        let message: string;
        let title: string;
        let unseeEvent: UnseenEvent;

        switch(action) {
            case 'addFriend': {
                title = 'Новый друг';
                message = `${payload.UserName} добавил Вас в список своих друзей`;
                break;
            }
            case 'removeFriend': {
                title = 'Информация';
                message = `${payload.UserName} удалил Вас в списка своих друзей`;
                break;
            }
            case 'requestToAddFriend': {
                const { status } = state;
                const { url } = this.router;
                const isUpdateUnseen: boolean =!url.includes('type=incoming');
                const isAddUnseen: boolean = isUpdateUnseen && status === 'add';
                
                if(status === 'add') {
                    title = 'Заявка в друзья'
                    message = `${payload.UserName} хочет добавить Вас в список своих друзей`;
                    actionTitle = 'Перейти к заявкам';
                } else {
                    unseeEvent = createUnseenEvent('remove', 'friendsRequests', 1, User);
                }
                
                if(isAddUnseen) {
                    unseeEvent = createUnseenEvent('add', 'friendsRequests', 1, User);
                }

                break;
            }

            case 'rejectToAddFriend': {
                unseeEvent = createUnseenEvent('remove', 'friendsRequests', 1, User);
                break;
            }
        }

        if(message) {
            const popup: Popup = new Popup(
                'changeFriendsState',
                title,
                actionTitle,
                message,
                nanoid(),
                payload
            );
        
            this.add(popup);
            this.noticeSerice.play('friend-request');
        }

        if(unseeEvent) {
            this.unseenViewService.update(unseeEvent);
        }
    }

    close(popupId: string): void {
        const popups: Popup[] = this.getStreamValue('popups$');
        const filtered: Popup[] = popups.filter(p => p._id !== popupId);

        this.popups$.next(filtered.length ? filtered : null)
    }

    add(popup: Popup): void {
        const popups: Popup[] = this.getStreamValue('popups$');
        const update: Popup[] = popups ? popups.concat(popup) : [popup];

        this.popups$.next(update);
    }

    action(event: SimpleAction<Popup>): void {
        const { action, payload } = event;

        action === 'default' ? this.defaultAction(payload) : this.actionByType(payload);
    }

    defaultAction(popup: Popup): void {
        const { modeType } = popup;

        switch(modeType) {
            default: {
                this.router.navigate(['/account', popup.state.User]);
            }
        }
    }

    actionByType(popup: Popup): void {
        const { modeType } = popup;

        switch(modeType) {
            case 'newMessage': {
                this.router.navigate(['/messages/dialogue', popup.state.Dialogue]);
                break;
            }
            case 'createDiscussion': {
                this.router.navigate(['/messages/dialogue', popup.state.Dialogue]);
                break;
            }
            case 'changeFriendsState': {
                const isNavigate: boolean = popup.state.action === 'requestToAddFriend';

                if(isNavigate) {
                    const ownId: string = this.getOwnId();
                    const friendsPage: PageLink = {id:2,page:"requests",title:"Заявки в друзья",query:{"type":"incoming"}};
                    this.storage.setItem('sessionStorage', 'friendsPage', friendsPage);

                    this.router.navigate(['/friends', ownId], {
                        queryParams: { page: 'requests', type: 'incoming' }
                    });
                } else {
                    this.router.navigate(['/account', popup.state.User]);
                }

                break;
            }
        }
    }

    destroy(): void {
        this.destroySubjects(['popups$']);
        unsubscriber(this.subs$);
    }
}