import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IVkResponse } from '../interfaces/IVkResponse';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { MessageResponse } from '../types/request-response.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class EditService {

    constructor(
        private http: HttpClient
    ) {}

    getSelectOptions(url: string): Observable<IVkResponse> {
        return this.http.jsonp<IVkResponse>(url, 'callback');
    }

    update<T>(info: T | T[], part: string, type: string): Observable<MessageResponse> {
        const params = new HttpParams({ fromObject: { type } });
        return this.http.patch<MessageResponse>(`@/edit/${part}`, info, { params });
    }

    remove(id: string, part: string): Observable<MessageResponse> {
        const params = new HttpParams({ fromObject: { part } });
        return this.http.delete<MessageResponse>(`@/edit/remove/${id}`, { params });
    }
}