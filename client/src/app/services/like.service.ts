import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Like } from '../models/Like';
import { Observable } from 'rxjs';
import { SystemServicesModule } from '../shared/modules/system.services.module';

@Injectable({
    providedIn: SystemServicesModule
})
export class LikeService {

    constructor(
        private http: HttpClient
    ) {}

    like(like: Like): Observable<null> {
        return this.http.patch<null>(`@/likes/update`, like);
    }
}