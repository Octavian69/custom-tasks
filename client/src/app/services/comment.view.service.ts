import { Injectable } from '@angular/core';
import { CommentService } from './comment.service';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { Comment } from '../models/Comment';
import { switchMapTo, map, takeUntil } from 'rxjs/operators';
import { ViewWorker } from '../shared/classes/view.worker';
import { IComment } from '../interfaces/IComment';
import { isExpand } from '../shared/handlers/handlers';
import { CommentFlags } from '../models/flags/CommentFlags';
import { MediaList } from '../models/MediaList';
import { CommentRequest } from '../models/CommentRequest';
import { LikeViewService } from './like.view.service';
import { SimpleAction } from '../models/SimpleAction';
import { IUser } from '../interfaces/IUser';
import { filterUnique } from '../shared/handlers/media.handlers';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { ResponseAction } from '../types/request-response.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class CommentViewService extends ViewWorker<CommentFlags> {
    
    comments$: BehaviorSubject<MediaList<IComment>> = new BehaviorSubject<MediaList<IComment>>(null);
    request: CommentRequest;
    subs$: string[] = [];
    
    

    constructor(
        private commentService: CommentService,
        private likeViewService: LikeViewService
    ) {
        super(new CommentFlags)
    }

    fetch(action: ResponseAction, req?: CommentRequest): void {
        const flagName: 'isLoading' | 'isExpand' = action === 'replace' ? 'isLoading' : 'isExpand';
        const currentList: MediaList<IComment> = this.comments$.getValue();

        this.setFlag(flagName, true);

        if(action === 'replace') this.request = req;

        const next = (response: MediaList<IComment>) => {
            this.setFlag(flagName, false);
            
            if(action === 'replace') {
                this.comments$.next(response);
            } else {
                const { rows, totalCount } = currentList;

                currentList.rows = rows.concat(response.rows);
                currentList.totalCount = totalCount;

                this.comments$.next(currentList);
            }
        } 

        of(null).pipe(
                switchMapTo(this.commentService.fetch(this.request)),
                map((response: MediaList<IComment>) => {
                    return filterUnique(currentList, response, action)
                }),
                takeUntil(this.unsubscriber$)
            )
            .subscribe({ next });
    }

    expand() {
        const comments: MediaList<IComment> = this.getStreamValue('comments$');

        const expand = isExpand(comments);

        if(expand) {
            this.request.page.skip++;
            this.fetch('expand');
        }
    }

    create(comment: Comment): void {

        this.setFlag('isRequest', true);

        const next = (create: IComment) => {
            
            const currentList: MediaList<IComment> = this.comments$.getValue();
            currentList.rows.unshift(create);
            currentList.totalCount++;
            this.comments$.next(currentList);

            this.setFlag('isRequest', false);
        }
        
        this.commentService.create(comment).subscribe({ next });
    }

    like(comment: IComment): void {
        const isRequest: boolean = this.likeViewService.getFlag('isRequest') as boolean;

        if(!isRequest) {
            const comments: MediaList<IComment> = this.getStreamValue('comments$');

            this.likeViewService.like(comment, 'Comment');
            const update: IComment = this.likeViewService.mutate(comment);
    
            const idx: number = comments.rows.findIndex(c => c._id === update._id);
    
            if(~idx) {
                comments.rows[idx] = update;
                this.comments$.next(comments);
            }
        }
    }

    updateCommentAvatar(comments: IComment[], user: IUser): IComment[] {

        return comments.map((comment: IComment) => {
            const isOwnComment: boolean = this.isOwn(comment.User);

            if(isOwnComment) {
                const { Avatar } = user;
    
                comment.Avatar = Avatar;
            }

            return comment;
        });

    }

    mutateDetail(event: SimpleAction<IUser | IComment>) {
        const { action, payload } = event;
        const comments: MediaList<IComment> = this.getStreamValue('comments$');

        switch(action) {
            case 'create-avatar': {
                const rows: IComment[] = this.updateCommentAvatar(comments.rows, payload as IUser);
                comments.rows = rows;
                
                this.comments$.next(comments);
                break;
            }
            case 'update-avatar': {
                const rows: IComment[] = this.updateCommentAvatar(comments.rows, payload as IUser);
                comments.rows = rows;
                
                this.comments$.next(comments);
                break;
            }
          };
    }

    destroy(): void {
        this.comments$.next(null);
        this.unsubscribe()
    }
}