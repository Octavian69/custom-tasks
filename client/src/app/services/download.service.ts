import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { saveAs } from 'file-saver';
import * as nanoid from 'nanoid';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { ViewWorker } from '../shared/classes/view.worker';
import { DownloadFlags } from '../models/flags/DownloadFlags';
import { environment } from '@env/environment';

@Injectable({
    providedIn: SystemServicesModule
})
export class DownloadService extends ViewWorker<DownloadFlags> {

    constructor(
        private http: HttpClient
    ){
        super(new DownloadFlags)
    }

    download(path: string, filename: string): void {
        this.setFlag('isDownload', true);
        
        const next = (data: Blob) => {
            saveAs(data, filename || nanoid());
            this.setFlag('isDownload', false);
        }

      this.http.get(`${environment.API_URL}/${path}`, { responseType: 'blob' })
        .subscribe({ next });
    }
}