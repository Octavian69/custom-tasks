import { Injectable } from '@angular/core';
import { ViewWorker } from '../shared/classes/view.worker';
import MessageDB from '../db/message.db';
import { MessageFlags } from '../models/flags/MessageFlags';
import { Router, ActivatedRoute } from '@angular/router';
import { IUser } from '../interfaces/IUser';
import { StorageService } from './storage.service';
import { SocketService } from './socket.service';
import { Message } from '../models/Message';
import { IMessage } from '../interfaces/IMessage';
import { BehaviorSubject, Subject } from 'rxjs';
import { MessageService } from './message.service';
import { tap, map, takeUntil, delay } from 'rxjs/operators';
import { MediaList } from '../models/MediaList';
import { PaginationRequest } from '../models/PaginationRequest';
import { DialoguesViewService } from './dialogues.view.service';
import { SimpleAction } from '../models/SimpleAction';
import { DialoguesService } from './dialogues.service';
import { IDialogue } from '../interfaces/IDialogue';
import { UnseenViewService } from './unseen.view.service';
import { createUnseenEvent } from '../shared/handlers/unseen.handlers';
import { UnseenEvent } from '../types/unseen.types';
import { SOKET_EVENTS } from '../namespaces/socket-events.namespaces';
import { filterUnique } from '../shared/handlers/media.handlers';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { LastMessage } from '../types/messages.types';
import { ResponseAction, MessageResponse } from '../types/request-response.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class MessageViewService extends ViewWorker<MessageFlags> {

    messages$: BehaviorSubject<MediaList<IMessage>> = new BehaviorSubject(null);
    edit$: BehaviorSubject<IMessage> = new BehaviorSubject(null);
    scroll$: Subject<ResponseAction> = new Subject();

    editMesssagesIds: string[] = [];
    deletionCandidatesIds: string[] = [];
    removedMessagesIds: string[] = [];

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private storage: StorageService,
        private messageService: MessageService,
        private socketService: SocketService,
        private dialoguesViewService: DialoguesViewService,
        private dialoguesService: DialoguesService,
        private unseenService: UnseenViewService
    ) {
        super(new MessageFlags, MessageDB);
    }

    fetch(request: PaginationRequest, action: ResponseAction): void {
        this.setFlag('isFetch', true);

        const messages: MediaList<IMessage> = this.getStreamValue('messages$');

        const next = (response: MediaList<IMessage>) => {
            
            if(action === 'replace') {
                this.messages$.next(response);
                this.scroll$.next('replace');
            } else {
                messages.rows.unshift(...response.rows);

                this.messages$.next(messages);
            };

            this.setFlag('isFetch', false);
        }

        this.messageService
        .fetch(request)
        .pipe(
            map((response: MediaList<IMessage>) => {
                const { count, rows } = response.rows.reduce((accum, current) => {
                    const isExclude: boolean = this.deletionCandidatesIds.includes(current._id);
                    
                    if(!isExclude) {
                        accum.rows.push(current)
                    } else {
                        accum.count++;
                    }

                    return accum;
                }, {count: 0, rows: []});

                response.rows = rows;
                response.totalCount -= count;

                return filterUnique(messages, response, action)
            }),
            takeUntil(this.unsubscriber$)
        )
        .subscribe({ next });
    }

    initSingleMessage(user?: IUser): void {
        const { queryParams } = this.activeRoute.snapshot;

        this.storage.setItem('sessionStorage', 'messageUser', user);

        this.router.navigate([], {
            relativeTo: this.activeRoute,
            queryParams: { ...queryParams, message_id: user._id }
        })
    }

    sendSingle(recepientId: string, message: Message): void {
        const next = (message: IMessage) => {}

        this.messageService
        .sendSingle(recepientId, message)
        .pipe(
            tap((message: Message) => {
                this.socketService.emitEvent(SOKET_EVENTS.MESSAGE_NEW, message);
            })
        )
        .subscribe({ next })
    }

    send(candidate: Message) {
        this.setFlag('isSendRequest', true);

        const next = (message: IMessage) => {
            const event: SimpleAction<IMessage> = new SimpleAction('add', message);
            this.mutate(event);
            this.dialoguesViewService.mutateDetail(event);
            this.setFlag('isSendRequest', false);
        } 

        this.messageService
        .createMessage(candidate)
        .pipe(
            tap((response: IMessage) => {
                const newMessage: IMessage = Object.assign({}, response);
                newMessage.IsOwn = false;

                this.socketService.emitEvent(SOKET_EVENTS.MESSAGE_NEW, newMessage);
            })
        )
        .subscribe({ next })
    }

    edit(Text: string): void {
        this.setFlag('isEditRequest', true);

        const message: IMessage = this.getStreamValue('edit$');
        const editMessage: IMessage = Object.assign({}, message, { Text });

        const next = (response: IMessage) => {
            const event: SimpleAction<IMessage> = new SimpleAction('edit', response);
            
            this.mutate(event);
            this.edit$.next(null);

            this.setFlag('isEditRequest', false);
        }

        this.messageService
        .edit(editMessage)
        .pipe(tap((response: IMessage) => {
            this.socketService.emitEvent<IMessage>(SOKET_EVENTS.MESSAGE_EDIT, response);
        }))
        .subscribe({ next });
    }

    remove(dialogueId: string) {
        const isRemoveRequest = this.getFlag('isRemoveRequest') as boolean;

        if(isRemoveRequest || this.deletionCandidatesIds.length === 0) return;

        this.setFlag('isRemoveRequest', true);

        const next = (response: MessageResponse) => {
            this.deletionCandidatesIds.splice(0);
            this.setFlag('isRemoveRequest', false);
        }
        

        this.messageService
        .remove(dialogueId, this.deletionCandidatesIds)
        .pipe(
            tap(() => {
                const payload = { Dialogue: dialogueId, messagesIds: this.deletionCandidatesIds}
                this.socketService.emitEvent(SOKET_EVENTS.MESSAGE_REMOVE, payload);
            })
        )
        .subscribe({ next })

    }

    addNewOpenDialogue(message: IMessage): void {
        const opened: IDialogue = this.dialoguesViewService.getOpen(message.Dialogue);
        const unseenEvent: UnseenEvent = createUnseenEvent('add', 'dialogues', 1, message.Dialogue);

        const LastMessage: LastMessage = {
            Text: message.Text,
            User: message.User,
            Created: message.Created,
            _id: message._id
        }

        if(opened) {
            opened.Unread = true;
            opened.UnreadMessages++;
            opened.TotalMessages++;
            opened.LastMessage = LastMessage;

            this.dialoguesViewService.addOpen(opened);

        } else {
            const next = (dialogue: IDialogue) => {
                dialogue.Unread = true;
                dialogue.UnreadMessages++;
                dialogue.TotalMessages++;
                dialogue.LastMessage = LastMessage;

                this.dialoguesViewService.addOpen(dialogue);
            }
    
           this.dialoguesService
           .fecthById(message.Dialogue) 
           .subscribe({ next });
        }

        this.unseenService.update(unseenEvent);
    }

    showEditMessage(messageId: string): void {
        this.editMesssagesIds.push(messageId);

        setTimeout(_ => {
            const idx: number = this.editMesssagesIds.findIndex(id => id === messageId)
            this.editMesssagesIds.splice(idx, 1);
        }, 5000)
    }

    setDeletionState(messageId: string) {
        const isRequest: boolean = this.getFlag('isRemoveRequest');
        
        if(isRequest) return

        const isExist: boolean = this.deletionCandidatesIds.includes(messageId);

        if(!isExist) {
            this.deletionCandidatesIds.push(messageId);
        } else {
            const idx: number = this.deletionCandidatesIds.findIndex(mId => mId === messageId);
            this.deletionCandidatesIds.splice(idx, 1);
        }
    }
 
    mutate(event: SimpleAction<IMessage | string[]>): void {
        const { action, payload } = event;
        const messages: MediaList<IMessage> = this.getStreamValue('messages$');

        switch(action) {

            case 'add': {
                messages.rows.push(payload as IMessage);
                messages.totalCount++;
                this.scroll$.next();
                break;
            }

            case 'edit': {
                const idx: number = messages.rows.findIndex(m => m._id === (<IMessage>payload)._id);
                
                if(~idx) messages.rows[idx] = <IMessage>payload;
                else return

                break;
            }

            case 'remove': {
                this.removedMessagesIds.push(...payload as string[]);

                setTimeout(_ => {
                    messages.rows = messages.rows.filter(m => !(<string[]>payload).includes(m._id));
                    messages.totalCount -= (payload as string[]).length;

                    this.messages$.next(messages);
                }, 4000)
                
                return;
            }
        }

        this.messages$.next(messages);
    }

    destroy(): void {
        this.destroySubjects(['messages$']);
        this.defaultFlagObject(new MessageFlags);
        this.unsubscribe();
    }
}