import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comment } from '../models/Comment';
import { Observable } from 'rxjs';
import { IComment } from '../interfaces/IComment';
import { CommentRequest } from '../models/CommentRequest';
import { MediaList } from '../models/MediaList';
import { SystemServicesModule } from '../shared/modules/system.services.module';

@Injectable({
    providedIn: SystemServicesModule
})
export class CommentService {
    constructor(
        private http: HttpClient
    ){}

    fetch(request: CommentRequest): Observable<MediaList<IComment>> {
        return this.http.post<MediaList<IComment>>(`@/comments/fetch/${request.refId}`, request);
    }

    create(comment: Comment): Observable<IComment> {
        return this.http.post<IComment>(`@/comments/create`, comment);
    }

    edit(id: string, Text: string): Observable<Comment>{
        return this.http.patch<Comment>(`@/comments/edit/${id}`, { Text });
    }
}