import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Audio } from '../models/Audio';
import { Observable } from 'rxjs';
import { IAudio } from '../interfaces/IAudio';
import { MediaRequest } from '../models/MediaRequest';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { PaginationResponse, MessageResponse } from '../types/request-response.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class AudioService {
    constructor(
        private http: HttpClient
    ) {}

    fetch(id: string, mediaRequest: MediaRequest): Observable<PaginationResponse<IAudio>> {
        return this.http.post<PaginationResponse<IAudio>>(`@/audio/fetch/${id}`, mediaRequest);
    }

    create(audio: FormData): Observable<Audio> {
        return this.http.post<Audio>('@/audio/create', audio);
    }

    edit(audio: IAudio): Observable<IAudio> {
        return this.http.patch<IAudio>(`@/audio/edit`, audio);
    }

    fetchByString(mediaRequest: MediaRequest): Observable<PaginationResponse<IAudio>> {
        return this.http.post<PaginationResponse<IAudio>>(`@/audio/fetch-by-string`, mediaRequest);
    }

    getById(id: string): Observable<IAudio> {
        return this.http.get<IAudio>(`@/audio/get-by-audio/${id}`);
    }

    addAudioInPlaylist(playlistId: string, audio: IAudio): Observable<IAudio> {
        return this.http.patch<IAudio>(`@/audio/add-to-playlist/${playlistId}`, audio);
    }

    removeAudioFromPlaylist(playlistId: string, audio: IAudio): Observable<MessageResponse> {
        const headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

        const httpOptions = {
            headers,
            body: {Path: audio.Path}
        }

        return this.http.delete<MessageResponse>(`@/audio/remove-from-playlist/${playlistId}`, httpOptions);
    }
}