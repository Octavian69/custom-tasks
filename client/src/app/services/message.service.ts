import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from '../models/Message';
import { IMessage } from '../interfaces/IMessage';
import { Observable } from 'rxjs';
import { PaginationRequest } from '../models/PaginationRequest';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { PaginationResponse, MessageResponse } from '../types/request-response.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class MessageService {
    constructor(
        private http: HttpClient
    ){}

    fetch(request: PaginationRequest): Observable<PaginationResponse<IMessage>> {
        return this.http.post<PaginationResponse<IMessage>>(`@/messages/fetch-messages/${request.referenceId}`, request);
    }

    sendSingle(recipientId: string, message: Message): Observable<IMessage> {
        return this.http.post<IMessage>(`@/messages/single/${recipientId}`, message);
    }

    createMessage(message: Message): Observable<IMessage> {
        return this.http.post<IMessage>(`@/messages/create-message`, message);
    }

    edit(message: IMessage): Observable<IMessage> {
        return this.http.patch<IMessage>(`@/messages/edit-message`, message);
    }

    remove(dialogueId: string, messagesIds: string[]): Observable<MessageResponse> {
        return this.http.patch<MessageResponse>(`@/messages/remove-messages/${dialogueId}`, messagesIds);
    }
}