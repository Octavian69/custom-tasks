import { Injectable } from '@angular/core';
import { ViewWorker } from '../shared/classes/view.worker';
import MessageDB from '../db/message.db';
import { DialoguesFlags } from '../models/flags/DialoguesFlags';
import { FriendsService } from './friends.service';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { MediaList } from '../models/MediaList';
import { IIncompleteUser } from '../interfaces/IIncompleteUser';
import { PaginationRequest } from '../models/PaginationRequest';
import { createFormData, deepCopy } from '../shared/handlers/handlers';
import { IDialogue } from '../interfaces/IDialogue';
import { DialoguesService } from './dialogues.service';
import { UnseenViewService } from './unseen.view.service';
import { Router } from '@angular/router';
import { StorageService } from './storage.service';
import { map, tap, mergeMap, takeUntil, switchMap } from 'rxjs/operators';
import { SocketService } from './socket.service';
import { Dialogue } from '../models/Dialogue';
import { SimpleAction } from '../models/SimpleAction';
import { IMessage } from '../interfaces/IMessage';
import { createUnseenEvent } from '../shared/handlers/unseen.handlers';
import { UnseenEvent } from '../types/unseen.types';
import { SOKET_EVENTS } from '../namespaces/socket-events.namespaces';
import { filterUnique } from '../shared/handlers/media.handlers';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { PageLink, ResponseAction } from '../types/request-response.types';
import { CreateDiscussion, NewDiscussion, LastMessage } from '../types/messages.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class DialoguesViewService extends ViewWorker<DialoguesFlags> {

    dialogues$: BehaviorSubject<MediaList<IDialogue>> = new BehaviorSubject(null);
    dialogue$: BehaviorSubject<IDialogue> = new BehaviorSubject(null);
    friends$: BehaviorSubject<MediaList<IIncompleteUser>> = new BehaviorSubject(null);
    search$: BehaviorSubject<MediaList<IIncompleteUser | IDialogue>> = new BehaviorSubject(null);
    open$: BehaviorSubject<IDialogue[]> = new BehaviorSubject([]);
    
    constructor(
        private router: Router,
        private dialoguesService: DialoguesService,
        private friendsService: FriendsService,
        private unseenService: UnseenViewService,
        private storage: StorageService,
        private socketService: SocketService
    ){
        super(new DialoguesFlags, MessageDB)
    }

    fetch(mediaRequest: PaginationRequest, action: ResponseAction): void {

        this.setFlag('isFetchDialogues', true);

        const dialogues: MediaList<IDialogue> = this.getStreamValue('dialogues$');
        
        const next = (response: MediaList<IDialogue>) => {

            if(action === 'replace') {
                this.dialogues$.next(response);
            } else {
                
                const rows = dialogues.rows.concat(response.rows);
                const totalCount = response.totalCount;

                this.dialogues$.next({ rows, totalCount });
            }

            this.setFlag('isFetchDialogues', false);
        }

        this.dialoguesService
        .fetch(mediaRequest)
        .pipe(
            map((response: MediaList<IDialogue>) => {
                const unique: MediaList<IDialogue> = filterUnique(dialogues, response, action);
    
                return this.mapIsOwnRows(unique);
            }),
            takeUntil(this.unsubscriber$)
        )
        .subscribe({ next });
    }

    fetchFriends(request: PaginationRequest, action: ResponseAction): void {
        this.setFlag('isFetchFriends', true);
        const mediaList: MediaList<IIncompleteUser> = this.getStreamValue('friends$');

        const next = (response: MediaList<IIncompleteUser>) => {
            if(action === 'replace') {
                this.friends$.next(response);
            } else {
                
                const rows = mediaList.rows.concat(response.rows);
                const totalCount = response.totalCount;

                this.friends$.next({ rows, totalCount });
            }

            this.setFlag('isFetchFriends', false);
        }

        this.friendsService
        .fetchIncomplete(request)
        .pipe(
            map((response: MediaList<IIncompleteUser>) => {
                return filterUnique(mediaList, response, action)
            }),
            takeUntil(this.unsubscriber$)
        )
        .subscribe({ next });
    }

    fetchDialogue(dialogueId: string): void {
        this.setFlag('isFetchDialogue', true);

        const open: IDialogue[] = this.getStreamValue('open$');
        const idx: number = open.findIndex(d => d._id === dialogueId);

        if(~idx) {
            const findDialogue: IDialogue = this.resetDialogue(open[idx]);

            open[idx] = findDialogue;

            this.open$.next(open);
            this.dialogue$.next(deepCopy(findDialogue));
            this.setFlag('isFetchDialogue', false);

        } else {
            const next = (response: IDialogue) => {
                const dialogue: IDialogue = this.resetDialogue(response);

                open.push(dialogue);
                this.open$.next(open);
                this.dialogue$.next(deepCopy(dialogue));
   
                this.setFlag('isFetchDialogue', true);
            }
    
            this.dialoguesService
            .fecthById(dialogueId)
            .pipe(
                map((dialogue: IDialogue) => this.mapIsOwnItem(dialogue)),
                takeUntil(this.unsubscriber$)
            )
            .subscribe({ next });
        }
    }

    fetchSearch(mediaRequest: PaginationRequest, action: ResponseAction): void {
        this.setFlag('isFetchSearch', true);

        const { state: { type } } = mediaRequest;
        const mediaList: MediaList<IIncompleteUser | IDialogue> = this.getStreamValue('search$');

        const next = (response: MediaList<IIncompleteUser | IDialogue>) => {
            
            if(action === 'replace') {
                this.search$.next(response)
            } else {
                
                const rows: Array<IIncompleteUser | IDialogue> = mediaList.rows.concat(response.rows);
                const totalCount: number = response.totalCount;

                this.search$.next({ rows, totalCount });
            }

            this.setFlag('isFetchSearch', false);
            
        }

        const request$: Observable<MediaList<IIncompleteUser | IDialogue>> = type === 'dialogue'
         ? this.friendsService.fetchIncomplete(mediaRequest)
         : this.dialoguesService.fetch(mediaRequest);

        request$
            .pipe(
                map((response: MediaList<IIncompleteUser>) => {
                    return filterUnique(mediaList, response, action)
                }),
                takeUntil(this.unsubscriber$)
            )
            .subscribe({ next });
    }

    createDiscussion(payload: CreateDiscussion, file: File = null): void {
        this.setFlag('isCreateDialogue', true);

        const body: CreateDiscussion | FormData = file 
        ? createFormData(file, 'dialogue-logo', payload)
        : payload

        const next = ({ dialogue, message }) => {
            this.changeDialogue(dialogue);
            this.setFlag('isShowCreateDiscussion', false);
            this.setFlag('isCreateDialogue', false);
            this.router.navigate(['/messages/dialogue', dialogue._id]);
        }

        this.dialoguesService
        .createDiscussion(body)
        .pipe(tap((response: NewDiscussion) => {
            const payload: NewDiscussion = deepCopy(response);
            const { Name: CreatorName } = this.getDecodeToken();

            payload.state = { CreatorName };
            this.socketService.emitEvent(SOKET_EVENTS.MESSAGE_CREATE_DISCUSSION, payload);
        }))
        .subscribe({ next });
    }

    navigateToDialogue(userId: string): void {
        const ownId: string = this.getOwnId();
        const participants: string[] = [userId, ownId];

        const next = (dialogue: IDialogue) => {
            this.changeDialogue(dialogue);

            this.router.navigate(['/messages/dialogue', dialogue._id]).then(_ => {
                this.setFlag('isNavigateToDetail', false);
            })
        }

        this.dialoguesService
        .fetchByParticipants(participants, 'dialogue')
        .pipe(
            switchMap((dialogue: IDialogue) => {
                if(dialogue) {
                    return of(dialogue)
                } else {
                    const newDialogue: Dialogue = new Dialogue(
                        null,
                        participants,
                        'dialogue',
                        ownId
                    );
    
                    return this.dialoguesService.createDialogue(newDialogue, userId);
                } 
            }),
            takeUntil(this.unsubscriber$)
        )
        .subscribe({ next });
        
    }

    changeDialogue(dialogue: IDialogue) {
        const open: IDialogue[] = this.getStreamValue('open$');
        const findDialogue: IDialogue = open.find(d => d._id === dialogue._id);

        if(!findDialogue) {
            open.unshift(dialogue);
            this.open$.next(open);
        }
        
        this.dialogue$.next(dialogue);
    }

    removeOpenDialogue(dialogue: IDialogue): void {
        const open: IDialogue[] = this.getStreamValue('open$');
        const currentDialogue: IDialogue = this.getStreamValue('dialogue$');
        const idx: number = open.findIndex(d => d._id == dialogue._id);
        const isEqual: boolean = currentDialogue && currentDialogue._id === dialogue._id;

        if(isEqual) {
            const messagePage: PageLink = this.storage.getItem('sessionStorage', 'messagePage');
            this.router.navigate(['/messages'], { queryParams: { page: messagePage.page } });
        }

        open.splice(idx, 1);
        this.open$.next(open);
    }

    addDialogue(dialogue: IDialogue): void {
        const mediaList: MediaList<IDialogue> = this.getStreamValue('dialogues$');

        mediaList.rows.unshift(dialogue);
        mediaList.totalCount++;

        this.dialogues$.next(mediaList);
    }

    addOpen(dialogue: IDialogue): void {
        const open: IDialogue[] = this.getStreamValue('open$');

        const update: IDialogue[] = open.filter(d => d._id !== dialogue._id);

        update.unshift( deepCopy(dialogue) );
        
        this.open$.next(update);
    }

    resetDialogue(dialogue: IDialogue): IDialogue {
        const copy: IDialogue = deepCopy(dialogue);

        copy.Unread = false;
        copy.UnreadMessages = 0;
        
        const { _id: dialogueId } = dialogue;

        const event: UnseenEvent = createUnseenEvent('remove', 'dialogues', 0, dialogueId);

        this.unseenService.update(event);

        return copy;
    }

    getOpen(dialogueId: string): IDialogue {
        const dialogues: IDialogue[] = this.getStreamValue('open$');

        const dialogue: IDialogue = dialogues.find(d => d._id === dialogueId);

        return dialogue ? deepCopy(dialogue) : null;
    }

    newMessage(message: IMessage, isEqualTypes: boolean): void {
        const unseenEvent: UnseenEvent = createUnseenEvent('add', 'dialogues', 1, message.Dialogue);
        const mediaList: MediaList<IDialogue> = this.getStreamValue('dialogues$');
        const idx: number = mediaList.rows.findIndex(d => d._id === message.Dialogue);
        const LastMessage: LastMessage = {
            Text: message.Text,
            User: message.User,
            Created: message.Created,
            _id: message._id
        }

        if(isEqualTypes && ~idx) {
            const [ find ] = mediaList.rows.splice(idx, 1);
            find.Unread = true;
            find.UnreadMessages++;
            find.TotalMessages++;
            find.LastMessage = LastMessage;

            mediaList.rows.unshift(find);
            this.dialogues$.next(mediaList);
            this.addOpen(find);
            this.unseenService.update(unseenEvent);

        } else {
            const next = (dialogue: IDialogue) => {
                dialogue.Unread = true;
                dialogue.UnreadMessages++;
                dialogue.TotalMessages++;
                dialogue.LastMessage = LastMessage;

                if(isEqualTypes) {
                    mediaList.rows.unshift(dialogue);
                    this.dialogues$.next(mediaList);
                }

                this.addOpen(dialogue);
            }

            this.dialoguesService
            .fecthById(message.Dialogue)
            .pipe(tap(() => {
                this.unseenService.update(unseenEvent);
            }))
            .subscribe({ next });
        }
    }

    readAllChats(): void {
        const open: IDialogue[] = this.getStreamValue('open$');
        const dialogues: MediaList<IDialogue> = this.getStreamValue('dialogues$');

        const updateOpen: IDialogue[] = open.map(this.resetUnread);

        if(dialogues) {
            const updateDialogues: IDialogue[] = dialogues.rows.map(this.resetUnread);

            dialogues.rows = updateDialogues;

            this.dialogues$.next(dialogues);
        };

        this.open$.next(updateOpen);
   }

   resetUnread(dialogue: IDialogue): IDialogue {
        const update: IDialogue = Object.assign({}, dialogue);

        update.Unread = false;
        update.UnreadMessages = 0;

        return update;
   }

    mutateDetail(event: SimpleAction<IMessage>): void {
        const { action, payload } = event;
        const dialogue: IDialogue = this.getStreamValue('dialogue$');

        switch(action) {
            case 'add': {
                dialogue.TotalMessages++;
                break;
            }

            case 'remove': {
                dialogue.TotalMessages--;
                break;
            }
        }

        this.dialogue$.next(dialogue);
    }

    destroy(): void {
        this.destroySubjects(['friends$', 'dialogue$', 'dialogues$']);
        this.open$.next([]);
        this.defaultFlagObject(new DialoguesFlags);
        this.unsubscribe()
    }
}
