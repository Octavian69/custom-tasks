import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ITask } from '../interfaces/ITask';
import { Page } from '../models/Page';
import { TaskFilter } from '../models/TaskFilter';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { PaginationResponse, MessageResponse } from '../types/request-response.types';


@Injectable({
    providedIn: SystemServicesModule
})
export class TaskService {
    
    constructor(
        private http: HttpClient
    ){}

    fetch(page: Page, body: TaskFilter): Observable<PaginationResponse<ITask>> {

        const { skip, limit } = page;

        const fromObject = { skip: String(skip), limit: String(limit) };

        const params = new HttpParams({ fromObject })

        return this.http.post<PaginationResponse<ITask>>('@/tasks', body, { params });
    }

    fetchById(id: string): Observable<ITask> {
        return this.http.get<ITask>(`@/tasks/${id}`);
    }

    create(task: ITask): Observable<ITask> {
        return this.http.post<ITask>('@/tasks/create', task);
    }

    update(task: ITask): Observable<ITask> {
        return this.http.patch<ITask>(`@/tasks/${task._id}`, task);
    }

    remove(taskIds: string[]): Observable<MessageResponse> {
        const headers = new HttpHeaders({'Content-Type': 'application/json'});
        
        const httpOptions = {
            headers,
            body: taskIds
        }

        return this.http.delete<MessageResponse>('@/tasks/remove', httpOptions);
    }

}