import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor() {}

    getItem<T>(storageName: 'localStorage' | 'sessionStorage', key: string): T {
        return JSON.parse( window[storageName].getItem(key) );
    }

    setItem<T>(storageName: 'localStorage' | 'sessionStorage', key: string, value: T): void {
        window[storageName].setItem( key, JSON.stringify(value) );
    }

    removeItem(storageName: 'localStorage' | 'sessionStorage', key: string): void {
        window[storageName].removeItem(key);
    }

    reset(): void {
        window['localStorage'].clear();
        window['sessionStorage'].clear();
    }
}