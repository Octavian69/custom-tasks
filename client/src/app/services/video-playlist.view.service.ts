import { Injectable } from '@angular/core';
import { ViewWorker } from '../shared/classes/view.worker';
import VideoDB from '../db/video.db';
import { VideoPlaylistsFlags } from '../models/flags/VideoPlaylistsFlags';
import { BehaviorSubject, Subscription, Subject } from 'rxjs';
import { IVideoPlaylist } from '../interfaces/IVideoPlaylist';
import { VideoPlaylistService } from './video-playlist.service';
import { MediaRequest } from '../models/MediaRequest';
import { createFormData } from '../shared/handlers/handlers';
import { MediaList } from '../models/MediaList';
import { SimpleAction } from '../models/SimpleAction';
import { StorageService } from './storage.service';
import { LikeViewService } from './like.view.service';
import { ToastrService } from 'ngx-toastr';
import { map, takeUntil } from 'rxjs/operators';
import { filterUnique } from '../shared/handlers/media.handlers';
import { SipmleAction, AnyOption } from '../types/shared.types';
import { PageLink, ResponseAction } from '../types/request-response.types';



@Injectable()
export class VideoPlaylistViewService extends ViewWorker<VideoPlaylistsFlags> {

    random: number = Math.random()

    main$: BehaviorSubject<MediaList<IVideoPlaylist>> = new BehaviorSubject(null);
    search$: BehaviorSubject<MediaList<IVideoPlaylist>> = new BehaviorSubject(null);
    playlist$: BehaviorSubject<IVideoPlaylist> = new BehaviorSubject(null);
    mutate$: Subject<SipmleAction<IVideoPlaylist>> = new Subject();

    constructor(
        private toastr: ToastrService,
        private playlistService: VideoPlaylistService,
        private likeViewService: LikeViewService,
        private storage: StorageService
    ) {
        super(new VideoPlaylistsFlags, VideoDB);
    }

    fetch(mediaRequest: MediaRequest, action: ResponseAction, reqFlag: string) {
        this.setFlag(reqFlag, true);

        const { listType } = mediaRequest;
        const listName = listType === 'main' ? 'main$' : 'search$';
        const list: MediaList<IVideoPlaylist> = this.getStreamValue(listName);

        const next = (response: MediaList<IVideoPlaylist>) => {

            if(action === 'replace') {
                this[listName].next(response);
            } else {
                const { rows, totalCount } = response;
                list.rows = list.rows.concat(rows);
                list.totalCount = totalCount;

                this[listName].next(list);
            }

            this.setFlag(reqFlag, false);
        }

        this.playlistService
            .fetch(mediaRequest)
            .pipe(
                map((response: MediaList<IVideoPlaylist>) => {
                    return filterUnique(list, response, action);
                }),
                takeUntil(this.unsubscriber$)
            )
            .subscribe({ next });
    }

    fetchPlaylist(playlistId: string): void {
        
        const next = (playlist: IVideoPlaylist) => {
            
            const isOwn: boolean = this.isOwn(playlist.User);
            this.setFlag('isCanMutateDetail', isOwn)
            
            this.playlist$.next(playlist)
        };

         this.playlistService
            .fetchById(playlistId)
            .pipe(takeUntil(this.unsubscriber$))
            .subscribe({ next });
    }

    create(playlist: IVideoPlaylist, file: File): void {

        this.setFlag('isCreatePlaylist', true);
        
        const body: FormData | IVideoPlaylist = file ? createFormData(file, 'playlist-logo', playlist) : playlist;

        const next = (create: IVideoPlaylist) => {
            const event: SimpleAction<IVideoPlaylist> = new SimpleAction('create', create);

            this.toastr.success(`Видеолист "${create.Title}" создан.`, 'Успешно!');

            this.setFlag('isCreatePlaylistModal', false);
            this.setFlag('isCreatePlaylist', false);

            this.mutate(event);
        }

        this.playlistService
            .create(body)
            .subscribe({ next })
    }

    add(): void {
        const playlist: IVideoPlaylist = this.getStreamValue('playlist$');

        this.setFlag('isMutate', true);

        const next = () => {
            const update: IVideoPlaylist = Object.assign({}, playlist, { IsHave: true });
            const event: SimpleAction<IVideoPlaylist> = new SimpleAction('add', update);
            
            this.toastr.success(`Плейлист ${playlist.Title} добавлен`, 'Успешно!');
            
            this.mutate(event);

            this.setFlag('isMutate', false);
        }

        this.playlistService
            .add(playlist._id)
            .subscribe({ next })
    }

    remove(): void {
        const playlist: IVideoPlaylist = this.getStreamValue('playlist$');

        this.setFlag('isMutate', true);

        const next = () => {
            const update: IVideoPlaylist = Object.assign({}, playlist, { IsHave: false });
            const event: SimpleAction<IVideoPlaylist> = new SimpleAction('remove', update);

            this.toastr.success(`Плейлист ${playlist.Title} удален`, 'Успешно!');
            this.mutate(event);

            this.setFlag('isMutate', false);
        }

        this.playlistService
            .remove(playlist._id)
            .subscribe({ next })
    }

    edit(playlist: IVideoPlaylist, payload: AnyOption): void {

        this.setFlag('isEdit', true);

        const next = () => {
            const edit: IVideoPlaylist = Object.assign({}, playlist, payload);

            const event: SimpleAction<IVideoPlaylist> = new SimpleAction('edit', Object.assign({}, edit));

            this.playlist$.next(edit);

            this.mutate(event);

            this.setFlag('isEdit', false);
        }

        this.playlistService
            .edit(playlist._id, payload)
            .subscribe({ next })
    }


    like(playlist: IVideoPlaylist): void {
        this.likeViewService.like(playlist, 'VideoPlaylist');

        const update: IVideoPlaylist = this.likeViewService.mutate(playlist);

        this.playlist$.next(update);
    }

    updateLogo(file: File): void {

        this.setFlag('isUpdateLogo', true);

        const formData = new FormData();
        const playlist: IVideoPlaylist = this.getStreamValue('playlist$');

        formData.append('playlist-logo', file, file.name);
        
        const next = ({ Path }) => {
            playlist.Logo = Path;

            this.playlist$.next(playlist);

            const update: IVideoPlaylist = Object.assign({}, playlist);
            const event: SimpleAction<IVideoPlaylist> = new SimpleAction('update-logo', update);

            this.mutateDetail(event, 'main');
            this.mutateDetail(event, 'search');

            this.setFlag('isUpdateLogo', false);
        }

        this.playlistService
                .updateLogo(playlist._id, formData)
                .subscribe({ next })
    }

    mutate(event: SimpleAction<IVideoPlaylist>) {
        const { action, payload } = event;
        const isMutate: boolean = this.getFlag('isCanMutateList') as boolean;
        const pageIndex: number = this.storage.getItem('sessionStorage', 'videoPage');
        const isMutateList: boolean = pageIndex === 1;

        if(isMutate) {

            const currentList: MediaList<IVideoPlaylist> = this.getStreamValue('main$');

            switch(action) {

                case 'create': {
                    if(isMutateList) { 
                        currentList.rows.unshift(payload);
                        currentList.totalCount++;
                        this.main$.next(currentList);
                    }  
                    
                    return
                }
                
                case 'add': {
                    currentList.rows.unshift(payload);
                    currentList.totalCount++;
    
                    break;
                }
    
                case 'remove': {
                    const idx: number = currentList.rows.findIndex(v => v._id === payload._id);
                    currentList.rows.splice(idx, 1);
                    currentList.totalCount--;
                    
                    break;
                }
            }

            
            this.main$.next(currentList);
        }
        
        this.playlist$.next(Object.assign({}, payload))
        this.mutateDetail(event, 'main');
        this.mutateDetail(event, 'search');
    }

    mutateDetail(event: SimpleAction<IVideoPlaylist>, listType: 'main' | 'search'): void {

        const { action, payload } = event;

        const listName = `${listType}$` as 'main$' | 'search$';
        const mediaList: MediaList<IVideoPlaylist> = this.getStreamValue(listName);

        if(mediaList) {

            const idx: number = mediaList.rows.findIndex(pl => pl._id === payload._id);

            if(~idx) {
                mediaList.rows[idx] = Object.assign({}, payload);

                this[listName].next(mediaList);
            }       
        }
    }

    mutateTotal(event: SimpleAction<string>, listType: 'main$' | 'search$'): void {
        
        const mediaList: MediaList<IVideoPlaylist> = this.getStreamValue(listType);
        const { action, payload: playlistId } = event;

        if(mediaList) {
            const idx: number = mediaList.rows.findIndex(pl => pl._id === playlistId);

            if(~idx) {

                const candidate: IVideoPlaylist = mediaList.rows[idx];
                let update: IVideoPlaylist;
                
                switch(action) {

                    case 'multi-add': {
                        const VideoTotal = candidate.VideoTotal + 1;

                        update = Object.assign({}, candidate, { VideoTotal });

                        break;
                    }

                    case 'multi-remove': {
                        const VideoTotal = candidate.VideoTotal - 1;

                        update = Object.assign({}, candidate, { VideoTotal });

                        break;
                    }
                }

                mediaList.rows[idx] = update;

                this[listType].next(mediaList);

            }
        }
    }

    reset(): void {
        this.destroySubjects(['main$', 'search$', 'playlist$']);
        this.unsubscribe();
    }

    destroy() {
        this.reset();
        this.defaultFlagObject(new VideoPlaylistsFlags);
    }

}