import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { MediaRequest } from '../models/MediaRequest';
import { Observable } from 'rxjs';
import { IVideo } from '../interfaces/IVideo';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { PaginationResponse, MessageResponse } from '../types/request-response.types';
import { AnyOption } from '../types/shared.types';



@Injectable({
    providedIn: SystemServicesModule
})
export class VideoService {

    constructor(
        private http: HttpClient
    ) {}

    fetch(mediaRequest: MediaRequest): Observable<PaginationResponse<IVideo>> {
        const { playlistId } = mediaRequest;
        
        return this.http.post<PaginationResponse<IVideo>>(`@/video/fetch/${playlistId}`, mediaRequest);
    }

    getById(videoId: string, detail: AnyOption = {}): Observable<IVideo> {
        const params: HttpParams = new HttpParams({ fromObject: detail });

        return this.http.get<IVideo>(`@/video/get-by-id/${videoId}`, {params});
    }

    create(formData: FormData): Observable<IVideo> {
        return this.http.post<IVideo>('@/video/create', formData);
    }

    edit(id: string, edit: AnyOption): Observable<MessageResponse> {
        return this.http.patch<MessageResponse>(`@/video/edit/${id}`, edit);
    }

    add(playlistId: string, video: IVideo): Observable<MessageResponse> {
        return this.http.patch<MessageResponse>(`@/video/add/${playlistId}`, video);
    }

    remove(playlstId: string, video: IVideo) {
        const headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
        const httpOptions = { headers, body: video };

        return this.http.delete(`@/video/remove/${playlstId}`, httpOptions);
    }
}