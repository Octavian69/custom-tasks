import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subject, interval, Subscription } from 'rxjs';
import { mergeMap, startWith } from 'rxjs/operators';
import { ViewWorker } from '../shared/classes/view.worker';
import AuthDB from '../db/auth.db';
import { AuthService } from './auth.service';
import { StorageService } from './storage.service';
import { WeatherService } from './weather.service';
import { unsubscriber } from '../shared/handlers/handlers';
import { AccountService } from './account.service';
import { User } from '../models/User';
import { AuthFlags } from '../models/flags/AuthFlags';
import { Authenticate, Login } from '../types/auth.types';
import { Coordinate } from '../types/shared.types';
import { WeatherResponse } from '../types//request-response.types';

@Injectable()
export class LoginService extends ViewWorker<AuthFlags> {

    weather$: Subject<WeatherResponse> = new Subject();
    subs$: Subscription[] = [];

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private storage: StorageService,
        private authService: AuthService,
        private accountService: AccountService,
        private weatherService: WeatherService
    ) {
        super(new AuthFlags, AuthDB)
    }

    getLogged(): boolean {
       return this.storage.getItem<boolean>('sessionStorage', 'isLoggedIn');
    }

    logIn({ access_token, refresh_token }: Authenticate) {
        this.updateTokens(access_token, refresh_token);
        this.storage.setItem('sessionStorage', 'isLoggedIn', true);
        const OwnId: string = this.getOwnId();
        this.router.navigateByUrl(`/account/${OwnId}`);
    }

    logOut() {
        this.storage.reset();
        this.router.navigate(['/login']);

        unsubscriber(this.subs$);
        this.destroySubjects(['weather$']);
    }

    login(login: Login): void {
        this.setFlag('isLogin', true);
        const next = (authenticate: Authenticate) => {
            this.logIn(authenticate);
            this.setFlag('isLogin', false);
        };

        const error = () => this.setFlag('isLogin', false)

        this.authService.login(login)
        .subscribe({ next, error });
    }

    registration(user: User): void {
        this.setFlag('isRegistration', true);
        const next = ({ message }) => {
            this.toastr.success(message, 'Успешно!');
            this.setFlag('isRegistration', false);
            this.router.navigateByUrl('/login');
        };

        this.authService.registration(user).subscribe({ next })
    }

    remove(): void {
        const next = ({ message }) => {
            this.toastr.success(message, 'Успешно!');
            this.logOut();
        }

        this.accountService.remove().subscribe({ next });
    }

    getWeather(coordinate: Coordinate): void {
        const intervalTime: number = 1000 * 60 * 30; // раз в полчаса

        const sub$: Subscription = interval(intervalTime).pipe(
            startWith(0),
            mergeMap(_ => this.weatherService.fetch(coordinate))

        ).subscribe((response: any) => {
            // const { current: { condition: { text, icon }, temp_c } } = response;
            // http://api.weatherstack.com/current?access_key=d439caf4f2dcdef3eda34509b6a4ec44&query=55.8135653,37.7208579&language=ru
            // this.weather$.next({ temp_c, text, icon });

        });

        this.subs$.push(sub$);
    }
}