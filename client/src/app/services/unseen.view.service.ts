import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription, Subject, pipe } from 'rxjs';
import { IUnseen } from '../interfaces/IUnseen';
import { ViewWorker } from '../shared/classes/view.worker';
import { UnseenService } from './unseen.service';
import { UnseenEvent, UnseenKey } from '../types/unseen.types';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { UnseenFlags } from '../models/flags/UnseenFlags';
import { AnyOption } from '../types/shared.types';
import { takeUntil } from 'rxjs/operators';

@Injectable({
    providedIn: SystemServicesModule
})
export class UnseenViewService extends ViewWorker<UnseenFlags>{

    unseen$: BehaviorSubject<IUnseen> = new BehaviorSubject(null);
    reset$: Subject<UnseenKey> = new Subject();

    constructor(
        private unseenService: UnseenService
    ) {
        super(new UnseenFlags());
    }

    init(): void {
        const next = (unseen: IUnseen) => {
            this.unseen$.next(unseen);
        }

        this.unseenService
            .fetch()
            .pipe(takeUntil(this.unsubscriber$))
            .subscribe({ next });
    }

    ixExists(key: UnseenKey, referenceId: string) {
        const unseen: IUnseen = this.getStreamValue('unseen$');

        return unseen.keys[key][referenceId] ? true : false;
    }

    isDefault(key: UnseenKey): boolean {
        const unseen: IUnseen = this.getStreamValue('unseen$');

        return unseen && !!Object.keys(unseen.keys[key]).length ? true : false;
    }

    update(event: UnseenEvent): void {

        const unseen: IUnseen = this.getStreamValue('unseen$');
        
        if(!unseen) return;

        const { keys } = unseen;
        const { action, referenceId, key, value } = event;
       
        const state: AnyOption = Object.assign({}, keys[key]);

        if(action === 'add') {
            state[referenceId] ? state[referenceId] += value : state[referenceId] = value;
        } else {
            if(state[referenceId]) delete state[referenceId];
            else return 
        }

        const next = (unseen: IUnseen) => {
            this.unseen$.next(unseen);
        }

        this.unseenService.update({ [key]: state }).subscribe({ next });
    }

    reset(key: UnseenKey): void {
        
        this.setFlag('isReset', true);

        const next = (unseen: IUnseen) => {
            this.reset$.next(key);
            this.unseen$.next(unseen);
            this.setFlag('isReset', false);
        }

        this.unseenService.reset({key}).subscribe({ next });
    }

    destroy(): void {
        this.unsubscribe();
    }
}