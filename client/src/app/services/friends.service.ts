import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PaginationRequest } from '../models/PaginationRequest';
import { Observable } from 'rxjs';
import { IUser } from '../interfaces/IUser';
import { IIncompleteUser } from '../interfaces/IIncompleteUser';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { PaginationResponse, MessageResponse } from '../types/request-response.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class FriendsService {
    constructor(
        private http: HttpClient
    ){}
    
    fetch(request: PaginationRequest): Observable<PaginationResponse<IUser>> {
        return this.http.post<PaginationResponse<IUser>>(`@/friends/fetch/${request.referenceId}`, request);
    }

    fetchRequests(request: PaginationRequest): Observable<PaginationResponse<IUser>> {
        return this.http.post<PaginationResponse<IUser>>(`@/friends/fetch-requests/${request.referenceId}`, request);
    }

    fetchMutual(request: PaginationRequest): Observable<PaginationResponse<IUser>> {
        return this.http.post<PaginationResponse<IUser>>(`@/friends/fetch-mutual/${request.referenceId}`, request);
    }

    fetchIncomplete(request: PaginationRequest): Observable<PaginationResponse<IIncompleteUser>> {
        return this.http.post<PaginationResponse<IIncompleteUser>>(`@/friends/fetch-incomplete/${request.referenceId}`, request);
    }

    fetchOnline(request: PaginationRequest): Observable<PaginationResponse<IUser>> {
        return this.http.post<PaginationResponse<IUser>>(`@/friends/fetch-online/${request.referenceId}`, request);
    }

    add(userId: string): Observable<MessageResponse> {
        return this.http.patch<MessageResponse>(`@/friends/add/${userId}`, {});
    }

    remove(userId: string): Observable<MessageResponse> {
        return this.http.delete<MessageResponse>(`@/friends/remove/${userId}`);
    }

    requestToAdd(userId: string, action: 'add' | 'remove'): Observable<MessageResponse> {
        return this.http.patch<MessageResponse>(`@/friends/request-to-add/${userId}`, {action});
    }

    rejectToAdd(userId: string): Observable<MessageResponse> {
        return this.http.patch<MessageResponse>(`@/friends/reject-to-add/${userId}`, {});       
    }
}