import { Injectable } from '@angular/core';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { NoticeType } from '../types/notice.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class NoticeService {
    
    audio: HTMLAudioElement = new Audio();
    type: NoticeType = 'message';


    constructor() {
        this.audio.volume = 1;
        this.audio.src = '/assets/sounds/message.mp3';
    }

    play(type: NoticeType): void {
        if(type === this.type) {
            this.audio.play().then()
            return
        }

        this.type = type;
        let src: string = '/assets/sounds/';

        switch(this.type) {

            case 'message': {
                src += 'message.mp3';
                break;
            }
            case 'friend-request': {
                src += 'friends-request.mp3';
                break;
            }
        }

        this.audio.src = src;
        this.audio.play().then();
        
        
    }
}