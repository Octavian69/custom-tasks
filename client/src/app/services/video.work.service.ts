import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, UrlTree } from '@angular/router';
import { BehaviorSubject, Subscription, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { ViewWorker } from '../shared/classes/view.worker';
import VideoDB from '../db/video.db';
import { VideoWorkFlags } from '../models/flags/VideoWorkFlags';
import { IVideo } from '../interfaces/IVideo';
import { ICurrentMediaList } from '../interfaces/ICurrentMediaList';
import { CurrentMediaList } from '../models/CurrentMediaList';
import { MediaList } from '../models/MediaList';
import { MediaRequest } from '../models/MediaRequest';
import { isExpand } from '../shared/handlers/handlers';
import { VideoService } from './video.service';
import { CommentViewService } from './comment.view.service';
import { LikeViewService } from './like.view.service';
import { SimpleAction } from '../models/SimpleAction';
import { IMultiVideoList } from '../interfaces/IMultiVideoList';
import { Page } from '../models/Page';
import { VideoPlaylistService } from './video-playlist.service';
import { filterUnique } from '../shared/handlers/media.handlers';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { SipmleAction, AnyOption } from '../types/shared.types';
import { ResponseAction } from '../types/request-response.types';


@Injectable({
    providedIn: SystemServicesModule
})
export class VideoWorkService extends ViewWorker<VideoWorkFlags> {

    playlistId: string;

    currentVideo$: BehaviorSubject<IVideo> = new BehaviorSubject<IVideo>(null);
    currentList$: BehaviorSubject<ICurrentMediaList<IVideo>> = new BehaviorSubject<ICurrentMediaList<IVideo>>(null);
    multiPlaylists$: BehaviorSubject<MediaList<IMultiVideoList>> = new BehaviorSubject(null);
    mutate$: Subject<SipmleAction<IVideo | string>> = new Subject();

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private videoService: VideoService,
        private playlistService: VideoPlaylistService,
        private commentViewService: CommentViewService,
        private likeViewService: LikeViewService
    ) {
        super(new VideoWorkFlags, VideoDB);
        this.init();
    }

    async init() {
        const urlTree: UrlTree = this.router.parseUrl(this.router.url);
        const { queryParams } = urlTree;
        const video_id: string = queryParams.video_id;
        
        if(video_id) {
            const url: string = urlTree.toString()
            const isDetailPage: boolean = url.includes('video-playlist');

            const detail_playlist_id: string = 
            isDetailPage 
            ? url.match(/video-playlist[/]{1}[a-zA-Z0-9]+/)[0].split('/')[1]
            : null
            
            const detail = isDetailPage ? { detail_playlist_id } : {};
            
            this.videoService
                .getById(video_id, detail)
                .pipe(takeUntil(this.unsubscriber$))
                .subscribe((video: IVideo) => {
                    this.changeVideo(video);
                })
        }
    }

    getMultiLists(page: Page, action: ResponseAction) {
        this.setFlag('isMultiLoadRequest', true);

        const currentVideo: IVideo = this.getStreamValue('currentVideo$');

        const next = (response: MediaList<IMultiVideoList>) => {
            
            if(action === 'replace') {
                this.multiPlaylists$.next(response);
            } else {
            
                const mediaList: MediaList<IMultiVideoList> = this.getStreamValue('multiPlaylists$');
                const { rows, totalCount } = response;

                mediaList.rows = mediaList.rows.concat(rows);
                mediaList.totalCount = totalCount;

                this.multiPlaylists$.next(mediaList);

            }

            this.setFlag('isMultiLoadRequest', false);
        }

        this.playlistService.fetchByCreator(page, currentVideo._id)
                .pipe(
                    map((response: MediaList<IMultiVideoList>) => {

                        if(this.playlistId) {
                            const idx: number = response.rows.findIndex(pl => pl._id === this.playlistId);

                            if(~idx) {
                                response.rows.splice(idx, 1);
                                response.totalCount--;

                                // если открыт созданный плейлист, удаляем его из списка
                            }
                        }

                        return response;
                    }),
                    takeUntil(this.unsubscriber$)
                )
                .subscribe({ next });
    }

    createVideoList(playlistId: string, mediaList: MediaList<IVideo>, mediaRequest: MediaRequest): ICurrentMediaList<IVideo> {
        const videoList: ICurrentMediaList<IVideo> = new CurrentMediaList(playlistId, mediaList, mediaRequest);

        return videoList;
    }

    changeCurrentList(videoList: ICurrentMediaList<IVideo>): void {
        this.currentList$.next(videoList);
    }

    changeVideo(video: IVideo): void {

        if(video) {
            this.currentVideo$.next(video);
            this.router.navigate([], {
                relativeTo: this.activeRoute,
                queryParams: { video_id: video._id }
            })
        } else {
            this.destroy();
        }

    }

    editable(): boolean {
        const { IsHave, User } = this.getStreamValue('currentVideo$');

        return this.isOwn(User) && IsHave;
    }

    next(): void {
        const isExpandRequest: boolean = <boolean>this.getFlag('isExpand');
        const currentList: ICurrentMediaList<IVideo> = this.currentList$.getValue();
        const currentVideo: IVideo = this.currentVideo$.getValue();

        if(isExpandRequest) return;

        if(!currentList) {
            return this.changeVideo(currentVideo);
        }

        
        const {mediaList: { rows, totalCount } } = currentList;
        const idx: number = rows.findIndex(v => v._id === currentVideo._id);
        const isLastVideo = idx === rows.length - 1;
        const isExpand: boolean = rows.length < totalCount && isLastVideo;
        const isPlayFirstVideo: boolean = isLastVideo && !isExpand;

        switch(true) {
            case isExpand: {
                this.expand(true);
                break;
            }

            case isPlayFirstVideo: {
                this.changeVideo(rows[0]);
                break;
            }

            default: {
                const video: IVideo = rows[idx + 1];
                this.changeVideo(video);
            }
        }
    }

    expand(isChange?: boolean) {
        const currentList: ICurrentMediaList<IVideo>  = this.currentList$.getValue();
        const { mediaList, mediaRequest } = currentList;

        
        const expand: boolean = isExpand(mediaList);

        if(expand) {
            this.setFlag('isExpand', true);
            mediaRequest.page.skip++;

            const next = (response: MediaList<IVideo>) => {
                
                mediaList.rows.push(...response.rows);

                this.currentList$.next(currentList);

                if(isChange) {
                    const nextVideo: IVideo = response.rows[0] || mediaList.rows[0];
                    this.changeVideo(nextVideo);
                };

                this.setFlag('isExpand', false);
                
            }

            this.videoService
            .fetch(mediaRequest)
            .pipe(
                map((response: MediaList<IVideo>) => {
                    return filterUnique(mediaList, response, 'expand')
                })
            )
            .subscribe({ next });
        }
        
    }


    add(video: IVideo): void {
        this.setFlag('isMutate', true); 

        const next = () => {
            this.mutate('add', video);
            this.setFlag('isMutate', false);
        }

        this.videoService
            .add(this.playlistId, video)
            .subscribe({ next });
    }

    remove(video: IVideo): void {
        this.setFlag('isMutate', true);

        const next = () => {
            this.mutate('remove', video);
            this.setFlag('isMutate', false);
        }

        this.videoService
            .remove(this.playlistId, video)
            .subscribe({ next });
    }

    like(video: IVideo): void {
        this.likeViewService.like(video, 'Video');

        this.mutate('like', video);
    }

    edit(payload: AnyOption) {

        this.setFlag('isEditRequest', true);

        const currentVideo: IVideo = this.getStreamValue('currentVideo$');
        
        const next = () => {
            const update: IVideo = Object.assign({}, currentVideo, payload);
            
            this.mutate('edit', update);
            this.setFlag('isEditRequest', false);
        }

        this.videoService
            .edit(currentVideo._id, payload)
            .subscribe({ next });
    }

    multiMutate(action: 'add' | 'remove', mutatePlaylist: IMultiVideoList) {
        const video: IVideo = this.getStreamValue('currentVideo$');

        this.flags.multiRequestArray.push(mutatePlaylist._id);

        const next = () => {

            const method = `multi-${action}` as 'multi-add' | 'multi-remove';
            const event: SimpleAction<string> = new SimpleAction(method, mutatePlaylist._id);

            
            const multiMediaList: MediaList<IMultiVideoList> = this.getStreamValue('multiPlaylists$');

            const playlistIdx: number = multiMediaList.rows.findIndex(pl => pl._id === mutatePlaylist._id);
            const reqIdx: number = this.flags.multiRequestArray.findIndex(id => id === mutatePlaylist._id);
            
            const IsHave  = !mutatePlaylist.IsHave;
            const update: IMultiVideoList = Object.assign({}, mutatePlaylist, { IsHave });
            
            multiMediaList.rows[playlistIdx] = update;
            
            this.multiPlaylists$.next(multiMediaList)
            this.mutate$.next(event);
            
            if(~reqIdx) this.flags.multiRequestArray.splice(reqIdx, 1);

        }

        this.videoService[action](mutatePlaylist._id, video)
               .subscribe({ next });
    }

    mutateOwnList(action: 'add' | 'remove', payload: IVideo) {
        const ownId: string = this.getOwnId();

        const next = () => {
            payload.IsHaveOwn = !payload.IsHaveOwn; 
            this.updateCurrentList(payload);
        }

        this.videoService[action](ownId, payload)
                .subscribe({ next });
    
    }

    mutate(action: 'like' | 'add' | 'remove' | 'edit', payload: IVideo): void {

        let update: IVideo;

        switch(action) {

            case 'like': {
                update = this.likeViewService.mutate(payload);
                break;
            }

            case 'add': {
                update = Object.assign({}, payload, { IsHave: true });
                break;
            }

            case 'remove': {
                update = Object.assign({}, payload, { IsHave: false });
                break;
            }

            case 'edit': {
                update = Object.assign({}, payload);
                break;
            }
        }

        const event: SimpleAction<IVideo> = new SimpleAction(action, update);
        
        this.updateCurrentList(update);
        this.currentVideo$.next(update);
        this.mutate$.next(event)
    }

    updateCurrentList(payload: IVideo) {
        const currentList: ICurrentMediaList<IVideo> = this.getStreamValue('currentList$');

        if(currentList) {
            const idx: number = currentList.mediaList.rows.findIndex(v => v._id === payload._id);
            
            if(~idx) {
                currentList.mediaList.rows[idx] = Object.assign({}, payload);
                this.currentList$.next(currentList);
            }  
        }
    }

    reset(): void {
        this.destroySubjects(['currentVideo$', 'currentList$']);
        this.commentViewService.destroy();
        this.unsubscribe();
    }


    destroy(): void {
        this.router.navigate([], {relativeTo: this.activeRoute, queryParams: {}});
        this.reset();
    }
}