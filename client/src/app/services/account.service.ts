import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Info } from '../models/Info';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { MessageResponse } from '../types/request-response.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class AccountService {
    constructor(
        private http: HttpClient
    ) {}

    fetch(id: string): Observable<Info> {
        return this.http.get<Info>(`@/account/info/${id}`);
    }

    fetchPart<T>(part: string): Observable<T> {
        const fromObject = { part };
        const params: HttpParams = new HttpParams({
            fromObject
        });

        return this.http.get<T>('@/account/info/part', { params });
    }

    remove(): Observable<MessageResponse> {
        return this.http.delete<MessageResponse>('@/account/remove');
    }
}