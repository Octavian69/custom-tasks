import { Injectable } from '@angular/core';
import { ViewWorker } from '../shared/classes/view.worker';
import AudioDB from '../db/audio.db';
import { IAudioPlaylist } from '../interfaces/IAudioPlaylist';
import { AudioPlaylistsFlags } from '../models/flags/AudioPlaylistsFlags';
import { BehaviorSubject } from 'rxjs';
import { AudioPlaylistsService } from './audio-playlists.service';
import { AudioPlaylist } from '../models/AudioPlaylist';
import { deepCopy, isExpand } from '../shared/handlers/handlers';
import { AudioService } from './audio.service';
import { MediaRequest } from '../models/MediaRequest';
import { MediaList } from '../models/MediaList';
import { IAudio } from '../interfaces/IAudio';
import { LIMIT } from '../namespaces/Namespaces';
import { Location } from '@angular/common';
import { Page } from '../models/Page';
import { ToastrService } from 'ngx-toastr';
import { AudioWorkService } from './audio.work.service';
import { Router } from '@angular/router';
import { ICurrentMediaList } from '../interfaces/ICurrentMediaList';
import { filterUnique } from '../shared/handlers/media.handlers';
import { map, takeUntil } from 'rxjs/operators';
import { ResponseAction, MessageResponse } from '../types/request-response.types';
import { CreateAudioPlaylist } from '../types/audio.types';

@Injectable()
export class AudioPlaylistsViewService extends ViewWorker<AudioPlaylistsFlags> {
    
    playlists$: BehaviorSubject<MediaList<IAudioPlaylist>> = new BehaviorSubject(null);
    playlist$: BehaviorSubject<IAudioPlaylist> = new BehaviorSubject<IAudioPlaylist>(null);
    mediaList$: BehaviorSubject<MediaList<IAudio>> = new BehaviorSubject(null);
    limit: number = LIMIT.AUDIO;
    AMediaRequest: MediaRequest;
    
    constructor(
        private location: Location,
        private router: Router,
        private toastr: ToastrService,
        private playlistService: AudioPlaylistsService,
        private audioService: AudioService,
        private audioWorker: AudioWorkService
    ) {
        super(new AudioPlaylistsFlags, AudioDB);
    }
    
    fetch(userId: string, action: ResponseAction, page: Page) {

        const isExpand: boolean = Object.is(action, 'expand');
        const playlists: MediaList<IAudioPlaylist> = this.getStreamValue('playlists$');

        if(isExpand) this.setFlag('isExpandPlaylists', true);

        const next = (response: MediaList<IAudioPlaylist>) => {
            
            if(action === 'replace') {
                this.playlists$.next(response);
            } else {
                
                const { rows, totalCount } = response;

                playlists.rows = playlists.rows.concat(rows);
                playlists.totalCount = totalCount;

                this.playlists$.next(playlists);
            }

            if(isExpand) this.setFlag('isExpandPlaylists', false);
        }

        this.playlistService
        .fetch(userId, page)
        .pipe(
            map((response: MediaList<IAudioPlaylist>) => {
                return filterUnique(playlists, response, action)

            }),
            takeUntil(this.unsubscriber$)
        )
        .subscribe({ next });
    }

    init(playlistId: string, watchId?: string): void {
        if(playlistId !== '0') {
            
            this.AMediaRequest = new MediaRequest('main', playlistId, this.limit, { watchId: watchId || playlistId });

            this.getPlaylistById(playlistId);
            this.getAudioByPlaylistId(this.AMediaRequest, 'replace');

        } else {
            const playlist: IAudioPlaylist = new AudioPlaylist();

            this.playlist$.next(playlist);
            this.mediaList$.next(new MediaList([], 0));
        }
    }


    expandAudio(): void {
        const mediaList: MediaList<IAudio> = this.getStreamValue('mediaList$');
        
        const expand: boolean = isExpand(mediaList);

        if(expand) {
            this.AMediaRequest.page.skip++;
            this.getAudioByPlaylistId(this.AMediaRequest, 'expand');
        }
    
    }

    getPlaylistById(playlistId: string): void {
        const next = (playlist: IAudioPlaylist) => {
            this.playlist$.next(playlist);
        }

        this.playlistService
        .getById(playlistId)
        .pipe(takeUntil(this.unsubscriber$))
        .subscribe({ next });
    }

    getAudioByPlaylistId(mediaRequest: MediaRequest, action: ResponseAction): void {

        const isExpand = Object.is(action, 'expand');
        const { playlistId } = mediaRequest;
        const currentList: MediaList<IAudio> = this.getStreamValue('mediaList$');

        if(isExpand) this.setFlag('isExpandAudio', true);

        const next = (response: MediaList<IAudio>) => {

            if(action === 'replace') {
                this.mediaList$.next(response);
            } else {
                const { rows, totalCount } = response;
                currentList.rows = currentList.rows.concat(rows);
                currentList.totalCount = totalCount;

                this.mediaList$.next(currentList);
            }

            if(isExpand) this.setFlag('isExpandAudio', false);
           
        }

       this.audioService
       .fetch(playlistId, mediaRequest)
       .pipe(
            map((response: MediaList<IAudio>) => {
               return filterUnique(currentList, response, action)
            }),
            takeUntil(this.unsubscriber$)
        )
       .subscribe({ next });

    }

    updateMedia(audio: IAudio, action: 'add' | 'remove'): void {

        const currentMedia: MediaList<IAudio> = this.mediaList$.getValue();
        
        if(action === 'add') {
            currentMedia.rows.unshift(audio);
            currentMedia.totalCount++;
        } else {
            const idx: number = currentMedia.rows.findIndex(a => a._id === audio._id);
            currentMedia.rows.splice(idx, 1);
            currentMedia.totalCount--;
        }

        this.mediaList$.next(currentMedia);
    }

    async play(playlist: IAudioPlaylist) {
        const { _id } = playlist;
        const mediaRequest = new MediaRequest('main', _id, 30);
        
        const mediaList: MediaList<IAudio> = await this.audioService.fetch(_id, mediaRequest).toPromise();

        const audioList = this.audioWorker.createList(mediaList, mediaRequest, _id);

        this.audioWorker.changeCurrentList(audioList);

        this.audioWorker.playSong(mediaList.rows[0]);

    }

    changeSong(audio: IAudio): void {
        const mediaList: MediaList<IAudio> = this.mediaList$.getValue();
        
        const copyList: MediaList<IAudio> = deepCopy(mediaList);
        const mediaRequest: MediaRequest = deepCopy(this.AMediaRequest);

        const audioList: ICurrentMediaList<IAudio> = this.audioWorker.createList(copyList, mediaRequest, mediaRequest.playlistId);

        this.audioWorker.changeCurrentList(audioList);
        this.audioWorker.playSong(audio)

    }

    create(value: IAudioPlaylist, file: File): void {
        
        this.setFlag('isEditRequest', true);
        
        const current: IAudioPlaylist = this.playlist$.getValue();
        const candidate = Object.assign({}, current, value);
        const { rows } = this.mediaList$.getValue();
        const body: CreateAudioPlaylist = { playlist: candidate, audios: rows }

        let createCandidate: CreateAudioPlaylist | FormData;
        
        if(file) {
            const formData: FormData = new FormData();

            formData.append('logo', file, file.name);
            formData.append('body', JSON.stringify(body))

            createCandidate = formData;

        }  else {
            createCandidate = body;
        }

        const next = (playlist: IAudioPlaylist) => {
            this.location.back();
            this.playlist$.next(null);
            this.setFlag('isEditRequest', false);
            this.updatePlaylists('create', playlist);
        }

       this.playlistService
       .create(createCandidate)
       .subscribe({ next });
    }

    edit(playlist: IAudioPlaylist, file: File): void {
        
        this.setFlag('isEditRequest', true);

        const current: IAudioPlaylist = this.playlist$.getValue();
        const candidate = Object.assign({}, current, playlist);

        let body: FormData | IAudioPlaylist;

        if(file) {
            const formData: FormData = new FormData();
            
            formData.append('logo', file, file.name);
            formData.append('body', JSON.stringify(candidate));
            
            body = formData;

        } else {

            body = candidate;
        }

        const next = (update: IAudioPlaylist) => {
            const { Title } = update;
            
            this.toastr.success(`Плейлист ${Title} обновлен`, 'Успешно!');
            
            this.updatePlaylists('edit', update);
            this.location.back();
            this.playlist$.next(null);
            
            this.setFlag('isEditRequest', false);
        }

        this.playlistService.edit(body).subscribe({ next });
    }

    add(playlist: IAudioPlaylist): void {
        this.setFlag('isMutateRequest', playlist._id);
        
        const next = (response: MessageResponse) => {
            this.toastr.success(`Плейлист ${playlist.Title} добавлен.`, 'Успешно!');
            this.updatePlaylists('mutate', playlist);
            this.setFlag('isMutateRequest', null);
        }

        this.playlistService.add(playlist._id).subscribe({ next });
    }

    remove(playlist: IAudioPlaylist): void{
        this.setFlag('isMutateRequest', playlist._id);
     
        const next = (response: MessageResponse) => {
            const isOwn: boolean = this.isOwn(playlist.Creator)

            const action: 'remove' | 'mutate' = isOwn ? 'remove' : 'mutate';

            const messageAction: string = isOwn ? 'удален' : 'убран';

            this.toastr.success(`Плейлист ${playlist.Title} ${messageAction} из Вашего списка.`, 'Успешно');
            
            this.updatePlaylists(action, playlist);
            this.setFlag('isMutateRequest', null);
        }

        this.playlistService.remove(playlist).subscribe({ next });
    }

    updatePlaylists(action: 'remove' | 'create' | 'edit' | 'mutate', media: IAudioPlaylist): void {
        const currentLists: MediaList<IAudioPlaylist> = this.playlists$.getValue();

        if(currentLists) {
            switch(action) {
                case 'create': {
                    currentLists.rows.unshift(media);
                    currentLists.totalCount++;
                    break;
                }
                case 'remove': {
                    this.location.back();
                    const idx: number = currentLists.rows.findIndex(p => p._id === media._id);
    
                    currentLists.rows.splice(idx, 1);
                    currentLists.totalCount--;
                    break;
                }
                case 'edit': {
                    const idx: number = currentLists.rows.findIndex(p => p._id === media._id);
                    currentLists.rows[idx] = media;
                    break;
                }
    
                case 'mutate': {
                    const idx: number = currentLists.rows.findIndex(p => p._id === media._id);
    
                    const isDetail: boolean = this.router.url.includes('playlist-detail');

                    media.IsHave = !media.IsHave;
    
                    currentLists.rows[idx] = media;
    
                    if(isDetail) this.playlist$.next(media);
    
                    break;
                }
            }
    
            this.playlists$.next(currentLists);
        }
        

    }

    addAudioToPlaylist(audio: IAudio): void {
        const { _id: playlistId } = this.playlist$.getValue();
        const currentPlaylists: MediaList<IAudioPlaylist> = this.playlists$.getValue();

        this.audioWorker.setFlag('requestAudioId', audio._id);

        const next = (media: IAudio) => {
            const currentList: MediaList<IAudio> = this.mediaList$.getValue();

            const plIdx: number = currentPlaylists.rows.findIndex(p => p._id === playlistId);

            if(~plIdx) {
                currentPlaylists.rows[plIdx].TotalCount++;
                this.playlists$.next(currentPlaylists)
            }
            
            currentList.rows.unshift(media);
            currentList.totalCount++;

            this.audioWorker.setFlag('requestAudioId', null);
        }

        this.audioService.addAudioInPlaylist(playlistId, audio).subscribe({ next });
    }

    removeAudioFromPlaylist(audio: IAudio): void {
        const { _id: playlistId } = this.playlist$.getValue();

        this.audioWorker.setFlag('requestAudioId', audio._id);

        const next = (response: MessageResponse) => {
            const currentList: MediaList<IAudio> = this.mediaList$.getValue();
            const currentPlaylists: MediaList<IAudioPlaylist> = this.playlists$.getValue();

            const idx: number = currentList.rows.findIndex(a => a.Path === audio.Path);

            currentList.rows.splice(idx, 1);
            currentList.totalCount--;

            const plIdx: number = currentPlaylists.rows.findIndex(p => p._id === playlistId);

            if(~plIdx) {
                currentPlaylists.rows[plIdx].TotalCount--;
                this.playlists$.next(currentPlaylists)
            }

            this.mediaList$.next(currentList);
            

            this.audioWorker.setFlag('requestAudioId', null);
        }

        this.audioService.removeAudioFromPlaylist(playlistId, audio).subscribe({ next });
    }

    destroy(): void {
        this.destroySubjects(['playlists$', 'playlist$', 'mediaList$']);
        this.defaultFlagObject(new AudioPlaylistsFlags);
        this.unsubscribe();
    }

}