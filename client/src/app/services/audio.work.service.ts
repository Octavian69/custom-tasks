import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ViewWorker } from '../shared/classes/view.worker';
import AudioDB from '../db/audio.db';
import { ICurrentMediaList } from '../interfaces/ICurrentMediaList';
import { IAudio } from '../interfaces/IAudio';
import { AudioWorkFlags } from '../models/flags/AudioWorkFlags';
import { CurrentMediaList } from '../models/CurrentMediaList';
import { pathManager } from '../shared/handlers/handlers';
import { MediaRequest } from '../models/MediaRequest';
import { AudioService } from './audio.service';
import { MediaList } from '../models/MediaList';
import { filterUnique } from '../shared/handlers/media.handlers';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { PaginationResponse } from '../types/request-response.types';
import { AudioNavIcons } from '../types/audio.types';

@Injectable({
    providedIn: SystemServicesModule
})
export class AudioWorkService extends ViewWorker<AudioWorkFlags> {

    elem$: HTMLAudioElement;
    
    currentSong$: BehaviorSubject<IAudio> = new BehaviorSubject<IAudio>(null);
    currentList$: BehaviorSubject<ICurrentMediaList<IAudio>> = new BehaviorSubject<ICurrentMediaList<IAudio>>(null);

    constructor(
        private audioService: AudioService,
    ) {
        super(new AudioWorkFlags, AudioDB);
        this.initAudio();
    }

    initAudio(): void {
        const elem$: HTMLAudioElement = document.createElement('audio'); 

        elem$.style.display = 'none';
        document.body.append(elem$);
        
        this.elem$ = elem$;

        // this.elem$.addEventListener('volumechange', _ => {})

        this.initTimeUpdateListener();
    }

    timeUpdateListener = (e: Event) => {
        
        if(this.elem$.ended) {
            const side = this.getFlag('isRepeat') ? 'repeat' : 'next';
            const isExpandRequest: boolean = <boolean>this.getFlag('isExpandRequest');

            if(!isExpandRequest) {
                this.shiftAudioList(side);   
            }
             
        }
    }

    initTimeUpdateListener(): void {
        this.elem$.addEventListener('timeupdate', this.timeUpdateListener)
    }

    isRepeat(): boolean {
        return this.getFlag('isRepeat') as boolean;
    }


    getRepeatTooltip() {
        const isRepeat: boolean = this.isRepeat();

        if(isRepeat) {
          return 'Снять повторение'
        } else {
          return 'Поставить на повтор'
        }
      }

    repeat() {
        const isRepeat: boolean = this.getFlag('isRepeat') as boolean;

        this.setFlag('isRepeat', !isRepeat);
    }

    playback(): void {
        const { paused } = this.elem$;
        const action: 'pause' | 'play' = paused ? 'play' : 'pause';
        
        const promise = this.elem$[action]() as Promise<void>;

        if(action === 'play') {
            //Обход браузерной ошибки воспроизведения DOMException
            promise.then(null, e => e);
        }
    }

    getCurrentTime(audio: IAudio): number {
        const isCurrent: boolean = this.isCurrentSong(audio._id);
        
        return isCurrent ? this.elem$.currentTime : null;
    }

    getPlayStatus(audio?: IAudio): boolean {
        
        if(!audio) return this.elem$.paused;

        const isCurrent: boolean = this.isCurrentSong(audio._id);

        return isCurrent ? this.elem$.paused : true;
    } 

    getAddRequestStatus(audioId: string): boolean {
        const isRequest: boolean = this.getFlag('requestAudioId') === audioId;
        
        return isRequest
    }

    getVolumeIcon(): string {
    
        const { volume } = this.elem$;
        
        switch(true) {
            case !volume: {
                return 'volume_off';
            }
    
            case volume <= 0.5: {
                return 'volume_down';
            }
            
            default: {
                return 'volume_up';
            }
        }
    }

    getVolumeValue(): number {
        const { volume } = this.elem$;
    
        return Math.floor(volume * 100);
    }

    getNavIcons(type: 'own' | 'other', page: string): AudioNavIcons {
        const icons: AudioNavIcons = this.getDataByArrayKeys(['audio', 'navIcons', type, page]);

        return icons;
    }

    setVolume(volume: number): void {
        this.elem$.volume = volume;
    }

    range(value: number) {
        const { Duration } = this.currentSong$.getValue();

        const time: number =  Duration / 100 * value;

        this.elem$.currentTime = time;
    }

    changeCurrentList(audiolist: ICurrentMediaList<IAudio>): void {
        this.currentList$.next(audiolist);
    }

    createList(mediaList: MediaList<IAudio>, mediaRequest: MediaRequest, id: string): ICurrentMediaList<IAudio> {
        return new CurrentMediaList(id, mediaList, mediaRequest);
    }

    playSong(audio: IAudio) {
        this.playback();
        this.currentSong$.next(audio);
        this.elem$.currentTime = 0;
        this.elem$.setAttribute('src', pathManager(audio.Path));
        this.playback();
    }

    async shiftAudioList(side: 'prev' | 'next' | 'repeat') {
        const currentSong: IAudio = this.currentSong$.getValue();
        const currentAudioList = this.currentList$.getValue();
        const {mediaList: {rows: currentList, totalCount} } = currentAudioList;
        const currentIdx: number = currentList.findIndex((song: IAudio) => song._id === currentSong._id);
        
        if(!~currentIdx) return;

        let audio: IAudio;

        switch(side) {

            case 'repeat': {
                return this.playSong(currentSong);
            }

            case 'next': {
                
                const isLastInCurrentList: boolean = currentIdx === currentList.length - 1;
                const isLastInOverList: boolean = isLastInCurrentList && currentList.length < totalCount;

                if(isLastInOverList) {
                    this.setFlag('isExpandRequest', true);

                    audio = await this.expandList(currentAudioList);

                    this.setFlag('isExpandRequest', false);

                    break;
                }


                audio = isLastInCurrentList ? currentList[0] : currentList[currentIdx + 1];

                break;
            }

            case 'prev': {
                
                const isFirstInCurrentList: boolean = currentIdx === 0;
                const isFirstInOverList: boolean = currentIdx === 0 && currentList.length < totalCount;
                
                if(isFirstInOverList) return;
                 // Срабатывает при условии, 
                //когда есть попытка воспроизвести предыдущую песню,
                // при 0 индексе и отсутсвии полного списка песене в плейлисте

                audio = isFirstInCurrentList ? currentList[currentList.length - 1] : currentList[currentIdx - 1];
                break;
            }
        }

        if(audio)  this.playSong( audio );

    }

    expandList(audioList: ICurrentMediaList<IAudio>): Promise<IAudio> {
        audioList.mediaRequest.page.skip++;
        
        return this.getMediaMethod(audioList.mediaRequest)
        .pipe(
            map(response => {
                const { rows: currentRows, totalCount: currentTotal } = audioList.mediaList;
                const unique: MediaList<IAudio> = filterUnique({rows: currentRows, totalCount: currentTotal}, response, 'expand');

                const isExpand: boolean = unique.totalCount === currentTotal;

                if(isExpand) {
                    audioList.mediaList.rows = currentRows.concat(unique.rows);
                    audioList.mediaList.totalCount = unique.totalCount;
                    this.currentList$.next(audioList);
                }

                return isExpand ? response.rows[0] : audioList.mediaList.rows[0];
            })
        )
        .toPromise();
    }

    getMediaMethod(mediaRequest: MediaRequest): Observable<PaginationResponse<IAudio>> {
        const { state, playlistId } = mediaRequest;

        return state.methodType === 'by-string' 
                ? this.audioService.fetchByString(mediaRequest)
                : this.audioService.fetch(playlistId, mediaRequest);
    }

    
    isCurrentSong(id: string): boolean {
        const currentSong = this.currentSong$.getValue();

        if(currentSong) {
           return Object.is(id, currentSong._id);
        }
        
        return false;
    }

    isCurrentList(playlistId: string, type: 'main' | 'search'): boolean {
        const currenList: ICurrentMediaList<IAudio> = this.currentList$.getValue();

        if(currenList) {
            const { _id, mediaRequest: { listType } } = currenList;

            return Object.is(playlistId, _id) && Object.is(type, listType)
        }

        return false;
    }

    close(): void {
        this.elem$.pause();
        this.destroySubjects(['currentList$', 'currentSong$']);
        this.setFlag('isRepeat', false);
    }

    destroy(): void {
        this.elem$.pause();
        this.elem$.removeEventListener('timeupdate', this.timeUpdateListener)
        this.elem$.remove();
        this.destroySubjects(['currentList$', 'currentSong$']);
        this.defaultFlagObject(new AudioWorkFlags);
        this.unsubscribe();
    }
}