import { BehaviorSubject, Subscription, Subject } from 'rxjs';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ViewWorker } from '../shared/classes/view.worker'
import PictureDB from '../db/picture.db';
import { PictureWorkFlags } from '../models/flags/PictureWorkFlags'
import { PictureService } from './picture.service';
import { IPicture } from '../interfaces/IPicture';
import { SliderManager } from '../models/info/SliderManager';
import { StorageService } from './storage.service';
import { IPictureAlbum } from '../interfaces/IPictureAlbum';
import { MediaList } from '../models/MediaList';
import { LIMIT } from '../namespaces/Namespaces';
import { Page } from '../models/Page';
import { SimpleAction } from '../models/SimpleAction';
import { LikeViewService } from './like.view.service';
import { IUser } from '../interfaces/IUser';
import { SystemServicesModule } from '../shared/modules/system.services.module';
import { Injectable } from '@angular/core';
import { Side } from '../types/picture.types';


@Injectable({
    providedIn: SystemServicesModule
})
export class PictureWorkService extends ViewWorker<PictureWorkFlags> {

    state = {};
    sliderManager$: BehaviorSubject<SliderManager> = new BehaviorSubject(null);
    pictures$: BehaviorSubject<IPicture[]> = new BehaviorSubject(null);
    picture$: BehaviorSubject<IPicture> = new BehaviorSubject(null);
    mutate$: Subject<SimpleAction<IPicture>> = new Subject();

    constructor(
        private router: Router,
        private activeRoute: ActivatedRoute,
        private pictureService: PictureService,
        private likeViewService: LikeViewService,
        private storage: StorageService
    ) {
        super(new PictureWorkFlags, PictureDB);
    }

    async init(pictureId: string) {
        const picture: IPicture = this.getStreamValue('picture$');

        if(!picture) {
            const ownUser: IUser = this.storage.getItem('sessionStorage', 'ownUser');
            const manager: SliderManager = this.storage.getItem('sessionStorage', 'sliderManager');
            const isFirst: boolean = manager.pictureIndex === 0;
            const isOwnAvatarsAlbum = manager.albumType === 'Avatars' && this.isOwn(manager.albumId);
            const isCompareAvatar: boolean = !isFirst && isOwnAvatarsAlbum;

            if(isCompareAvatar) {
                const { Path } = await this.pictureService.fetchSlide(pictureId).toPromise(); 
                const isAvatar: boolean = ownUser.Avatar === Path;

                if(isAvatar) {
                    manager.page.skip = 0;
                    manager.pictureIndex = 0;
                    manager.currentIndex = 0;
                }
            }

            const pictures: IPicture[] = await this.getSlides(manager);
            const picture: IPicture = pictures.find(p => p._id === pictureId);
            
            this.sliderManager$.next(manager);
            this.pictures$.next(pictures);
            this.picture$.next(picture);

        }
    }

    changeSlide(picture: IPicture, album: IPictureAlbum, mediaList: MediaList<IPicture>): void {
        const pictureIndex: number = mediaList.rows.findIndex(p => p._id === picture._id);
        const skip: number = Math.floor((pictureIndex / LIMIT.PICTURE));
        const startIdx: number = skip * LIMIT.PICTURE;
        const currentIdx: number = pictureIndex - startIdx;
        const page: Page = new Page(skip, LIMIT.PICTURE);
        const { _id, Title, TotalCount, User, Type: albumType} = album;
        const { Type: pictureType } = picture;
        const IsOwnAlbum: boolean = this.isOwn(User)
        const pictures: IPicture[] = mediaList.rows.slice(startIdx, startIdx + LIMIT.PICTURE);
        const { queryParams } = this.activeRoute.snapshot;
        const query: Params = Object.assign({}, queryParams, { picture_id:  picture._id});
        const manager: SliderManager = new SliderManager(
                                        page, 
                                        currentIdx, 
                                        pictureIndex,
                                         _id, 
                                        albumType, 
                                        Title, 
                                        TotalCount, 
                                        pictureType,
                                        IsOwnAlbum);

        this.storage.setItem('sessionStorage', 'sliderManager', manager);
        this.pictures$.next(pictures);
        this.picture$.next(picture);
        this.sliderManager$.next(manager);
        this.router.navigate([], { relativeTo: this.activeRoute, queryParams: query });
    }

    async setSlide(side: Side) {
        const manager: SliderManager = this.getStreamValue('sliderManager$');
        const pictures: IPicture[] = this.getStreamValue('pictures$');
        const { currentIndex, totalCount, pictureIndex } = manager;
        
        const isLessThenLimit: boolean = totalCount <= LIMIT.PICTURE;

        let curIdx: number;
        let picIdx: number;
        let picture: IPicture;
        let updatePictures: IPicture[];
        
        if(isLessThenLimit) {
            const isFirstSlide: boolean = side === 'previous' && currentIndex === 0;
            const isLastSlide: boolean = side === 'next' && currentIndex === totalCount - 1;
            
            let idx: number;
            
            switch(true) {
                case isFirstSlide: {
                    idx = totalCount - 1;
                    break;
                }
                case isLastSlide: {
                    idx = 0;
                    break;
                }
                default: {
                    idx = side === 'next' ? currentIndex + 1 : currentIndex - 1;
                }
            }

            curIdx = idx;
            picIdx = idx;
            picture = pictures[idx];

        } else {
            const isFirstTotalSlide: boolean = side === 'previous' && pictureIndex === 0;
            const isLastTotalSlide: boolean = side === 'next' && pictureIndex + 1 === totalCount;
            const isFirstLimitSlide: boolean = side === 'previous' && currentIndex === 0;
            const isLastLimitSlide: boolean = side === 'next' && !pictures[currentIndex + 1];

            switch(true) {
                case isFirstTotalSlide: {
                    manager.page.skip = Math.floor(totalCount / LIMIT.PICTURE);
                    
                    if(totalCount % LIMIT.PICTURE === 0) manager.page.skip--;

                    updatePictures = await this.getSlides(manager);
                    
                    curIdx = updatePictures.length - 1;
                    picIdx = totalCount - 1;
                    picture = updatePictures[curIdx];

                    break;
                }

                case isLastTotalSlide: {
                    curIdx = 0;
                    picIdx = 0;
                    manager.page.skip = 0;

                    updatePictures = await this.getSlides(manager);
                    picture = updatePictures[curIdx];

                    break;
                }

                case isFirstLimitSlide: {
                    manager.page.skip--;
                    
                    updatePictures = await this.getSlides(manager);
                    
                    curIdx = updatePictures.length - 1;
                    picIdx = pictureIndex - 1;


                    picture = updatePictures[curIdx];
                    break;
                }

                case isLastLimitSlide: {
                    manager.page.skip++;
                    
                    updatePictures = await this.getSlides(manager);

                    curIdx = 0;
                    picIdx = pictureIndex + 1;
                    picture = updatePictures[curIdx];
                    break;
                }

                default: {
                    curIdx = side === 'next' ? currentIndex + 1 :  currentIndex - 1;
                    picIdx = side === 'next' ? pictureIndex + 1 : pictureIndex - 1;
                    picture = pictures[curIdx];
                }
            }
        }

       

                    
        manager.currentIndex = curIdx;
        manager.pictureIndex = picIdx;
        this.storage.setItem('sessionStorage', 'sliderManager', manager);

        const { queryParams } = this.activeRoute.snapshot;
        const query: Params = Object.assign({}, queryParams, { picture_id: picture._id});
        this.router.navigate([], { relativeTo: this.activeRoute, queryParams: query });
        
        this.sliderManager$.next(manager);
        this.picture$.next(picture);

        if(updatePictures) this.pictures$.next(updatePictures)
    }

    async getSlides(manager: SliderManager) {
        this.setFlag('isChangeRequest', true);

        const { page, albumId, albumType } = manager;
   
        const pictures: IPicture[] = await this.pictureService
        .fetchBySlider(albumId, page, albumType)
        .toPromise();
    
        this.setFlag('isChangeRequest', false);
    
        return pictures;
    }

    editable(): boolean {
        const picture: IPicture = this.getStreamValue('picture$');
        const ownId: string= this.getOwnId();

        return picture ? Object.is(ownId, picture.User) : false;
    }

    edit(Description: string): void {
        this.setFlag('isEditRequest', true);
        const picture: IPicture = this.getStreamValue('picture$');

        const next = () => {

            const update = Object.assign({}, picture, { Description });
            const event: SimpleAction<IPicture> = new SimpleAction('edit', update);
            
            this.picture$.next(update);
            this.mutate(event);
            this.mutate$.next(event);

            this.setFlag('isEditRequest', false);
        };

        this.pictureService
                .edit(picture._id, { Description })
                .subscribe({ next });
    }

    like(): void {
        const picture: IPicture = this.getStreamValue('picture$');

        this.likeViewService.like(picture, 'Picture');

        const update: IPicture = this.likeViewService.mutate(picture);
        const event: SimpleAction<IPicture> = new SimpleAction('like', update);

        this.picture$.next(update);
        this.mutate(event);
        this.mutate$.next(event);
    }

    save(picture: IPicture): void {
        this.setFlag('isSaveRequest', true);

        const next = () => {
            picture.IsHaveSaved = !picture.IsHaveSaved;

            const event: SimpleAction<IPicture> = new SimpleAction('save', picture);

            this.picture$.next(picture);
            this.mutate$.next(event);
            this.mutate(event);

            this.setFlag('isSaveRequest', false);
        }

        const sub$: Subscription = this.pictureService
                .save(picture)
                .subscribe({ next })
    }


    mutate(event: SimpleAction<IPicture>): void {
        const { action, payload } = event;
        const pictures: IPicture[] = this.getStreamValue('pictures$');
        const idx: number = pictures.findIndex(p => p._id === payload._id);

        if(~idx) {
            switch(action) {
                case 'edit': {
                    const update: IPicture = Object.assign({}, payload);
                    pictures[idx] = update;

                    break;
                }

                case 'like': {
                    const update: IPicture = Object.assign({}, payload);
                    pictures[idx] = update;

                    break;
                }
            }

            this.pictures$.next(pictures);
        }
    }

    destoy(): void {
        const { queryParams } = this.activeRoute.snapshot
        const query: Params = Object.assign({}, queryParams);

        delete query.picture_id;

        this.router.navigate([], {relativeTo: this.activeRoute, queryParams: query});
        this.destroySubjects(['sliderManager$', 'pictures$', 'picture$']);
        this.storage.removeItem('sessionStorage', 'sliderManager');
        this.defaultFlagObject(new PictureWorkFlags);
        this.state = {};
    }
    
}