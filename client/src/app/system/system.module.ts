import { NgModule } from "@angular/core";

//Modules
import { SystemRoutingModule } from '../routes/system.routing.module';
import { SharedModule } from '../shared/modules/shared.module';
import { SystemServicesModule } from '../shared/modules/system.services.module';

//Components
import { SystemPageComponent } from './system-page/system-page.component';

//Services
import { SystemGuard } from '../guards/system.guard';


@NgModule({
    declarations: [
        SystemPageComponent
    ],
    imports: [
        SystemRoutingModule,
        SharedModule,
        SystemServicesModule
    ],
    providers: [
        SystemGuard
    ]

})
export class SystemModule {}