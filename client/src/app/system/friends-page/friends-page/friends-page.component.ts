import { Component, OnInit, OnDestroy } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { unsubscriber } from 'src/app/shared/handlers/handlers';
import { FriendsViewService } from 'src/app/services/friends.view.service';

@Component({
  selector: 'cst-friends-page',
  templateUrl: './friends-page.component.html',
  styleUrls: ['./friends-page.component.scss']
})
export class FriendsPageComponent implements OnInit, OnDestroy {

  userId: string;
  isOwn: boolean = false;
  params: Params = { page: null };

  subs$: Subscription[] = [];

  constructor(
    private activeRoute: ActivatedRoute,
    private viewService: FriendsViewService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    unsubscriber(this.subs$);
    this.viewService.reset();
  }

  init(): void {
    this.initParamsSub();
    this.intQuerySub();
  }

  initParamsSub(): void {
    const sub$: Subscription = this.activeRoute.params.subscribe((params: Params) => {
      const { id } = params;

      this.viewService.reset();
      this.userId = id;
      this.isOwn = this.viewService.isOwn(id);
    });

    this.subs$.push(sub$);
  }

  intQuerySub(): void {
    const sub$: Subscription = this.activeRoute.queryParams.subscribe((params: Params) => {
      this.params = params;
    });

    this.subs$.push(sub$);
  }

}
