import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { FriendsViewService } from 'src/app/services/friends.view.service';
import { Observable, Subscription } from 'rxjs';
import { MediaList } from 'src/app/models/MediaList';
import { IUser } from 'src/app/interfaces/IUser';
import { unsubscriber, isExpand } from 'src/app/shared/handlers/handlers';
import { PaginationRequest } from 'src/app/models/PaginationRequest';
import { Page } from 'src/app/models/Page';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { MessageViewService } from 'src/app/services/message.view.service';
import { FriendsFlags } from 'src/app/models/flags/FriendsFlags';
import { filter } from 'rxjs/operators';
import { StorageService } from 'src/app/services/storage.service';
import { SocketService } from 'src/app/services/socket.service';
import { SOKET_EVENTS } from 'src/app/namespaces/socket-events.namespaces';
import { FriendState, FriendsRequest } from 'src/app/types/friends.types';
import { UnseenViewService } from 'src/app/services/unseen.view.service';
import { ResponseAction } from 'src/app/types/request-response.types';

@Component({
  selector: 'cst-friends-requests',
  templateUrl: './friends-requests.component.html',
  styleUrls: ['./friends-requests.component.scss']
})
export class FriendsRequestsComponent implements OnInit, OnDestroy {

  users$: Observable<MediaList<IUser>> = this.viewService.getStream('main$');

  modeType: FriendsRequest;
  mediaRequest: PaginationRequest;
  
  requestArray: string[] = this.viewService.getField('requestArray');
  flags: FriendsFlags = this.viewService.getFlagObject();

  subs$: Subscription[] = [];

  @Input('userId') userId: string;
  @Input('isOwn') isOwn: boolean;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private viewService: FriendsViewService,
    private messageViewService: MessageViewService,
    private socketService: SocketService,
    private unseenService: UnseenViewService,
    private storage: StorageService,
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    unsubscriber(this.subs$);
    this.viewService.destroySubjects(['main$']);
    this.storage.removeItem('sessionStorage', 'friendsReqQuery');
  }

  init(): void {
    this.modeType = this.getModeType();
    this.initActiveRouteSub();
    this.initFriendsSocketSub();
    this.navigate(this.modeType);
  }

  initActiveRouteSub(): void {
    const sub$: Subscription = this.activeRoute
    .queryParamMap
    .pipe(
      filter((params: ParamMap) => !!params.get('type'))
    )
    .subscribe((params: ParamMap) => {
      const type = params.get('type') as FriendsRequest;

      this.viewService.destroySubjects(['main$']);
      this.updateUnseen(type);
      this.createRequest();
      this.fetch('replace');
    });

    this.subs$.push(sub$);
  }

  initFriendsSocketSub(): void {
    const sub$: Subscription = this.socketService
    .getListener(SOKET_EVENTS.FRIENDS_CHANGE_STATE)
    .subscribe((event: FriendState) => {
        const { action, User } = event;
        const user: IUser = this.viewService.getUserById(User);

        const isAddFriend: boolean = action === 'addFriend' && this.modeType === 'outgoing';
        const isRejectToFriend: boolean = action === 'rejectToAddFriend' && this.modeType === 'outgoing';
        const isRequestToAdd: boolean = action === 'requestToAddFriend' && this.modeType === 'incoming';
        const isRemoveRequest: boolean = isRequestToAdd && event.state.status === 'remove';
        const isGetNewUser: boolean = isRequestToAdd && event.state.status === 'add' && !user;
        
        if(isAddFriend || isRejectToFriend || isRemoveRequest) {
          this.viewService.filter(User);
        }

        if(isGetNewUser) {
            this.viewService.getNewFriend(User);
        }
      
    });

    this.subs$.push(sub$);
  }

  updateUnseen(type: FriendsRequest): void {
    if(type === 'incoming') {
      this.unseenService.reset('friendsRequests');
    }
  }

  getModeType(): FriendsRequest {
    return this.modeType 
    || this.storage.getItem('sessionStorage','friendsReqQuery') 
    || 'incoming';
  }

  getEmptyText(): string {
    const type: string = this.modeType === 'incoming' ? 'Входящих' : 'Исходящих';
    return `${type} заявок нет.`
  }

  getLinkStyles(type: FriendsRequest) {
    const isEqual = this.modeType === type;

    return {
      'active': isEqual,
      'box-shadow': isEqual,
      'deactive': !isEqual
    }
  }

  isRequest(userId: string): boolean {
    return this.requestArray.includes(userId)
  }

  trackByFn(index: number, user: IUser): string {
    return user._id;
  }

  fetch(action: ResponseAction): void {
    this.viewService.fetch('fetchRequests', this.mediaRequest, action);
  }

  createRequest(): void {
    const ownId: string = this.viewService.getOwnId();
    this.mediaRequest = new PaginationRequest(ownId, new Page(0, LIMIT.USERS), { type: this.modeType, listType: 'main' })
  } 

  navigate(type: FriendsRequest): void {
    const { queryParams } = this.activeRoute.snapshot;

    this.modeType = type;
    this.storage.setItem('sessionStorage', 'friendsReqQuery', type);
    this.router.navigate([], {
      relativeTo: this.activeRoute,
      queryParams: { ...queryParams, type }
    })
  }

  message(user: IUser): void {
    this.messageViewService.initSingleMessage(user);
  }

  removeRequest(user: IUser): void {
    this.viewService.requestToAdd(user._id);
  }

  reject(user: IUser): void {
    this.viewService.rejectToAdd(user._id);
  }

  add(user: IUser): void {
    this.viewService.add(user._id);
  }

  expand(): void {
    const mediaList: MediaList<IUser> = this.viewService.getStreamValue('main$');

    const expand: boolean = isExpand(mediaList);

    if(expand) {
      this.mediaRequest.page.skip++;
      this.fetch('expand');
    }
  }

}
