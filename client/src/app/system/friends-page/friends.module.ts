import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { FriendsRoutingModule } from './friends.routing.module';
import { FriendsPageComponent } from './friends-page/friends-page.component';
import { FriendsMainListComponent } from './friends-main-list/friends-main-list.component';
import { FriendsRequestsComponent } from './friends-requests/friends-requests.component';
import { FriendsMutualComponent } from './friends-mutual/friends-mutual.component';
import { FriendsNavComponent } from './friends-nav/friends-nav.component';
import { FriendsViewService } from 'src/app/services/friends.view.service';
import { FriendsCardComponent } from './friends-card/friends-card.component';

@NgModule({
    declarations: [
        FriendsPageComponent,
        FriendsMainListComponent,
        FriendsRequestsComponent,
        FriendsMutualComponent,
        FriendsNavComponent,
        FriendsCardComponent
    ],
    imports: [
        SharedModule,
        FriendsRoutingModule
    ],
    providers: [
        FriendsViewService
    ]
})
export class FriendsModule {}