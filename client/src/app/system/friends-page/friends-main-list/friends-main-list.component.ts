import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { FriendsViewService } from 'src/app/services/friends.view.service';
import { FriendsFlags } from 'src/app/models/flags/FriendsFlags';
import { PaginationRequest } from 'src/app/models/PaginationRequest';
import { MediaList } from 'src/app/models/MediaList';
import { IUser } from 'src/app/interfaces/IUser';
import { isExpand, unsubscriber } from 'src/app/shared/handlers/handlers';
import { Page } from 'src/app/models/Page';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { MessageViewService } from 'src/app/services/message.view.service';
import { SocketService } from 'src/app/services/socket.service';
import { SOKET_EVENTS } from 'src/app/namespaces/socket-events.namespaces';
import { FriendState } from 'src/app/types/friends.types';
import { ResponseAction } from 'src/app/types/request-response.types';

@Component({
  selector: 'cst-friends-main-list',
  templateUrl: './friends-main-list.component.html',
  styleUrls: ['./friends-main-list.component.scss']
})
export class FriendsMainListComponent implements OnInit, OnDestroy {

  userId: string;
  
  main$: Observable<MediaList<IUser>> = this.viewService.getStream('main$');
  search$: Observable<MediaList<IUser>> = this.viewService.getStream('search$');
  
  mainMediaReq: PaginationRequest;
  searchMediaReq: PaginationRequest;

  searchControl: FormControl = new FormControl(null, [], []);

  flags: FriendsFlags = this.viewService.getFlagObject();
  subs$: Subscription[] = [];

  @Input('userId') set _userId(id: string) {
      this.userId = id;
      this.searchControl.patchValue(null, { emitEvent: false });
      this.searchMediaReq = null;
      this.init();
  }
  @Input('isOwn') isOwn: boolean;

  constructor(
    private viewService: FriendsViewService,
    private messageViewService: MessageViewService,
    private socketService: SocketService
  ) { }

  ngOnInit() {
     this.initFriendsSokcetSub();
  }

  ngOnDestroy() {
    this.viewService.destroySubjects(['main$', 'search$']);
    unsubscriber(this.subs$);
  }

  init(): void {
    this.createRequest('main');
    this.fetch('main', 'replace');
    this.initSearchSub();
  }

  initSearchSub(): void {
    const sub$: Subscription = this.searchControl.valueChanges
    .pipe(debounceTime(400))
    .subscribe((value: string) => {
      if(value) {
        if(!this.searchMediaReq) this.createRequest('search');

        this.mainMediaReq.filters.Name = value;
        this.searchMediaReq.filters.Name = value;

        this.mainMediaReq.page.skip = 0;
        this.searchMediaReq.page.skip = 0;

        this.fetch('search', 'replace');

      } else {
        this.searchMediaReq = null;
        this.mainMediaReq.page.skip = 0;
        delete this.mainMediaReq.filters.Name;
      }

      this.fetch('main', 'replace');
    });

    this.subs$.push(sub$);
  }

  initFriendsSokcetSub(): void {
    const sub$: Subscription = this.socketService
    .getListener(SOKET_EVENTS.FRIENDS_CHANGE_STATE)
    .subscribe((event: FriendState) => {
      const isOwn: boolean = this.viewService.isOwn(this.userId);
      if(isOwn) {
        this.viewService.mutateMainDetail(event);
      }
    });

    this.subs$.push(sub$);
  }

  fetch(listType: 'main' | 'search', action: ResponseAction): void {
    const reqName = this.getRequestName(listType);

    this.viewService.fetch('fetch', this[reqName], action);
  }

  createRequest(listType: 'main' | 'search'): void {
    const reqName = this.getRequestName(listType);
    
    this[reqName] = new PaginationRequest(this.userId, new Page(0, LIMIT.USERS), { listType });
  }

  trackByFn(index: number, user: IUser): string {
    return user._id;
  }

  getRequestName(listType: 'main' | 'search'): 'mainMediaReq' | 'searchMediaReq' {
    return `${listType}MediaReq` as 'mainMediaReq' | 'searchMediaReq';
  }

  getFlagName(listType: 'main' | 'search'): 'isMainRequest' | 'isSearchRequest' {
    return listType === 'main' ? 'isMainRequest' : 'isSearchRequest';
  }

  getDisabledExpand(): boolean {
    const isMainRequest = this.viewService.getFlag('isMainRequest') as boolean;
    const isSearchRequest = this.viewService.getFlag('isSearchRequest') as boolean;

    return isMainRequest || isSearchRequest;
  }

  message(user: IUser): void {
    this.messageViewService.initSingleMessage(user);
  }

  ending(): void {
    this.expand('main');

    if(this.searchMediaReq) {
      this.expand('search');
    }
  }

  expand(listType: 'main' | 'search'): void {
    const listName = `${listType}$` as 'main$' | 'search$';
    const flagName = this.getFlagName(listType)
    const mediaList: MediaList<IUser> = this.viewService.getStreamValue(listName);

    const expand: boolean = isExpand(mediaList);
    const isRequest: boolean = this.viewService.getFlag(flagName) as boolean;

    if(expand && !isRequest) {
      const reqName = this.getRequestName(listType);

      this[reqName].page.skip++;

      this.fetch(listType, 'expand');
    }
  }
}
