import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FriendsViewService } from 'src/app/services/friends.view.service';
import { PageLink } from 'src/app/types/request-response.types';
import { StatsOption } from 'src/app/types/friends.types';
import { MediaNavComponent } from 'src/app/shared/components/media-nav/media-nav.component';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'cst-friends-nav',
  templateUrl: './friends-nav.component.html',
  styleUrls: ['./friends-nav.component.scss']
})
export class FriendsNavComponent implements OnInit {
  options: PageLink[];
  isSetOptions: boolean = false;
  userId: string;
  isOwn: boolean;

  @ViewChild('mediaNavRef', { read: MediaNavComponent, static: false }) mediaNavRef: MediaNavComponent

  @Input('userId') set _userId(id: string) {
    this.userId = id
  };
  @Input('isOwn') set _isOwn(val: boolean) {
    this.isOwn = val;

    this.initOptions();
  }

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private viewService: FriendsViewService,
    private storage: StorageService
  ) { }

  ngOnInit() {
    this.init();
  }

  init(): void {
    this.initOptions();
  }

  initOptions(): void {

    this.isSetOptions = true;

    const options: PageLink[] = this.viewService.getDataByArrayKeys(['friends', 'navList']);
    
    this.options = options.filter((link: PageLink) => {
      const { page } = link;
      const exceptionPage: 'requests' | 'mutual' = this.isOwn ? 'mutual' : 'requests';
      
      return page !== exceptionPage;
    });

    setTimeout(_ => { 
      this.isSetOptions = false;
    }, 0);
  }

  navigateStats(option: StatsOption): void {
    const { pageName } = option;

    switch(pageName) {
      case 'friendsPage': {
        const currentLineActiveIdx: number = this.mediaNavRef.lineRef$.getCurrentIdx();

        if(currentLineActiveIdx !== 0) {
          this.mediaNavRef.lineRef$.setCurrentIdx(0);
        }

        break;
      }
    }
  }

  setPage(link: PageLink): void {
    const queryParams: Params = {page: link.page};

    this.router.navigate([], {
      relativeTo: this.activeRoute,
      queryParams
    })
  }
}
