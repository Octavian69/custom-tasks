import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FriendsViewService } from 'src/app/services/friends.view.service';
import { MessageViewService } from 'src/app/services/message.view.service';
import { IUser } from 'src/app/interfaces/IUser';
import { MediaList } from 'src/app/models/MediaList';
import { Observable } from 'rxjs';
import { PaginationRequest } from 'src/app/models/PaginationRequest';
import { Page } from 'src/app/models/Page';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { isExpand } from 'src/app/shared/handlers/handlers';
import { FriendsFlags } from 'src/app/models/flags/FriendsFlags';
import { ResponseAction } from 'src/app/types/request-response.types';

@Component({
  selector: 'cst-friends-mutual',
  templateUrl: './friends-mutual.component.html',
  styleUrls: ['./friends-mutual.component.scss']
})
export class FriendsMutualComponent implements OnInit, OnDestroy {

  users$: Observable<MediaList<IUser>> = this.viewService.getStream('main$');
  flags: FriendsFlags = this.viewService.getFlagObject();
  mediaRequest: PaginationRequest;

  @Input('userId') userId: string;
  @Input('isOwn') isOwn: boolean;

  constructor(
    private viewService: FriendsViewService,
    private messageViewService: MessageViewService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.viewService.destroySubjects(['main$']);
  }

  init(): void {
    this.createRequest();
    this.fetch('replace');
  }

  fetch(action: ResponseAction): void {
    this.viewService.fetch('fetchMutual', this.mediaRequest, action);
  }

  createRequest(): void {
    this.mediaRequest = new PaginationRequest(this.userId, new Page(0, LIMIT.USERS), { listType: 'main' });
  }

  message(user: IUser): void {
    this.messageViewService.initSingleMessage(user);
  }

  expand(): void {
    const mediaList: MediaList<IUser> = this.viewService.getStreamValue('main$');

    const expand: boolean = isExpand(mediaList);

    if(expand) {
      this.mediaRequest.page.skip++;
      this.fetch('expand');
    }
  }

}
