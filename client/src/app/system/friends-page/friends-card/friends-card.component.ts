import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import EditDB from '../../../db/edit.db';
import { IUser } from 'src/app/interfaces/IUser';
import { getDataByArrayKeys, completeInfoRows } from 'src/app/shared/handlers/handlers';
import { Router, Params } from '@angular/router';
import { formatDate } from '@angular/common';
import { getFilledStats, completeStats } from 'src/app/shared/handlers/friends.handlers';
import { StatsOption, FriendsRequest } from 'src/app/types/friends.types';
import { InfoRow } from 'src/app/types/account.types';

@Component({
  selector: 'cst-friends-card',
  templateUrl: './friends-card.component.html',
  styleUrls: ['./friends-card.component.scss']
})
export class FriendsCardComponent implements OnInit {

  stats: StatsOption[] = [];
  info: InfoRow[] = [];
  
  @Input('modeType') modeType: FriendsRequest;
  @Input('user') user: IUser;
  @Input('request') request: boolean= false;

  @Output('message') _message = new EventEmitter<IUser>();
  @Output('removeRequest') _removeRequest = new EventEmitter<IUser>();
  @Output('reject') _reject = new EventEmitter<IUser>();
  @Output('add') _add = new EventEmitter<IUser>();

  constructor(
    private router: Router
  ) {}

  ngOnInit() {
    this.initStatsList();
    this.initInfoList();
  }

  initStatsList(): void {
    const filled: string[] = getFilledStats(this.user);

    if(filled.length) {
      this.completeStats(filled);
    } 
  }

  initInfoList(): void {
    const filled: [string, any][] = Object.entries(this.user.MainInfo).filter(([key, value]) => {
      if(value && String(value)) return true
      else false
    });

    if(filled.length) {
      this.completeInfo();
    }
  }

  trackByFn(index: number): number {
    return index;
  }

  completeStats(filled: string[]): void {
    this.stats = completeStats(filled, this.user);
  }

  completeInfo(): void {
    const configuration: any = getDataByArrayKeys(['edit', 'forms', 'Main'], EditDB);
    this.info = completeInfoRows(this.user.MainInfo, configuration).map((row: InfoRow) => {

      row.value = row.type === 'date' ? formatDate(row.value, 'mediumDate', 'ru') : String(row.value);

      return row;
    });    
  }

  navigate(path: string, query?: Params): void {
    this.router.navigate([path, this.user._id], { queryParams: query || {} });
  }

  message(): void {
    this._message.emit(this.user);
  }

  removeRequest(): void {
    this._removeRequest.emit(this.user);
  }

  reject(): void {
    this._reject.emit(this.user);
  }

  add(): void {
    this._add.emit(this.user);
  }

}
