import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FriendsPageComponent } from './friends-page/friends-page.component';

const routes: Routes = [
    {path: '', component: FriendsPageComponent, children: [
        
    ]}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FriendsRoutingModule {}