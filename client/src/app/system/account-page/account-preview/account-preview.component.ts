import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Router, Params } from '@angular/router';
import { AccountViewService } from 'src/app/services/account.view.service';
import { PictureWorkService } from 'src/app/services/picture.work.service';
import { AccountFlags } from 'src/app/models/flags/AccountFlags';
import { PictureService } from 'src/app/services/picture.service';
import { PictureAlbumsService } from 'src/app/services/picture-albums.service';
import { IPictureAlbum } from 'src/app/interfaces/IPictureAlbum';
import { IPicture } from 'src/app/interfaces/IPicture';
import { Page } from 'src/app/models/Page';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { MediaList } from 'src/app/models/MediaList';
import { IUser } from 'src/app/interfaces/IUser';
import { MessageViewService } from 'src/app/services/message.view.service';
import { ToastrService } from 'ngx-toastr';
import { SocketService } from 'src/app/services/socket.service';
import { Subscription, Observable } from 'rxjs';
import { unsubscriber } from 'src/app/shared/handlers/handlers';
import { FriendState } from 'src/app/types/friends.types';
import { SOKET_EVENTS } from 'src/app/namespaces/socket-events.namespaces';
import { IIncompleteUser } from 'src/app/interfaces/IIncompleteUser';

@Component({
  selector: 'cst-account-preview',
  templateUrl: './account-preview.component.html',
  styleUrls: ['./account-preview.component.scss']
})
export class AccountPreviewComponent implements OnInit, OnDestroy {
  
  previewFriends$: Observable<MediaList<IIncompleteUser>> = this.viewService.getStream('previewFriends$');
  previewOnlineFriends$: Observable<MediaList<IIncompleteUser>> = this.viewService.getStream('previewOnlineFriends$');

  flags: AccountFlags = this.viewService.getFlagObject();
  isEmpty: boolean = false;

  subs$: Subscription[] = []
  
  @Input('user') user: IUser;
  @Output('showCreate') _showCreate = new EventEmitter<void>();
  @Output('showOnline') _showOnline = new EventEmitter<void>();
  @Output('slide') _slide = new EventEmitter<void>()

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private viewService: AccountViewService,
    private pictureWorker: PictureWorkService,
    private pictureService: PictureService,
    private albumService: PictureAlbumsService,
    private messageViewService: MessageViewService,
    private socketService: SocketService
  ) { }

  ngOnInit() {
    this.initChangeFriendsStateSub();
  }

  ngOnDestroy() {
    unsubscriber(this.subs$);
  }

  initChangeFriendsStateSub(): void {
    const sub$: Subscription = this.socketService
    .getListener(SOKET_EVENTS.FRIENDS_CHANGE_STATE)
    .subscribe((payload: FriendState) => {
        const isCurrentUser: boolean = payload.User === this.user._id;

        if(isCurrentUser) {
          this.viewService.mutateDetail(payload);
        }
    });

    this.subs$.push(sub$);
  }

  getOutgoingText(): string {
    return this.user.IsOutgoingRequest ? 'Отменить заявку' : 'Добавить в друзья';
  }

  navigate() {
    this.router.navigate(['/edit'], { queryParams: { page: 'Main' } });
  }

  previewUsersAction(action: 'online' | 'all'): void {
    if(action === 'all') {
      const queryParams: Params = { page: 'main' };

      this.router.navigate(['/friends', this.user._id], { queryParams });
    } else {
      this._showOnline.emit()
    }
  }

  async slide() {
    const isInitSlider: boolean = this.viewService.getFlag('isInitSlider') as boolean;

    if(isInitSlider || this.isEmpty) return;
    this.viewService.setFlag('isInitSlider', true);

    const page: Page = new Page(0, LIMIT.PICTURE);
    const album: IPictureAlbum = await this.albumService.fetchById(this.user._id).toPromise();
    const pictures: IPicture[] = await this.pictureService.fetchBySlider(this.user._id, page, album.Type).toPromise();
    const mediaList: MediaList<IPicture> = new MediaList(pictures, album.TotalCount);

    if(pictures.length) {
      this.pictureWorker.changeSlide(pictures[0], album, mediaList);
    } else {
      this.isEmpty = true;
      this.toastr.info('Альбом пуст', 'Информация')
    }
    
    this.viewService.setFlag('isInitSlider', false);
  }

  showCreateModal(e: Event): void {
    e.stopPropagation();
    this._showCreate.emit();
  }

  message(): void {
    this.messageViewService.initSingleMessage(this.user);
  }

  removeFriend(): void {
    this.viewService.removeFriend();
  }

  requestToAdd(): void {
    this.viewService.requestToAdd();
  }

  add(): void {
    this.viewService.addFriend();
  }

  rejectFriend(): void {
    this.viewService.rejectFriend();
  }
}
