import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IIncompleteUser } from 'src/app/interfaces/IIncompleteUser';
import { MediaList } from 'src/app/models/MediaList';
import { PreviewFriends } from 'src/app/types/friends.types';
import { Router } from '@angular/router';
import { LIMIT } from 'src/app/namespaces/Namespaces';

@Component({
  selector: 'cst-account-preview-users',
  templateUrl: './account-preview-users.component.html',
  styleUrls: ['./account-preview-users.component.scss']
})
export class AccountPreviewUsersComponent {
  
  @Input('modeType') modeType: PreviewFriends;
  @Input('users') users: MediaList<IIncompleteUser>;
  @Output('action') _action: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    private router: Router
  ) { }

  action() {
    const isDisabled: boolean = this.disable();

    if(!isDisabled) {
      this._action.emit();
    }
  }

  disable(): boolean {
    return this.users.totalCount < LIMIT.USERS_PREVIEW;
  }

  navigate(user: IIncompleteUser): void {
    this.router.navigate(['/account', user._id]);
  }

  getText(): string {
    const text: string = 'Друзья';

    return this.modeType === 'online' ? `${text} онлайн` : text;
  }

  getTotal() {
    return `(${this.users.totalCount})`
  }

}
