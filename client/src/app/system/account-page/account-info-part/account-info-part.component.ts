import { Component, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { fade } from 'src/app/shared/animations/animation';
import * as moment from 'moment';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { InfoPart } from 'src/app/types/account.types';



@Component({
  selector: 'cst-account-info-part',
  templateUrl: './account-info-part.component.html',
  styleUrls: ['./account-info-part.component.scss'],
  animations: [fade],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountInfoPartComponent {

  isShowEdit: boolean = false;
  
  @Input('part') part: InfoPart;
  @Input('showRow') showRow: boolean = true;
  @Input('showSide') showSide: boolean = false;
  @Input('isOwn') isOwn: boolean = false;
  @Output('navigate') _navigate = new EventEmitter<InfoPart>();

  navigate(): void {
    this._navigate.emit(this.part);
  }

  getShowClass(): ICssStyles {
    
    return {
      'left-show': this.showSide,
      'right-show': !this.showSide,
    }
  }

  getValue(value: any, type: string) {
    return type === 'date' ? moment(value).locale('ru').format('LL') : value;
  }

}
