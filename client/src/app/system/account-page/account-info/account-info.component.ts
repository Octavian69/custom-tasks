import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import EditDb from '../../../db/edit.db'
import { AccountViewService } from 'src/app/services/account.view.service';
import { fade, transitionHeight } from 'src/app/shared/animations/animation';
import { Info } from 'src/app/models/Info';
import { Router } from '@angular/router';
import { AccountFlags } from 'src/app/models/flags/AccountFlags';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { UserViewService } from 'src/app/services/user.view.service';
import { IUser } from 'src/app/interfaces/IUser';
import { completeInfoRows } from 'src/app/shared/handlers/handlers';
import { EditLink, TEditPageName } from 'src/app/types/edit.types';
import { InfoRow, InfoPart } from 'src/app/types/account.types';
import { Length } from 'src/app/types/validation.types';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'cst-account-info',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.scss'],
  animations: [fade, transitionHeight]
})
export class AccountInfoComponent implements OnInit, OnDestroy {
  
  mainPart: InfoPart = null;
  parts: InfoPart[] = [];
  links: EditLink[] = this.viewService.getDataByArrayKeys(['edit', 'list'], EditDb);
  arrayFieldNames: string[] = this.viewService.getDataByArrayKeys(['account', 'arrayForms']);
  fieldsToDraw: string[] = this.viewService.getDataByArrayKeys(['account', 'fieldsToDraw']);
  destroyFlagNames: string[] = ['isShowDetails', 'isShowStatus'];
  lengths: Length = this.viewService.getValidatorLength('REGISTRATION');
  flags: AccountFlags = this.viewService.getFlagObject();

  @Input('user') user: IUser;
  @Input('info') info: Info;

  constructor(
    private router: Router,
    private viewService: AccountViewService,
    private storage: StorageService,
    private userViewService: UserViewService
  ) { }

  ngOnInit() {
    this.initMainInfo();
    this.completeInfoParts();
  }

  ngOnDestroy() {
    this.destroyFlagNames.forEach((name: string) => this.viewService.setFlag(name, false));
  }

  trackByFn(index: number, item: any): number {
    return index;
  }

  initMainInfo(): void {
    const main: any = Object.keys(this.info['Main']).reduce((obj, current) =>{
      obj[current] = this.info['Main'][current];

      return obj;
    }, {});
    
    this.mainPart = this.completePart('Single', main, 'Main');
  }

  editable(): boolean {
    return this.viewService.isOwn(this.user._id);
  }

  isOwn(): boolean {
    return this.viewService.isOwn(this.user._id);
  }

  completeInfoParts() {

    const fieldsToDraw: string[] = Object.keys(this.info).filter((field: string) => this.fieldsToDraw.includes(field));
    const parts: InfoPart[] = fieldsToDraw.reduce((parts: any[], current: TEditPageName) => {
      const type: 'Single' | 'Array' = this.arrayFieldNames.includes(current) ? 'Array' : 'Single';
      const part: InfoPart = this.completePart(type, this.info[current], current);

      if(part)  parts.push(part);
      
      return parts;
    }, []);

    parts.sort((a, b) => a.id - b.id);

    this.parts = parts;
  }

  completePart(type: 'Single' | 'Array', part: any, partName: TEditPageName): InfoPart {
    const configuration: any = this.viewService.getDataByArrayKeys(['edit', 'forms', partName], EditDb);
    const { path, title, id } = this.links.find((item: EditLink) => item.path === partName);
    let rows: InfoRow[] | InfoRow[][] = [];

    switch(type) {

      case 'Single': {
        rows = this.completeRows(part, configuration);
        
        break;
      }

      case 'Array': {
        const boxes: InfoRow[][] = [];

         part.forEach(p => {
          const b: InfoRow[] = this.completeRows(p, configuration);

          if(b.length) boxes.push(b);

        });

        rows = boxes;
      }
    }

    if(rows.length) {
      const part: InfoPart = { path, title, type, rows, id };

      return part;
    }

    return null;
  }


  completeRows(part: any, configuration: any): InfoRow[] {
    return completeInfoRows(part, configuration)
  }

  getDetailStyles(): ICssStyles {
    const isOwn: boolean = this.isOwn();
    const isOtherDefaultInfo: boolean = !this.parts.length && !isOwn;

    return {
      'own-detail': isOwn || (!isOwn && !isOtherDefaultInfo),
      'other-detail': !isOwn && isOtherDefaultInfo,
    }
  }

  getStatusText(): string {
    return this.user.Online ? 'Online' : 'Offline';
  }


  getDetailText(): string {
    const isShowDetails: boolean = <boolean>this.viewService.getFlag('isShowDetails');
    const isDefaultInfo: boolean = !this.parts.length;
    const isOwn: boolean  = this.isOwn()

    switch(true) {
      
      case !isOwn && isDefaultInfo: {
        return `${this.user.Name} не заполнил(а) информацию о себе`;
      }

      case isDefaultInfo: {
        return 'Заполнить информацию о себе';
      }

      case isShowDetails: {
        return 'Скрыть подробную информацию';
      }

      case !isShowDetails: {
        return 'Показать подробную информацию';
      }
    }
  }

  getStatusCss(): ICssStyles {
    return { yellow: true, 'yellow-hover': this.editable() };
  }

  detailAction(): void {
    const isDefaultInfo: boolean = this.parts.length ? false : true;
    const isDefaultOther: boolean = isDefaultInfo && !this.isOwn()
     
    if(isDefaultOther) return
    
    if(isDefaultInfo && this.isOwn()) {
      this.router.navigate(['/edit'], { queryParams: { page: 'Main' } });
      
    } 
    else this.showDetails();
  }

  navigate(part: InfoPart) {
    const { path: page } = part;
    this.storage.setItem('sessionStorage', 'editPage', part.id);
    this.storage.setItem('sessionStorage', 'editPageName', page);
    this.router.navigate(['/edit'], { queryParams: { page } })
  }

  editStatus(Status: string): void {
    this.userViewService.edit({Status});
  }

  showDetails(): void {
    const isShowDetails: boolean = <boolean>this.viewService.getFlag('isShowDetails');

    this.viewService.setFlag('isShowDetails', !isShowDetails);
  }
}
