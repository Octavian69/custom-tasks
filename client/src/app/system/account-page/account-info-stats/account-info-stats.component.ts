import { Component, OnInit, Input } from '@angular/core';
import { IUser } from 'src/app/interfaces/IUser';
import { getFilledStats, completeStats } from 'src/app/shared/handlers/friends.handlers';
import { Params, Router } from '@angular/router';
import { StatsOption } from 'src/app/types/friends.types';

@Component({
  selector: 'cst-account-info-stats',
  templateUrl: './account-info-stats.component.html',
  styleUrls: ['./account-info-stats.component.scss']
})
export class AccountInfoStatsComponent implements OnInit {

  stats: StatsOption[] = [];
  
  @Input('user') user: IUser;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.initStats(this.user);
  }

  initStats(user: IUser): void {
    const filled: string[] = getFilledStats(user);

    if(filled.length) {
      this.stats = completeStats(filled, user);
    } 
  }

  navigateTo(path: string, queryParams: Params = {}): void {
    this.router.navigate([path, this.user._id], { queryParams })
  }

}
