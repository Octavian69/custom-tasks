import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MediaList } from 'src/app/models/MediaList';
import { isExpand } from 'src/app/shared/handlers/handlers';
import { Page } from 'src/app/models/Page';
import { IUser } from 'src/app/interfaces/IUser';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { PaginationRequest } from 'src/app/models/PaginationRequest';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ResponseAction } from 'src/app/types/request-response.types';
import { AnyOption } from 'src/app/types/shared.types';


@Component({
  selector: 'cst-account-online-users',
  templateUrl: './account-online-users.component.html',
  styleUrls: ['./account-online-users.component.scss']
})
export class AccountOnlineUsersComponent implements OnInit {
  
  mediaRequest: PaginationRequest;
  
  @Input('userId') userId: string;
  @Input('users') users: MediaList<IUser>;
  @Input('request') request: boolean = false;
  @Input('navigate') _navigate: EventEmitter<string> = new EventEmitter<string>()
  @Output('fetch') _fetch: EventEmitter<SimpleAction<PaginationRequest>> = new EventEmitter<SimpleAction<PaginationRequest>>();
  @Output('hide') _hide: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    private router: Router
  ){}

  ngOnInit() {
    this.initRequest();
    this.initUsers();
  }

  initUsers(): void {
    const next = () => {
      this.fetch('replace');
    }

    of(null).pipe(delay(0)).subscribe({ next });
  }

  initRequest(): void {
    const state: AnyOption = { listType: 'main' };
    const page: Page = new Page(0, 1);
    // const page: Page = new Page(0, LIMIT.USERS);

    this.mediaRequest = new PaginationRequest(this.userId, page, state);
    this.mediaRequest.filters = { Online: true };
  }

  fetch(type: ResponseAction) {
    const event: SimpleAction<PaginationRequest> = new SimpleAction(type, this.mediaRequest);
    this._fetch.emit(event);
  }

  trackByFn(index: number, user: IUser): string {
    return user._id;
  }

  hide(): void {
    this._hide.emit();
  }

  navigate(event: SimpleAction<IUser>): void {
    const { payload: { _id }, state: { path, queryParams } } = event;
    
    this.router.navigate([path, _id], { queryParams });
  }

  expand(): void {
    const expand: boolean = isExpand(this.users);

    if(expand) {
      this.mediaRequest.page.skip++;
      this.fetch('expand');
    } 
  }
}
