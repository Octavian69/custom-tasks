import { Component, OnInit, Input } from '@angular/core';
import { IUser } from 'src/app/interfaces/IUser';
import { Info } from 'src/app/models/Info';

@Component({
  selector: 'cst-account-detail',
  templateUrl: './account-detail.component.html',
  styleUrls: ['./account-detail.component.scss']
})
export class AccountDetailComponent implements OnInit {

  @Input('user') user: IUser;
  @Input('info') info: Info;

  constructor() { }

  ngOnInit() {
  }

}
