import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AccountViewService } from 'src/app/services/account.view.service';
import { Observable, Subscription } from 'rxjs';
import { Info } from 'src/app/models/Info';
import { unsubscriber } from 'src/app/shared/handlers/handlers';
import { AccountFlags } from 'src/app/models/flags/AccountFlags';
import { UserViewService } from 'src/app/services/user.view.service';
import { UserFlags } from 'src/app/models/flags/UserFlags';
import { IUser } from 'src/app/interfaces/IUser';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { UnseenViewService } from 'src/app/services/unseen.view.service';
import { IUnseen } from 'src/app/interfaces/IUnseen';
import { PaginationRequest } from 'src/app/models/PaginationRequest';
import { MediaList } from 'src/app/models/MediaList';
import { AccountNavLink } from 'src/app/types/account.types';
import { SipmleAction } from 'src/app/types/shared.types';

@Component({
  selector: 'cst-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.scss']
})
export class AccountPageComponent implements OnInit, OnDestroy {
  info$: Observable<Info> = this.viewService.getStream('info$');
  onlineFriends$: Observable<MediaList<IUser>> = this.viewService.getStream('onlineFriends$');
  user$: Observable<IUser> = this.viewService.getStream('user$');
  unseen$: Observable<IUnseen> = this.unseenService.getStream('unseen$');

  userId: string;

  navList: AccountNavLink[] = this.viewService.getDataByArrayKeys(['account', 'navList']);

  userFlags: UserFlags = this.userViewService.getFlagObject();
  flags: AccountFlags = this.viewService.getFlagObject();

  subs$: Subscription[] = [];

  constructor(
    private activeRouter: ActivatedRoute,
    private router: Router,
    private viewService: AccountViewService,
    private userViewService: UserViewService,
    private unseenService: UnseenViewService
  ) {}

  ngOnInit() {
    this.initRouterSub();
    this.initOwnUserMutateSub();
  };

  ngOnDestroy() {
    this.viewService.setFlag('isLoad', false);
    this.viewService.destroy();
    unsubscriber(this.subs$);
  }

  initRouterSub(): void {
   const sub$: Subscription = this.activeRouter.params.subscribe(({id}) => {
     this.userId = id;
     this.viewService.destroy();
     this.viewService.fetch(this.userId);
   });

   this.subs$.push(sub$);
  }


  initOwnUserMutateSub(): void {
    const mutate$: Observable<SimpleAction<IUser>> = this.userViewService.getStream('mutate$');

    const sub$: Subscription = mutate$.subscribe((event: SimpleAction<IUser>) => {
      const user: IUser = this.viewService.getStreamValue('user$');
      const isOwn: boolean = this.viewService.isOwn(user._id);

      if(isOwn) {
        this.viewService.emitStream('user$', event.payload);
      }
    })

    this.subs$.push(sub$);
  }

  navigate(link: AccountNavLink): void {
    const { status, path, query } = link;
    const OwnId: string = this.viewService.getOwnId();
    const url: string = status === 'private' ? `/${path}` : `/${path}/${OwnId}`;
    const queryParams: Params = query || {};
  
    this.router.navigate([url], { queryParams });
  }

  fetchOnlineUsers(event: SipmleAction<PaginationRequest>): void {
    this.viewService.fetchOnlineUsers(event);
  }

  createAvatar(file: File): void {
    this.userViewService.createAvatar(file);
  }
}
