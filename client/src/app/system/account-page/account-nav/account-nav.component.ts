import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IUnseen } from 'src/app/interfaces/IUnseen';
import { AccountNavLink } from 'src/app/types/account.types';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { ANToggleMenu } from 'src/app/shared/animations/animation';

@Component({
  selector: 'cst-account-nav',
  templateUrl: './account-nav.component.html',
  styleUrls: ['./account-nav.component.scss'],
  animations: [ANToggleMenu]
})
export class AccountNavComponent {

  isShowMenu: boolean = false;

  @Input('list') list: AccountNavLink[];
  @Input('unseen') set _unseen(unseen: IUnseen) {
    if(unseen) {
      this.completeUnseens(unseen);
    }
  }
  @Output('navigate') _navigate = new EventEmitter<AccountNavLink>();

  navigate(link: AccountNavLink): void {
    this._navigate.emit(link);
  }

  completeUnseens(unseen: IUnseen) {
      this.list = this.list.map(item => {
        if(item.unseen) {
          const { key } = item.unseen;
          const value: number = Object.keys(unseen.keys[key]).length; 

          item.unseen.value = value;
        }

        return item;
      })
  }

  getOpenIconStyles(): ICssStyles {
    return {
      'open-icon': this.isShowMenu,
      'close-icon': !this.isShowMenu
    }
  }

  getOpenMenuStyles(): ICssStyles {
    return {
      'open-menu': this.isShowMenu,
      'box-shadow': this.isShowMenu,
      'close-menu': !this.isShowMenu
    }
  }
}
