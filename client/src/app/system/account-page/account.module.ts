import { NgModule } from "@angular/core";
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { AccountPageComponent } from './account-page/account-page.component';
import { AccountViewService } from 'src/app/services/account.view.service';
import { AccountRoutingModule } from './account.routing.module';
import { AccountInfoComponent } from './account-info/account-info.component';
import { AccountInfoPartComponent } from './account-info-part/account-info-part.component';
import { AccountNavComponent } from './account-nav/account-nav.component';
import { AccountPreviewComponent } from './account-preview/account-preview.component';
import { AccountPreviewUsersComponent } from './account-preview-users/account-preview-users.component';
import { AccountOnlineUsersComponent } from './account-online-users/account-online-users.component';
import { AccountOnlineUsersItemComponent } from './account-online-users-item/account-online-users-item.component';
import { AccountInfoStatsComponent } from './account-info-stats/account-info-stats.component';
import { AccountDetailComponent } from './account-detail/account-detail.component';

@NgModule({
    declarations: [
        AccountPageComponent,
        AccountInfoComponent,
        AccountInfoPartComponent,
        AccountPreviewComponent,
        AccountNavComponent,
        AccountPreviewUsersComponent,
        AccountOnlineUsersComponent,
        AccountOnlineUsersItemComponent,
        AccountInfoStatsComponent,
        AccountDetailComponent
    ],
    imports: [
        SharedModule,
        AccountRoutingModule
    ],
    providers: [
        AccountViewService
    ]
})
export class AccountModule {}