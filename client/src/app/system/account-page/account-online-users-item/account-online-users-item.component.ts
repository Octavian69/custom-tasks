import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IUser } from 'src/app/interfaces/IUser';
import { getFilledStats, completeStats } from 'src/app/shared/handlers/friends.handlers';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { Params } from '@angular/router';
import { StatsOption } from 'src/app/types/friends.types';



@Component({
  selector: 'cst-account-online-users-item',
  templateUrl: './account-online-users-item.component.html',
  styleUrls: ['./account-online-users-item.component.scss']
})
export class AccountOnlineUsersItemComponent implements OnInit {

  stats: StatsOption[] = [];

  @Input('user') user: IUser;
  @Output('navigate') _navigate: EventEmitter<SimpleAction<IUser>> = new EventEmitter();

  ngOnInit() {
    this.initStats();
  }

  initStats(): void {
    const filled: string[] = getFilledStats(this.user);

    if(filled.length) {
      this.stats = completeStats(filled, this.user);
    } 
  }

  navigate(path: string, queryParams: Params = {}) {
    const event: SimpleAction<IUser> = new SimpleAction('navigate', this.user, { path, queryParams });

    this._navigate.emit(event);
  }

}
