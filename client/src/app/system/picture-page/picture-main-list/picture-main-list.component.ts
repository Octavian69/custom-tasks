import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { MediaList } from 'src/app/models/MediaList';
import { IPicture } from 'src/app/interfaces/IPicture';
import { PictureViewService } from 'src/app/services/picture.view.service';
import { Page } from 'src/app/models/Page';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { isExpand } from 'src/app/shared/handlers/handlers';
import { PictureFlags } from 'src/app/models/flags/PictureFlags';
import { PictureWorkService } from 'src/app/services/picture.work.service';
import { IPictureAlbum } from 'src/app/interfaces/IPictureAlbum';
import { PictureAlbum } from 'src/app/models/PictureAlbum';
import { AnyOption, Simple } from 'src/app/types/shared.types';

@Component({
  selector: 'cst-picture-main-list',
  templateUrl: './picture-main-list.component.html',
  styleUrls: ['./picture-main-list.component.scss']
})
export class PictureMainListComponent implements OnInit, OnDestroy {
  
  pictures$: Observable<MediaList<IPicture>> = this.viewService.getStream('pictures$');
  
  page: Page = new Page(0, LIMIT.PICTURE);
  flags: PictureFlags = this.viewService.getFlagObject();
  
  @Input('userId') userId: string;

  constructor(
    private activeRouter: ActivatedRoute,
    private viewService: PictureViewService,
    private pictureWorker: PictureWorkService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.viewService.destroy();
  }

  init(): void {
    this.userId = this.activeRouter.snapshot.params.id;
    this.viewService.fetch(this.userId, this.page, 'replace', 'Created');
  }

  trackByYear(index: number, year: Simple<IPicture[]>): string {
    return year.key;
  }

  trackByFn(index: number, picture: IPicture): number {
    return index;
  }

  modify(mediaList: MediaList<IPicture>): AnyOption {
    if(mediaList) {
      const { rows } = mediaList;

      const years: AnyOption = rows.reduce((accum: AnyOption, p: IPicture) => {

        const year: number = new Date(p.Created).getFullYear();

        if(!accum[year]) accum[year] = [];

        accum[year].push(p);
        
        return accum;
      }, {});

      return years;
    }

    return {};
  }

  slide(picture: IPicture): void {
    const pictures: MediaList<IPicture> = this.viewService.getStreamValue('pictures$');
    const album: IPictureAlbum = new PictureAlbum(this.userId, 'Main', 'Общие фотографии');
    const { totalCount } = pictures;

    album.TotalCount = totalCount;
    album._id = this.userId;

    this.pictureWorker.changeSlide(picture, album, pictures);
  }

  expand(): void {
    const mediaList: MediaList<IPicture> = this.viewService.getStreamValue('pictures$');

    const expand: boolean = isExpand(mediaList);

    if(expand) {
      this.page.skip++;
      this.viewService.fetch(this.userId, this.page, 'expand', 'Created');
    }
  }

}
