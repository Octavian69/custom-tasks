import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { PictureViewService } from 'src/app/services/picture.view.service';
import { PictureAlbumsViewService } from 'src/app/services/picture-albums.view.service';
import { PictureAlbumComponent } from './picture-album/picture-album.component';
import { PictureAlbumDetailComponent } from './picture-album-detail/picture-album-detail.component';
import { PictureAlbumUnitComponent } from './picture-album-unit/picture-album-unit.component';
import { PictureMainListComponent } from './picture-main-list/picture-main-list.component';
import { PictureAlbumsComponent } from './picture-albums/picture-albums.component';
import { PictureRoutingModule } from './picture.routing.module';
import { PicturePageComponent } from './picture-page/picture-page.component';
import { PictureNavComponent } from './picture-nav/picture-nav.component';
import { PictureCreateComponent } from './picture-create/picture-create.component';
import { PictureCreateUnitComponent } from './picture-create-unit/picture-create-unit.component';
import { PictureReplaceComponent } from './picture-replace/picture-replace.component';
import { PictureReplaceUnitComponent } from './picture-replace-unit/picture-replace-unit.component';

@NgModule({
    declarations: [
        PicturePageComponent,
        PictureAlbumComponent, 
        PictureAlbumDetailComponent, 
        PictureAlbumUnitComponent, 
        PictureMainListComponent, 
        PictureAlbumsComponent, 
        PictureNavComponent,
        PictureCreateComponent,
        PictureCreateUnitComponent,
        PictureReplaceUnitComponent,
        PictureReplaceComponent
    ],
    imports: [
        PictureRoutingModule,
        SharedModule
    ],
    providers: [
        PictureViewService,
        PictureAlbumsViewService
    ]
})
export class PictureModule {}