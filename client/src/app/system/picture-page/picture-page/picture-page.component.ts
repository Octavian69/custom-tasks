import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PictureViewService } from 'src/app/services/picture.view.service';
import { PictureAlbumsViewService } from 'src/app/services/picture-albums.view.service';
import { StorageService } from 'src/app/services/storage.service';
import { PageLink } from 'src/app/types/request-response.types';

@Component({
  selector: 'cst-picture-page',
  templateUrl: './picture-page.component.html',
  styleUrls: ['./picture-page.component.scss']
  
})
export class PicturePageComponent implements OnInit, OnDestroy {

  userId: string;
  navList: PageLink[];
  link: PageLink;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private viewService: PictureViewService,
    private albumViewService: PictureAlbumsViewService,
    private storage: StorageService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.viewService.destroy();
    this.albumViewService.destroy();
    this.storage.removeItem('sessionStorage', 'picturePage');
  }

  init(): void {
    this.userId = this.activeRoute.snapshot.params.id;
    this.navList = this.viewService.getDataByArrayKeys(['picture', 'navList']);
  }

  getUserId(): string {
    return this.viewService.getField('userId');
  }

  isOwn(): boolean {
    return this.viewService.isOwn(this.userId);
  }

  isCreate(): boolean {
    return this.router.url.includes('album_id') && this.isOwn();
  }

  isActiveUrl(page: string): boolean {
    return this.router.url.includes(page);
  }

  setPage(link: PageLink): void {
    const { page } = link;

    const isInitDetailPage: boolean = !this.link && this.isActiveUrl('detail');

    if(!isInitDetailPage) {
      this.router.navigate([], {
        queryParams: { page }
      });
    }

    this.link = link;
  }

  create() {
    const query: Params = this.activeRoute.snapshot.queryParams;

    this.router.navigate([], {
      relativeTo: this.activeRoute,
      queryParams: { ...query, album_id: 0 }
    })
  }

  hideAlbumModal(): void {
    const { queryParams: { page } } = this.activeRoute.snapshot;

    this.router.navigate([], {
      relativeTo: this.activeRoute,
      queryParams: { page }
    })
  }
}
