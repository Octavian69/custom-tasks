import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { PictureAlbumsViewService } from 'src/app/services/picture-albums.view.service';
import { ActivatedRoute } from '@angular/router';
import { PictureViewService } from 'src/app/services/picture.view.service';
import { PictureAlbumsFlags } from 'src/app/models/flags/PictureAlbumsFlags';
import { PictureFlags } from 'src/app/models/flags/PictureFlags';
import { Observable, Subscription } from 'rxjs';
import { MediaList } from 'src/app/models/MediaList';
import { IPicture } from 'src/app/interfaces/IPicture';
import { IPictureAlbum } from 'src/app/interfaces/IPictureAlbum';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { scaleShow } from 'src/app/shared/animations/animation';
import { isExpand, unsubscriber } from 'src/app/shared/handlers/handlers';
import { Page } from 'src/app/models/Page';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { PictureWorkService } from 'src/app/services/picture.work.service';
import { IUser } from 'src/app/interfaces/IUser';
import { UserViewService } from 'src/app/services/user.view.service';
import { PictureReplaceAlbum } from 'src/app/types/picture.types';

@Component({
  selector: 'cst-picture-album-detail',
  templateUrl: './picture-album-detail.component.html',
  styleUrls: ['./picture-album-detail.component.scss'],
  animations: [scaleShow]
})
export class PictureAlbumDetailComponent implements OnInit, OnDestroy {

  albumId: string;
  modeType: 'edit' | 'view';
  selectedLimit: number = LIMIT.PICTURE_SELECTED;
  page: Page = new Page(0, LIMIT.PICTURE);
  subs$: Subscription[] = [];

  pictures$: Observable<MediaList<IPicture>> = this.pictureViewService.getStream('pictures$');
  detail$: Observable<IPictureAlbum> = this.albumViewService.getStream('detail$');

  albumFlags: PictureAlbumsFlags = this.albumViewService.getFlagObject();
  pictureFlags: PictureFlags = this.pictureViewService.getFlagObject();

  @Input('userId') userId: string;

  constructor(
    private activeRouter: ActivatedRoute,
    private userViewService: UserViewService,
    private albumViewService: PictureAlbumsViewService,
    private pictureViewService: PictureViewService,
    private pictureWorker: PictureWorkService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.pictureViewService.destroy();
    this.albumViewService.destroySubjects(['detail$']);
    unsubscriber(this.subs$);
  }

  init() {
    const { modeType, detail_id } = this.activeRouter.snapshot.queryParams;

    this.modeType = modeType;
    this.albumId = detail_id;
    
    this.initMutateSub();
    this.initMutateSliderSub();
    this.initUserMutateSub();
    this.pictureViewService.fetch(detail_id, this.page, 'replace');
    this.albumViewService.initDetail(detail_id);
  }

  initMutateSub(): void {
    const mutate$: Observable<SimpleAction<IPicture | IPicture[] | string[]>> = this.pictureViewService.getStream('mutate$');

    const sub$: Subscription = mutate$.subscribe((event) => {
      this.albumViewService.mutateDetail(event);
    });

    this.subs$.push(sub$);
  }

  initMutateSliderSub(): void {
    const mutate$: Observable<SimpleAction<IPicture>> = this.pictureWorker.getStream('mutate$');
    
    const sub$: Subscription = mutate$.subscribe((event: SimpleAction<IPicture>) => {
      this.pictureViewService.mutate(event);
    });

    this.subs$.push(sub$);
  }

  initUserMutateSub(): void {
    const mutate$: Observable<SimpleAction<IUser>> = this.userViewService.getStream('mutate$');

    const sub$: Subscription = mutate$.subscribe((event: SimpleAction<IUser>) => {
      const { action, payload } = event;
      const detail: IPictureAlbum = this.albumViewService.getStreamValue('detail$');
      const IsOwnAlbum: boolean = this.albumViewService.isOwn(detail.User);
      const isUpdateAvatar: boolean = action.includes('avatar');

      if(IsOwnAlbum && isUpdateAvatar) {
        const { Avatar } = payload;
        detail.UserAvatar = Avatar;
        this.albumViewService.emitStream('detail$', detail);
      }
    });  

    this.subs$.push(sub$);
  }

  trackByFn(index: number, picture: IPicture): string {
    return picture._id;
  }

  isSelected(pictureId: string): boolean {
    const pictureIds: string[] = this.pictureViewService.getFlag('multiRequestArray') as string[];

    return pictureIds.includes(pictureId);
  }

  isDisabledMutate() {
    return this.pictureViewService.getFlag('isMutateRequest');
  }

  isShowMutate(): boolean {
    const { length } = this.pictureViewService.getFlag('multiRequestArray') as string[];
    
    return length > 0;
  }

  isDisabledSelect() {
    const { length } = this.pictureViewService.getFlag('multiRequestArray') as string[];

    return length >= this.selectedLimit;
  }

  getTotalStyles(total: number): ICssStyles {
    const isEven: boolean = total % 2 === 0;

    return {
      'even': isEven,
      'odd': !isEven
    }
  }

  getConfirmText(): string {
    const { length } = this.pictureViewService.getFlag('multiRequestArray') as string[];

    return `удалить ${length} фотографий`;
  }

  slide(picture: IPicture): void {
    const album: IPictureAlbum = this.albumViewService.getStreamValue('detail$');
    const pictures: MediaList<IPicture> = this.pictureViewService.getStreamValue('pictures$');

    this.pictureWorker.changeSlide(picture, album, pictures);
  }

  select(picture: IPicture): void {
    const pictureIds: string[] = this.pictureViewService.getFlag('multiRequestArray') as string[];
    
    const isExist: boolean = pictureIds.includes(picture._id);

    if(isExist) {
      const idx: number = pictureIds.findIndex((id: string) => id === picture._id);

      pictureIds.splice(idx, 1);

    } else {
      pictureIds.push(picture._id);
    }

  }

  showPictureModal(modal: string, flag: boolean): void {
    this.pictureViewService.setFlag(modal, flag);
  }

  getSelectedPicturePaths(pictureIds: string[]): string[] {
    const pictures: MediaList<IPicture> = this.pictureViewService.getStreamValue('pictures$');
    const paths: string[] = pictures.rows.filter(p => pictureIds.includes(p._id)).map(p => p.Path);

    return paths;
  }

  create(files: File[]): void {
    this.pictureViewService.create(this.albumId, files);
  }

  remove(flag: boolean): void {
    if(flag) {
      const pictureIds: string[] = this.pictureViewService.getFlag('multiRequestArray') as string[];
      const picturePaths: string[] = this.getSelectedPicturePaths(pictureIds);

      this.pictureViewService.remove(this.albumId, pictureIds, picturePaths);
    }

    this.showPictureModal('isShowConfirm', false);
  }

  replace(album: PictureReplaceAlbum): void {
    const pictureIds: string[] = this.pictureViewService.getFlag('multiRequestArray') as string[];
    const picturePaths: string[] = this.getSelectedPicturePaths(pictureIds);

    const { _id: toAlbumId } = album;

    this.pictureViewService.replace(toAlbumId, this.albumId, pictureIds, picturePaths);
  }

  expand(): void {
    const mediaList: MediaList<IPicture> = this.pictureViewService.getStreamValue('pictures$');

    const expand: boolean = isExpand(mediaList);

    if(expand) {
      this.page.skip++;
      this.pictureViewService.fetch(this.albumId, this.page, 'expand');
    }
  }
}
