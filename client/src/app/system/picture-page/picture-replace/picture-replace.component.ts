import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { PictureAlbumsService } from 'src/app/services/picture-albums.service';
import { BehaviorSubject } from 'rxjs';
import { MediaList } from 'src/app/models/MediaList';
import { Page } from 'src/app/models/Page';
import { isExpand } from 'src/app/shared/handlers/handlers';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { PictureReplaceAlbum } from 'src/app/types/picture.types';
import { ResponseAction } from 'src/app/types/request-response.types';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'cst-picture-replace',
  templateUrl: './picture-replace.component.html',
  styleUrls: ['./picture-replace.component.scss']
})
export class PictureReplaceComponent implements OnInit, OnDestroy {

  albums$: BehaviorSubject<MediaList<PictureReplaceAlbum>> = new BehaviorSubject(null);

  page: Page = new Page(0, LIMIT.PICTURE_REPLACE_ALBUMS);
  isFetch: boolean = false;

  @Input('request') request: boolean = false;
  @Input('albumId') albumId: string;
  @Output('replace') _replace = new EventEmitter<PictureReplaceAlbum>();
  @Output('hide') _hide = new EventEmitter<void>();

  constructor(
    private albumService: PictureAlbumsService
  ) { }

  ngOnInit() {
    this.fetch('replace');
  }

  ngOnDestroy() {}


  fetch(action: ResponseAction): void {
    this.isFetch = true;
    
    const next = (response: MediaList<PictureReplaceAlbum>) => {

        if(action === 'replace') {
            this.albums$.next(response);
        } else {
            const albums: MediaList<PictureReplaceAlbum> = this.albums$.getValue();

            const rows: PictureReplaceAlbum[] = albums.rows.concat(response.rows);
            const totalCount: number = response.totalCount;

            this.albums$.next({rows, totalCount});
        }

        this.isFetch = false;
    }
    
    this.albumService
            .fetchReplaceAlbums(this.albumId, this.page)
            .pipe(untilDestroyed(this))
            .subscribe({ next });

  }

  trackByFn(index: number, album: PictureReplaceAlbum): string {
      return album._id;
  }
  
  hide(): void {
    this._hide.emit();
  }

  expand() {
    const albums: MediaList<PictureReplaceAlbum> = this.albums$.getValue();
    const expand: boolean = isExpand(albums);
    
    if(expand) {
      this.page.skip++;
      this.fetch('expand');
    }
  }

  replace(album: PictureReplaceAlbum): void {
    this._replace.emit(album);
  }

}
