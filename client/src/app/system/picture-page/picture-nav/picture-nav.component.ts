import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { PageLink } from 'src/app/types/request-response.types';

@Component({
  selector: 'cst-picture-nav',
  templateUrl: './picture-nav.component.html',
  styleUrls: ['./picture-nav.component.scss']
})
export class PictureNavComponent {

  @Input('userId') userId: string;
  @Input('isOwn') isOwn: boolean = false;
  @Input('options') options: PageLink[];
  @Output('setPage') _setPage = new EventEmitter<PageLink>();
  @Output('create') _create = new EventEmitter<null>();

  constructor(
    private router: Router
  ) {}

  setPage(link: PageLink) {
    this._setPage.emit(link);
  }

  isDetail(): boolean {
    return this.router.url.includes('detail')
  }

  create() {
    this._create.emit();
  }

}
