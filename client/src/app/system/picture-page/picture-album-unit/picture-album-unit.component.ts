import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IPictureAlbum } from 'src/app/interfaces/IPictureAlbum';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { SimpleAction } from 'src/app/models/SimpleAction';

@Component({
  selector: 'cst-picture-album-unit',
  templateUrl: './picture-album-unit.component.html',
  styleUrls: ['./picture-album-unit.component.scss']
})
export class PictureAlbumUnitComponent implements OnInit {

  @Input('album') album: IPictureAlbum;
  @Output('edit') _edit = new EventEmitter<IPictureAlbum>();
  @Output('detail') _detail = new EventEmitter<SimpleAction<IPictureAlbum>>()

  constructor() { }

  ngOnInit() {
  }

  getDescriptionClass(): ICssStyles {
    return {
      'preview__album-description': this.album.Description
    }
  }

  edit() {
    this._edit.emit(this.album);
  }

  detail(action: 'edit' | 'view'):void {
    const event: SimpleAction<IPictureAlbum> = new SimpleAction(action, this.album);

    this._detail.emit(event);
  }

}
