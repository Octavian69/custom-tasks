import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PictureReplaceAlbum } from 'src/app/types/picture.types';

@Component({
  selector: 'cst-picture-replace-unit',
  templateUrl: './picture-replace-unit.component.html',
  styleUrls: ['./picture-replace-unit.component.scss']
})
export class PictureReplaceUnitComponent {

  @Input('album') album: PictureReplaceAlbum;
  @Output('replace') _replace = new EventEmitter<PictureReplaceAlbum>();

  replace() {
    this._replace.emit(this.album);
  }

}
