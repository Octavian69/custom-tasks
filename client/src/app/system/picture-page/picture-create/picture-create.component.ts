import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { checkedFile } from 'src/app/shared/handlers/handlers';
import { scaleShow } from 'src/app/shared/animations/animation';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { LIMIT } from 'src/app/namespaces/Namespaces';

@Component({
  selector: 'cst-picture-create',
  templateUrl: './picture-create.component.html',
  styleUrls: ['./picture-create.component.scss'],
  animations: [scaleShow]
})
export class PictureCreateComponent {

  limit: number = LIMIT.PICTURE_CREATED;
  files: File[] = [];
  previews: string[] = [];

  @Input('request') request: boolean = false;
  @Output('create') _create = new EventEmitter<File[]>();
  @Output('hide') _hide = new EventEmitter<void>();

  constructor(
    private toastr: ToastrService
  ) { }

  getCreateClass(): ICssStyles {
    return {
      'exists': this.files.length > 0,
      'not-exists': !this.files.length
    }
  }

  changeFiles(e: Event): void {
    const input: HTMLInputElement = e.target as HTMLInputElement;
    const files: File[] = Array.from(input.files);

    
    const validFiles: File[] = [];

    let checkedErrors: string[] = [];

    for(let i = 0; i < files.length; i++) {
      const { isValid, errors } = checkedFile('PICTURE', files[i]);

      if(isValid)  {
        validFiles.push(files[i]);
      } else if(!isValid && !checkedErrors.length) {
        checkedErrors.push(...errors);
      }

      if(validFiles.length >= this.limit) break;

    }

    const isLimitError: boolean = (validFiles.length + this.files.length) > this.limit;

    if(isLimitError) {
      checkedErrors.push(`Максимальное количество файлов ${this.limit}`)
    }

    if(checkedErrors.length) {
      this.toastr.warning(checkedErrors.toString(), 'Внимание');
    }

    const isExpand: boolean = (this.files.length < this.limit) && validFiles.length > 0;

    if(isExpand) {
      const expandLimit: number = this.limit - this.files.length;
      const expandFiles: File[] = validFiles.slice(0, expandLimit);

      this.files.push(...expandFiles);
      this.createPreviews(expandFiles)
    }

    input.value = null;
  };
  
  
  trackByFn(index: number, preview: string): string{
    return preview;
  }

  createPreviews(files: File[]): void {
    files.forEach((file: File) => {
      const preview: string = URL.createObjectURL(file);

      this.previews.push(preview)
    })
  }

  revokeUrl(idx: number): void {
    const preview: string = this.previews[idx];

    URL.revokeObjectURL(preview);
  }

  remove(idx: number): void {
    this.files.splice(idx, 1);
    this.previews.splice(idx, 1);
  }

  hide(): void {
    this._hide.emit();
  }

  isDisableActions(): boolean {
    return !this.files.length || this.request;
  }

  isDisabledSelect(): boolean {
    return this.files.length >= this.limit || this.request;
  }

  create(): void {
    this._create.emit(this.files);
  }
}
