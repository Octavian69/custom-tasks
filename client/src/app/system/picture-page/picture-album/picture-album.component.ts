import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PictureAlbumsViewService } from 'src/app/services/picture-albums.view.service';
import { Observable, Subscription } from 'rxjs';
import { IPictureAlbum } from 'src/app/interfaces/IPictureAlbum';
import { checkedFile, unsubscriber } from 'src/app/shared/handlers/handlers';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { tap } from 'rxjs/operators';
import { defaultSizeAnim } from 'src/app/shared/animations/animation';
import { PictureAlbumsFlags } from 'src/app/models/flags/PictureAlbumsFlags';
import { Length, FormObjectErrors } from 'src/app/types/validation.types';

@Component({
  selector: 'cst-picture-album',
  templateUrl: './picture-album.component.html',
  styleUrls: ['./picture-album.component.scss'],
  animations: [defaultSizeAnim('padding-top', '300ms ease-in')]
})
export class PictureAlbumComponent implements OnInit, OnDestroy {

  album$: Observable<IPictureAlbum> = this.initAlbumSub();

  modeType: 'create' | 'edit';

  file: File;
  preview: string;

  form: FormGroup;
  flags: PictureAlbumsFlags = this.viewService.getFlagObject();
  lengths: Length = this.viewService.getValidatorLength('PICTURE_ALBUM');
  errors: FormObjectErrors = this.viewService.completeDbErrors(['picture', 'formErrors']);
  
  subs$: Subscription[] = [];

  @Output('hide') _hide = new EventEmitter<void>();

  constructor(
    private toastr: ToastrService,
    private activeRoute: ActivatedRoute,
    private viewService: PictureAlbumsViewService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    unsubscriber(this.subs$);
    this.viewService.destroySubjects(['album$']);
  }

  init(): void {
    const { album_id } = this.activeRoute.snapshot.queryParams;

    this.modeType = album_id === '0' ? 'create' : 'edit';
    
    this.initForm();
    this.initMutateSub();

    this.viewService.fetchAlbum(album_id);
  }

  initForm(): void {
    this.form = new FormGroup({
      Title: new FormControl(null, Validators.required),
      Description: new FormControl(null)
    });
  }

  initAlbumSub(): Observable<IPictureAlbum> {
    const stream$: Observable<IPictureAlbum> = this.viewService.getStream('album$');

    return stream$.pipe(tap((album: IPictureAlbum) => {
        if(album) this.form.patchValue(album);
    }));
  }

  initMutateSub(): void {
    const mutate$: Observable<SimpleAction<IPictureAlbum>> = this.viewService.getStream('mutate$');

    const sub$ = mutate$.subscribe((action: SimpleAction<IPictureAlbum>) => {
      this.hide();
    })

    this.subs$.push(sub$)
  }

  getConfirmText(): string {
    const album: IPictureAlbum = this.viewService.getStreamValue('album$');

    return `удалить альбом "${album.Title}"`;
  }

  getLogoActionText(album: IPictureAlbum): string {
    const action: string = album.Logo || this.preview ? 'Изменить' : 'Выбрать';

    return action;
  }

  isShowRemove(): boolean {
    const album: IPictureAlbum = this.viewService.getStreamValue('album$');
    
    if(album) {
      return album.Type === 'Created' && this.modeType !== 'create' ? true : false;
    }

    return false;
  }

  showConfirm(flag): void {
    this.viewService.setFlag('isShowConfirm', flag);
  }

  showLogo(type: 'container' | 'preview' | 'logo', album?: IPictureAlbum): boolean {
    switch(type) {
      case 'container': {
        if(!album) return false;

        return this.preview || album.Logo ? true : false;
      }

      case 'preview': {
        return !!this.preview;
      }

      case 'logo': {
        return album && album.Logo && !this.preview;
      }
    }
  }

  hide(): void {
    this._hide.emit();
  }

  changeFile(e: Event): void {
    const file: File = (e.target as HTMLInputElement).files[0];

    const { isValid, errors } = checkedFile('PICTURE', file);

    if(!isValid) {
      this.toastr.warning(errors.toString(), 'Внимание');
      return 
    }

    this.file = file;
    this.preview = URL.createObjectURL(file);
  }

  revokeUrl(): void {
    URL.revokeObjectURL(this.preview);
  }


  remove(flag: boolean): void {
    if(flag) {
      this.viewService.remove();
      this.hide();
    }

    this.showConfirm(flag);
  }

  onSubmit() {
    const { value, dirty } = this.form;

    if(!dirty && !this.file) {
      this.toastr.info('Поля формы не изменялись', 'Информация');
      return
    }

    const current: IPictureAlbum = this.viewService.getStreamValue('album$');
    const album: IPictureAlbum = Object.assign({}, current, value);

    this.viewService[this.modeType](album, this.file);
  }

}
