import { Component, Input, Output, EventEmitter, HostBinding } from '@angular/core';
import { IPicture } from 'src/app/interfaces/IPicture';
import { scaleShow } from 'src/app/shared/animations/animation';

@Component({
  selector: 'cst-picture-unit',
  templateUrl: './picture-unit.component.html',
  styleUrls: ['./picture-unit.component.scss'],
  animations: [scaleShow]
})
export class PictureUnitComponent {

  @HostBinding('@scaleShow') scaleShow = true;

  @Input('picture') picture: IPicture;
  @Input('modeType') modeType: 'view' | 'edit';
  @Input('selected') selected: boolean = false;
  @Input('disabledSelect') disabledSelect: boolean = false;
  @Output('select') _select = new EventEmitter<IPicture>();
  @Output('slide') _slide = new EventEmitter<IPicture>();

  getSelectIcon(): string {
    return this.selected ? 'bookmark' : 'bookmark_border';
  }
  
  isDisabled(): boolean {
    return this.disabledSelect && !this.selected;
  }

  select(e: Event): void {
    e.stopPropagation();

    if(this.modeType === 'edit') {

      const disabled: boolean = this.isDisabled();

      if(!disabled) {
        this._select.emit(this.picture);
      }
    }
  }

  slide(): void {
    this._slide.emit(this.picture);
  }
}
