import { Component, Output, EventEmitter, Input, HostBinding } from '@angular/core';
import { scaleShow, defaultSizeAnim } from 'src/app/shared/animations/animation';
import { checkedFile } from 'src/app/shared/handlers/handlers';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'cst-picture-avatar-create',
  templateUrl: './picture-avatar-create.component.html',
  styleUrls: ['./picture-avatar-create.component.scss'],
  animations: [scaleShow, defaultSizeAnim('padding-top', '300ms ease-in')]
})
export class PictureAvatarCreateComponent {

  file: File;
  preview: string;

  @HostBinding('@scaleShow') scaleShow: boolean = true;
  
  @Input('request') request: boolean = false;
  @Output('create') _create = new EventEmitter<File>();
  @Output('hide') _hide = new EventEmitter<void>();

  constructor(
    private toastr: ToastrService
  ) {}

  changePicture(e: Event): void {
    const file: File = (e.target as HTMLInputElement).files[0];

    const { isValid, errors } = checkedFile('PICTURE', file);

    if(!isValid) {
      this.toastr.warning(errors.toString(), 'Внимание')
      return
    };

    this.file = file;
    this.preview = URL.createObjectURL(file);
  }

  revokeUrl(): void {
    URL.revokeObjectURL(this.preview);
  }

  hide(): void {
    this._hide.emit();
  }

  create(): void {
    this._create.emit(this.file);
  }

}
