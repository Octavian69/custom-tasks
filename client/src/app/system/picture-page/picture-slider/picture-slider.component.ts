import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { PictureWorkService } from 'src/app/services/picture.work.service';
import { IPicture } from 'src/app/interfaces/IPicture';
import { SliderManager } from 'src/app/models/info/SliderManager';
import { unsubscriber } from 'src/app/shared/handlers/handlers';
import { IUser } from 'src/app/interfaces/IUser';
import { PictureWorkFlags } from 'src/app/models/flags/PictureWorkFlags';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { UserViewService } from 'src/app/services/user.view.service';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { Side } from 'src/app/types/picture.types';
import { Length } from 'src/app/types/validation.types';


@Component({
  selector: 'cst-picture-slider',
  templateUrl: './picture-slider.component.html',
  styleUrls: ['./picture-slider.component.scss']
})
export class PictureSliderComponent implements OnInit, OnDestroy {

  picture$: Observable<IPicture> = this.pictureWorker.getStream('picture$');
  sliderManager$: Observable<SliderManager> = this.pictureWorker.getStream('sliderManager$');
  
  lengths: Length = this.pictureWorker.getValidatorLength('PICTURE');
  flags: PictureWorkFlags = this.pictureWorker.getFlagObject();
  subs$: Subscription[] = [];

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private pictureWorker: PictureWorkService,
    private userViewService: UserViewService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.pictureWorker.destoy();
    unsubscriber(this.subs$);
  }

  init(): void {
    const { picture_id } = this.activeRoute.snapshot.queryParams;
    this.pictureWorker.init(picture_id);
    this.initUserMutateSub()
  }

  updateAvatar(pictures: IPicture[], avatar: string): IPicture[] {
    return pictures.map(picture => {
      picture.CreatorAvatar = avatar;

      return picture;
    })
  }

  initUserMutateSub(): void {
    const mutate$: Observable<SimpleAction<IUser>> = this.userViewService.getStream('mutate$');

    const sub$: Subscription = mutate$.subscribe((event: SimpleAction<IUser>) => {
        const { action, payload } = event;
        const { Avatar } = payload;
        const manager: SliderManager = this.pictureWorker.getStreamValue('sliderManager$');

        if(manager.IsOwnAlbum) {
          const picture: IPicture = this.pictureWorker.getStreamValue('picture$');
          const pictures: IPicture[] = this.pictureWorker.getStreamValue('pictures$');


          if(action.includes('avatar')) {
            const updatePictures = this.updateAvatar(pictures, Avatar);
            picture.CreatorAvatar = Avatar;

            this.pictureWorker.emitStream('picture$', picture);
            this.pictureWorker.emitStream('pictures$', updatePictures);
          }
        }
    });  

    this.subs$.push(sub$);
  }

  getDescCssStyles(): ICssStyles {
    return { 'v-description': true };
  }

  getLikeStyles(): ICssStyles {
    return {
      'font-size': '2.3rem'
    }
  }

  getDescriptionText(): string {
    const { Description } = this.pictureWorker.getStreamValue('picture$');
    const isEditable: boolean = this.editable();
    const isExists: boolean = !!Description;

    if(!isEditable) {
      return isExists ? Description : 'Описание';
    } else {
      return isExists ? Description : 'Редактировать описание';
    }
  }

  editable(): boolean {
    return this.pictureWorker.editable();
  }

  setSlide(side: Side) {
    this.pictureWorker.setSlide(side);
  }

  createPictureUser(): IUser {
    const picture: IPicture = this.pictureWorker.getStreamValue('picture$');
    const { CreatorId: _id, CreatorName: Name, CreatorAvatar: Avatar} = picture;

    const user: IUser = { _id, Name, Avatar };

    return user;
  }

  hide(): void {
    this.pictureWorker.destoy();
  }

  navigateTo(user: IUser): void {
    this.router.navigate(['/account', user._id]);
  }

  like(): void {
    this.pictureWorker.like();
  }

  edit(description: string): void {
    this.pictureWorker.edit(description)
  }

  save(picture: IPicture): void {
    const { isSaveRequest } = this.flags;
    
    if(!isSaveRequest) {
      this.pictureWorker.save(picture);
    }
  }

}
