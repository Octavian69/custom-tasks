import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { PictureAlbumsViewService } from 'src/app/services/picture-albums.view.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { PictureAlbumsFlags } from 'src/app/models/flags/PictureAlbumsFlags';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { Page } from 'src/app/models/Page';
import { Observable } from 'rxjs';
import { IPictureAlbum } from 'src/app/interfaces/IPictureAlbum';
import { MediaList } from 'src/app/models/MediaList';
import { isExpand } from 'src/app/shared/handlers/handlers';
import { SimpleAction } from 'src/app/models/SimpleAction';

@Component({
  selector: 'cst-picture-albums',
  templateUrl: './picture-albums.component.html',
  styleUrls: ['./picture-albums.component.scss']
})
export class PictureAlbumsComponent implements OnInit, OnDestroy {

  albums$: Observable<MediaList<IPictureAlbum>> = this.viewService.getStream('albums$');

  @Input('userId') userId: string;
  isOwn: boolean = false;
  
  page: Page;
  limit: number = LIMIT.PICTURE_ALBUMS;
  flags: PictureAlbumsFlags = this.viewService.getFlagObject();

  constructor(
    private router: Router,
    private activeRouter: ActivatedRoute,
    private viewService: PictureAlbumsViewService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.viewService.setFlag('isCanMutate', false);
    this.viewService.destroySubjects(['albums$']);
  }

  init(): void {
    this.isOwn = this.viewService.isOwn(this.userId);
    this.page = new Page(0, this.limit);

    this.viewService.setFlag('isCanMutate', this.isOwn);

    this.viewService.fetch(this.userId, this.page, 'replace');
  }

  trackByFn(index: number, album: IPictureAlbum): string {
    return album._id;
  }

  expand(): void {
    const mediaList: MediaList<IPictureAlbum> = this.viewService.getStreamValue('albums$');
    const expand: boolean = isExpand(mediaList);

    if(expand) {
      this.page.skip++;
      this.viewService.fetch(this.userId, this.page, 'expand');
    }
  }

  edit(album: IPictureAlbum) {
    this.router.navigate([], {
      relativeTo: this.activeRouter,
      queryParams: {
        page: 'albums',
        album_id: album._id
      }
    });
  }

  detail(event: SimpleAction<IPictureAlbum>): void {
    const { action, payload } = event;

    const queryParams: Params = { 
      page: 'detail',
      detail_id: payload._id,
      modeType: action
     };

    this.router.navigate([], {queryParams});
  }

}
