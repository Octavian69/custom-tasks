import { Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { IPicture } from 'src/app/interfaces/IPicture';
import { SliderManager } from 'src/app/models/info/SliderManager';
import { changeTop, fade, fadeEvent, scaleShow } from 'src/app/shared/animations/animation';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { interval, Subscription } from 'rxjs';
import { MatSliderChange } from '@angular/material/slider';
import { Side } from 'src/app/types/picture.types';

@Component({
  selector: 'cst-picture-detail',
  templateUrl: './picture-detail.component.html',
  styleUrls: ['./picture-detail.component.scss'],
  animations: [ changeTop(300, 100), fade, fadeEvent, scaleShow ]
})
export class PictureDetailComponent implements OnDestroy {
  
  interval$: Subscription;
  intevalStep: number = 3;
  checkedInterval: boolean = false;

  fullscreenStatus: boolean = false;

  @Input('picture') picture: IPicture;
  @Input('manager') manager: SliderManager;
  @Input('request') request: boolean = false;
  @Output('setSlide') _setSlide = new EventEmitter<Side>();
  @Output('save') _save = new EventEmitter<IPicture>();

  ngOnDestroy() {
    this.setInterval(false);
  }

  load(e: Event) {
    // const elem$  = e.target as HTMLImageElement;

    // console.log(elem$)
    // console.log(elem$.naturalHeight)
    // console.log(elem$.naturalWidth)
  }

  getFullScreenIcon(): string {
    return this.fullscreenStatus ? 'fullscreen_exit' : 'fullscreen';
  }

  isShowInterval(): boolean {
    return this.manager.totalCount > 1 && (this.fullscreenStatus || this.checkedInterval);
  }

  fullscreen(flag: boolean): void {
    this.fullscreenStatus = flag;
  }

  setInterval(flag: boolean): void {

    if(flag) {
      const step: number = this.intevalStep * 1000;

      this.interval$ = interval(step).subscribe(_ => {
        this.setSlide('next');
      })

    } else{

      if(this.interval$) {
        this.interval$.unsubscribe();
        this.interval$ = null;
      }
    }
  }

  changeStep(e: MatSliderChange): void {
    const { value } = e;

    if(!this.checkedInterval) this.checkedInterval = true;
    
    this.setInterval(false);

    this.intevalStep = value;

    this.setInterval(true);
  }

  show(e: MatCheckboxChange): void {
    const { checked } = e;

    this.checkedInterval = checked;
    this.setInterval(checked);

  }

  setSlide(side: Side): void {
    if(!this.request) this._setSlide.emit(side);
  }

  save(picture: IPicture): void {
    this._save.emit(picture);
  }
}
