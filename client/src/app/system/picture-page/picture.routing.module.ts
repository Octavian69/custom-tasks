import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PicturePageComponent } from './picture-page/picture-page.component';
import { PictureAlbumsComponent } from './picture-albums/picture-albums.component';
import { PictureMainListComponent } from './picture-main-list/picture-main-list.component';
import { PictureAlbumDetailComponent } from './picture-album-detail/picture-album-detail.component';

const routes: Routes = [
    {path: '', component: PicturePageComponent, children: [
        {path: 'main-list/:id', component: PictureMainListComponent},
        {path: 'albums/:id', component: PictureAlbumsComponent},
        {path: 'detail/:id', component: PictureAlbumDetailComponent},
    ]}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PictureRoutingModule {}