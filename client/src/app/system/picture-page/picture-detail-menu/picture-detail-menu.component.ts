import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SliderManager } from 'src/app/models/info/SliderManager';
import { IPicture } from 'src/app/interfaces/IPicture';
import { IUser } from 'src/app/interfaces/IUser';
import { UserViewService } from 'src/app/services/user.view.service';
import { Observable } from 'rxjs';
import { scaleShow } from 'src/app/shared/animations/animation';

@Component({
  selector: 'cst-picture-detail-menu',
  templateUrl: './picture-detail-menu.component.html',
  styleUrls: ['./picture-detail-menu.component.scss'],
  animations: [scaleShow]
})
export class PictureDetailMenuComponent {
  
  user$: Observable<IUser> = this.userViewService.getStream('user$');

  @Input('picture') picture: IPicture;
  @Input('user') user: IUser;
  @Input('manager') manager: SliderManager;

  @Output('save') _save = new EventEmitter<IPicture>();

  constructor(
    private userViewService: UserViewService
  ) { }

  isShowMenu(): boolean {
    return this.isShowUpdateAvatar() || this.isShowSaved();
  }

  isShowUpdateAvatar(): boolean {
    const user: IUser = this.userViewService.getStreamValue('user$');

    if(user) {
      return user.Avatar === this.picture.Path ? false : true;
    }

    return false;
  }

  isShowSaved(): boolean {
    const { IsOwnAlbum, albumType } = this.manager;

    return albumType === 'Saved' && IsOwnAlbum ? false : true;
  }

  getSavedIcon(): string {
    const { IsHaveSaved } = this.picture;

    return IsHaveSaved ? 'folder' : 'folder_open'
  }

  getSavedText(): string {
    const { IsHaveSaved } = this.picture;

    return IsHaveSaved ? 'Убрать из сохраненных' : 'Добавить в сохраненные';
  }

  updateAvatar(): void {
    this.userViewService.updateAvatar(this.picture);
  }

  save(): void {
    this._save.emit(this.picture);
  }

}
