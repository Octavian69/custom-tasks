import { Component, OnInit, Input, OnDestroy, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, AbstractControl } from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { Subscription } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';
import { InfoControl } from 'src/app/models/info/InfoControl';
import { SHARED } from 'src/app/namespaces/Namespaces';
import { topShow, transitionHeight, scaleShow } from 'src/app/shared/animations/animation';
import { EditViewService } from 'src/app/services/edit.view.service';
import { IVkResponse } from 'src/app/interfaces/IVkResponse';
import { ListOption } from 'src/app/types/shared.types';
import { MatSelect } from '@angular/material/select';
import { FormControlType, MaterialControlRef } from 'src/app/types/form.types';

@AutoUnsubscribe({ arrayName: 'subs$' })
@Component({
  selector: 'cst-edit-input',
  templateUrl: './edit-input.component.html',
  styleUrls: ['./edit-input.component.scss'],
  providers: [ 
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: SHARED.DATE_FORMATS},
  ],
  animations: [topShow, transitionHeight, scaleShow]
})
export class EditInputComponent implements OnInit, OnDestroy {
  value: any;

  isShowSearch: boolean = false;
  searchData: ListOption[] = [];

  subs$: Subscription[] = [];

  searchControl: FormControl;

  matSelectRef: MatSelect;

  @Input('form') form: FormGroup;
  @Input('config') config: InfoControl;
  @Output('reset') _reset: EventEmitter<string[]> = new EventEmitter();

  constructor(
    private adapter: DateAdapter<any>,
    private viewService: EditViewService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {}

  init(): void {
    this.adapter.setLocale('ru');

    if(this.config.async) {
      this.vkApiSub();
    } 
  }

  initialize(ref$: MaterialControlRef, type: FormControlType) : void {
    switch(type) {
      case 'select': {
        this.matSelectRef = ref$ as MatSelect;
        break;
      }
    }
  }

  vkApiSub(): void {
    const { controlName } = this.config;

    const { value } = this.form.get(controlName);

    if(value) {
      this.config.selectData.push(value);
    } 

    this.form.get(controlName).valueChanges
      .pipe(untilDestroyed(this))
      .subscribe((value: ListOption) => {
        this.reset();
  
        if(value) {
          this.makeVkRequest();
        }
      });
  }

  focus(e: Event) {
    if(this.config.async) {
      this.makeVkRequest();
    }
  }

  makeVkRequest(): void {
    const { method, params, requestStatus } = this.viewService.completeVkQuery(this.form, this.config);

    if(requestStatus && this.config.isCanRequest) {
      const { controlName } = this.config;
      const { value } = this.form.get(controlName);
  
      const next = (response: IVkResponse) => {
        const { response: { items } } = response;
  
        if(value) {
          const idx: number = items.findIndex(c => c.id === value.id);

          ~idx ? items[idx] = value : items.unshift(value);
        } 

        this.config.selectData = items;
        this.config.isCanRequest = false;
        this.actions();
      }
  
      this.viewService
      .getOptionsRequest(method, params)
      .pipe(untilDestroyed(this))
      .subscribe({ next });
    }
  }

  actions(): void {
    switch(this.config.type) {
      case "select": {
        setTimeout(_ => {
          this.matSelectRef.open();
        }, 0);
        break;
      }
    }
  }

  reset(): void {
    const { controlName } = this.config;

    const dependentControls: string[] = this.viewService.getControlDeps(controlName);
    this._reset.emit(dependentControls);
  }

  minDate(): Date {
    const { controlName } = this.config;
    const start: AbstractControl = this.form.get('StartDate');

    switch(controlName) {
      case 'StartDate': {
        return null
      }
      case 'EndDate': {
        return start.value || null;
      }
      default: {
        return null
      }
    }
  }

  maxDate(): Date {
    const { controlName } = this.config;
    const end: AbstractControl = this.form.get('EndDate');

    switch(controlName) {
      case 'StartDate': {
        return end.value || null;
      }
      case 'EndDate': {
        return null;
      }
      default: {
        return new Date();
      }
    }
  }

  showSearch(): void {
    if(this.isShowSearch) {
      this.isShowSearch = !this.isShowSearch;
      this.searchControl = null;
      this.searchData = [];
    } else {
      this.searchControl = new FormControl('', [], []);
      this.initSearch();
      this.isShowSearch = !this.isShowSearch;
    } 
  }

  initSearch(): void {
    this.searchControl.valueChanges.pipe(
      debounceTime(400),
      switchMap((value: string) => {
        return this.viewService.getSearchOptions(this.form, this.config, value);
      }),
      untilDestroyed(this)
    ).subscribe((res: IVkResponse) => {
       const { response: { items } } = res;
       this.searchData = items;
    })
  }

  setValue(item: ListOption): void {
    const { controlName, selectData } = this.config;
    selectData.push(item);
    this.form.get(controlName).setValue(item);
    this.form.markAsTouched();
    this.showSearch();
  }
}
