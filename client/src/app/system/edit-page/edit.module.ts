import { NgModule } from "@angular/core";
import { EditPageComponent } from './edit-page/edit-page.component';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { EditRoutingModule } from './edit.routing.module';
import { EditListComponent } from './edit-nav/edit-nav.component';
import { EditViewService } from 'src/app/services/edit.view.service';
import { EditInputComponent } from './edit-input/edit-input.component';
import { EditFormComponent } from './edit-form/edit-form.component';
import { EditAddFormComponent } from './edit-add-form/edit-add-form.component';
import { EditEducationHeaderComponent } from './edit-education-header/edit-education-header.component';
import { EditSingleFormComponent } from './edit-single-form/edit-single-form.component';
import { EditArrayFormsComponent } from './edit-array-forms/edit-array-forms.component';

@NgModule({
    declarations: [
        EditPageComponent, 
        EditListComponent, 
        EditInputComponent, 
        EditFormComponent, 
        EditAddFormComponent,
        EditEducationHeaderComponent,
        EditSingleFormComponent,
        EditArrayFormsComponent
    ],
    imports: [
        SharedModule,
        EditRoutingModule
    ],
    providers: [
        EditViewService
    ]
})
export class EditModule {}