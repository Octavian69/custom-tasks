import { Component, Input, Output, EventEmitter } from '@angular/core';
import { EditSingleState } from 'src/app/models/EditSingleState';
import { EditFlags } from 'src/app/models/flags/EditFlags';

@Component({
  selector: 'cst-edit-single-form',
  templateUrl: './edit-single-form.component.html',
  styleUrls: ['./edit-single-form.component.scss']
})
export class EditSingleFormComponent {

  @Input('flags') flags: EditFlags;
  @Input('state') state: EditSingleState;
  @Output('submitForm') submit = new EventEmitter<void>();
  
  onSubmit(): void {
    this.submit.emit();
  }
}
