import { Component, OnDestroy } from '@angular/core';
import { topShow, fade } from 'src/app/shared/animations/animation';
import { EditViewService } from 'src/app/services/edit.view.service';
import { EditFlags } from 'src/app/models/flags/EditFlags';
import { EditLink } from 'src/app/types/edit.types';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'cst-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss'],
  animations: [topShow, fade]
})
export class EditPageComponent implements OnDestroy {

  flags: EditFlags = this.viewService.getFlagObject();
  navOptions: EditLink[] = this.viewService.getDataByArrayKeys(['edit', 'list']);
  pageTitle: string;
  isEducation: boolean = false;

  constructor(
    public viewService: EditViewService,
    private storage: StorageService
  ) { }

  ngOnDestroy() {
    this.storage.removeItem('sessionStorage', 'editPageTitle');
    this.storage.removeItem('sessionStorage', 'editPageName');
    this.viewService.destroy();
  }
  
  initState(path: string) {
    this.viewService.initState(path);
  }

  setPage(editLink: EditLink): void {
    const { title, path, id } = editLink;
    
    this.pageTitle = title;
    this.isEducation = path.includes('Education');
    this.storage.setItem('sessionStorage', 'editPage', id);
    this.storage.setItem('sessionStorage', 'editPageTitle', title);
    this.storage.setItem('sessionStorage', 'editPageName', path);

    this.initState(path);
  }
}
