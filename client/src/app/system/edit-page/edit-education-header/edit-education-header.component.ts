import { Component, ViewChild, ElementRef, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { EditLink, TEditPageName } from 'src/app/types/edit.types';

@Component({
  selector: 'cst-edit-education-header',
  templateUrl: './edit-education-header.component.html',
  styleUrls: ['./edit-education-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditEducationHeaderComponent {
  
  @Output('page') page = new EventEmitter<EditLink>();
  @ViewChild('edRef', { static: true }) edRef: ElementRef;

  constructor(
    private router: Router
  ) { }

  getStyles(): ICssStyles {
    const { queryParams: { page } } = this.router.parseUrl(this.router.url);
    const index: number = page === 'Education' ? 0 : 1;
    const { nativeElement } = this.edRef;

    const width: number = nativeElement.children[index].clientWidth;
    const offset: number = index ? nativeElement.children[0].clientWidth : 0;

    return {
      width: `${width}px`,
      left: `${offset}px`
    }
  }

  navigate(path: TEditPageName, title: string): void {
    this.router.navigate(['/edit'], {
      queryParams: {
        page: path
      }
    });

    this.page.emit({ path, title, id: 3 });
  }

}
