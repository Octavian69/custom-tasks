import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { InfoControl } from 'src/app/models/info/InfoControl';

@Component({
  selector: 'cst-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent {

  @Input('form') form: FormGroup;
  @Input('configuration') configuration: InfoControl[];
  @Input('first') first: boolean;
  @Output('confirm') _confirm = new EventEmitter<string>();

  confirm() {
    const { _id } = this.form.value;
    this._confirm.emit(_id);
  }

  resetControls(controls: string[]): void {

    if(!controls) return
    const filtered: InfoControl[] = this.configuration.filter(c => controls.includes(c.controlName));

    filtered.forEach(c => {
      c.selectData = [];
      c.isCanRequest = true;
      this.form.get([c.controlName]).patchValue(null, { emitEvent: false });
    })
  }

}
