import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cst-edit-add-form',
  templateUrl: './edit-add-form.component.html',
  styleUrls: ['./edit-add-form.component.scss']
})
export class EditAddFormComponent {

  @Input() text: string = '';
  @Output('add') add = new EventEmitter<void>();

  constructor() { }

  action() {
    this.add.emit();
  }

}
