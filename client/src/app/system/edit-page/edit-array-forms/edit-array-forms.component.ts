import { Component, Input, Output, EventEmitter } from '@angular/core';
import { EditArrayState } from 'src/app/models/EditArrayState';
import { EditFlags } from 'src/app/models/flags/EditFlags';
import { EditViewService } from 'src/app/services/edit.view.service';

@Component({
  selector: 'cst-edit-array-forms',
  templateUrl: './edit-array-forms.component.html',
  styleUrls: ['./edit-array-forms.component.scss']
})
export class EditArrayFormsComponent {

  addFormTitles: any = this.viewService.getDataByArrayKeys(['edit', 'addFormTitles']);
  removeFormTitles: any = this.viewService.getDataByArrayKeys(['edit', 'removeFormTitles']);

  @Input('flags') flags: EditFlags;
  @Input('state') state: EditArrayState;
  @Output('confirm') confirm = new EventEmitter<string>();
  @Output('remove') remove = new EventEmitter<boolean>();
  @Output('addForm') addForm = new EventEmitter<void>();
  @Output('submit') submit = new EventEmitter<void>();

  constructor(
    private viewService: EditViewService
  ) { }

  showConfirm(id: string): void {
    this.confirm.emit(id);
  }

  confirmRemove(flag: boolean): void {
    this.remove.emit(flag);
  }

  addDefaultForm(): void {
    this.addForm.emit();
  }

  onSubmit(): void {
    this.submit.emit();
  }

}
