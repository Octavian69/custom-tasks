import { Component, Output, EventEmitter, Input, ChangeDetectionStrategy, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { EditLink, TEditPageName } from 'src/app/types/edit.types';
import { topShow } from 'src/app/shared/animations/animation';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'cst-edit-nav',
  templateUrl: './edit-nav.component.html',
  styleUrls: ['./edit-nav.component.scss'],
  animations: [topShow],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditListComponent {
  
  options: EditLink[];
  fullOptions: EditLink[]

  @HostBinding('@topShow') topShow: any;
  
  @Input('options') set _options(options: EditLink[]) {
    if(options) {
      this.options = this.getOptions(options);
      this.fullOptions = options;
    } 
  }
  @Output('page') page = new EventEmitter<EditLink>();

  constructor(
    private router: Router,
    private storage: StorageService
  ) { }

  getOptions(options: EditLink[]): EditLink[] {
    return options.filter((option: EditLink) => option.path !== 'HigherEducation');
  }

  setPage(idx: number): void {

    let option: EditLink;

    switch(idx) {
      case 3: { //Education
        const pageName: TEditPageName = this.storage.getItem('sessionStorage', 'editPageName');

        if(pageName === 'HigherEducation') {
          option = this.fullOptions.find(opt => opt.path === 'HigherEducation');
        } else {
          option = this.options[idx];
        }

        break;
      }

      default: {
        option = this.options[idx]
      }
    }
    // const option: EditLink = this.options[idx];

    this.router.navigate(['/edit'], { 
      queryParams: {page: option.path}
     });

    this.page.emit(option);
  }

}
