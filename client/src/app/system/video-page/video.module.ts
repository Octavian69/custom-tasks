import { NgModule } from '@angular/core';
import { VideoUnitModalComponent } from './video-unit-modal/video-unit-modal.component';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { VideoRoutingModule } from './video.routing.module';
import { VideoPlaylistViewService } from 'src/app/services/video-playlist.view.service';
import { VideoMainListComponent } from './video-main-list/video-main-list.component';
import { VideoPlaylistsComponent } from './video-playlists/video-playlists.component';
import { VideoUnitComponent } from './video-unit/video-unit.component';
import { VideoPlaylistsUnitComponent } from './video-playlists-unit/video-playlists-unit.component';
import { VideoPageComponent } from './video-page/video-page.component';
import { VideoNavComponent } from './video-nav/video-nav.component';
import { VideoFiltersComponent } from './video-filters/video-filters.component';
import { VideoCreatePlaylistModalComponent } from './video-create-playlist-modal/video-create-playlist-modal.component';
import { VideoPlaylistDetailComponent } from './video-playlist-detail/video-playlist-detail.component';

@NgModule({
    declarations: [
        VideoPageComponent,
        VideoMainListComponent,
        VideoUnitComponent,
        VideoUnitModalComponent,
        VideoPlaylistsComponent,
        VideoPlaylistsUnitComponent, 
        VideoNavComponent, 
        VideoFiltersComponent, 
        VideoCreatePlaylistModalComponent, 
        VideoPlaylistDetailComponent
    ],
    imports: [
        SharedModule,
        VideoRoutingModule
    ],
    providers: [
        VideoPlaylistViewService
    ]
})
export class VideoModule{}