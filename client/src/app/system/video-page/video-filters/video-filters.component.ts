import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { fade, animateHeight } from 'src/app/shared/animations/animation';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { unsubscriber } from 'src/app/shared/handlers/handlers';
import { FilterOptions, FilterControls } from 'src/app/types/media.types';
import { AnyOption, FilterSettings } from 'src/app/types/shared.types';

@Component({
  selector: 'cst-video-filters',
  templateUrl: './video-filters.component.html',
  styleUrls: ['./video-filters.component.scss'],
  animations: [fade, animateHeight('15px')]
})
export class VideoFiltersComponent implements OnInit, OnDestroy {

  Duration: FormControl;
  LikesTotal: FormControl;
  Created: FormControl;

  isActiveFilters: boolean = false;
  isShowFilters: boolean = false;

  filters: AnyOption = {};
  sorted: AnyOption = {};

  subs$: Subscription[] = []

  @Input('controls') controls: FilterControls;
  @Input('filterOptions') filterOptions: FilterOptions;
  @Input('sortedOptions') sortedOptions: FilterOptions;
  @Output('filter') _filter = new EventEmitter<FilterSettings>();
  @Output('reset') _reset = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
    this.initControls();
  }

  ngOnDestroy() {
    unsubscriber(this.subs$);
  }

  initControls(): void {
    const concatControls: string[] = this.getConcatControls();

    concatControls.forEach((controlName: string) => {
      this[controlName] = new FormControl(null, [], []);
    });

    this.controls.sorted.forEach((controlName: string) => {
      this.initSortSub(controlName);
    });
  }

  initSortSub(controlName: string): void {
    const sub$: Subscription =(this[controlName] as FormControl).valueChanges.subscribe((value: any) => {
      const currentSort: string = Object.keys(this.sorted)[0];

      
      if(currentSort && currentSort !== controlName) {
        (this[currentSort] as FormControl as FormControl).reset(null, { emitEvent: false });
      }
      
      this.sorted = value ? { [controlName]: value } : {};
      
      this.submitFilter();
    });

    this.subs$.push(sub$);

  }

  getConcatControls(): string[] {
    const { filter, sorted } = this.controls;
    const concatControls: string[] = [...filter, ...sorted];

    return concatControls;
  }

  getFilterIconClass(): ICssStyles {
    return {
      'icon': !this.isActiveFilters,
      'active': this.isActiveFilters
    }
  }

  getFilterTooltip(): string {
    const action: string = this.isShowFilters ? 'Скрыть' : 'Показать';

    return `${action} фильтр`
  }

  getAnimationState(): string {
    return String(this.isShowFilters);
  }

  showFilters(): void {
    this.isShowFilters = !this.isShowFilters;
  }

  changeFilter(key: string , value: any): void {
    if(!value) {
      delete this.filters[key];
    } else {
      this.filters[key] = value;
    }

    this.submitFilter();
  }

  resetControls(): void {
    const concatControls: string[] = this.getConcatControls();

    concatControls.forEach((controlName: string) => {
      (this[controlName] as FormControl).patchValue(null, { emitEvent: false });
    })
  }

  reset(): void {
    this.filters = {};
    this.sorted = {};

    this.resetControls();

    this.submitFilter();
  }

  detectedCompleteFilters() {
    
    const concatControls: string[] = this.getConcatControls();
    
    const isComplete: boolean = concatControls.some((controlName: string) => {
      return this[controlName].value;
    });

    this.isActiveFilters = isComplete;
  }

  submitFilter(): void {
    this.detectedCompleteFilters();


    if(this.isActiveFilters) {
      this._filter.emit({ filters: this.filters, sorted: this.sorted });
    } else {
      this._reset.emit()
    }
  }

}
