import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { FormControl } from '@angular/forms';
import { VideoViewService } from 'src/app/services/video.view.service';
import { ActivatedRoute, Router } from '@angular/router';
import { VideoMainFlags } from 'src/app/models/flags/VideoMainFlags';
import { MediaRequest } from 'src/app/models/MediaRequest';
import { LIMIT } from 'src/app/namespaces/Namespaces'
import { Observable, Subscription } from 'rxjs';
import { MediaList } from 'src/app/models/MediaList';
import { IVideo } from 'src/app/interfaces/IVideo';
import { debounceTime } from 'rxjs/operators';
import { VideoWorkService } from 'src/app/services/video.work.service';
import { ICurrentMediaList } from 'src/app/interfaces/ICurrentMediaList';
import { CurrentMediaList } from 'src/app/models/CurrentMediaList';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { unsubscriber, deepCopy, isExpand } from 'src/app/shared/handlers/handlers';
import { FilterOptions, FilterControls } from 'src/app/types/media.types';
import { FilterSettings, SipmleAction } from 'src/app/types/shared.types';

@Component({
  selector: 'cst-video-main-list',
  templateUrl: './video-main-list.component.html',
  styleUrls: ['./video-main-list.component.scss']
})
export class VideoMainListComponent implements OnInit, OnDestroy {

  creatorId: string;
  
  main$: Observable<MediaList<IVideo>> = this.viewService.getStream('main$');
  search$: Observable<MediaList<IVideo>> = this.viewService.getStream('search$');

  playlistId: string;
  searchControl: FormControl = new FormControl(null, [], []);
  mainMediaReq: MediaRequest;
  searchMediaReq: MediaRequest;
  flags: VideoMainFlags = this.viewService.getFlagObject();
  subs$: Subscription[] = [];
  
  filterControls: FilterControls = this.viewService.getDataByArrayKeys(['video', 'filterControls']);
  filterOptions: FilterOptions = this.viewService.getDataByArrayKeys(['video', 'filterOptions']);
  sortedOptions: FilterOptions = this.viewService.getDataByArrayKeys(['video', 'sortedOptions']);
  
  @Input('creatorId') set isCanMutate(value: string) {
    if(value) {
      const isOwn: boolean = this.viewService.isOwn(value);
      this.viewService.setFlag('isCanMutate', isOwn);
      this.videoWorker.setFlag('isCanMutate', isOwn);
      
      this.creatorId = value;
    }
  }
  
  @Input('modeType') modeType: 'main' | 'detail' = 'main';
  @Input('hideAddIcon') hideAddIcon: boolean = false;
  @Input('showClass') showClass: string;
  @Output('mutate') _mutate = new EventEmitter<SipmleAction<IVideo>>();

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private viewService: VideoViewService,
    private videoWorker: VideoWorkService
  ) { }

  ngOnInit() {
    this.playlistId = this.activeRoute.snapshot.params.id;
    this.initWorker();
    this.initSubs();
    this.init();
  }
  
  ngOnDestroy() {
    this.viewService.reset();
    unsubscriber(this.subs$);

    this.viewService.setFlag('isCanMutate', false);
    this.videoWorker.setFlag('isCanMutate', false);
    this.videoWorker.setFlag('isShowAddInOwn', false);
    this.videoWorker.setField('playlistId', null);
  }

  

  init(): void {
    this.createMediaReq('main');
    this.viewService.fetch(this.mainMediaReq, 'replace', 'isLoadingMain');
  }

  initWorker() {
    const isOwn: boolean = this.viewService.isOwn(this.playlistId);

    this.videoWorker.setField('playlistId', this.playlistId);
    this.videoWorker.setFlag('isShowAddInOwn', !isOwn);

  }

  initSubs(): void{
    this.initSearchSub();
    this.initMutateSub();
  }

  createMediaReq(type: 'main' | 'search'): void {
    const reqName = type === 'main' ? 'mainMediaReq' : 'searchMediaReq';
    this[reqName] = new MediaRequest(type, this.playlistId, LIMIT.VIDEO); 
  }

  initMutateSub(): void {
    const sub$: Subscription = this.videoWorker.getStream('mutate$')
      .subscribe((event: SimpleAction<IVideo>) => {
          const { action } = event;

          const isDetatil: boolean = Object.is(action, 'edit') || Object.is(action, 'like');

          if(isDetatil) {
            this.viewService.mutateDetail(event, 'main');
            this.viewService.mutateDetail(event, 'search');
          } else {
            this.viewService.mutate(event);
          }
      })

      this.subs$.push(sub$);
  }

  initSearchSub(): void {
    const sub$: Subscription = this.searchControl.valueChanges
    .pipe(debounceTime(400))
    .subscribe((value: string) => {
      if(value) {
        if(!this.searchMediaReq) this.createMediaReq('search');

        this.mainMediaReq.filters.Title = value;
        this.searchMediaReq.filters.Title = value;
        this.mainMediaReq.page.skip = 0;
        this.searchMediaReq.page.skip = 0;

        this.viewService.fetch(this.mainMediaReq, 'replace', 'isLoadingMain');
        this.viewService.fetch(this.searchMediaReq, 'replace', 'isLoadingSearch');

      } else {
        this.searchMediaReq = null;
        this.init();
      }
    });

    this.subs$.push(sub$);
  }
  
  getIsOwn(): boolean {
    return this.viewService.isOwn(this.playlistId);
  }

  getShowClass(): ICssStyles {
    return {
      [this.showClass]: true
    }
  }

  getCssListClass(listName: 'main$' | 'search$'): ICssStyles {
    const mediaList: MediaList<IVideo> = this.viewService.getStreamValue(listName);

    const isCenter: boolean = this.modeType === 'detail' && mediaList && mediaList.rows.length > 1

    return {
      'content-center': isCenter
    }
  }

  getDisableExpand(): boolean {
    const isExpandMain: boolean = this.viewService.getFlag('isLoadingMain') as boolean;
    const isExpandSearch: boolean = this.viewService.getFlag('isLoadingSearch') as boolean; 

    return isExpandMain || isExpandSearch;
  }

  getStatus(): boolean {
    const isOwnList: boolean = this.getIsOwn();

    const isAddInOwnList: boolean = this.modeType === 'main' && !isOwnList;

    return isAddInOwnList;
  }

  trackByFn(index: number, video: IVideo): string {
    return video._id
  }

  updateFilters(reqType: 'main' | 'search', settings: FilterSettings): void {
    const reqName = `${reqType}MediaReq` as 'mainMediaReq' | 'searchMediaReq';
    const req: MediaRequest = this[reqName];

    if(req) {
      const flagName = reqType === 'main' ? 'isLoadingMain' : 'isLoadingSearch';
      const { filters, sorted } = settings;

      req.page.skip = 0;
      req.filters = filters;
      req.sorted = sorted;
      
      this.viewService.fetch(req, 'replace', flagName);
    }

  }

  includedRequest(video: IVideo): boolean {
    return this.flags.mutateRequestArray.includes(video._id);
  }

  navigate(userId: string): void {
    this.router.navigate(['/account', userId]);
  }

  play(video: IVideo, listType: 'main' | 'search'): void {
    const reqName: 'mainMediaReq' | 'searchMediaReq' = listType === 'main' ? 'mainMediaReq' : 'searchMediaReq';
    const videoList: MediaList<IVideo> = this.viewService.getStreamValue(`${listType}$`);

    const candidateList: MediaList<IVideo> = deepCopy(videoList);
    const candidateReq: MediaRequest = deepCopy(this[reqName]);
    
    const currentList: ICurrentMediaList<IVideo> = new CurrentMediaList(this.playlistId, candidateList, candidateReq);

    this.videoWorker.changeCurrentList(currentList);
    this.videoWorker.changeVideo(video);
  }

  mutate(video: IVideo): void {
    const status: boolean = this.getStatus();
    const field = status ? 'IsHaveOwn' : 'IsHave';

    const method: 'add' | 'remove' =  video[field] ? 'remove' : 'add';
    const playlistId: string = status ? this.viewService.getOwnId() : this.playlistId;

    this.flags.mutateRequestArray.push(video._id);
  
    this.viewService[method](playlistId, video, field);

    if(this.modeType === 'detail') {
      const event: SimpleAction<IVideo> = new SimpleAction(method, video);
      this._mutate.emit(event);
    }
  }


  filter(settings: FilterSettings): void {
    if(!this.searchMediaReq) {
      this.createMediaReq('search');
    }

    this.updateFilters('main', settings);
    this.updateFilters('search', settings);
  }

  resetFilters() {
    const { value } = this.searchControl;

    this.updateFilters('main', { sorted: {}, filters: {} });

    if(value) {
      this.updateFilters('search', { sorted: {}, filters: {} })
    } else {
      this.searchMediaReq = null;
    }
  }

  ending(): void {
    this.expand('main');

    if(this.searchMediaReq) {
      this.expand('search');
    }
  }

  expand(reqType: 'search' | 'main'): void {
    const listName = reqType === 'main' ? 'main$' : 'search$';
    const reqName = reqType === 'main' ? 'mainMediaReq' : 'searchMediaReq';

    const currentVideo: IVideo = this.videoWorker.getStreamValue('currentVideo$');

    const mediaList: MediaList<IVideo> = this.viewService.getStreamValue(listName);

    
    const expand: boolean = isExpand(mediaList) && !currentVideo;

    if(expand) {
      const flagName = reqType === 'search' ? 'isLoadingSearch' : 'isLoadingMain';
      this[reqName].page.skip++;
      this.viewService.fetch(this[reqName], 'expand', flagName);
    }
  }
}
