import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';
import { Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';
import { IVideo } from 'src/app/interfaces/IVideo';
import { VideoWorkService } from 'src/app/services/video.work.service';
import { VideoWorkFlags } from 'src/app/models/flags/VideoWorkFlags';
import { unsubscriber, pathManager } from 'src/app/shared/handlers/handlers';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { changeTop } from 'src/app/shared/animations/animation';
import { RewindType } from 'src/app/types/video.types';
import { VIDEO } from 'src/app/namespaces/video.namespaces';

@Component({
  selector: 'cst-video-slider-playback',
  templateUrl: './video-slider-playback.component.html',
  styleUrls: ['./video-slider-playback.component.scss'],
  animations: [changeTop(200, 100)]
})
export class VideoSliderPlaybackComponent implements OnInit, AfterViewInit, OnDestroy {
  video: IVideo;

  currentTime: number = 0;
  previewCurrentTime: number = 0;

  isInitListener: boolean = false;
  
  offsetPreview: number = 0;
  isShowPreview: boolean = false;
  isFullScreen: boolean = false;
  flags: VideoWorkFlags = this.videoWorker.getFlagObject();
  subs$: Subscription[] = [];

  @HostListener('document:keydown.space', ['$event'])
    stopAutoScroll(e: KeyboardEvent) {
      e.preventDefault();
    }

  @HostListener('document:keyup.space')
    setPlayback() {
      this.playback();  
    }

  @HostListener('window:keyup.arrowleft', ['$event'])
  @HostListener('window:keyup.arrowright', ['$event'])
    rewindVideo(e: KeyboardEvent) {
      e.preventDefault();

      const side: RewindType = e.keyCode === 37 ? 'back' : 'forward';

      this.rewind(side);
    }

  @HostListener('window:keyup.shift.arrowright')
    nextVideo() {
      this.next();
    }
  
  @ViewChild('videoRef', { static: true }) videoRef: ElementRef;
  @ViewChild('previewRef', { static: false }) previewRef: ElementRef;

  constructor(
    private videoWorker: VideoWorkService
  ) { }
  
  ngOnInit() {
  }

  ngAfterViewInit() {
    this.initVideoStream();
  }

  ngOnDestroy() {
    this.destroyListener();
    unsubscriber(this.subs$);
  }

  destroyListener(): void {
    const elem$: HTMLVideoElement = this.getVideoElement();
    elem$.removeEventListener('timeupdate', this.updateTime)
  }

  initTimeUpdateListener() {
    const elem$: HTMLVideoElement = this.getVideoElement();

    elem$.addEventListener('timeupdate', this.updateTime)
  }

  updateTime = (e: Event): void => {
    const { currentTime } = e.target as HTMLVideoElement;
    this.currentTime = currentTime;
  }

  getVideoElement(): HTMLVideoElement {
    return this.videoRef.nativeElement;
  }

  getRangeLineWidth(): ICssStyles {
    const { Duration } = this.video;
    const width = ((this.currentTime * 100) / Duration) + '%';

    return { width };
  }

  getFullscreenText(): string {
    return this.isFullScreen
    ? 'Выйти из полноэкранного режима (Tab)'  
    : 'Полноэкранный режим (Tab)'
  }

  initVideoStream(): void {
    const sub$: Subscription = this.videoWorker.getStream('currentVideo$').pipe(delay(0))
      .subscribe((video: IVideo) => {

        const isEqual: boolean = this.video && video && video.Path === this.video.Path;

        this.video = video;

        if(!isEqual) this.changeVideo(video);
        
      })

      this.subs$.push(sub$);
  }

  rewind(side: RewindType): void {
    const elem$: HTMLVideoElement = this.getVideoElement();
    const { duration, currentTime } = elem$;

    const time: number = side === 'forward' ? currentTime + VIDEO.REWIND_TIME : currentTime - VIDEO.REWIND_TIME;

    switch(true) {
      case time > duration: {
        elem$.currentTime = duration;
        // this.next();
        break;
      }
      case time < 0: {
        elem$.currentTime = 0;
        break;
      }

      default: {
        elem$.currentTime = time;
      }
    }

  }

  getPlayIcon(): string {
    const elem$: HTMLVideoElement = this.getVideoElement();

    const { paused } = elem$;

    return paused ? 'play_arrow' : 'pause';
  }

  getVolumeValue(): number {
    const { volume } = this.getVideoElement();
    
    return Math.floor(volume * 100);
  }

  formatVolumeLabel = (value: number) => {
    return this.getVolumeValue();
  }

  getVolumeIcon(): string {
    const { volume } = this.getVideoElement();
        
    switch(true) {
        case !volume: {
            return 'volume_off';
        }

        case volume <= 0.5: {
            return 'volume_down';
        }
        
        default: {
            return 'volume_up';
        }
    }
  }

  getVolumeIconClass(): ICssStyles {
    const volumeIcon: string = this.getVolumeIcon();

    return { [volumeIcon]: true };
  }

  getFullScreenIcon(): string {
    return this.isFullScreen ? 'fullscreen_exit' : 'fullscreen';
  }

  getDurationPoint(e: MSGestureEvent): number {
    const { Duration } = this.video;
    const { offsetX, currentTarget } = e;
    const { offsetWidth } = currentTarget as HTMLDivElement;

    const offsetPoint: number = Math.floor( (offsetX * 100) / offsetWidth );
    const durationPoint: number = (Duration / 100) * offsetPoint;
    
    return durationPoint;
  }

  updatePreview(e: MSGestureEvent) {
    const { nativeElement } = this.previewRef;

    this.offsetPreview = e.offsetX;
    this.setTime(e, nativeElement);
    this.previewCurrentTime = nativeElement.currentTime;
  }

  setTime(e: MSGestureEvent, elem$: HTMLVideoElement): void {
    elem$.currentTime = this.getDurationPoint(e);
  }

  setRangeVolume(event: MatSliderChange): void {
    const elem$: HTMLVideoElement = this.getVideoElement();
      
    const { value } = event;
    const volume: number = value / 100;
    
    elem$.volume = volume;
  }

  setVolume(): void {
    const elem$: HTMLVideoElement = this.getVideoElement();
    const { volume } = elem$;

    elem$.volume = volume > 0 ? 0 : .5;
  
  }

  fullscreen(flag: boolean): void {
    this.isFullScreen = flag;
  }

  playback(): void {
    const elem$: HTMLVideoElement = this.getVideoElement();
    
    const { paused } = elem$;

    const action: 'play' | 'pause' = paused ? 'play' : 'pause';

     const promise: Promise<void> = elem$[action]() as Promise<void>

     if(action === 'play') {
        promise.then(null, e => e);
     }
  }

  changeVideo(video: IVideo): void {
    const elem$: HTMLVideoElement = this.getVideoElement();

    this.playback();
    elem$.currentTime = 0;
    elem$.setAttribute('src', pathManager(video.Path));
    this.playback();
  }

  next(): void {
    this.videoWorker.next();
  }

  reset(): void {

  }

  like(): void {
    this.videoWorker.like(this.video);
  }

  mutate(): void {
    const method: 'add' | 'remove' = this.video.IsHave ? 'remove' : 'add';
    
    this.videoWorker[method](this.video);
  }

}
