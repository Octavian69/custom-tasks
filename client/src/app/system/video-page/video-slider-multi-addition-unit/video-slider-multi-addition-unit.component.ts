import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IMultiVideoList } from 'src/app/interfaces/IMultiVideoList';

@Component({
  selector: 'cst-video-slider-multi-addition-unit',
  templateUrl: './video-slider-multi-addition-unit.component.html',
  styleUrls: ['./video-slider-multi-addition-unit.component.scss']
})
export class VideoSliderMultiAdditionUnitComponent implements OnInit {
  
  @Input('playlist') playlist: IMultiVideoList;
  @Input('request') request: boolean = false;
  @Output('mutate') _mutate = new EventEmitter<IMultiVideoList>();

  constructor() { }

  ngOnInit() {
  }

  mutate() {
    if(!this.request) this._mutate.next(this.playlist);
  }

}
