import { Component, OnInit, Input } from '@angular/core';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { IVideoPlaylist } from 'src/app/interfaces/IVideoPlaylist';
import { VideoPlaylistViewService } from 'src/app/services/video-playlist.view.service';
import { FormControl } from '@angular/forms';
import { MediaRequest } from 'src/app/models/MediaRequest';
import { Observable, Subscription } from 'rxjs';
import { MediaList } from 'src/app/models/MediaList';
import { VideoPlaylistsFlags } from 'src/app/models/flags/VideoPlaylistsFlags';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { ActivatedRoute, Router } from '@angular/router';
import { unsubscriber, isExpand } from 'src/app/shared/handlers/handlers';
import { debounceTime } from 'rxjs/operators';
import { VideoWorkService } from 'src/app/services/video.work.service';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { MediaNavigate } from 'src/app/types/media.types';

@Component({
  selector: 'cst-video-playlists',
  templateUrl: './video-playlists.component.html',
  styleUrls: ['./video-playlists.component.scss']
})
export class VideoPlaylistsComponent implements OnInit {
  
  main$: Observable<MediaList<IVideoPlaylist>> = this.viewService.getStream('main$');
  search$: Observable<MediaList<IVideoPlaylist>> = this.viewService.getStream('search$');

  userId: string;
  searchControl: FormControl = new FormControl(null, [], []);
  mainMediaReq: MediaRequest;
  searchMediaReq: MediaRequest;
  limit: number = LIMIT.VIDEO_PLAYLISTS;
  flags: VideoPlaylistsFlags = this.viewService.getFlagObject();
  subs$: Subscription[] = [];

  @Input('showClass') showClass: string;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private viewService: VideoPlaylistViewService,
    private videoWorker: VideoWorkService
  ) { }

  ngOnInit() {
    this.userId = this.activeRoute.snapshot.params.id;
    this.initSubs();
    this.init();
  }
  
  ngOnDestroy() {
    this.viewService.reset();
    unsubscriber(this.subs$);
  }

  init(): void {
    const isOwn: boolean = this.viewService.isOwn(this.userId);
    this.viewService.setFlag('isCanMutateList', isOwn);

    this.createMediaReq('main');
    this.viewService.fetch(this.mainMediaReq, 'replace', 'isLoadingMain');
  }

  initSubs(): void{
    this.initSearchSub();
    this.initMutateSub();
  }

  initMutateSub(): void {
    const sub$: Subscription = this.videoWorker.getStream('mutate$')
        .subscribe((event: SimpleAction<string>) => {
          this.viewService.mutateTotal(event, 'main$');
          this.viewService.mutateTotal(event, 'search$');
        })

      this.subs$.push(sub$)
  }

  initSearchSub(): void {
    const sub$: Subscription = this.searchControl.valueChanges
    .pipe(debounceTime(400))
    .subscribe((value: string) => {
      if(value) {
        if(!this.searchMediaReq) this.createMediaReq('search');

        this.mainMediaReq.searchString = value;
        this.searchMediaReq.searchString = value;
        
        this.viewService.fetch(this.mainMediaReq, 'replace', 'isLoadingMain');
        this.viewService.fetch(this.searchMediaReq, 'replace', 'isLoadingSearch');

      } else {
        this.searchMediaReq = null;
        this.init();
      }
    });

    this.subs$.push(sub$);
  }


  createMediaReq(type: 'main' | 'search'): void {
    const reqName = type === 'main' ? 'mainMediaReq' : 'searchMediaReq';
    this[reqName] = new MediaRequest(type, this.userId, this.limit); 
  }

  getShowClass(): ICssStyles {
    return {
      [this.showClass]: true
    }
  }

  getDisableExpand(): boolean {
    const isExpandMain: boolean = this.viewService.getFlag('isLoadingMain') as boolean;
    const isExpandSearch: boolean = this.viewService.getFlag('isLoadingSearch') as boolean; 

    return isExpandMain || isExpandSearch;
  }

  trackByFn(index: number, playlist: IVideoPlaylist): string {
    return playlist._id;
  }

  navigate(event: MediaNavigate<IVideoPlaylist>): void {
    const { navigateTo, payload } = event;
    const { User, _id } = payload;

    const path: string = navigateTo === 'user' ? `/account/${User}` : `${this.router.url}/video-playlist/${_id}`;

    this.router.navigateByUrl(path);
  
  }

  ending(): void {
    this.expand('main');

    if(this.searchMediaReq) {
      this.expand('search');
    }
  }

  expand(reqType: 'search' | 'main'): void {
    const listName = reqType === 'main' ? 'main$' : 'search$';
    const reqName = reqType === 'main' ? 'mainMediaReq' : 'searchMediaReq';
    
    const mediaList: MediaList<IVideoPlaylist> = this.viewService.getStreamValue(listName);
    
    const expand: boolean = isExpand(mediaList);

    if(expand) {
      const flagName = reqType === 'search' ? 'isLoadingSearch' : 'isLoadingMain';
      this[reqName].page.skip++;
      this.viewService.fetch(this[reqName], 'expand', flagName);
    }
  }

  
}
