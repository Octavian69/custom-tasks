import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { IVideo } from 'src/app/interfaces/IVideo';
import { VideoWorkService } from 'src/app/services/video.work.service';
import { CurrentMediaList } from 'src/app/models/CurrentMediaList';
import { containCssClass } from 'src/app/shared/handlers/handlers';
import { VideoWorkFlags } from 'src/app/models/flags/VideoWorkFlags';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';

@Component({
  selector: 'cst-video-slider',
  templateUrl: './video-slider.component.html',
  styleUrls: ['./video-slider.component.scss']
})
export class VideoSliderComponent {

  video$: Observable<IVideo> = this.videoWorker.getStream('currentVideo$');
  list$: Observable<CurrentMediaList<IVideo>> = this.videoWorker.getStream('currentList$');

  maxHeight: string;
  flags: VideoWorkFlags = this.videoWorker.getFlagObject();

  constructor(
    private videoWorker: VideoWorkService
  ) { }

  getMainStyles(): ICssStyles {
    return {
      height: document.documentElement.scrollHeight + 'px'
    }
  }

  hide(e: Event) {
    const elem$ = e.target as HTMLDivElement;
    const isHide: boolean = containCssClass(elem$, ['main', 'close-icon']);
  
    if(isHide) this.videoWorker.destroy();
  }

  expand(): void {
    this.videoWorker.expand();
  }

  play(video: IVideo): void {
    this.videoWorker.changeVideo(video);
  }

  resize(params: DOMRectReadOnly) {
    const { top } = params;

    this.maxHeight = `${top}px`;
  }

  trackByFn(index: number, video: IVideo): string {
    return video._id
  }
}
