import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { IVideo } from 'src/app/interfaces/IVideo';
import { VideoWorkService } from 'src/app/services/video.work.service';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { VideoWorkFlags } from 'src/app/models/flags/VideoWorkFlags';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { AnyOption, SipmleAction } from 'src/app/types/shared.types';
import { FormObjectErrors, Length } from 'src/app/types/validation.types';

@Component({
  selector: 'cst-video-slider-info',
  templateUrl: './video-slider-info.component.html',
  styleUrls: ['./video-slider-info.component.scss']
})
export class VideoSliderInfoComponent implements OnInit {

  _video: IVideo;

  mutate$: Observable<SipmleAction<IVideo>> = this.videoWorker.getStream('mutate$');
  subs$: Subscription[] = [];
  
  flags: VideoWorkFlags = this.videoWorker.getFlagObject();
  lengths: Length = this.videoWorker.getValidatorLength('VIDEO');
  errors: FormObjectErrors = this.videoWorker.completeDbErrors(['video', 'formErrors']);
  

  @Input('video') set video(value: IVideo) {
    if(value) this._video = value
  } 

  @Output('navigate') _navigate = new EventEmitter<void>();

  constructor(
    private router: Router,
    private videoWorker: VideoWorkService
  ) { }

  ngOnInit() {
    this.initMutateSub();
  }

  initMutateSub() {
    const sub$: Subscription = this.mutate$.subscribe((event: SipmleAction<IVideo>) => {
      const { action, payload } = event;

      switch(action) {
        case 'edit': {
          Object.assign(this._video, payload);
          break;
        }
      }
    });

    this.subs$.push(sub$);
  }

  getTitleValidators(): ValidatorFn[]{
    return [Validators.required];
  }

  getTitleCssClasses(): ICssStyles {
    return {'v-title': true};
  }

  getDescCssClasses(): ICssStyles {
    return {'v-description': true};
  }

  getDescriptionText(): string {
    const { Description } = this._video;
    const isEditable: boolean = this.editable();
    const isExists: boolean = !!Description;

    if(!isEditable) {
      return isExists ? Description : 'Описание';
    } else {
      return isExists ? Description : 'Редактировать описание';
    }
  }

  showAddInOwn(): boolean {
    return this.videoWorker.getFlag('isShowAddInOwn') as boolean;
  }

  mutateOwnList(): void {
    const method: 'add' | 'remove' = this._video.IsHaveOwn ? 'remove' : 'add';
    this.videoWorker.mutateOwnList(method, this._video);
  }


  showMultiAddition(flag: boolean): void {
    this.videoWorker.setFlag('isShowMultiMutate', flag);
  }

  editable(): boolean {
    return this.videoWorker.editable();
  }

  navigate(): void {
    const { User } = this.videoWorker.getStreamValue('currentVideo$');

    this.videoWorker.reset();
    this.router.navigate(['/account', User]);
  }

  edit(value: string, controlName: 'Title' | 'Description'): void {
    const payload: AnyOption = { [controlName]: value };
    this.videoWorker.edit(payload);
  }

}
