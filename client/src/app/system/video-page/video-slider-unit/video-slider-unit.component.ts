import { Component,  Input, Output, EventEmitter } from '@angular/core';
import { IVideo } from 'src/app/interfaces/IVideo';

@Component({
  selector: 'cst-video-slider-unit',
  templateUrl: './video-slider-unit.component.html',
  styleUrls: ['./video-slider-unit.component.scss']
})
export class VideoSliderUnitComponent {

  @Input('video') video: IVideo;
  @Output('play') _play = new EventEmitter<IVideo>();

  play(): void {
    this._play.emit(this.video);
  }

}
