import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { checkedFile } from 'src/app/shared/handlers/handlers';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { VideoPlaylistViewService } from 'src/app/services/video-playlist.view.service';
import { VideoPlaylist } from 'src/app/models/VideoPlaylist';
import { scaleShow } from 'src/app/shared/animations/animation';
import { VideoPlaylistsFlags } from 'src/app/models/flags/VideoPlaylistsFlags';
import { FormObjectErrors, Length } from 'src/app/types/validation.types';

@Component({
  selector: 'cst-video-create-playlist-modal',
  templateUrl: './video-create-playlist-modal.component.html',
  styleUrls: ['./video-create-playlist-modal.component.scss'],
  animations: [scaleShow]
})
export class VideoCreatePlaylistModalComponent implements OnInit {

  form: FormGroup;
  file: File;
  lengths: Length = this.viewService.getValidatorLength('VIDEO_PLAYLIST');
  errors: FormObjectErrors = this.viewService.completeDbErrors(['video', 'formErrors']);
  flags: VideoPlaylistsFlags = this.viewService.getFlagObject();
  preview: string;

  @Output('hide') _hide = new EventEmitter<null>();

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private viewService: VideoPlaylistViewService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      Title: [null, [Validators.required]],
      Description: [null]
    })
  }

  changeLogo(e: Event): void {
    this.reset();
    const file: File = (e.target as HTMLInputElement).files[0];

    const { isValid, errors } = checkedFile('PICTURE', file);

    if(!isValid) {
      this.toastr.warning(errors.toString(), 'Внимание!');

      return
    }

    this.file = file;

    this.preview = URL.createObjectURL(file);
  }

  reset(): void {
    this.file = null;
    this.preview = null;
  }

  revokeUrl() {
    URL.revokeObjectURL(this.preview);
  }

  hide() {
    this._hide.emit()
  }

  onSubmit() {
    const { value: { Title, Description } } = this.form;
    const User = this.viewService.getOwnId();
    const playlist = new VideoPlaylist(Title, User, Description);
    
    this.viewService.create(playlist, this.file);
  
  }

}
