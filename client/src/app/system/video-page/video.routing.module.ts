import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VideoPageComponent } from './video-page/video-page.component';
import { VideoUnitModalComponent } from './video-unit-modal/video-unit-modal.component';
import { VideoPlaylistDetailComponent } from './video-playlist-detail/video-playlist-detail.component';

const routes: Routes = [
    { path: '', component: VideoPageComponent, children: [
        {path: 'video-unit/:id', component: VideoUnitModalComponent},
        {path: 'video-playlist/:id', component: VideoPlaylistDetailComponent}
    ] }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class VideoRoutingModule{}