import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { IVideoPlaylist } from 'src/app/interfaces/IVideoPlaylist';
import { MediaNavigate } from 'src/app/types/media.types';

@Component({
  selector: 'cst-video-playlists-unit',
  templateUrl: './video-playlists-unit.component.html',
  styleUrls: ['./video-playlists-unit.component.scss']
})
export class VideoPlaylistsUnitComponent implements OnInit {

  @Input('playlist') playlist: IVideoPlaylist;
  @Output('navigate') _navigate = new EventEmitter<MediaNavigate<IVideoPlaylist>>();

  constructor() { }

  ngOnInit() {
  }

  navigate(navigateTo: 'user' | 'playlist'): void {
    this._navigate.emit({ navigateTo, payload: this.playlist })
  }
}
