import { Component, OnInit, OnDestroy } from '@angular/core';
import { IMultiVideoList } from 'src/app/interfaces/IMultiVideoList';
import { VideoWorkFlags } from 'src/app/models/flags/VideoWorkFlags';
import { VideoWorkService } from 'src/app/services/video.work.service';
import { Observable } from 'rxjs';
import { MediaList } from 'src/app/models/MediaList';
import { Page } from 'src/app/models/Page';
import { isExpand } from 'src/app/shared/handlers/handlers';

@Component({
  selector: 'cst-video-slider-multi-addition',
  templateUrl: './video-slider-multi-addition.component.html',
  styleUrls: ['./video-slider-multi-addition.component.scss']
})
export class VideoSliderMultiAdditionComponent implements OnInit, OnDestroy {
  
  multiPlaylists$: Observable<MediaList<IMultiVideoList>> = this.videoWorker.getStream('multiPlaylists$');

  page: Page = new Page(0, 30);
  flags: VideoWorkFlags = this.videoWorker.getFlagObject();

  constructor(
    private videoWorker: VideoWorkService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.videoWorker.destroySubjects(['multiPlaylists$']);
    this.videoWorker.setFlag('multiRequestArray', [] as any);
  }

  init(): void {
    this.videoWorker.getMultiLists(this.page, 'replace');
  }

  trackByFn(index: number, playlist: IMultiVideoList): string {
    return playlist._id;
  }

  isRequest(playlistId: string): boolean {
    return this.flags.multiRequestArray.includes(playlistId);
  }

  hide(): void {
    this.videoWorker.setFlag('isShowMultiMutate', false);
  }

  mutate(playlist: IMultiVideoList): void {
    const action: 'add' | 'remove' = playlist.IsHave ? 'remove' : 'add';

    this.videoWorker.multiMutate(action, playlist)
  }

  expand(): void {
    const mediaList: MediaList<IMultiVideoList>  = this.videoWorker.getStreamValue('multiPlaylists$');

    const expand: boolean = isExpand(mediaList);

    if(expand) {
      this.page.skip++;
      this.videoWorker.getMultiLists(this.page, 'expand');
    }
  }

}
