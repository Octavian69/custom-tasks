import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IVideo } from 'src/app/interfaces/IVideo';

@Component({
  selector: 'cst-video-unit',
  templateUrl: './video-unit.component.html',
  styleUrls: ['./video-unit.component.scss']
})
export class VideoUnitComponent implements OnInit {

  @Input('video') video: IVideo;
  @Input('request') request: boolean;
  @Input('hideAddIcon') hideAddIcon: boolean = false;
  @Input('isOwn') isOwn: boolean = false;
  @Output('navigate') _navigate = new EventEmitter<string>();
  @Output('play') _play = new EventEmitter<IVideo>();
  @Output('mutate') _mutate = new EventEmitter<IVideo>()

  constructor() { }

  ngOnInit() {
  }

  getHaveField(): boolean {
    const field = this.isOwn ? 'IsHaveOwn' : 'IsHave';

    return this.video[field]
  }

  navigate(): void {
    this._navigate.emit(this.video.User);
  }

  play(): void {
    this._play.emit(this.video);
  }

  mutate(): void {
    this._mutate.emit(this.video);
  }

}
