import { Component, OnInit, OnDestroy } from '@angular/core';
import { VideoViewService } from 'src/app/services/video.view.service';
import { StorageService } from 'src/app/services/storage.service';
import { VideoMainFlags } from 'src/app/models/flags/VideoMainFlags';
import { VideoPlaylistViewService } from 'src/app/services/video-playlist.view.service';
import { VideoPlaylistsFlags } from 'src/app/models/flags/VideoPlaylistsFlags';
import { ActivatedRoute } from '@angular/router';
import { VideoWorkService } from 'src/app/services/video.work.service';
import { PageLink } from 'src/app/types/request-response.types';

@Component({
  selector: 'cst-video-page',
  templateUrl: './video-page.component.html',
  styleUrls: ['./video-page.component.scss']
})
export class VideoPageComponent implements OnInit, OnDestroy {

  userId: string;

  flags: VideoMainFlags = this.viewService.getFlagObject();
  playlistFlags: VideoPlaylistsFlags = this.playlistViewService.getFlagObject();
  showClass: 'right-show'| 'left-show' = 'left-show';
  link: PageLink;
  navList: PageLink[] = this.viewService.getDataByArrayKeys(['video', 'navList']);

  constructor(
    private activeRouter: ActivatedRoute,
    private playlistViewService: VideoPlaylistViewService,
    private viewService: VideoViewService,
    private videoWorker: VideoWorkService,
    private storage: StorageService
  ) { }

  ngOnInit(): void {
    this.userId = this.activeRouter.snapshot.params.id;
    // this.initPage();
  }

  ngOnDestroy() {
    this.storage.removeItem('sessionStorage', 'videoPage');
    this.viewService.destroy();
    this.playlistViewService.destroy();
  }

  isOwn(): boolean {
    return this.viewService.isOwn(this.userId);
  }

  setPage(page: PageLink) {
      this.showClass = this.link && page.id > this.link.id ? 'right-show': 'left-show';
      this.link = page;
  }

  showVideoModal(flag: boolean): void {
    this.viewService.setFlag('isCreateVideoModal', flag)
  }

  showPlaylistModal(flag: boolean): void {
    this.playlistViewService.setFlag('isCreatePlaylistModal', flag);
  }

  create(type: 'playlist' | 'video'): void {
    const method = type === 'video' ? 'showVideoModal' : 'showPlaylistModal';
    
    this[method](true);
  }
}
