import { Component, OnInit, OnDestroy, HostBinding, ViewChild } from '@angular/core';
import { fade } from 'src/app/shared/animations/animation';
import { AnimationTriggerMetadata } from '@angular/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { containCssClass, unsubscriber, checkedFile } from 'src/app/shared/handlers/handlers';
import { Observable, Subscription } from 'rxjs';
import { IVideoPlaylist } from 'src/app/interfaces/IVideoPlaylist';
import { VideoPlaylistViewService } from 'src/app/services/video-playlist.view.service';
import { IVideo } from 'src/app/interfaces/IVideo';
import { VideoWorkService } from 'src/app/services/video.work.service';
import { MediaRequest } from 'src/app/models/MediaRequest';
import { FormControl, Validators } from '@angular/forms';
import { VideoPlaylistsFlags } from 'src/app/models/flags/VideoPlaylistsFlags';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { ToastrService } from 'ngx-toastr';
import { VideoMainListComponent } from '../video-main-list/video-main-list.component';
import { Length, FormObjectErrors } from 'src/app/types/validation.types';
import { AnyOption, SipmleAction } from 'src/app/types/shared.types';

@Component({
  selector: 'cst-video-playlist-detail',
  templateUrl: './video-playlist-detail.component.html',
  styleUrls: ['./video-playlist-detail.component.scss'],
  animations: [fade]
})
export class VideoPlaylistDetailComponent implements OnInit, OnDestroy {
  
  playlist$: Observable<IVideoPlaylist> = this.viewService.getStream('playlist$');
  
  playlistId: string;

  Title: FormControl = new FormControl(null, Validators.required);
  Description: FormControl = new FormControl(null);
  
  isEditTitle: boolean = false;
  isEditDescription: boolean = false;
  
  flags: VideoPlaylistsFlags = this.viewService.getFlagObject();
  lengths: Length = this.viewService.getValidatorLength('VIDEO_PLAYLIST');
  errors: FormObjectErrors = this.viewService.completeDbErrors(['video', 'formErrors']);
  subs$: Subscription[] = [];


  @HostBinding('@fade') fade: AnimationTriggerMetadata;
  @ViewChild(VideoMainListComponent, { static: false }) listRef: VideoMainListComponent;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private activeRoute: ActivatedRoute,
    private viewService: VideoPlaylistViewService,
    private videoWorker: VideoWorkService,
  ) { }

  ngOnInit() {
    const { id } = this.activeRoute.snapshot.params;
    this.playlistId = id;

    this.init();
  }

  ngOnDestroy() {
    this.viewService.destroySubjects(['playlist$']);
    this.videoWorker.setFlag('isShowAddInOwn', false);
    this.videoWorker.setFlag('isCanMutateDetail', false);
    unsubscriber(this.subs$);
  }

  init(): void {
    this.initPlaylist();
    this.initMutateSub();
  }

  initMutateSub(): void {
    const sub$: Subscription = this.videoWorker.getStream('mutate$')
      .subscribe((event: SimpleAction<IVideo>) => {
        this.mutateDetail(event);
      });
    
      this.subs$.push(sub$);
  }

  mutateDetail(event: SimpleAction<IVideo>): void {
    const { action } = event;
    const playlist: IVideoPlaylist = this.viewService.getStreamValue('playlist$');
    
    let update: IVideoPlaylist;

    switch(action) {
      case 'add': {
        const VideoTotal: number = playlist.VideoTotal + 1;
        update = Object.assign({}, playlist, { VideoTotal });
        break;
      }

      case 'remove': {
        const VideoTotal: number = playlist.VideoTotal - 1;
        update = Object.assign({}, playlist, { VideoTotal });
        break;
      }
    }

    if(update) {
      const event: SipmleAction<IVideoPlaylist> = new SimpleAction(action, update);

      this.viewService.emitStream('playlist$', update);
      this.viewService.mutateDetail(event, 'main');
      this.viewService.mutateDetail(event, 'search');
    }
  }

  initPlaylist(): void {
    this.videoWorker.setFlag('isShowAddInOwn', true);
    this.viewService.fetchPlaylist(this.playlistId);
  }

  isOwnPlaylist(): boolean {
    const { User } = this.viewService.getStreamValue('playlist$');

    return this.viewService.isOwn(User);
  }

  getDescription(text: string): string {
    const isOwnPlaylist: boolean = this.isOwnPlaylist();

    if(isOwnPlaylist) {
      return text || 'Редактировать описание';
    } else {
      return text || 'Описание отсутствует';
    }
    
  }

  getDisableExpand(): boolean {
    if(!this.listRef) return true;

    return this.listRef.getDisableExpand()
  }

  getAddText(IsHave: boolean): string {
    return IsHave ? 'Убрать из моего списка' : 'Добавить в мой список';
  }

  getAddIcon(IsHave: boolean): string {
    return IsHave ? 'check' : 'add';
  }

  showControl(flagName: 'isEditTitle' | 'isEditDescription'): void {
    const isEditable: boolean = this.editable();

    if(isEditable) {
      const fieldName: string = flagName === 'isEditTitle' ? 'Title' : 'Description';
      const value = this.viewService.getStreamValue('playlist$')[fieldName];

      (this[fieldName] as FormControl).patchValue(value)

      this[flagName] = !this[flagName];
    }
  }

  updateLogo(e: Event): void {
    const file: File = (e.target as HTMLInputElement).files[0];

    const { isValid, errors } = checkedFile('PICTURE', file);

    if(!isValid) {
      this.toastr.warning(errors.toString(), 'Внимание!');
      return
    }

    this.viewService.updateLogo(file);
  }  

  editable(): boolean {
    const { User } = this.viewService.getStreamValue('playlist$');

    const isEdit: boolean = this.viewService.isOwn(User);

    return isEdit;
  }

  createMediaRequest(type: 'main' | 'search'): void {
    const reqName = `${type}MediaReq`;

    this[reqName] = new MediaRequest(type, this.playlistId, 15);
  }

  back(): void {
    const url: string = this.router.url.split('/video-playlist')[0];
    this.router.navigateByUrl(url);
  }

  hide(e: Event): void {
    const elem$ = e.target as HTMLElement;
    const isHide: boolean = containCssClass(elem$, ['main']);

    if(isHide) this.back();
  }

  navigate(userId: string): void {
    this.router.navigate(['/account', userId]);
  }

  play(video: IVideo): void {

  }

  mutate(IsHave: boolean) {
    
    if(this.flags.isMutate) return

    const method: 'add' | 'remove' = IsHave ? 'remove' : 'add';

    this.viewService[method]();
  }


  edit(controlName: 'Title' | 'Description'): void {
    const playlist: IVideoPlaylist = this.viewService.getStreamValue('playlist$');
    const { value, invalid } = this[controlName];
    const flagName = `isEdit${controlName}` as 'isEditTitle' | 'isEditDescription';
    const currentValue: string = playlist[controlName];
    const isEqual: boolean = Object.is(value, currentValue)

    if(invalid || isEqual) {
      this[controlName].patchValue(playlist[controlName]);
    } else {
      const payload: AnyOption = { [controlName]: value };
      this.viewService.edit(playlist, payload);
    }

    this.showControl(flagName);
  }

  ending() {
    this.listRef.ending()
  }

}
