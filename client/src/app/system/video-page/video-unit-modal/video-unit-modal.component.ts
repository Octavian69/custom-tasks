import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { checkedFile, removeExtName } from 'src/app/shared/handlers/handlers';
import { ToastrService } from 'ngx-toastr';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Video } from 'src/app/models/Video';
import { VideoViewService } from 'src/app/services/video.view.service';
import { scaleShow } from 'src/app/shared/animations/animation';
import { VideoMainFlags } from 'src/app/models/flags/VideoMainFlags';
import { CreateVideoCandidate } from 'src/app/types/video.types';
import { Length, FormObjectErrors } from 'src/app/types/validation.types';

@Component({
  selector: 'cst-video-unit-modal',
  templateUrl: './video-unit-modal.component.html',
  styleUrls: ['./video-unit-modal.component.scss'],
  animations: [scaleShow]
})
export class VideoUnitModalComponent implements OnInit {

  form: FormGroup;
  file: File;
  lengths: Length = this.viewService.getValidatorLength('VIDEO');
  flags: VideoMainFlags = this.viewService.getFlagObject();
  errors: FormObjectErrors = this.viewService.completeDbErrors(['video', 'formErrors']);
  preview: string;

  @Output('hide') _hide = new EventEmitter<null>();

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private viewService: VideoViewService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      Title: [null, [Validators.required]],
      Description: [null]
    })
  }

  changeVideo(e: Event): void {
    
    this.reset();
    const file: File = (e.target as HTMLInputElement).files[0];

    const { isValid, errors } = checkedFile('VIDEO', file);

    if(!isValid) {
      this.toastr.warning(errors.toString(), 'Внимание!');

      return
    }

    const filename: string = removeExtName(file.name);
    this.form.get('Title').patchValue(filename);
    this.file = file;
    setTimeout(_ => this.preview = URL.createObjectURL(file), 0);
  }

  reset(): void {
    this.file = null;
    this.preview = null;
    this.form.reset();
  }

  revokeUrl() {
    URL.revokeObjectURL(this.preview);
  }

  hide() {
    this._hide.emit()
  }

  onSubmit() {
    this.revokeUrl();

    const { value: { Title, Description } } = this.form;
    const Playlist = this.viewService.getOwnId();
    const video = new Video(Title, Description, Playlist, Playlist);
    
    const candidate: CreateVideoCandidate = { video, file: this.file };
    this.viewService.create(candidate);
  
  }

}
