import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PageLink } from 'src/app/types/request-response.types';

@Component({
  selector: 'cst-video-nav',
  templateUrl: './video-nav.component.html',
  styleUrls: ['./video-nav.component.scss']
})
export class VideoNavComponent {

  
  @Input('userId') userId: string;
  @Input('navList') options: PageLink[];
  @Input('isOwn') isOwn: boolean = false;
  @Output('setPage') page = new EventEmitter<PageLink>();
  @Output('create') _create = new EventEmitter<'playlist' | 'video'>();

  constructor() {}

  setPage(link: PageLink): void {
    this.page.emit(link);
  }

  create(type: 'playlist' | 'video'): void {
    this._create.emit(type);
  }

}
