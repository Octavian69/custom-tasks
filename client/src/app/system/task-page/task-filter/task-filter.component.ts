import { Component, OnInit, Output, EventEmitter, Input, HostBinding } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TaskFilter } from 'src/app/models/TaskFilter';
import { topShow } from 'src/app/shared/animations/animation';
import { TaskFilterOptions } from 'src/app/types/task.types';
import { Length } from 'html2canvas/dist/types/css/types/length';
import { AnyOption } from 'src/app/types/shared.types';

@Component({
  selector: 'cst-task-filter',
  templateUrl: './task-filter.component.html',
  styleUrls: ['./task-filter.component.scss'],
  animations: [topShow]
})
export class TaskFilterComponent implements OnInit {

  form: FormGroup;
  now: Date = new Date();
  
  @HostBinding('@topShow')
  @Input('lengths') lengths: Length;
  @Input('filters') filters: TaskFilter;
  @Input('statusList') statusList: AnyOption;
  @Output('filter') filter = new EventEmitter<TaskFilterOptions>();
  @Output('hide') _hide = new EventEmitter<void>();

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.form = this.fb.group({
      Title: [null, [], []],
      Completed: [null, [], []],
      StartDate: [null, [], []],
      EndDate: [null, [], []],
      Description: [null, [], []],
    })

    this.form.patchValue(this.filters);
  }

  hide(e: Event) {
    const { classList } = <HTMLDivElement>e.target;
    const isHide: boolean = Array.from(classList).includes('hide');

    if(isHide) this._hide.emit();
    
  }

  resetControl(e: Event, controlName: string): void {
    e.stopPropagation();

    const { value } = this.form.get(controlName);

    this.form.get(controlName).reset();
    if(value) this.form.markAsTouched();
  }

  reset(): void {
    this.form.reset();
    this.form.markAsTouched();
    this.onSubmit(false);
  }

  onSubmit(isActive: boolean = true): void {
    if(this.form.touched) {

      const filter: TaskFilter = this.form.value;      
      const options: TaskFilterOptions = { filter, isActive };

      this.filter.emit(options);
       
    } else {
      this.toastr.info('Значения формы не изменялись.', 'Информация');
    }
  }

}
