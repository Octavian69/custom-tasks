import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TaskViewService } from 'src/app/services/task.view.service';
import { fadeEvent } from 'src/app/shared/animations/animation';
import { Crumb } from 'src/app/types/task.types';

@Component({
  selector: 'td-task-page',
  templateUrl: './task-page.component.html',
  styleUrls: ['./task-page.component.scss'],
  animations: [ fadeEvent ]
})
export class TaskPageComponent implements OnDestroy {
  
  animationState: boolean = false;
  
  crumbs$: Observable<Crumb[]> = this.viewService.getStream<Crumb[]>('crumbs$');

  constructor(
    private router: Router,
    private viewService: TaskViewService
  ) { }


  navigate() {
    this.router.navigate(['/tasks']);
  }

  ngOnDestroy() {
    this.viewService.destroy();
  }
}
