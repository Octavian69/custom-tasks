import { Component, OnInit, OnDestroy } from '@angular/core';
import { TaskViewService } from 'src/app/services/task.view.service';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { ITask } from 'src/app/interfaces/ITask';
import { PageEvent } from '@angular/material/paginator';
import { fade } from 'src/app/shared/animations/animation';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { TaskFlags } from 'src/app/models/flags/TaskFlags';
import { TaskFilterOptions, TaskIndicator } from 'src/app/types/task.types';

@Component({
  selector: 'cst-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
  animations: [fade]
})
export class TaskListComponent implements OnInit, OnDestroy {

  flags: TaskFlags = this.viewService.getFlagObject();
  tasks$: Observable<ITask[]> = this.viewService.getStream<ITask[]>('tasks$');

  columns: string[];
  indicators: TaskIndicator[];

  constructor(
    private viewService: TaskViewService
  ) { }
  
  ngOnInit() {
    this.initLists();
    this.viewService.fetch();
  }

  ngOnDestroy() {
    this.viewService.destroySubjects(['tasks$'])
  }

  initLists(): void {
    this.columns = this.viewService.getDataByArrayKeys(['tasks', 'columns']);
    this.indicators = this.viewService.getDataByArrayKeys(['tasks', 'indicators']);
  }

  getStatus(status: boolean): string {
    return status ? 'Завершена' : 'Не завершена';
  }

  getRowStyles(row: ITask): ICssStyles {

    const { Completed, EndDate, _id } = row;

    const compareDate: string = moment(EndDate).format('YYYY-MM-DD');
    const now: string = moment().format('YYYY-MM-DD');

    const isBefore: boolean = moment(compareDate).isBefore(now);
    const isSame: boolean = moment(compareDate).isSame(now);
    const isAfter: boolean = moment(compareDate).isAfter(now);

    return {
      'before': isBefore && !isSame,
      'same': isSame,
      'after': isAfter,
      'complete': Completed,
      'select': this.viewService.selected.includes(_id)
    }
  }

  getConfirmText(): string {
    return `удалить задачи: ${this.viewService.selected.length} шт.`
  }

  trackByIndicators(index: number): number {
    return index;
  }

  select(id: string): void {
    this.viewService.select(id);
  }

  selectAll(): void {
    this.viewService.selectAll();
  }

  setPage(page: PageEvent) {
    const { pageIndex, pageSize } = page;

    this.viewService.setPage(pageIndex, pageSize)
  }

  showConfirm(flag: boolean) {
    this.viewService.setFlag('isShowConfirm', flag);
  }

  remove(confirm: boolean) {
    if(confirm) {
      this.viewService.remove();
    }

    this.showConfirm(false)
  }

  showHideFitler(flag: boolean) {
    this.viewService.setFlag('isShowFilter', flag);
  }

  filter(options: TaskFilterOptions): void {
    const { filter, isActive } = options;

    this.showHideFitler(false);
    this.viewService.setFlag('isActiveFilter', isActive);

    this.viewService.filter(filter);
  }
  

}
