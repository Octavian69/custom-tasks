import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { TaskViewService } from 'src/app/services/task.view.service';
import { ITask } from 'src/app/interfaces/ITask';
import { ActivatedRoute } from '@angular/router';
import { unsubscriber, resetHoursDate } from 'src/app/shared/handlers/handlers';
import { TaskFlags } from 'src/app/models/flags/TaskFlags';
import { AnyOption } from 'src/app/types/shared.types';
import { Length } from 'src/app/types/validation.types';

@Component({
  selector: 'cst-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.scss']
})
export class TaskEditComponent implements OnInit {

  
  task$: Observable<ITask>;
  flags: TaskFlags = this.viewService.getFlagObject();
  printStyles: string = this.viewService.getDataByArrayKeys(["tasks", "PrintStyles"]);
  printContainerStyles: string = this.viewService.getDataByArrayKeys(["tasks", "ContainerStyles"]);

  form: FormGroup;
  task: ITask;
  modeType: 'create' | 'update' = 'create';
  header: string;
  lengths: Length = this.viewService.getValidatorLength('TASKS');
  now: Date = new Date();
  subs$: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    public viewService: TaskViewService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.initForm();
    this.initDateSub();
    this.initTaskSub();
    this.initRouterSub();
  }

  ngOnDestroy() {
    unsubscriber(this.subs$);
    this.viewService.setCrumbs();
    this.viewService.destroySubjects(['task$']);
  }

  initRouterSub() {
    const sub$: Subscription = this.activeRoute.params
    .subscribe(({ id }) => {
      this.viewService.setCrumbs();
      this.initModeType(id)
    })

    this.subs$.push(sub$);
  }

  initTaskSub(): void {
    this.task$ = this.viewService.getStream<ITask>('task$')
      .pipe(tap((task: ITask) => {
        if(task) {
          this.task = task;
          this.form.patchValue(task);
          setTimeout(_ => this.checkedInitState(), 0)
        }
      }));
  }


  initDateSub(): void {
    const sub$: Subscription = this.form.get('StartDate').valueChanges.subscribe((StartDate: Date) => {
        if(StartDate) {
          const { value: EndDate } = this.form.get('EndDate');
          (StartDate && EndDate) && moment(StartDate).isAfter( moment(EndDate) )
            ? this.form.get('EndDate').patchValue(moment(StartDate).add(1, 'day'))
            : null;
        }
    });

    this.subs$.push(sub$);
  }

  initModeType(id: string): void {
    this.modeType = id === '0' ? 'create' : 'update';
    this.header = this.modeType === 'create' ? 'Создание' : 'Редактирование';

    this.viewService.fetchById(id);
  }

  initForm(): void {
    const { Description } = this.lengths.MAX;
    this.form = this.fb.group({
      Title: ['', [Validators.required], []],
      Completed: [null, [Validators.required], []],
      StartDate: [null, [Validators.required], []],
      EndDate: [null, [Validators.required], []],
      Description: ['', [Validators.maxLength(Description)], []]
    })
  }

  getMaxStartDate(): Date {
    const { value } = this.form.get('EndDate');

    if(this.modeType === 'create') {
      return value;
    } else {

      const end: moment.Moment = resetHoursDate(value);
      const now: moment.Moment = resetHoursDate(this.now);

      return value && end.isBefore(now) ? null : value;
    }
  }

  getMinEndDate(): Date {
    const { value } = this.form.get('StartDate');

    if(this.modeType === 'create') {
      return value || this.now;
    } else {
        
      return value && moment(value).isAfter(this.now) ? value : this.now;
    }
  }

  disabledStatusOption(): (opt: AnyOption) => boolean {
    
    function disabled(opt: AnyOption): boolean {
      return this.modeType === "create" && opt.value;
    }

    return disabled.bind(this);
  }

  checkedInitState(): void {
    this.checkedEndDate();
  }

  checkedEndDate(): void {
    const { value } = this.form.get('EndDate');  

    const end: moment.Moment = resetHoursDate(value);
    const now: moment.Moment = resetHoursDate(this.now);

    if(end.isBefore(now)) {
      this.form.get('EndDate').setErrors({ matDatepickerMin: true }, {emitEvent: true});
    }
  }

  onSubmit() {

    if(this.form.touched) {

      const { value } = this.form;
      const task = Object.assign({}, this.task, value);
      this.viewService[this.modeType](task);
      this.form.markAsUntouched();

    } else {

      this.toastr.info('Значения формы не изменялись.', 'Информация');
    }
  }

}
