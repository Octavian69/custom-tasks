import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TaskViewService } from 'src/app/services/task.view.service';
import { Observable } from 'rxjs';
import { ITask } from 'src/app/interfaces/ITask';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';

@Component({
  selector: 'cst-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss']
})
export class TaskDetailComponent implements OnInit, OnDestroy {
  
  task$: Observable<ITask> = this.viewService.getStream('task$');

  constructor(
    private activeRoute: ActivatedRoute,
    private viewService: TaskViewService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.viewService.emitStream('task$', null)
    this.viewService.setCrumbs();
  }

  getPrintStyles(key: 'ContainerStyles' | 'PrintStyles'): string {
    return this.viewService.getDataByArrayKeys(['tasks', key])
  }

  getStatusClass(): ICssStyles {
    const task: ITask = this.viewService.getStreamValue('task$');

    return {
      'completed': task && task.Completed,
      'not-completed': task && !task.Completed
    }
  }

  init(): void {
    const taskId: string = this.activeRoute.snapshot.paramMap.get('id');
    this.viewService.fetchById(taskId);
  }


}
