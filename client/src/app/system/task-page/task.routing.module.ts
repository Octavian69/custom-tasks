import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { TaskPageComponent } from './task-page/task-page.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskEditComponent } from './task-edit/task-edit.component';

const routes: Routes = [
    { path: '', component: TaskPageComponent, children: [
        { path: '', component: TaskListComponent },
        { path: 'detail/:id', component: TaskDetailComponent},
        { path: 'edit/:id', component: TaskEditComponent}
    ]}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TaskRoutingModule {}