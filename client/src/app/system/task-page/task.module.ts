import { NgModule } from "@angular/core";
import { TaskViewService } from 'src/app/services/task.view.service';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskPageComponent } from './task-page/task-page.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskFilterComponent } from './task-filter/task-filter.component';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { TaskRoutingModule } from './task.routing.module';
import { TaskEditComponent } from './task-edit/task-edit.component';

@NgModule({
    declarations: [
        TaskPageComponent,
        TaskListComponent,
        TaskDetailComponent,
        TaskFilterComponent,
        TaskEditComponent
    ],
    imports: [
        SharedModule,
        TaskRoutingModule
    ],
    providers: [TaskViewService]
})
export class TaskModule {}