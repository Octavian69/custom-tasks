import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { MessagePageComponent } from './message-page/message-page.component';
import { MessageRoutingModule } from './message.routing.module';
import { MessageUnitComponent } from './message-unit/message-unit.component';
import { MessageDialoguesComponent } from './message-dialogues/message-dialogues.component';
import { MessageDialoguesDetailComponent } from './message-dialogues-detail/message-dialogues-detail.component';
import { MessageDialogueCreateComponent } from './message-dialogue-create/message-dialogue-create.component';
import { MessageDialogueUnitComponent } from './message-dialogue-unit/message-dialogue-unit.component';
import { MessageNavComponent } from './message-nav/message-nav.component';
import { MessageChatsComponent } from './message-chats/message-chats.component';
import { MessageDialogueCreateUserComponent } from './message-dialogue-create-user/message-dialogue-create-user.component';
import { MessageSearchDialogueUnitComponent } from './message-search-dialogue-unit/message-search-dialogue-unit.component';
import { MessageChatsReadComponent } from './message-chats-read/message-chats-read.component';


@NgModule({
    declarations: [
        MessagePageComponent,
        MessageUnitComponent,
        MessageDialoguesComponent,
        MessageDialoguesDetailComponent,
        MessageDialogueCreateComponent,
        MessageDialogueUnitComponent,
        MessageNavComponent,
        MessageChatsComponent,
        MessageDialogueCreateUserComponent,
        MessageSearchDialogueUnitComponent,
        MessageChatsReadComponent
    ],
    imports: [
        SharedModule,
        MessageRoutingModule
    ],
    providers: []
})
export class MessageModule {}