import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IMessage } from 'src/app/interfaces/IMessage';
import { fade } from 'src/app/shared/animations/animation';


@Component({
  selector: 'cst-message-unit',
  templateUrl: './message-unit.component.html',
  styleUrls: ['./message-unit.component.scss'],
  animations: [fade]
})
export class MessageUnitComponent {

  @Input('message') message : IMessage;
  @Input('isShowEditMessage') isShowEditMessage: boolean;
  @Input('isDeletionCandidate') isDeletionCandidate: boolean;

  @Output('navigate') _navigate = new EventEmitter<IMessage>();
  @Output('edit') _edit = new EventEmitter<IMessage>();
  @Output('setDeletionState') _setDeletionState = new EventEmitter<IMessage>();
  
  navigate(): void {
    this._navigate.emit(this.message);
  }

  edit(): void {
    this._edit.emit(this.message);
  }

  setDeletionState() {
    this._setDeletionState.emit(this.message);
  }
}
