import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MessageViewService } from 'src/app/services/message.view.service';
import { Router } from '@angular/router';
import { PageLink } from 'src/app/types/request-response.types';

@Component({
  selector: 'cst-message-nav',
  templateUrl: './message-nav.component.html',
  styleUrls: ['./message-nav.component.scss']
})
export class MessageNavComponent implements OnInit {

  options: PageLink[] = this.messageViewService.getDataByArrayKeys(['messages', 'navList']);

  @Output('create') _create = new EventEmitter<void>();

  constructor(
    private router: Router,
    private messageViewService: MessageViewService
  ) { }

  ngOnInit() {
  }

  navigate(link: PageLink): void {
    const url: string = `/messages?page=${link.page}`;

    this.router.navigateByUrl(url);
  }

  create() {
    this._create.emit();
  }
}
