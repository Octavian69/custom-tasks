import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { IDialogue } from 'src/app/interfaces/IDialogue';
import { DialoguesViewService } from 'src/app/services/dialogues.view.service';
import { Observable, Subscription } from 'rxjs';
import { MediaList } from 'src/app/models/MediaList';
import { DialoguesFlags } from 'src/app/models/flags/DialoguesFlags';
import { isExpand, unsubscriber } from 'src/app/shared/handlers/handlers';
import { PaginationRequest } from 'src/app/models/PaginationRequest';
import { Page } from 'src/app/models/Page';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { IIncompleteUser } from 'src/app/interfaces/IIncompleteUser';
import { SocketService } from 'src/app/services/socket.service';
import { IMessage } from 'src/app/interfaces/IMessage';
import { SOKET_EVENTS } from 'src/app/namespaces/socket-events.namespaces';
import { DialogueType, NewDiscussion } from 'src/app/types/messages.types';

@Component({
  selector: 'cst-message-dialogues',
  templateUrl: './message-dialogues.component.html',
  styleUrls: ['./message-dialogues.component.scss']
})
export class MessageDialoguesComponent implements OnInit, OnDestroy{

  dialogues$: Observable<MediaList<IDialogue>> = this.viewService.getStream('dialogues$');
  search$: Observable<MediaList<IDialogue | IIncompleteUser>> = this.viewService.getStream('search$');
  modeType: DialogueType;
  mediaRequest: PaginationRequest;
  searchRequest: PaginationRequest;
  
  searchControl: FormControl = new FormControl('', [], []);
  flags: DialoguesFlags = this.viewService.getFlagObject();
  subs$: Subscription[] = [];

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private viewService: DialoguesViewService,
    private socketService: SocketService
  ) { }

  ngOnInit() {
    this.init();
  }
  
  ngOnDestroy() {
    unsubscriber(this.subs$);
  }

  init(): void {
    this.initMediaRequest();
    this.initRouterSub(); 
    this.initSearchSub();
    this.initCreateDiscussionSub();
    this.initNewMessageSub();
  }

  initRouterSub(): void {
    const sub$: Subscription = this.activeRoute.queryParams.subscribe((params: Params) => {
      const { page } = params;
      this.modeType = page;
      this.reset();
      this.initMediaRequest();
      this.viewService.fetch(this.mediaRequest, 'replace');
    });

    this.subs$.push(sub$);
  }

  initSearchSub(): void {
    const sub$: Subscription = this.searchControl.valueChanges
    .pipe(debounceTime(400))
    .subscribe((value: string) => {
      
      if(value) {
        
        if(!this.searchRequest) this.initSearchRequest();

        const searchKey: 'Name' | 'Title' = this.modeType === 'dialogue' ? 'Name' : "Title";
        
        this.searchRequest.filters[searchKey] = value;

        this.viewService.fetchSearch(this.searchRequest, 'replace');


      } else {
        this.searchRequest = null;
        this.viewService.emitStream('search$', null);
      }
    })

    this.subs$.push(sub$);
  }

  initSearchRequest(): void {
    const page: Page = new Page(0, LIMIT.DIALOGUES);
    const ownId: string = this.viewService.getOwnId();
    const state: object = { type: this.modeType };

    this.searchRequest = new PaginationRequest(ownId, page, state);
    this.mediaRequest.filters.Type = this.modeType;
    
  }

  initMediaRequest(): void {
    const ownId: string = this.viewService.getOwnId();
    const page: Page = new Page(0, LIMIT.DIALOGUES);
    this.mediaRequest = new PaginationRequest(ownId, page);

    this.mediaRequest.filters.Type = this.modeType;
  }

  initNewMessageSub(): void {
    const sub$: Subscription = this.socketService
    .getListener(SOKET_EVENTS.MESSAGE_NEW)
    .subscribe((message: IMessage) => {
      const isEqualTypes: boolean = message.DialogueType === this.modeType;
      this.viewService.newMessage(message, isEqualTypes);
    })

    this.subs$.push(sub$);
  }

  initCreateDiscussionSub(): void {
    const sub$: Subscription = this.socketService
    .getListener(SOKET_EVENTS.MESSAGE_CREATE_DISCUSSION)
    .subscribe((payload: NewDiscussion) => {
      const { message, dialogue } = payload;

      if(this.modeType === 'discussion') {
        this.viewService.addDialogue(dialogue);
      }
    })

    this.subs$.push(sub$);
  }

  getSearchPlaceholder(): string {
    const type: string = this.modeType === 'dialogue' ? 'имя своего друга' : 'название беседы';

    return `Введите ${type}`;
  }

  getEmptyText(): string {
    const listName: string = this.modeType === 'dialogue' ? 'диалогов' : 'дискуссий';

    return `Список ${listName} пуст`;
  }

  getSearchEmptyText(): string {
    const listName: string = this.modeType === 'dialogue' ? 'друзей' : 'дискуссий';

    return `Список ${listName} пуст`;
  }

  navigateToDialogue(event: IDialogue | IIncompleteUser): void {
    const isNavigate: boolean = this.viewService.getFlag('isNavigateToDetail') as boolean;

    if(isNavigate) return;

    this.viewService.setFlag('isNavigateToDetail', true);

    if(this.modeType === 'dialogue') {
        
      this.viewService.navigateToDialogue(event._id);
  
    } else {
      this.viewService.changeDialogue(event as IDialogue);

      this.router.navigate(['/messages/dialogue', event._id]).then(_ => {
        this.viewService.setFlag('isNavigateToDetail', false);
      })
    }
  }

  navigate(dialogue: IDialogue): void {
    this.viewService.changeDialogue(dialogue);
    this.router.navigate(['messages/dialogue', dialogue._id]);
  }

  trackByFn(index: number, dialogue: IDialogue): string {
    return dialogue._id;
  }

  reset(): void {
    this.viewService.destroySubjects(['dialogues$']);
    this.searchRequest = null;
    this.searchControl.patchValue(null, { emitEvent: false });
    this.viewService.destroySubjects(['search$']);
  }

  expand(): void {
    const list: 'dialogues$' | 'search$' = this.searchRequest ? 'search$' : 'dialogues$';
    const method: 'fetch' | 'fetchSearch' = this.searchRequest ? 'fetchSearch' : 'fetch';
    const request: 'mediaRequest' | 'searchRequest' = this.searchRequest ? 'searchRequest' : 'mediaRequest';
    
    this.expandList(list, method, request);
  }

  expandList(list: 'search$' | 'dialogues$', method: 'fetch' | 'fetchSearch', request: 'mediaRequest' | 'searchRequest'): void {
    const medialList: MediaList<IDialogue> = this.viewService.getStreamValue(list);
    const expand: boolean = isExpand(medialList);

    if(expand) {
      this[request].page.skip++;
      this.viewService[method](this[request], 'expand');
    }
  }
}
