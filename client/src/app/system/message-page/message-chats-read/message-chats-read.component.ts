import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cst-message-chats-read',
  templateUrl: './message-chats-read.component.html',
  styleUrls: ['./message-chats-read.component.scss']
})
export class MessageChatsReadComponent implements OnInit {

  @Output('read') _read: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  read(): void {
    this._read.emit();
  }

}
