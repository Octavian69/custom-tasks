import { Component, OnInit } from '@angular/core';
import { Popup } from 'src/app/models/Popup';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { PopupsViewService } from 'src/app/services/popups.view.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'cst-message-popups',
  templateUrl: './message-popups.component.html',
  styleUrls: ['./message-popups.component.scss']
})
export class MessagePopupsComponent implements OnInit {
  
  popups$: Observable<Popup[]> = this.viewService.getStream('popups$');

  constructor(
    private viewService: PopupsViewService
  ) { }

  ngOnInit() {
    
  }

  trackByFn(index: number, popup: Popup): string {
    return popup._id;
  }

  close(popupId: string) {
    this.viewService.close(popupId);
  }

  action(event: SimpleAction<Popup>) {
    this.viewService.action(event)
  }
  

}
