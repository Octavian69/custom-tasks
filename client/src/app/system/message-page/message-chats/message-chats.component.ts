import { Component, OnInit, OnDestroy } from '@angular/core';
import { DialoguesViewService } from 'src/app/services/dialogues.view.service';
import { Observable, Subscription } from 'rxjs';
import { IDialogue } from 'src/app/interfaces/IDialogue';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { Dialogue } from 'src/app/models/Dialogue';
import { unsubscriber } from 'src/app/shared/handlers/handlers';
import { UnseenViewService } from 'src/app/services/unseen.view.service';
import { MessagePage } from 'src/app/types/messages.types';
import { UnseenKey } from 'src/app/types/unseen.types';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'cst-message-chats',
  templateUrl: './message-chats.component.html',
  styleUrls: ['./message-chats.component.scss']
})
export class MessageChatsComponent implements OnInit, OnDestroy {
  
  open$: Observable<IDialogue[]> = this.getOpenSub();
  dialogue$: Observable<Dialogue> = this.viewService.getStream('dialogue$');
  resetUnreadChats$: Observable<UnseenKey> = this.useenService.getStream('reset$');

  subs$: Subscription[] = [];

  currentIndex: number = null;
  page: MessagePage;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private viewService: DialoguesViewService,
    private useenService: UnseenViewService
  ) { }

  ngOnInit() {
    this.initRouteSub();
    this.initDialogueSub();
    this.initResetUnreadSub();
  }

  ngOnDestroy() {
    unsubscriber(this.subs$);
  }

  getOpenSub(): Observable<IDialogue[]> {
    return this.viewService.getStream('open$').pipe(
      tap((dialogues: IDialogue[]) => {
        this.currentIndex = this.getCurrentIndex();
      })
    )
  }

  initDialogueSub(): void {
   const sub$: Subscription = this.viewService.getStream('dialogue$').subscribe((dialogue: IDialogue) => {
      this.currentIndex = this.getCurrentIndex();
    });

    this.subs$.push(sub$);
  }

  initRouteSub(): void {
    const sub$: Subscription = this.activeRoute.queryParamMap.subscribe((params: ParamMap) => {
      this.page = params.get('page') as MessagePage;
    });

    this.subs$.push(sub$);
  }

  initResetUnreadSub(): void {
    const sub$: Subscription = this.resetUnreadChats$.subscribe((key: UnseenKey) => {
      if(key === 'dialogues') {
        this.viewService.readAllChats();
      }
    });

    this.subs$.push(sub$);
  }

  getCssClass(idx: number): ICssStyles {
    return {
      'active': idx === this.currentIndex,
      'deactive': idx !== this.currentIndex
    }
  }

  getCurrentIndex(): number {
    const open: IDialogue[] = this.viewService.getStreamValue('open$');
    const dialogue: IDialogue = this.viewService.getStreamValue('dialogue$');

    if(dialogue) {
      const idx: number = open.findIndex(d => d._id === dialogue._id);
      return ~idx ? idx : null;
    } 

    return null;
  }

  setDialogue(dialogue: IDialogue, idx: number): void {
    this.setCurrentIndex(idx);
    this.router.navigate(['/messages/dialogue', dialogue._id]);
  }

  setCurrentIndex(idx: number): void {
    this.currentIndex = idx;
  }

  close(e: Event, dialogue: IDialogue, idx: number): void {
    e.stopPropagation();
    
    this.viewService.removeOpenDialogue(dialogue);
  }

  isShowReadAll(): boolean {
    const isShowPage: boolean = this.page === 'dialogue' || this.page === 'discussion';

    return this.useenService.isDefault('dialogues') && isShowPage;
  }

  readAllChats(): void {
    const isReset = this.useenService.getFlag('isReset') as boolean;

    if(!isReset) {
      this.useenService.reset('dialogues');
    }
  }
}
