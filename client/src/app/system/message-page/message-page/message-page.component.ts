import { Component, OnInit, OnDestroy } from '@angular/core';
import { DialoguesViewService } from 'src/app/services/dialogues.view.service';
import { DialoguesFlags } from 'src/app/models/flags/DialoguesFlags';
import { MessageViewService } from 'src/app/services/message.view.service';

@Component({
  selector: 'cst-message-page',
  templateUrl: './message-page.component.html',
  styleUrls: ['./message-page.component.scss']
})
export class MessagePageComponent implements OnInit, OnDestroy {

  dFlags: DialoguesFlags = this.dialogViewService.getFlagObject();

  constructor(
    private messageViewService: MessageViewService,
    private dialogViewService: DialoguesViewService
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.messageViewService.destroy();
    this.dialogViewService.destroy();
  }

  showCreateDiscussion(flag: boolean): void {
    this.dialogViewService.setFlag('isShowCreateDiscussion', flag)
  }
}
