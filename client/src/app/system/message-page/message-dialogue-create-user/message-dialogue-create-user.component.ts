import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IIncompleteUser } from 'src/app/interfaces/IIncompleteUser';

@Component({
  selector: 'cst-message-dialogue-create-user',
  templateUrl: './message-dialogue-create-user.component.html',
  styleUrls: ['./message-dialogue-create-user.component.scss']
})
export class MessageDialogueCreateUserComponent implements OnInit {

  @Input('user') user: IIncompleteUser;
  @Input('selected') selected: boolean;
  @Input('request') request: boolean;
  @Input('disable') disable: boolean;

  @Output('select') _select = new EventEmitter<IIncompleteUser>()

  constructor() { }

  ngOnInit() {
  }

  select(): void {
    this._select.emit(this.user)
  }
  

}
