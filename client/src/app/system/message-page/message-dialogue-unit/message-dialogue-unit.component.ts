import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IDialogue } from 'src/app/interfaces/IDialogue';
import { isOwn } from 'src/app/shared/handlers/handlers';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';

@Component({
  selector: 'cst-message-dialogue-unit',
  templateUrl: './message-dialogue-unit.component.html',
  styleUrls: ['./message-dialogue-unit.component.scss']
})
export class MessageDialogueUnitComponent {

  @Input('dialogue') dialogue: IDialogue;
  @Input('first') first: boolean = false;
  @Output('navigate') _navigate = new EventEmitter<IDialogue>();

  constructor() {}

  getMessageTitle() {
    const { User } = this.dialogue.LastMessage;
    
    return isOwn(User) ? 'Вы:' : '';
  }

  navigate(): void {
    this._navigate.emit(this.dialogue);
  }

  getMessageClass(): ICssStyles {
    const isEmptyMessage: boolean = !this.dialogue.LastMessage.Text;

    return {
      'message__text': !isEmptyMessage,
      'empty-media-text message__empty': isEmptyMessage
    }
  }
}
