import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Renderer2, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription, Observable, Subject } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { MessageFlags } from 'src/app/models/flags/MessageFlags';
import { MediaList } from 'src/app/models/MediaList';
import { MessageViewService } from 'src/app/services/message.view.service';
import { DialoguesViewService } from 'src/app/services/dialogues.view.service';
import { IMessage } from 'src/app/interfaces/IMessage';
import { IDialogue } from 'src/app/interfaces/IDialogue';
import { PaginationRequest } from 'src/app/models/PaginationRequest';
import { Page } from 'src/app/models/Page';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { isExpand, unsubscriber } from 'src/app/shared/handlers/handlers';
import { IUser } from 'src/app/interfaces/IUser';
import { SocketService } from 'src/app/services/socket.service';
import { Message } from 'src/app/models/Message';
import { delay, tap } from 'rxjs/operators';
import { SimpleAction } from 'src/app/models/SimpleAction';
import { SOKET_EVENTS } from 'src/app/namespaces/socket-events.namespaces';
import { TypingEvent, SwitchRoom, NewDiscussion } from 'src/app/types/messages.types';
import { fade } from 'src/app/shared/animations/animation';
import { Length } from 'src/app/types/validation.types';
import { ResponseAction } from 'src/app/types/request-response.types';
import * as moment from 'moment';

@Component({
  selector: 'cst-message-dialogues-detail',
  templateUrl: './message-dialogues-detail.component.html',
  styleUrls: ['./message-dialogues-detail.component.scss'],
  animations: [fade]
})
export class MessageDialoguesDetailComponent implements OnInit, AfterViewInit, OnDestroy {

  messages$: Observable<MediaList<IMessage>> = this.viewService.getStream('messages$');
  edit$: Observable<IMessage> = this.getEditSub();
  dialogue$: Observable<IDialogue> = this.getDialogueSub();
  typing$: Subject<string> = new Subject();
  typingTimeout$: any;
  emitTypingTimeout$: any;
  editMesssagesIds: string[] = this.viewService.getField('editMesssagesIds');
  deletionCandidatesIds: string[] = this.viewService.getField('deletionCandidatesIds');
  removedMessagesIds: string[] = this.viewService.getField('removedMessagesIds');
  
  messageRequest: PaginationRequest;

  textControl: FormControl = new FormControl('');

  currentDialogueId: string = null;

  contentHeight: number;
  
  lengths: Length = this.viewService.getValidatorLength('MESSAGE');
  flags: MessageFlags = this.viewService.getFlagObject();
  subs$: Subscription[] = [];

  @ViewChild('contentRef', { static: true }) contentRef: ElementRef
  @ViewChild('listRef', { static: false }) listRef: ElementRef;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private renderer: Renderer2,
    private viewService: MessageViewService,
    private dialoguesViewService: DialoguesViewService,
    private socketService: SocketService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngAfterViewInit() {
    this.setHeight();
  }

  ngOnDestroy() {
    unsubscriber(this.subs$);
    this.socketService.emitEvent(SOKET_EVENTS.MESSAGE_SWITCH_ROOM, {leaveRoom: this.currentDialogueId});
    this.reset();
    this.viewService.remove(this.currentDialogueId);
  }

  init(): void {
    this.initRouteSub();
    this.initScrollSub();
    this.initCreateDiscussionSub();
    this.initNewMessageSub();
    this.initTypingSub();
    this.initEditSub();
    this.initRemoveSub();
  }

  initRouteSub(): void {
    this.activeRoute.params
      .pipe(
        tap(_ => {
          this.viewService.unsubscribe();
          this.dialoguesViewService.unsubscribe();
        }),
        untilDestroyed(this)
      )
      .subscribe((params: Params) => {
        this.reset();
        this.initMessageRequest(params.id);
        this.dialoguesViewService.fetchDialogue(params.id);
        this.viewService.fetch(this.messageRequest, 'replace');
      });
  }

  getDialogueSub(): Observable<IDialogue> {
    return (this.dialoguesViewService.getStream('dialogue$') as Observable<IDialogue>)
    .pipe(
      tap((dialogue: IDialogue) => {
        const isSwitch: boolean = dialogue && dialogue._id !== this.currentDialogueId;

        if(isSwitch) {
          this.switchRoom(dialogue);
        }
      })
    );
  }

  getEditSub(): Observable<IMessage> {
    return this.viewService.getStream('edit$').pipe(
      tap((message: IMessage) => {
        const value: string = message ? message.Text : '';

        this.textControl.setValue(value);
      })
    )
  }

  getDeletionState(messageId: string): boolean {
    return this.deletionCandidatesIds.includes(messageId) || this.removedMessagesIds.includes(messageId);
  }

  typingMessage() {
    if(this.emitTypingTimeout$) return;
    
    this.initEmitTyping();

    const { Name: UserName } = this.viewService.getDecodeToken();
    const { _id: DialogueId } = this.dialoguesViewService.getStreamValue('dialogue$');

    const payload: TypingEvent = { UserName, DialogueId };

     this.socketService.emitEvent(SOKET_EVENTS.MESSAGE_TYPING, payload);
  }

  switchRoom(dialogue: IDialogue) {
    const leaveRoom: string = this.currentDialogueId;
    const joinRoom: string  = dialogue._id;
    const payload: SwitchRoom = { leaveRoom, joinRoom };

    
    this.currentDialogueId = joinRoom;
    this.socketService.emitEvent(SOKET_EVENTS.MESSAGE_SWITCH_ROOM, payload);
    this.viewService.remove(leaveRoom);
  }

  initNewMessageSub(): void {
    this.socketService
    .getListener(SOKET_EVENTS.MESSAGE_NEW)
    .pipe(untilDestroyed(this))
    .subscribe((message: IMessage) => {
      const dialogue: IDialogue = this.dialoguesViewService.getStreamValue('dialogue$');
      const isEqualDialogues: boolean = Object.is(dialogue._id, message.Dialogue);
      
      if(!isEqualDialogues) {
        this.viewService.addNewOpenDialogue(message);
      } else {
        const event: SimpleAction<IMessage> = new SimpleAction('add', message);
        this.viewService.mutate(event);
        this.dialoguesViewService.mutateDetail(event);
      }
    });
  }

  initTypingSub(): void {
    this.socketService
    .getListener(SOKET_EVENTS.MESSAGE_TYPING)
    .pipe(untilDestroyed(this))
    .subscribe(({ UserName }) => {
      this.showTyping(UserName);
    });
  }

  initEditSub(): void {
    this.socketService
    .getListener(SOKET_EVENTS.MESSAGE_EDIT)
    .pipe(untilDestroyed(this))
    .subscribe((message: IMessage) => {
      const isCurrentDialgue: boolean = message.Dialogue === this.currentDialogueId;

      if(isCurrentDialgue) {
        const event: SimpleAction<IMessage> = new SimpleAction('edit', message);
        this.viewService.mutate(event);
        this.viewService.showEditMessage(message._id);
      }
    });
  }

  initRemoveSub(): void {
    this.socketService
    .getListener(SOKET_EVENTS.MESSAGE_REMOVE)
    .pipe(untilDestroyed(this))
    .subscribe(({ Dialogue, messagesIds }) => {
      const isCurrentDialgue: boolean = Dialogue === this.currentDialogueId;

      if(isCurrentDialgue) {
        const event: SimpleAction<string[]> = new SimpleAction('remove', messagesIds);
        this.viewService.mutate(event);
      }
    });
  }

  showTyping(UserName: string): void {
    if(this.typingTimeout$) return;

    this.typing$.next(`${UserName} печатает сообщение`);

    this.typingTimeout$ = setTimeout(() => {
      this.resetTyping();
    }, 2500)
  }

  initEmitTyping(): void {
    this.emitTypingTimeout$ = setTimeout(() => {
      this.resetEmitTyping();
    }, 2500)
  }

  resetEmitTyping(): void {
    clearTimeout(this.emitTypingTimeout$);
    this.emitTypingTimeout$ = null;
  }

  resetTyping(): void {
    clearTimeout(this.typingTimeout$);
    this.typingTimeout$ = null;
    this.typing$.next(null);
  }

  initCreateDiscussionSub(): void {
    this.socketService
    .getListener(SOKET_EVENTS.MESSAGE_CREATE_DISCUSSION)
    .pipe(untilDestroyed(this))
    .subscribe((payload: NewDiscussion) => {
      const { message, dialogue } = payload;
      
      this.dialoguesViewService.addOpen(dialogue);
    })
  }

  initMessageRequest(dialogueId: string): void {
    const page: Page = new Page(0, LIMIT.MESSAGES);
    this.messageRequest = new PaginationRequest(dialogueId, page);
  }

  initScrollSub(): void {
    this.viewService.getStream('scroll$')
    .pipe(delay(10), untilDestroyed(this))
    .subscribe((action?: ResponseAction) => {
      if(this.listRef) {
        const { nativeElement } = this.listRef;
        const { scrollHeight, scrollTop, clientHeight } = nativeElement;
        const offset: number = scrollHeight - clientHeight - 200;

        if(scrollTop > offset || action === 'replace') {
          nativeElement.scrollTop = nativeElement.scrollHeight;
        } 
      }
    });
  }

  getPlaceholder(): string {
    const message: IMessage = this.viewService.getStreamValue('edit$');

    let placeholder;

    if(message) {
      const { Created, Text } = message;
      const formatDate: string = moment(Created).locale('ru').format('MMMM D, YYYY');
      const messagePart: string = Text.length > 10 ? `${Text.slice(0, 10)}...` : Text;

      placeholder = `Редактировать сообщение: "${messagePart}" от (${formatDate}).`;

    } else {
      placeholder = 'Введите сообщение';
    }

    return placeholder;
  }

  disabled(): boolean {
    return !this.textControl.value || this.viewService.getFlag('isSendRequest') as boolean;
  }

  disabledEdit(): boolean {
    return !this.textControl.value || this.viewService.getFlag('isEditRequest') as boolean;
  }

  reset(): void {
    this.dialoguesViewService.destroySubjects(['dialogue$']);
    this.viewService.destroySubjects(['messages$']);
    this.resetTyping();
    this.initEmitTyping();
  }

  setHeight(): void {
    const $content = this.contentRef.nativeElement;

    let offset: number= 0;
    let parent: any = $content;

    while(parent) {
      offset += parent.offsetTop;
      parent = parent.offsetParent;
    }

    this.renderer.setStyle($content, 'height', `calc(100vh - ${offset}px - 2.7rem)`);
  }

  trackByFn(index: number, message: IMessage): string {
    return message._id;
  }

  navigateToUser(payload: IUser | IMessage, type: 'user' | 'message'): void {
    const accountId: string = type == 'user' ? (payload as IUser)._id : (payload as IMessage).User; 
    this.router.navigate(['/account', accountId]);
  }

  expand(): void {
    const messages: MediaList<IMessage> = this.viewService.getStreamValue('messages$');

    const expand: boolean = isExpand(messages);

    if(expand) {
      this.messageRequest.page.skip = messages.rows.length;
      this.viewService.fetch(this.messageRequest, 'expand');
    }
  }

  send(): void {
    const { value } = this.textControl;
    const { _id: Dialogue }: IDialogue = this.dialoguesViewService.getStreamValue('dialogue$');
    const ownId: string = this.viewService.getOwnId();

    const message: Message = new Message(
      value,
      ownId,
      Dialogue
    );

    this.viewService.send(message);

    this.textControl.reset();
  }
  
  setDeletetionState(message: IMessage): void {
     this.viewService.setDeletionState(message._id);
  }

  edit(message: IMessage): void {
    const isEdit = this.viewService.getFlag('isEditRequest') as boolean;
    if(isEdit && message) return

    this.viewService.emitStream('edit$', message);
  }

  sendEdit(): void {
    const { value } = this.textControl;
    const message: IMessage = this.viewService.getStreamValue('edit$');
    const isEdit = this.viewService.getFlag('isEditRequest') as boolean;
    const isEqual: boolean = Object.is(message.Text, value);
    const editable: boolean = !isEqual && !isEdit;

    if(isEqual) {
      this.edit(null);
    }

    if(editable) {
      this.viewService.edit(value);
    }
  }
}
