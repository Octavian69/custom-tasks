import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { IUser } from 'src/app/interfaces/IUser';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { FormControl, Validators } from '@angular/forms';
import { MessageViewService } from 'src/app/services/message.view.service';
import { Message } from 'src/app/models/Message';

@Component({
  selector: 'cst-message-modal',
  templateUrl: './message-modal.component.html',
  styleUrls: ['./message-modal.component.scss']
})
export class MessageModalComponent implements OnInit {

  user: IUser;

  messageControl: FormControl;
  limit: number = LIMIT.MESSAGE;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private storage: StorageService,
    private viewService: MessageViewService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.storage.removeItem('sessionStorage', 'messageUser');
  }

  init(): void {
    this.user = this.storage.getItem('sessionStorage', 'messageUser');
    this.initControl();
  }

  initControl(): void {
    this.messageControl = new FormControl(null, [Validators.required], [])
  }

  hide(): void {
    const { queryParams } = this.activeRoute.snapshot;
    
    const query: Params = Object.assign({}, queryParams);

    delete query['message_id']

    this.router.navigate([], {
      relativeTo: this.activeRoute,
      queryParams: query
    })
  }

  navigateToUser(user: IUser): void {
    this.router.navigate(['/account', user._id]);
  }

  sendMessage(): void {
    const { value: Text } = this.messageControl;
    const User = this.viewService.getOwnId()
    const message: Message = new Message(Text, User);

    this.viewService.sendSingle(this.user._id, (message));

    this.hide();
  }

}
