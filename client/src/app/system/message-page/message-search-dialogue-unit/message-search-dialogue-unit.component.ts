import { Component, Input, EventEmitter, Output } from '@angular/core';
import { IDialogue } from 'src/app/interfaces/IDialogue';
import { IIncompleteUser } from 'src/app/interfaces/IIncompleteUser';
import { DialogueType } from 'src/app/types/messages.types';

@Component({
  selector: 'cst-message-search-dialogue-unit',
  templateUrl: './message-search-dialogue-unit.component.html',
  styleUrls: ['./message-search-dialogue-unit.component.scss']
})
export class MessageSearchDialogueUnitComponent {

  @Input('unit') unit: IDialogue | IIncompleteUser;
  @Input('modeType') modeType: DialogueType;
  @Output('navigate') _navigate = new EventEmitter<IDialogue | IIncompleteUser>();

  getText(): string {
    const key: 'Name' | 'Title' = this.modeType === 'dialogue' ? 'Name' : 'Title';

    return this.unit[key];
  }

  getLogo(): string {
    const key: 'Logo' | 'Avatar' = this.modeType === 'dialogue' ? 'Avatar' : 'Logo';

    return this.unit[key];
  }

  navigate(): void {
    this._navigate.emit(this.unit);
  }

}
