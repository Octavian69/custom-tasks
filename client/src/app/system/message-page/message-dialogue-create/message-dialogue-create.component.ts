import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { MediaList } from 'src/app/models/MediaList';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { unsubscriber, isExpand, checkedFile } from 'src/app/shared/handlers/handlers';
import { PaginationRequest } from 'src/app/models/PaginationRequest';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { Page } from 'src/app/models/Page';
import { DialoguesViewService } from 'src/app/services/dialogues.view.service';
import { IIncompleteUser } from 'src/app/interfaces/IIncompleteUser';
import { DialoguesFlags } from 'src/app/models/flags/DialoguesFlags';
import { debounceTime } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Dialogue } from 'src/app/models/Dialogue';
import { Message } from 'src/app/models/Message';
import { FormObjectErrors, Length } from 'src/app/types/validation.types';
import { DialogueType, CreateDiscussion } from 'src/app/types/messages.types';

@Component({
  selector: 'cst-message-dialogue-create',
  templateUrl: './message-dialogue-create.component.html',
  styleUrls: ['./message-dialogue-create.component.scss']
})
export class MessageDialogueCreateComponent implements OnInit, OnDestroy {

  friends$: Observable<MediaList<IIncompleteUser>> = this.viewService.getStream('friends$');

  form: FormGroup;
  searchControl: FormControl = new FormControl(null);
  mediaRequest: PaginationRequest;
  selectedIds: string[] = [];
  file: File;
  errors: FormObjectErrors = this.viewService.completeDbErrors(['messages', 'formErrors', 'dialogues'])
  preview: string = null;
  dialogueLengths: Length = this.viewService.getValidatorLength('DIALOGUE');
  messageLengths: Length = this.viewService.getValidatorLength('MESSAGE');
  flags: DialoguesFlags = this.viewService.getFlagObject();
  subs$: Subscription[] = [];

  @Output('hide') _hide = new EventEmitter<void>();

  constructor(
    private toastr: ToastrService,
    private viewService: DialoguesViewService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.viewService.destroySubjects(['friends$'])
    unsubscriber(this.subs$);
  }

  init(): void {
    this.initRequest();
    this.viewService.fetchFriends(this.mediaRequest, 'replace');
    this.initSearchSub();
    this.initForm();
  }

  initForm(): void {
    this.form = new FormGroup({
      Title: new FormControl(null, [Validators.required]),
      FirstMessage: new FormControl(null, [Validators.required])
    })
  }

  initRequest(): void {
    const page = new Page(0, LIMIT.USERS_INCOMPLETE);
    const ownId: string = this.viewService.getOwnId();

    this.mediaRequest = new PaginationRequest(ownId, page);
  }

  initSearchSub(): void {
    const sub$: Subscription = this.searchControl
      .valueChanges
      .pipe(debounceTime(400))
      .subscribe((value: string) => {
        this.selectedIds = [];
        this.mediaRequest.filters.Name = value;
        this.mediaRequest.page.skip = 0;
        this.viewService.fetchFriends(this.mediaRequest, 'replace');
      });

      this.subs$.push(sub$);
  }

  changePreview(e: Event): void {
    this.resetFile()
    const file: File = (e.target as HTMLInputElement).files[0];

    const { isValid, errors } = checkedFile('PICTURE', file);

    if(!isValid) {
      this.toastr.warning(errors.toString(), 'Внимание');
      return
    }

    this.file = file;
    this.preview = URL.createObjectURL(file);
  }

  resetFile(): void {
    this.file = null;
    this.preview = null;
  }

  revoke(): void {
    URL.revokeObjectURL(this.preview);
  }

  hide(): void {
    this._hide.emit();
  }

  expand() {
    const mediaList: MediaList<IIncompleteUser> = this.viewService.getStreamValue('friends$');
    const expand: boolean = isExpand(mediaList);

    if(expand) {
      this.mediaRequest.page.skip++;
      this.viewService.fetchFriends(this.mediaRequest, 'expand');
    }
  }

  select(user: IIncompleteUser) {
    const idx: number = this.selectedIds.indexOf(user._id);

    if(~idx) {
      this.selectedIds.splice(idx, 1);
    } else if(this.disabledSelected()) {
        this.toastr.warning('Максимальное количество участников: 10', 'Внимание');
    } else {
      this.selectedIds.push(user._id);
    }
  }

  disabledSelected(): boolean {
    return this.selectedIds.length >= 10;
  }

  disabled(): boolean {
    const { invalid } = this.form;
    
    return invalid 
           || this.selectedIds.length < 2
           || this.viewService.getFlag('isCreateDialogue') as boolean;
  }

  selected(userId: string): boolean {
    return this.selectedIds.includes(userId);
  }

  create(): void {
    const { value: { Title, FirstMessage } } = this.form;
    const Type: DialogueType = 'discussion';
    const User: string = this.viewService.getOwnId();
    const Participants: string[] = this.selectedIds.concat(User);
    
    const dialogue: Dialogue = new Dialogue(Title, Participants, Type, User);
    const message: Message = new Message(FirstMessage, User);

    const payload: CreateDiscussion = { dialogue, message };

    this.viewService.createDiscussion(payload, this.file)

  }

}
