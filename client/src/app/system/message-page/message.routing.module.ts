import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessagePageComponent } from './message-page/message-page.component';
import { MessageDialoguesComponent } from './message-dialogues/message-dialogues.component';
import { MessageDialoguesDetailComponent } from './message-dialogues-detail/message-dialogues-detail.component';

const routes: Routes = [
    { path: '', component: MessagePageComponent, children: [
        {path: '', component: MessageDialoguesComponent},
        {path: 'dialogue/:id', component: MessageDialoguesDetailComponent}
    ]}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MessageRoutingModule{}