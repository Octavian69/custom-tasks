import { Component, OnInit, OnDestroy } from '@angular/core';
import { AudioWorkService } from 'src/app/services/audio.work.service';
import { VideoWorkService } from 'src/app/services/video.work.service';
import { Router } from '@angular/router';
import { UserViewService } from 'src/app/services/user.view.service';
import { SocketService } from 'src/app/services/socket.service';
import { PopupsViewService } from 'src/app/services/popups.view.service';
import { Observable } from 'rxjs';
import { Popup } from 'src/app/models/Popup';
import { UnseenViewService } from 'src/app/services/unseen.view.service';
import { EditViewService } from 'src/app/services/edit.view.service';

@Component({
  selector: 'td-system-page',
  templateUrl: './system-page.component.html',
  styleUrls: ['./system-page.component.scss']
})
export class SystemPageComponent implements OnInit, OnDestroy {

  popups$: Observable<Popup[]> = this.popupService.getStream('popups$');

  constructor(
    private router: Router,
    private audioWorker: AudioWorkService,
    private videoWorker: VideoWorkService,
    private userViewService: UserViewService,
    private socketService: SocketService,
    private popupService: PopupsViewService,
    private unseenViewService: UnseenViewService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.audioWorker.destroy();
    this.videoWorker.destroy();
    this.userViewService.destroySubjects(['user$']);
    this.unseenViewService.destroySubjects(['unseen$']);
  }

  init(): void {
    this.userViewService.init();
    this.socketService.init();
    this.popupService.init();
    this.unseenViewService.init();
  }

  showComponent(sliderString): boolean {
    return this.router.url.includes(sliderString);
  }

  showMessagePopups(): boolean {
    return !this.router.url.includes('messages');
  }

  routerSub(): void {
    // const sub: Subscription = this.router.events
    //   .pipe(
    //     tap(e => console.log(e)),
    //     filter((e: RouterEvent) => e instanceof NavigationEnd))
    //   .subscribe(_ => {
    //     this.animationState = !this.animationState;
    //   })

    //  this.subs$.push(sub);
   }
}
