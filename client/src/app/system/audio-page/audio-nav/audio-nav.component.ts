import { Component, Output, EventEmitter, Input } from '@angular/core';
import { PageLink } from 'src/app/types/request-response.types';

@Component({
  selector: 'cst-audio-nav',
  templateUrl: './audio-nav.component.html',
  styleUrls: ['./audio-nav.component.scss']
})
export class AudioNavComponent {


  @Input('list') options: PageLink[];
  @Input('userId') userId: string;
  @Input('isOwn') isOwn: boolean = false;
  @Output('page') page = new EventEmitter<PageLink>();
  @Output('create') _create = new EventEmitter<'playlist' | 'audio'>();


  setPage(link: PageLink): void {
    this.page.emit(link);
  }

  create(type: 'playlist' | 'audio'): void {
    this._create.emit(type);
  }

}
