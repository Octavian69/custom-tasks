import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IAudioPlaylist } from 'src/app/interfaces/IAudioPlaylist';

@Component({
  selector: 'cst-audio-playlist',
  templateUrl: './audio-playlist.component.html',
  styleUrls: ['./audio-playlist.component.scss']
})
export class AudioPlaylistComponent {

  
  @Input('playlist') playlist: IAudioPlaylist;
  @Input('isOwn') isOwn: boolean;
  @Input('isMutateRequest') isMutateRequest: boolean = false;

  @Output('edit') _edit = new EventEmitter<IAudioPlaylist>();
  @Output('navigateToUser') _navigate = new EventEmitter<string>();
  @Output('add') _add = new EventEmitter<IAudioPlaylist>();
  @Output('play') _play = new EventEmitter<IAudioPlaylist>();
  @Output('remove') _remove = new EventEmitter<IAudioPlaylist>();
  @Output('detail') _detail = new EventEmitter<IAudioPlaylist>();

  getAddIcon(): 'check' | 'add' {
    return this.playlist.IsHave ? 'check' : 'add';
  }

  getAddTooltip(): string {
    const action: string =  this.playlist.IsHave ? 'Убрать' : 'Добавить';

    return `${action} плейлист`
  }

  edit(): void {
    this._edit.emit(this.playlist);
  }

  mutate(): void {
    const action = this.playlist.IsHave ? '_remove' : '_add';

    this[action].emit(this.playlist);
  }

  navigate(): void {
    this._navigate.emit(this.playlist.User);
  }

  play(): void {
    this._play.emit(this.playlist);
  }

  detail(): void {
    this._detail.emit(this.playlist);
  }
}
