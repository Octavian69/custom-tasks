import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { IAudioPlaylist } from 'src/app/interfaces/IAudioPlaylist';
import { AudioPlaylistsViewService } from 'src/app/services/audio-playlists.view.service';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Page } from 'src/app/models/Page';
import { MediaList } from 'src/app/models/MediaList';
import { AudioPlaylistsFlags } from 'src/app/models/flags/AudioPlaylistsFlags';
import { ToastrService } from 'ngx-toastr';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { isExpand } from 'src/app/shared/handlers/handlers';

@Component({
  selector: 'cst-audio-playlists',
  templateUrl: './audio-playlists.component.html',
  styleUrls: ['./audio-playlists.component.scss']
})
export class AudioPlaylistsComponent implements OnInit, OnDestroy {
  
  userId: string;
  page: Page = new Page(0, LIMIT.AUDIO_PLAYLISTS);
  detail$: BehaviorSubject<IAudioPlaylist> = this.viewService.getField('detail$');
  playlists$: BehaviorSubject<MediaList<IAudioPlaylist>> = this.viewService.getField('playlists$');
  flags: AudioPlaylistsFlags = this.viewService.getFlagObject();
  
  @Input('showClass') showClass: string;

  constructor(
    private router: Router,
    private activeRouter: ActivatedRoute,
    private toastr: ToastrService,
    private viewService: AudioPlaylistsViewService
  ) { }

  ngOnInit() {
    const { id } = this.activeRouter.snapshot.params;
    this.userId = id;
    this.viewService.fetch(this.userId, 'replace', this.page);
  }

  ngOnDestroy() {
    this.viewService.destroySubjects(['playlists$']);
  }

  getShowClass(): ICssStyles {
    return {
      [this.showClass]: true
    }
  }

  trackByFn(index: number, playlist: IAudioPlaylist): string {
    return playlist._id;
  }

  isMutateRequest(playlistId: string): boolean {
    return this.flags.isMutateRequest === playlistId;
  }

  add(playlist: IAudioPlaylist): void {
    this.viewService.add(playlist)
  }

  remove(playlist: IAudioPlaylist): void {
    this.viewService.remove(playlist)
  }

  detail(playlist: IAudioPlaylist): void {
    this.router.navigate(['playlist-detail', playlist._id], { relativeTo: this.activeRouter });
  }

  isOwn(playlist: IAudioPlaylist): boolean {
    const { Creator } = playlist;
    return this.viewService.isOwn(Creator);
  }

  expand(): void {
    const mediaList: MediaList<IAudioPlaylist> = this.viewService.getStreamValue('playlists$');

    const expand: boolean = isExpand(mediaList);

    if(expand) {
      this.page.skip++;
      this.viewService.fetch(this.userId, 'expand', this.page)
    }
  }

  play(playlist: IAudioPlaylist): void {
    const { TotalCount } = playlist;

    if(!TotalCount) {
      this.toastr.info('Список аудиозаписей пуст', 'Информация');
    } else {
      this.viewService.play(playlist);
    }
  }
  
  navigateToUser(userId: string): void {
    this.router.navigate(['/account', userId]);
  }
  
  edit(playlist: IAudioPlaylist): void {
    this.router.navigate(['playlist-edit', playlist._id], { relativeTo: this.activeRouter });
  }

}
