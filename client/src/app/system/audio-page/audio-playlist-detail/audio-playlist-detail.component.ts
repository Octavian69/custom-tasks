import { Component, OnInit } from '@angular/core';
import { AudioPlaylistsViewService } from 'src/app/services/audio-playlists.view.service';
import { BehaviorSubject } from 'rxjs';
import { IAudioPlaylist } from 'src/app/interfaces/IAudioPlaylist';
import { MediaList } from 'src/app/models/MediaList';
import { Location } from '@angular/common';
import { AudioWorkService } from 'src/app/services/audio.work.service';
import { AudioPlaylistsFlags } from 'src/app/models/flags/AudioPlaylistsFlags';
import { ActivatedRoute } from '@angular/router';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { MediaRequest } from 'src/app/models/MediaRequest';
import { ICurrentMediaList } from 'src/app/interfaces/ICurrentMediaList';
import { IAudio } from 'src/app/interfaces/IAudio';
import { AudioViewService } from 'src/app/services/audio.view.service';

@Component({
  selector: 'cst-audio-playlist-detail',
  templateUrl: './audio-playlist-detail.component.html',
  styleUrls: ['./audio-playlist-detail.component.scss']
})
export class AudioPlaylistDetailComponent implements OnInit {

  playlist$: BehaviorSubject<IAudioPlaylist> = this.viewService.getField('playlist$');
  mediaList$: BehaviorSubject<MediaList<IAudio>> = this.viewService.getField('mediaList$');

  flags: AudioPlaylistsFlags = this.viewService.getFlagObject();

  constructor(
    private location: Location,
    private activeRouter: ActivatedRoute,
    public audioViewService: AudioViewService,
    public viewService: AudioPlaylistsViewService,
    public audioWorker: AudioWorkService
  ) { }

  ngOnInit() {
    const { id } = this.activeRouter.snapshot.params;
    const ownId: string = this.viewService.getOwnId()
    this.viewService.init(id, ownId);
  }

  getMutateText(): string {
    const { IsHave, Creator } = this.playlist$.getValue();
    const isOwn: boolean = this.viewService.isOwn(Creator);

    const isRemove: boolean = IsHave && !isOwn;
    const isDelete: boolean = IsHave && isOwn;
    const isAdd: boolean = !IsHave;
  
    switch(true) {
      case isRemove: {
        return 'Убрать из списка моих плейлистов';
      }
      case isDelete: {
        return 'Удалить плейлист';
      }
      case isAdd: {
        return 'Добавить в список моих плейлистов';
      }
    }
  }

  getPlayClass(): ICssStyles {
    const isPaused: boolean = this.audioWorker.getPlayStatus();

    return {
      'played': !isPaused,
      'paused': isPaused
    }
  }

  getPlayIcon(): string {

    const playlist: IAudioPlaylist = this.viewService.getStreamValue('playlist$');

    const isCurrent: boolean = this.audioWorker.isCurrentList(playlist._id, 'main');
    const isPaused: boolean = this.audioWorker.getPlayStatus();

    return (isCurrent && isPaused ) || !isCurrent ? 'play_circle_filled' : 'pause_circle_filled';
  }

  playback(side: 'prev' | 'next'): void {
    const isExpandRequest: boolean = <boolean>this.audioWorker.getFlag('isExpandRequest');

    if(!isExpandRequest) {
      this.audioWorker.shiftAudioList(side);
    }
  }

  play() {
    const playlist: IAudioPlaylist = this.viewService.getStreamValue('playlist$');

    const isCurrent: boolean = this.audioWorker.isCurrentList(playlist._id, 'main');

    if(isCurrent) {
      this.audioWorker.playback();
    } else {
      const mediaList = this.mediaList$.getValue();
      const mediaRequest = new MediaRequest('main', playlist._id, 30);

      const audioList: ICurrentMediaList<IAudio> = this.audioWorker.createList(mediaList, mediaRequest, playlist._id);

      this.audioWorker.changeCurrentList(audioList);
      this.audioWorker.playSong(mediaList.rows[0]);
    }
  }

  hide() {
    this.location.back();
  }

  isMutateRequest(_id: string): boolean {
    return this.audioWorker.getAddRequestStatus(_id);
  }

  mutateAudio(audio: IAudio, action: 'add' | 'remove'): void {
    const method = action === 'add' ? 'addAudioInPlaylist' : 'removeAudioFromPlaylist';

    this.audioViewService[method](audio, false);
  }

  mutate(): void {
    const playlist: IAudioPlaylist = this.viewService.getStreamValue('playlist$');
    const method: 'add' | 'remove' = playlist.IsHave ? 'remove' : 'add';
    

    this.viewService[method](playlist);
  }

}
