import { Component, OnInit, OnDestroy, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { unsubscriber, checkedFile } from 'src/app/shared/handlers/handlers';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { topShow } from 'src/app/shared/animations/animation';
import { AudioPlaylist } from 'src/app/models/AudioPlaylist';
import { MediaRequest } from 'src/app/models/MediaRequest';
import { AudioWorkService } from 'src/app/services/audio.work.service';
import { IAudio } from 'src/app/interfaces/IAudio';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { AudioPlaylistsViewService } from 'src/app/services/audio-playlists.view.service';
import { AudioPlaylistsFlags } from 'src/app/models/flags/AudioPlaylistsFlags';
import { ActivatedRoute } from '@angular/router';
import { IAudioPlaylist } from 'src/app/interfaces/IAudioPlaylist';
import { tap } from 'rxjs/operators';
import { Location } from '@angular/common';
import { MediaList } from 'src/app/models/MediaList';
import { ToastrService } from 'ngx-toastr';
import { Length, FormObjectErrors } from 'src/app/types/validation.types';
import { AudioNavIcons } from 'src/app/types/audio.types';

@Component({
  selector: 'cst-audio-playlist-modal',
  templateUrl: './audio-playlist-modal.component.html',
  styleUrls: ['./audio-playlist-modal.component.scss'],
  animations: [topShow]
})
export class AudioPlaylistModalComponent implements OnInit, OnDestroy {
  
  playlist$: Observable<IAudioPlaylist> = this.initPlaylistSub();
  mediaList$: BehaviorSubject<MediaList<IAudio>> = this.viewService.getField('mediaList$');
  
  modeType: 'playlist-create' | 'playlist-edit';
  playlistId: string;
  icons: AudioNavIcons;
  audioLimit: number = LIMIT.AUDIO;
  isAddInPlaylist: boolean = false;
  selectedAudios: IAudio[] = [];
  selectedIds: string[] = [];
  form: FormGroup;
  file: File;
  preview: string;
  subs$: Subscription[] = [];
  playlist: AudioPlaylist;
  mediaRequest: MediaRequest;
  flags: AudioPlaylistsFlags = this.viewService.getFlagObject();
  errors: FormObjectErrors = this.viewService.completeDbErrors(['audio', 'formErrors', 'Playlist'], 'Playlist');
  lengths: Length = this.viewService.getValidatorLength('PLAYLIST');
  selectedLimit: number = LIMIT.AUDIO_SELECTED;

  @HostBinding('@topShow') topShow;

  constructor(
    private location: Location,
    private activeRouter: ActivatedRoute,
    private toastr: ToastrService,
    private viewService: AudioPlaylistsViewService,
    public audioWorker: AudioWorkService
  ) { }

  ngOnInit() {
    const { id } = this.activeRouter.snapshot.params;
    this.playlistId = id;
    this.init();
    
  }

  ngOnDestroy() {
    unsubscriber(this.subs$);
    this.viewService.destroySubjects(['playlist$', 'mediaList$']);
    this.viewService.setField('AMediaRequest', null);
    this.viewService.setFlag('isShowAddList', false);
  };

  init(): void {
    this.modeType = this.playlistId === '0' ? 'playlist-create' : 'playlist-edit';
    this.initForm();
    this.viewService.init(this.playlistId);
  }

  initForm(): void {
    this.form = new FormGroup({
      Title: new FormControl(null, [Validators.required], []),
      Description: new FormControl(null, [], [])
    })
  }


  initPlaylistSub(): Observable<IAudioPlaylist> {
    return this.viewService.getStream('playlist$').pipe(
      tap((playlist: IAudioPlaylist) => {
        if(!playlist) return
        
        this.preview = playlist.Logo;
        this.form.patchValue(playlist);
      })
    )
  }

  showSearch(flag: boolean): void {
    this.viewService.setFlag('isShowAddList', flag);
  }

  getTitle(): string {
    return this.modeType === 'playlist-create' ? 'Создать' : 'Редактировать';
  }

  getLogoStyle(): ICssStyles {

    return {
      display: !this.preview ? 'flex' : ''
    }
  }

  getNavIcons(): AudioNavIcons {
    return this.audioWorker.getNavIcons('own', this.modeType);
  }

  getDisabledStatus(): boolean {
    return this.form.invalid || this.form.untouched || this.flags.isEditRequest;
  }

  getDisabledSelect(): boolean {
    return this.selectedIds.length >= this.selectedLimit;
  }


  revokeUrl(): void {
    URL.revokeObjectURL(this.preview);
  }

  hide(): void {
    this.location.back();
  }

  addLogo(e: Event): void {
    const file = (e.target as HTMLInputElement).files[0]; 

    const { isValid, errors } = checkedFile('PICTURE', file);

    if(isValid) {
      this.file = file;
      this.preview = URL.createObjectURL(file);
      this.form.markAsTouched();
    } else {
      this.toastr.warning(errors.toString(), 'Внимание!');
    }
  }

  changeSong(audio: IAudio): void {
    
    if(this.modeType === 'playlist-create') {
      const rows: IAudio[] = this.selectedAudios.concat();
      const totalCount: number = rows.length;
      const mediaList: MediaList<IAudio> = new MediaList(rows, totalCount);
      const mediaRequest: MediaRequest = new MediaRequest('main', '0');
      const audioList = this.audioWorker.createList(mediaList, mediaRequest, '0');

      this.audioWorker.changeCurrentList(audioList);
      this.audioWorker.playSong(audio);

    } else {
      
      this.viewService.changeSong(audio);
    }
  }

  select(audio: IAudio): void {
    const idx: number = this.selectedAudios.findIndex(a => a._id === audio._id);

    const action: 'add' | 'remove' = !~idx ? 'add' : 'remove';

    if(action === 'add') {
      this.selectedAudios.push(audio);
      this.selectedIds.push(audio._id);

    } else {

      this.selectedAudios.splice(idx, 1);
      this.selectedIds.splice(idx, 1);

    }

    this.viewService.updateMedia(audio, action);
  }

  add(audio: IAudio): void {
    this.viewService.addAudioToPlaylist(audio);
  }

  remove(audio: IAudio): void {
    this.viewService.removeAudioFromPlaylist(audio);
  }

  expand(): void {
    this.viewService.expandAudio();
  }
  
  onSubmit(): void {
    const { value } = this.form;

    const method: 'create' | 'edit' = this.modeType === 'playlist-create' ? 'create' : 'edit';
    
    this.viewService[method](value, this.file); 
  }

}
