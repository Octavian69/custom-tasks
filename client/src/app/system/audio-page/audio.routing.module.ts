import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AudioPageComponent } from './audio-page/audio-page.component';
import { AudioSongModalComponent } from './audio-song-modal/audio-song-modal.component';
import { AudioPlaylistModalComponent } from './audio-playlist-modal/audio-playlist-modal.component';
import { AudioPlaylistDetailComponent } from './audio-playlist-detail/audio-playlist-detail.component';

const routes: Routes = [
    {path: '', component: AudioPageComponent, children: [
        { path: 'audio-edit/:id', component: AudioSongModalComponent },
        { path: 'playlist-edit/:id', component: AudioPlaylistModalComponent },
        { path: 'playlist-detail/:id', component: AudioPlaylistDetailComponent }
    ]}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AudioRoutingModule {}
