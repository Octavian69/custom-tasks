import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { AudioService } from 'src/app/services/audio.service';
import { AudioWorkService } from 'src/app/services/audio.work.service';
import { AudioViewService } from 'src/app/services/audio.view.service';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';
import { AudioFlags } from 'src/app/models/flags/AudioFlags';
import { unsubscriber, deepCopy, isExpand } from 'src/app/shared/handlers/handlers';
import { MediaRequest } from 'src/app/models/MediaRequest';
import { MediaList } from 'src/app/models/MediaList';
import { IAudio } from 'src/app/interfaces/IAudio';
import { ICurrentMediaList } from 'src/app/interfaces/ICurrentMediaList';
import { LIMIT } from 'src/app/namespaces/Namespaces';
import { filterUnique } from 'src/app/shared/handlers/media.handlers';
import { PaginationResponse, ResponseAction } from 'src/app/types/request-response.types';
import { AudioNavIcons } from 'src/app/types/audio.types';
import { UpdateMediaList } from 'src/app/types/media.types';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe({ arrayName: 'subs$' })
@Component({
  selector: 'cst-audio-main-list',
  templateUrl: './audio-main-list.component.html',
  styleUrls: ['./audio-main-list.component.scss']
})
export class AudioMainListComponent implements OnInit, OnDestroy {
  playlistId: string;
  watchPlaylistId: string; // ID нужен серверу для просмотра в нем песен,которые уже имеются в плейлисте

  mainLoading: boolean = false;
  searchLoading: boolean = false;
   
  icons: AudioNavIcons;
  mainRequest: MediaRequest;
  searchRequest: MediaRequest;
  main: MediaList<IAudio>;
  search: MediaList<IAudio>;
  create$: Observable<IAudio> = this.viewService.getStream('create$');

  searchControl: FormControl = new FormControl(null, [], []);

  flags: AudioFlags = this.viewService.getFlagObject();
  subs$: Subscription[] = [];

  audioLimit: number = LIMIT.AUDIO;

  @Input('showClass') showClass: string;
  @Input('modeType') modeType: 'playlist-create' | 'playlist-edit' | 'self' | 'default' = "default";
  @Input('selectedIds') selectedIds: string [] = [];
  @Input('disabledSelect') disabledSelect: boolean = false;

  @Output('select') _select = new EventEmitter<IAudio>();
  @Output('add') _add = new EventEmitter<IAudio>();
  @Output('remove') _remove = new EventEmitter<IAudio>();
  @Output('multiAddition') _multiAddition = new EventEmitter<IAudio>();

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private audioService: AudioService,
    public viewService: AudioViewService,
    public audioWorker: AudioWorkService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.viewService.destroyOwn();
    this._multiAddition.emit(null);
  }

  init(): void {
    this.initIds();
    this.initIcons();
    this.initRequests();
    this.initSearchSub();
    this.initCreateSub();
    this.initOwnListSub();
    this.initList();
  }

  initIds(): void {
    // Основной список композиций при созданнии профиля получает _id самого профиля.
    const { id } = this.activeRoute.snapshot.params;
    this.playlistId = this.modeType !== 'default' ? this.viewService.getOwnId() : id;

    this.watchPlaylistId = this.modeType === 'playlist-edit' ? id : this.viewService.getOwnId();
  }

  initIcons(): void {
    const type: 'own' | 'other' = this.modeType !== 'default' ? 'own' : 'other';
    
    const page = this.modeType.includes('playlist') ? this.modeType : 'default';
    
    this.icons = this.audioWorker.getNavIcons(type, page);
  }

  initRequests() {
    const state = { watchId: this.watchPlaylistId };

    this.mainRequest = new MediaRequest('main', this.playlistId, this.audioLimit, state);
    this.searchRequest = new MediaRequest('search', this.playlistId, this.audioLimit, state);
      
  }


  initSearchSub(): void {
    const sub$: Subscription = this.searchControl.valueChanges
    .pipe(debounceTime(400))
    .subscribe(
      (value: string) => {

        if(value) {
          this.mainRequest.searchString = value;
          this.searchRequest.searchString = value;

          this.fetch(this.searchRequest);
          this.fetch(this.mainRequest);

        } else {
          this.initRequests();
          this.initList();
          this.search = null;
        }  
      }
    )

    this.subs$.push(sub$);
  }

  initCreateSub(): void {
    const sub$: Subscription = this.create$.subscribe((audio: IAudio) => {
      this.main.totalCount++;
      this.main.rows.unshift(audio);
    })

    this.subs$.push(sub$);
  }

  initOwnListSub(): void {
    const isInit: boolean = Object.is(this.modeType, 'self');

    if(isInit) {
      const sub$: Subscription = this.viewService.getStream('ownList$').subscribe(
        (update: UpdateMediaList<IAudio>) => {
          
          const { action, media } = update;

          switch(action) {
            case 'add': {
              this.main.rows.unshift(media)
              this.main.totalCount++;

              break;
            }
            case 'remove': {
              const idx: number = this.main.rows.findIndex(a => a._id === media._id);

              this.main.rows.splice(idx, 1);
              this.main.totalCount--;

              break;
            }
            case 'edit': {
              const idx: number = this.main.rows.findIndex(a => a._id === media._id);

              this.main.rows[idx] = media;

              break;
            }
          }
        }
      );

      this.subs$.push(sub$);
    }
  }

  initList(): void {
    this.fetch(this.mainRequest);
  }

  fetch(mediaRequest: MediaRequest, action: ResponseAction = 'replace') {
    const { listType } = mediaRequest;
    const loadFlagName = `${listType}Loading` as 'mainLoading' | 'searchLoading';

    if(this[loadFlagName]) return

    const listName = this.getListNameByType(listType);
    const mediaList: MediaList<IAudio> = this[listName];

    this[loadFlagName] = true;
    
    const next = (response: PaginationResponse<IAudio>) => {

        if(action === 'replace') {
          this[listName] = response;
        } else {
          mediaList.rows = this[listName].rows.concat(response.rows);
          mediaList.totalCount = response.totalCount;
        }

        this[loadFlagName] = false;
    }

    this.audioService
        .fetch(this.playlistId, mediaRequest)
        .pipe(
            map((response: MediaList<IAudio>) => {
                return filterUnique(mediaList, response, action)
            })
        )
        .subscribe({ next });
  }



  getShowClass(): ICssStyles {
    return {
      [this.showClass]: true
    }
  }

  isRequest(): boolean {
    return this.mainLoading || this.searchLoading;
  }
 
  expandList(type: 'main' | 'search'): void {
    const listName = `${type}` as 'main' | 'search';
    const reqName = `${type}Request` as 'mainRequest' | 'searchRequest';

    const list: MediaList<IAudio> = this[listName];
    const req: MediaRequest = this[reqName];

    if(list) {
      const expand: boolean = isExpand(list);
      
      if(expand) {
        req.page.skip++;
        this.fetch(req, 'expand');
      }
    }
  }

  expand(): void {
    this.expandList('main');
    this.expandList('search');
  }

  changeSong(audio: IAudio, type: 'main' | 'search'): void {
      const listName = this.getListNameByType(type);
      const reqName = this.getReqNameByType(type);
      const candidateList: MediaList<IAudio> =  deepCopy(this[listName]);
      const candidateRequest: MediaRequest = deepCopy(this[reqName]);
      const { playlistId } = candidateRequest;

      const audioList: ICurrentMediaList<IAudio> = this.audioWorker.createList(candidateList, candidateRequest, playlistId);

      this.audioWorker.changeCurrentList(audioList);
      this.audioWorker.playSong(audio);
  }

  select(audio: IAudio): void {
    this._select.emit(audio);
  }

  edit(audio: IAudio): void {
    this.router.navigate(['audio-edit', audio._id], { relativeTo: this.activeRoute });
  }

  add(audio: IAudio): void {
    const isOwn: boolean = this.viewService.isOwn(this.playlistId);

    const isPlaylist: boolean = this.modeType.includes('playlist');

    if(isPlaylist) {
      this._add.emit(audio);
    } else {
      this.viewService.addAudioInPlaylist(audio, isOwn);
    }
  }

  remove(audio: IAudio): void {
    const isOwn: boolean = this.viewService.isOwn(this.playlistId);
    const isPlaylist: boolean = this.modeType.includes('playlist');

    if(isPlaylist) {
      this._remove.emit(audio);
    } else {
      this.viewService.removeAudioFromPlaylist(audio, isOwn);
    }
  }

  multiAddition(audio: IAudio): void {
    this._multiAddition.emit(audio);
  }

  getListNameByType(type: 'main' | 'search'): 'main' | 'search' {
      return type === 'main' ? 'main' : 'search';
  }

  getReqNameByType(type: 'main' | 'search'): 'mainRequest' | 'searchRequest' {
      return type === 'main' ? 'mainRequest' : 'searchRequest';
  }

  trackByFn(index: number, audio: IAudio): string {
    return audio._id;
  }
}
