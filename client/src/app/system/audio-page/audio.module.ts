import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { AudioRoutingModule } from './audio.routing.module';
import { AudioViewService } from 'src/app/services/audio.view.service';
import { AudioPageComponent } from './audio-page/audio-page.component';
import { AudioMainListComponent } from './audio-main-list/audio-main-list.component';
import { AudioNavComponent } from './audio-nav/audio-nav.component';
import { AudioPlaylistsComponent } from './audio-playlists/audio-playlists.component';
import { AudioSongModalComponent } from './audio-song-modal/audio-song-modal.component';
import { AudioPlaybackComponent } from './audio-playback/audio-playback.component';
import { AudioPlaylistModalComponent } from './audio-playlist-modal/audio-playlist-modal.component';
import { AudioPlaylistsViewService } from 'src/app/services/audio-playlists.view.service';
import { AudioPlaylistComponent } from './audio-playlist/audio-playlist.component';
import { AudioPlaylistDetailComponent } from './audio-playlist-detail/audio-playlist-detail.component';
import { AudioMultiAdditionComponent } from './audio-multi-addition/audio-multi-addition.component';
import { AudioMultiAdditionPlaylistComponent } from './audio-multi-addition-playlist/audio-multi-addition-playlist.component';

@NgModule({
    declarations: [
        AudioPageComponent, 
        AudioMainListComponent, 
        AudioNavComponent, 
        AudioPlaylistsComponent, 
        AudioSongModalComponent, 
        AudioPlaybackComponent,
        AudioPlaylistModalComponent,
        AudioPlaylistComponent,
        AudioPlaylistDetailComponent,
        AudioMultiAdditionComponent,
        AudioMultiAdditionPlaylistComponent
    ],
    imports: [
        SharedModule,
        AudioRoutingModule
    ],
    providers: [ 
        AudioViewService,
        AudioPlaylistsViewService
    ]
})
export class AudioModule {}