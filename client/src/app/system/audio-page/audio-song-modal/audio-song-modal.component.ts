import { Component, OnInit, HostBinding, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrService, ActiveToast } from 'ngx-toastr';
import { checkedFile } from 'src/app/shared/handlers/handlers';
import { scaleShow, topShow } from 'src/app/shared/animations/animation';
import { AudioViewService } from 'src/app/services/audio.view.service';
import { Observable } from 'rxjs';
import { IAudio } from 'src/app/interfaces/IAudio';
import { tap } from 'rxjs/operators';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ListOption } from 'src/app/types/shared.types';
import { CheckedFile, Length, FormObjectErrors } from 'src/app/types/validation.types';
import { CreateAudioCandidate } from 'src/app/types/audio.types';

@Component({
  selector: 'cst-audio-song-modal',
  templateUrl: './audio-song-modal.component.html',
  styleUrls: ['./audio-song-modal.component.scss'],
  animations: [scaleShow, topShow]
})
export class AudioSongModalComponent implements OnInit, OnDestroy {

  @HostBinding('@scaleShow') scaleShow;
  
  audio$: Observable<IAudio> = this.initAudioSub();
  modeType: 'create' | 'edit';
  form: FormGroup;
  isInitEditSettings: boolean = false;
  file: File;
  genres: ListOption[] = this.viewService.getDataByArrayKeys(['audio', 'genres']);
  errors: FormObjectErrors;
  lengths: Length;


  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private activeRoute: ActivatedRoute,
    private location: Location,
    private viewService: AudioViewService,
  ) { }

  ngOnInit() {
    const { id } = this.activeRoute.snapshot.params;
    this.initForm();
    this.viewService.init(id);
  }

  ngOnDestroy(): void {
    this.viewService.audio$.next(null);
  }

  initAudioSub(): Observable<IAudio> {
    return (this.viewService.getStream('audio$') as Observable<IAudio>).pipe(
      tap((audio: IAudio) => {
          if(!audio) return;

          const modeType = !audio._id ? 'create' : 'edit';

          if(modeType === 'edit') {
            this.initEditSettings();
            this.form.patchValue(audio);
          }

          this.modeType = modeType;
        }
    ))
  }

  initEditSettings(): void {
    this.lengths = this.viewService.getValidatorLength('AUDIO');
    this.errors = this.viewService.completeDbErrors(['audio', 'formErrors', 'Audio'], 'Audio');
    
    this.createControl('Artist');
    this.createControl('Title');
  }

  createControl(controlName: string): void {
    this.form.addControl(controlName, new FormControl(null, [Validators.required]));
  }

  initForm() {
    this.form = this.fb.group({
      Genre: [null, [], []]
    })
  }

  getDisabled(): boolean {
    const isRequest: boolean = <boolean>this.viewService.getFlag('isRequest');

    if(this.modeType === 'create') {
      return !this.file || isRequest;
    } else {
      return this.form.invalid || isRequest;
    }
  }

  hide(): void {
    this.location.back();
  }


  changeSong(e: Event): ActiveToast<any> | void {
    
    this.reset();

    const file: File = (<HTMLInputElement>e.target).files[0];
    const checked: CheckedFile = checkedFile('AUDIO', file);
    

    if(!checked.isValid) {
      const errors: string = checked.errors.toString();
      return this.toastr.warning(errors, 'Внимание!');
    }
    
    
    this.file = file;
  }

  resetControl(e: Event, controlName: string): void {
    e.stopPropagation();

    this.form.get(controlName).reset();
  }

  reset(): void {
    this.form.reset();
    this.file = null;
  }

  onSubmit(): void {
    if(this.modeType === 'edit' && this.form.untouched) {
      this.toastr.info('Поля формы не изменялись.', 'Информация');
    }

    const { value: audio } = this.form;
    

    if(this.modeType === 'create') {
      const candidate: CreateAudioCandidate = { audio, file: this.file };
      this.viewService.createAudio(candidate);
    } else {
      this.viewService.editAudio(audio);
    }
  }
}
