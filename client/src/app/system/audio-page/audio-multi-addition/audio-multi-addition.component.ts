import { Component, Output, EventEmitter, Input } from '@angular/core';
import { IAudio } from 'src/app/interfaces/IAudio';
import { AudioPlaylistsService } from 'src/app/services/audio-playlists.service';
import { AudioService } from 'src/app/services/audio.service';
import { Page } from 'src/app/models/Page';
import { MediaList } from 'src/app/models/MediaList';
import { BehaviorSubject, Observable } from 'rxjs';
import { isExpand } from 'src/app/shared/handlers/handlers';
import { ICreatedPlaylist } from 'src/app/interfaces/ICreatorAudioList';
import { parentAnimation } from 'src/app/shared/animations/animation';
import { SipmleAction } from 'src/app/types/shared.types';
import { MessageResponse, ResponseAction } from 'src/app/types/request-response.types';


@Component({
  selector: 'cst-audio-multi-addition',
  templateUrl: './audio-multi-addition.component.html',
  styleUrls: ['./audio-multi-addition.component.scss'],
  animations: [parentAnimation]
})
export class AudioMultiAdditionComponent {
  
  playlists$: BehaviorSubject<MediaList<ICreatedPlaylist>> = new BehaviorSubject(null);
  
  activeAudio: IAudio;
  isRequest: boolean = false;
  requestPlaylists: string[] = [];
  page: Page = new Page(0, 10);
  

  @Input('audio') set audio(value: IAudio) {
    if(!value) return
    this.activeAudio = value;
    this.fetch('replace');
  }

  @Output('hide') _hide = new EventEmitter<void>();

  constructor(
    private audioService: AudioService,
    private playlistService: AudioPlaylistsService
  ) { }

  fetch(action: ResponseAction): void {

    this.isRequest = true
    
    const next = (response: MediaList<ICreatedPlaylist>) => {
      if(action === 'replace') {
        this.playlists$.next(response);
      } else {
        const currentList: MediaList<ICreatedPlaylist> = this.playlists$.getValue();
        const { rows, totalCount } = response;

        currentList.rows = currentList.rows.concat(rows);
        currentList.totalCount = totalCount;

        this.playlists$.next(currentList);
      }

      this.isRequest = false;
    }

   this.playlistService.getCreatedByUser(this.activeAudio, this.page).subscribe({ next });
  }

  isCurentRequest(playllistId: string): boolean {
    
    return this.requestPlaylists.includes(playllistId);
  }

  expand(): void {
    const mediaList: MediaList<ICreatedPlaylist> = this.playlists$.getValue();

    const expand: boolean = isExpand(mediaList);

    if(expand) {
      this.page.skip++;
      this.fetch('expand');
    }
  }

  hide(): void {
    this._hide.emit();
  }

  mutate(sipmleAction: SipmleAction<string>): void {
    const { action, payload: playlistId } = sipmleAction;

    this.requestPlaylists.push(playlistId);

    const next = () => {
      const playlists: MediaList<ICreatedPlaylist> = this.playlists$.getValue();
      const plIdx: number = playlists.rows.findIndex(p => p._id === playlistId);
      const reqIdx: number = this.requestPlaylists.findIndex(p => p === playlistId);
      const { IsHave } = playlists.rows[plIdx];

      playlists.rows[plIdx].IsHave = !IsHave;
      this.requestPlaylists.splice(reqIdx, 1);

      this.playlists$.next(playlists);
    }

    const reqName: 'addAudioInPlaylist' | 'removeAudioFromPlaylist' = action === 'add' ? 'addAudioInPlaylist' : 'removeAudioFromPlaylist';

    (this.audioService[reqName](playlistId, this.activeAudio) as Observable<IAudio | MessageResponse>)
                                .subscribe({ next });
  }

}
