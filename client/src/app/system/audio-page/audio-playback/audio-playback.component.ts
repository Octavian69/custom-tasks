import { Component, HostBinding, Input } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';
import { AudioWorkService } from 'src/app/services/audio.work.service';
import { fade } from 'src/app/shared/animations/animation';
import { IAudio } from 'src/app/interfaces/IAudio';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';

@Component({
  selector: 'cst-audio-playback',
  templateUrl: './audio-playback.component.html',
  styleUrls: ['./audio-playback.component.scss'],
  animations: [fade]
})
export class AudioPlaybackComponent {

  elem$: HTMLAudioElement = this.audioWorker.getField('elem$');

  @HostBinding('@fade') fade = true;
  @Input('audio') audio: IAudio;
  
  constructor(
    public audioWorker: AudioWorkService
  ) { }

 
  formatVolumeLabel = (value: number) => {
    return this.audioWorker.getVolumeValue();
  }

  getIconClass(): ICssStyles {
    const icon: string = this.audioWorker.getVolumeIcon();

    return {
      'muted': icon === 'volume_off',
      'low': icon === 'volume_down',
      'medium': icon === 'volume_up'
    }
  }

  repeat(): void {
    this.audioWorker.repeat();
  }

  setVolume(): void {
    const { volume } = this.elem$;

    const value: number = !volume ? 1 : 0;

    this.audioWorker.setVolume(value);
    
  }

  setRangeVolume(event: MatSliderChange): void {
    const { value } = event;
    const volume: number = value / 100;
    

    this.audioWorker.setVolume(volume);
  }


  playback(side: 'prev' | 'next'): void {
    const isExpandRequest: boolean = <boolean>this.audioWorker.getFlag('isExpandRequest');

    if(!isExpandRequest) {
      this.audioWorker.shiftAudioList(side);
    }
  }
}
