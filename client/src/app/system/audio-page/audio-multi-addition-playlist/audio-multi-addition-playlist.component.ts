import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ICreatedPlaylist } from 'src/app/interfaces/ICreatorAudioList';
import { SipmleAction } from 'src/app/types/shared.types';

@Component({
  selector: 'cst-audio-multi-addition-playlist',
  templateUrl: './audio-multi-addition-playlist.component.html',
  styleUrls: ['./audio-multi-addition-playlist.component.scss']
})
export class AudioMultiAdditionPlaylistComponent{

  @Input('playlist') playlist: ICreatedPlaylist;
  @Input('request') request: boolean;
  @Output('mutate') _mutate = new EventEmitter<SipmleAction<string>>();


  mutate(): void {
    const action: 'add' | 'remove' = this.playlist.IsHave ? 'remove' : 'add';
    const payload: string = this.playlist._id;

    this._mutate.emit({ action, payload });

  }
}
