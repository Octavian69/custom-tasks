import { Component, OnInit, OnDestroy } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { AudioViewService } from 'src/app/services/audio.view.service';
import { AudioFlags } from 'src/app/models/flags/AudioFlags';
import { fade } from 'src/app/shared/animations/animation';
import { AudioWorkService } from 'src/app/services/audio.work.service';
import { AudioPlaylistsFlags } from 'src/app/models/flags/AudioPlaylistsFlags';
import { AudioPlaylistsViewService } from 'src/app/services/audio-playlists.view.service';
import { Router, ActivatedRoute } from '@angular/router';
import { IAudio } from 'src/app/interfaces/IAudio';
import { PageLink } from 'src/app/types/request-response.types';

@Component({
  selector: 'cst-audio-page',
  templateUrl: './audio-page.component.html',
  styleUrls: ['./audio-page.component.scss'],
  animations: [fade]
})
export class AudioPageComponent implements OnInit, OnDestroy {

  userId: string;
  
  activeMultiAudio: IAudio;
  audioFlags: AudioFlags = this.viewService.getFlagObject();
  plFlags: AudioPlaylistsFlags = this.plViewService.getFlagObject();
  navList: PageLink[] = this.viewService.getDataByArrayKeys(['audio', 'navList']);
  link: PageLink;
  showClass: 'left-show' | 'right-show' = 'right-show';

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private storage: StorageService,
    public viewService: AudioViewService,
    public plViewService: AudioPlaylistsViewService,
    public audioWorker: AudioWorkService
  ) { }

  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.storage.removeItem('sessionStorage', 'audioPage');
    this.viewService.destroy();
    this.plViewService.destroy();
  }

  init(): void {
    this.userId = this.activeRoute.snapshot.params.id;
    this.initCurrentPage();
  }

  isOwn(): boolean {
    return this.viewService.isOwn(this.userId);
  }
  
  initCurrentPage(): void {
    const audioPage: PageLink = this.storage.getItem('sessionStorage', 'audioPage') || this.navList[0];

    this.setPage(audioPage);
  }

  getMainListModeType(): 'self' | 'default' {
    const { id } = this.activeRoute.snapshot.params;
    const type: 'self' | 'default' = this.viewService.isOwn(id) ? 'self' : 'default';

    return type;
  }

  setPage(link: PageLink): void {

    this.showClass = this.link && link.id > this.link.id ? 'right-show': 'left-show';
    this.link = link;
    this.viewService.setFlag('currentPage', link.page);

  }


  create(type: 'playlist' | 'song'): void {
    const path = `${type}-edit` as 'audio-edit' | 'playlist-edit';

    this.router.navigate([path, '0'], { relativeTo: this.activeRoute });                                           
  }

  multiAddition(audio: IAudio): void {
    this.activeMultiAudio = audio;
  }

  resetMultiAddition() {
    this.activeMultiAudio = null;
  }
}
