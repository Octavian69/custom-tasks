import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';
import { fade, changeTop } from '../../../shared/animations/animation';
import { Audio } from 'src/app/models/Audio';
import { durationConverterFn } from '../../../shared/handlers/handlers';
import { IAudio } from 'src/app/interfaces/IAudio';
import { ICssStyles } from 'src/app/interfaces/ICssStyles';

@Component({
  selector: 'cst-audio-song',
  templateUrl: './audio-song.component.html',
  styleUrls: ['./audio-song.component.scss'],
  animations: [fade, changeTop(0.3, 100)]
})
export class AudioSongComponent implements OnInit {
  
  addStatus: boolean;
  _currentTime: number = 0;
  _isCurrent: boolean = false;

  @Input('currentTime') set currentTime(value: number) {
    if(this._isCurrent) {
      this._currentTime = value;
    } 
  } 

  @Input('isCurrent') set isCurrent(value: boolean) {
    this._isCurrent = value;
    this._currentTime = 0;
  };

  @Input('audio') audio: IAudio;
  @Input('paused') paused: boolean = true;
  @Input('isDisabledPick') isDisabledPick: boolean = false;
  @Input('modeType') modeType: 'list' | 'playback' = 'list';
  @Input('isAddInOwnList') isAddInOwnList: boolean = false;
  @Input('checked') checked: boolean = false;
  @Input('navIcons') navIcons: string[] = [];
  @Input('disabledSelect') disabledSelect: boolean = false;
  @Input('isMutateRequest') isMutateRequest: boolean = false;
  
  
  @Output('changeSong') _changeSong = new EventEmitter<Audio>();
  @Output('playback') playback = new EventEmitter<void>();
  @Output('select') _select = new EventEmitter<IAudio>();
  @Output('range') _range = new EventEmitter<number>();
  @Output('multiAddition') _multiAddition = new EventEmitter<IAudio>();
  @Output('add') _add = new EventEmitter<IAudio>();
  @Output('edit') _edit = new EventEmitter<IAudio>();
  @Output('remove') _remove = new EventEmitter<IAudio>();

  ngOnInit() {
    this.addStatus = this.audio.IsHave;
  }


  changeSong(): void {
    this._currentTime = 0;
    this._changeSong.emit(this.audio);
  }

  getDuration(): number {
    return this._isCurrent ? this._currentTime : this.audio.Duration;
  }

  getRangeValue(): number {
    const value: number = (this._currentTime * 100) / this.audio.Duration;

    return value;
  }

  getAddIconStyles(): ICssStyles {
    return {
      'add': this.addStatus,
      'remove': !this.addStatus
    }
  }

  getAddIcon(): string {
    return this.addStatus ? 'check' : 'add';
  }

  getMultiListTypes(): string[] {
    return ['Default', 'Created']
  }

  getMutateTooltip(): string {
      const action: string = this.addStatus ? 'Удалить ' : ' Добавить ';

      return action + this.getTooltipSuffix();
    
  }

  getTooltipSuffix(): string {
    if(this.isAddInOwnList) {
      return this.addStatus ? 'из своего списка' : 'в свой список'
    }

    return '';
  }

  isShowNaviate() {
    const icons: string[] = ['edit', 'add-multi'];
    
    return this.navIcons.some(icon => icons.includes(icon));
  }

  formatRangeLabel = (value: number) => {
    return durationConverterFn(this.getDuration()); 
  }

  action(): void {
    if(this._isCurrent) {
      this.playback.emit()
    } else {
      this.changeSong();
    }
  }

  range(event: MatSliderChange): void {
    const { value } = event;

    this._range.emit(value);
  }

  select(): void {
    this._select.emit(this.audio);
  }

  edit(): void {
    this._edit.emit(this.audio);
  }

  mutate(): void {
    const action = this.addStatus ? '_remove' : '_add';
    
    this.addStatus = !this.addStatus;

    this[action].emit(this.audio);
  }

  multiAddition(): void {
    this._multiAddition.emit(this.audio);
  }
}
