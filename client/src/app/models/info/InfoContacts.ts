import { ListOption } from 'src/app/types/shared.types';

export class InfoContacts {
    constructor(
        public Country: ListOption = null,
        public City: ListOption = null,
        public Address: string = null,
        public Phone: string = null,
        public AdditionalPhone: string = null,
        public Skype: string = null,
        public WebSite: string = null,
    ) {}
}