import * as nanoid from 'nanoid';
import { SHARED } from 'src/app/namespaces/Namespaces';
import { ListOption } from 'src/app/types/shared.types';

export class InfoCareer {
    constructor(
        public CompanyName: string = null,
        public Country: ListOption = null,
        public City: ListOption = null,
        public StartDate: Date = null,
        public EndDate: Date = null,
        public Position: string = null,
        public _id: string = nanoid(SHARED.NANOID_SALT)
    ){}
}