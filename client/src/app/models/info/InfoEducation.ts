import * as nanoid from 'nanoid';
import { SHARED } from 'src/app/namespaces/Namespaces';
import { ListOption } from 'src/app/types/shared.types';

export class InfoEducation {
    constructor(
        public Country: ListOption = null,
        public City: ListOption = null,
        public School: ListOption = null,
        public StartDate: Date = null,
        public EndDate: Date = null,
        public _id: string = nanoid(SHARED.NANOID_SALT)
    ){}
}