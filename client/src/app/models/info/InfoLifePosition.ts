export class InfoLifePosition {
    constructor(
        public PoliticalPreference: number = null,
        public Outlook: string = null,
        public MainInLife: number = null,
        public MainInPeople: number = null,
        public AttitudeToSmoking: number = null,
        public AttitudeToAlcohol: number = null,
        public Inspiration: string = null
    ) {}
}