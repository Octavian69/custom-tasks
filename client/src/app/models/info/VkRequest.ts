import { environment } from '@env/environment';
import { HttpParams } from '@angular/common/http';

export class VkRequest {

    url: string;    // `${vkDataBaseUrl}.getCountries?need_all=1&v=5.101&access_token=${vkAPIToken}`, 'callback'
    
    constructor(method: string, params: any) {
        this.completeUrl(method, params);
    }

    completeUrl(method: string, params: any) {
        const { vkAPIToken, vkDataBaseUrl, vkApiVersion } = environment;
        const fromObject = {
            ...params,
            v: vkApiVersion,
            access_token: vkAPIToken
        };

        const queryParams: string = new HttpParams({fromObject}).toString();

        this.url = `${vkDataBaseUrl}.${method}?${queryParams}`
    }
}