import { Page } from '../Page';
import { PictureAlbumType, PictureType } from 'src/app/types/picture.types';

export class SliderManager {
   constructor(
       public page: Page,
       public currentIndex: number, // индекс картинки в медиалисте запроса
       public pictureIndex: number, //индекс картинки в общем списке
       public albumId: string,
       public albumType: PictureAlbumType | 'Main',
       public albumTitle: string,
       public totalCount: number,
       public pictureType: PictureType,
       public IsOwnAlbum: boolean,
       public heigth?: number,
       public width?: number
   ){}
}