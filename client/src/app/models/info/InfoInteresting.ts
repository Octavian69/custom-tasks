export class InfoInteresting {

    constructor(
        public Activity: string = null,
        public Interesting: string = null,
        public Music: string = null,
        public Films: string = null,
        public Games: string = null,
        public Quotes: string = null,
        public About: string = null
    ){}
}