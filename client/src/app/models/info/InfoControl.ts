import { FormControlType } from 'src/app/types/form.types';

export class InfoControl {
    type: FormControlType;
    title: string;
    controlName: string;
    multiple:  boolean;
    async: boolean;
    isCanRequest: boolean;
    search: boolean;
    selectData: any[];

    constructor(options) {
        this.setOptions(options);
    }

    setOptions(options) {
        Object.keys(options).forEach(key => this[key] = JSON.parse(JSON.stringify(options[key])));
    }
}