export class InfoMain {
    constructor(
        public Name: string = null,
        public LastName: string = null,
        public Gender: number = 1,
        public Birthday: Date = null,
        public City: string = null,
        public Language: number[] = []
    ) {}
}