export class VideoPlaylistsFlags {
    
    isCanMutateDetail: boolean = false; 
    isCanMutateList: boolean = false;
    isMutate: boolean = false;
    isCreatePlaylistModal: boolean = false;
    isRequest: boolean = false;
    isCreate: boolean = false;
    isCreatePlaylist: boolean = false;
    isEdit: boolean = false;
    isLoadingMain: boolean = false;
    isLoadingSearch: boolean = false;
    isUpdateLogo: boolean = false;
    mutateRequestArray: string[] = [];

    constructor(){}
}