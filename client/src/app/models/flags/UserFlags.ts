export class UserFlags {
    isFetch: boolean = false;
    isEdit: boolean = false;
    isCreateAvatar: boolean = false;
    isUpdateAvatar: boolean = false;
    isShowCreateAvatar: boolean = false;

    constructor() {}
}