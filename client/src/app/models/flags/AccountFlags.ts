export class AccountFlags {
    public isLoad: boolean = false;
    public isFetchUser: boolean = false;
    public isShowStatus: boolean = false;
    public isShowDetails: boolean = false;
    public isSlidesRequest: boolean = false;
    public isShowCreateAvatar: boolean = false;
    public isFriendsRequest: boolean = false;
    public isInitSlider: boolean = false;
    public isEditInfo: boolean = false;
    public isShowOnlineModal: boolean = false;
    public isFetchOnlineUsers: boolean = false;

    constructor() {}
}