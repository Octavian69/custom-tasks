export class VideoWorkFlags {
    isCanMutate: boolean = false;
    isMutate: boolean = false;
    isRequest: boolean = false;
    isEditRequest: boolean = false;
    isExpand: boolean = false;
    isLoadComments: boolean = false;
    isShowAddInOwn: boolean = false; 
    isMultiLoadRequest: boolean = false;
    isShowMultiMutate: boolean = false;
    multiRequestArray: string[] = []

    constructor() {}
}