export class AudioWorkFlags {
    public isRepeat: boolean = false;
    public isExpandRequest: boolean = false;
    public requestAudioId: string = null;

    constructor() {}
}