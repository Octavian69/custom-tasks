export class MessageFlags {
    public isFetch: boolean = false
    public isSendRequest: boolean = false;
    public isEditRequest: boolean = false;
    public isRemoveRequest: boolean = false;

    constructor() {}
}