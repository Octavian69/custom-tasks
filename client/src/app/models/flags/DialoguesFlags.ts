export class DialoguesFlags {
    public isFetchDialogues: boolean = false;
    public isFetchDialogue: boolean = false;
    public isFetchFriends: boolean = false;
    public isFetchSearch: boolean = false;
    public isCreateDialogue: boolean = false;
    public isShowCreateDiscussion: boolean = false;
    public isNavigateToDetail: boolean = false;

    constructor(){}
}