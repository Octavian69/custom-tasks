export class PictureFlags {

    isFetch: boolean = false;
    isShowCreateModal: boolean = false;
    isShowReplaceModal: boolean = false;
    isShowConfirm: boolean = false;
    isMutateRequest: boolean = false;
    multiRequestArray: string[] = [];
    
    constructor() {}
}