export class AudioPlaylistsFlags {
    public modeType: 'playlist-create' | 'playlist-edit' = null;
    public isShowAddList: boolean = false;
    public isRequest: boolean = false;
    public isEditRequest: boolean = false;
    public isMutateRequest: string = null;
    public isExpandPlaylists: boolean = false;
    public isExpandAudio: boolean = false;

    constructor(){}
}