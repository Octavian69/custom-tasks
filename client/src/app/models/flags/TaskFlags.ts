export class TaskFlags {
    public isShowFilter: boolean = false;
    public isShowConfirm: boolean = false;
    public isActiveFilter: boolean = false;
    public isRequest: boolean = false;

    constructor() {}
}