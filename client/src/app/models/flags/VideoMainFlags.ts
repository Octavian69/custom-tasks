export class VideoMainFlags {
    
    isCanMuatate: boolean = false;
    isCreateVideoModal: boolean = false;
    isRequest: boolean = false;
    isCreate: boolean = false;
    isLoadingMain: boolean = false;
    isLoadingSearch: boolean = false;
    mutateRequestArray: string[] = [];

    constructor() {}
}