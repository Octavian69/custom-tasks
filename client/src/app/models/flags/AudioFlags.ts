export class AudioFlags {
    public isLoadPlaylist: boolean = false;
    public currentPage: "own" | "playlist" = 'own';
    public isRequest: boolean = false;
    public isShowAddAudioList: boolean = false;
    public isMediaRequest: boolean = false;
    public isDisabledPick: boolean = false;
    public isAddInPlaylist: boolean = false;

    constructor() {}
}