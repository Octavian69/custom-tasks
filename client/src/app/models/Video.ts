import { IVideo } from '../interfaces/IVideo';

export class Video implements IVideo {
    constructor(
        public Title: string,
        public Description: string,
        public Playlist: string,
        public User: string,
        public Path: string = null,
        public Poster: string = null,
        public Likes: string[] = [],
        public Duration: number = null,
        public CreatorName: string = null,
        public AmountAdditions: number = 1,
        public Created: Date = new Date(),
        public Updated: Date = new Date(),
        public _id?: string,
    ) {}
}