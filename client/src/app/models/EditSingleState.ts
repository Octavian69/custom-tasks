import { FormGroup } from '@angular/forms';
import { InfoControl } from './info/InfoControl';

export class EditSingleState {
    constructor(
        public form: FormGroup = null,
        public configuration: InfoControl[] = null,
        public part: string = null
    ) {}
}