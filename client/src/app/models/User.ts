export class User {
    constructor(
        public Login: string,
        public Name: string,
        public Password: string,
        public _id?: string
    ) {}
}