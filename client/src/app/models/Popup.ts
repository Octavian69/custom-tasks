import { AnyOption } from '../types/shared.types';

export class Popup {
    constructor(
        public modeType: string,
        public title: string,
        public actionTitle: string,
        public message: string,
        public _id: string,
        public state?: AnyOption
    ){}
}