import { IAudio } from '../interfaces/IAudio';

export class Audio implements IAudio {
    constructor(
        public Genre: number = null,
        public Title: string = null,
        public Artist: string = null,
        public Path: string = null,
        public Playlist: string = null,
        public Duration: number = null,
        public Logo: string = null,
        public Created: Date = new Date(),
        public Type: 'Created' | 'Added' = 'Created',
        public User?: string,
        public _id?: string
    ) {}
}