import { Page } from './Page';
import { LIMIT } from '../namespaces/Namespaces';
import { CommentRefType } from '../types/comment.types';

export class CommentRequest {
    page: Page = new Page;
    
    constructor(
        public refId: string,
        public Type: CommentRefType,
        limit: number = LIMIT.COMMENTS
    ) {
        this.page.limit = limit;
    }
}