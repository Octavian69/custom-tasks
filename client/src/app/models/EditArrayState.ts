import { FormGroup } from '@angular/forms';
import { InfoControl } from './info/InfoControl';

export class EditArrayState {

    constructor(
        public forms: FormGroup[] = [],
        public configuration: InfoControl[][] = null,
        public part: string = null
    ) {

    }
}