export class PicturePage {
    constructor(
        public Type: 'Avatar' | 'Saved' | 'Album',
        public limit: number = 10,
        public page: number = 0
    ) {}
}