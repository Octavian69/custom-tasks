import { PaginationResponse } from '../types/request-response.types';

export class MediaList<T> implements PaginationResponse<T> {
    constructor(
        public rows: T[] = [],
        public totalCount: number = null
    ) {}
}