export class Page {
    constructor(
        public skip: number = 0,
        public limit: number = 5
    ) {}
}