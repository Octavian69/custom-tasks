export class TotalStats {
    constructor(
        public Friends: number = 0,
        public Audio: number = 0,
        public Video: number = 0,
        public PictureAlbums:number = 0,
        public AudioPlaylists: number = 0,
        public VideoPlaylists: number = 0,
        public User: string = null
    ){}
}