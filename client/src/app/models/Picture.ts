import { IPicture } from '../interfaces/IPicture';
import { PictureType } from '../types/picture.types';

export class Picture implements IPicture {
    constructor(
        public User?: string,
        public Album?: string,
        public Path?: string,
        public Type: PictureType = 'Created',
        public Created: Date = new Date(),
        public Updated: Date = new Date(),
        public Description: string = null,
        public Likes: string[] = []
    ) {}
}