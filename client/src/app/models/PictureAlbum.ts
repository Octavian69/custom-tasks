import { IPictureAlbum } from '../interfaces/IPictureAlbum';
import { PictureAlbumType } from '../types/picture.types';

export class PictureAlbum implements IPictureAlbum {
    
    constructor(
        public User: string,
        public Type: PictureAlbumType = 'Created',
        public Title: string = null,
        public Description: string = null,
        public Logo: string = null,
        public Created: Date = new Date(),
        public Updated: Date = new Date(),
        public TotalCount = 0,
        public _id?: string 
    ) {}
}