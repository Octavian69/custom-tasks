import { IComment } from '../interfaces/IComment';

export class Comment implements IComment {
    constructor(
        public Text: string,
        public Type: 'Picture' | 'Video' | 'Post', 
        public RefersID: string,
        public User: string,
        public Likes: string[] = [],
        public Created: Date = new Date(),
    ){}
}