import { IAudioPlaylist } from '../interfaces/IAudioPlaylist';

export class AudioPlaylist implements IAudioPlaylist {
    constructor(
        public Title: string = null,
        public Description: string = null,
        public Logo: string = null,
        public CreatorName: string = null,
        public Created: Date = new Date(),
        public Updated: Date = new Date(),
        public Type: 'Default' | 'Created' | 'Added' = 'Created',
        public ListPaths: string[] = [],
        public TotalCount: number = 1,
        public OriginalPlaylist: string = null,
        public Creator: string = null,
        public User?: string,
        public _id?: string
    ) {}
}