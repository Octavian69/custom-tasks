import { ICurrentMediaList } from '../interfaces/ICurrentMediaList';
import { MediaRequest } from './MediaRequest';
import { MediaList } from './MediaList';

export class CurrentMediaList<T> implements ICurrentMediaList<T> {
    constructor(
        public _id: string = null,
        public mediaList: MediaList<T>,
        public mediaRequest: MediaRequest
    ){}
}