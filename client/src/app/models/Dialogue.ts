import { DialogueType } from '../types/messages.types';

export class Dialogue {
    constructor(
        public Title: string,
        public Participants: string[],
        public Type: DialogueType,
        public User: string,
        public Created: Date = new Date(),
        public Updated: Date = new Date(),
        public TotalMessages: number = 1,
        public Logo: string = null,
        public _id?: string
    ){}
}