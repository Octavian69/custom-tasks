export class ArrowType {
    constructor(
        public prev: string,
        public next: string
    ){}
}