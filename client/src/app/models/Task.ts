import { ITask } from '../interfaces/ITask';

export class Task implements ITask {
    constructor(
        public User: string,
        public StartDate: Date = new Date(),
        public EndDate: Date = new Date(),
        public Description: string = null,
        public Title: string = 'Новая задача',
        public Completed: boolean = false,
        public _id?: string
    ) {
        this.EndDate.setDate(this.StartDate.getDate() + 1);
    }
}