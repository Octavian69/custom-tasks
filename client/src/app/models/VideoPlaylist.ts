import { IVideoPlaylist } from '../interfaces/IVideoPlaylist';

export class VideoPlaylist implements IVideoPlaylist {
    constructor(
        public Title: string,
        public User: string,
        public Description: string = null,
        public Type: 'Default' | 'Created' = 'Created',
        public Logo: string = null,
        public CreatorName: string = null,
        public AmountAdditions: number = 1,
        public Likes: string[] = [],
        public ListIds: string[] = [],
        public Created: Date = new Date(),
        public Updated: Date = new Date(),
        public _id?: string,
    ) {}
}