import { InfoMain } from './info/InfoMain';
import { InfoContacts } from './info/InfoContacts';
import { InfoInteresting } from './info/InfoInteresting';
import { InfoEducation } from './info/InfoEducation';
import { InfoHigherEducation } from './info/InfoHigherEducation';
import { InfoCareer } from './info/InfoCareer';
import { InfoMilitary } from './info/InfoMilitary';
import { InfoLifePosition } from './info/InfoLifePosition';

export class Info {
    constructor(
        public Main: InfoMain,
        public Contacts: InfoContacts,
        public Interesting: InfoInteresting,
        public Education: InfoEducation[],
        public HigherEducation: InfoHigherEducation[],
        public Career: InfoCareer[],
        public Military: InfoMilitary[],
        public LifePosition: InfoLifePosition,
        public User?: string,
        public _id?: string
    ) {}
}