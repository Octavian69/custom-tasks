import { Page } from './Page';
import { AnyOption } from '../types/shared.types';
import { ResponseAction } from '../types/request-response.types';

export class PaginationRequest {
    public filters: AnyOption = {};
    public sorted: AnyOption = {};
 
    constructor(
        public referenceId: string,
        public page: Page,
        public state: AnyOption = {}
    ) {}

    updateFilters(filters: AnyOption, action: ResponseAction): void {
        const exists: AnyOption = Object.keys(filters).reduce((accum: AnyOption, key: string) => {
            const value: any = filters[key];

            if(value) {
                accum[key] = value
            }

            return accum;
        }, {});

        this.filters = action === 'replace' ? exists : { ...this.filters, ...exists };
    }

    updateSorted(sorted: AnyOption) {
        this.sorted = sorted;
    }

    reset(key?: 'sorted' | 'filters'): void {
        if(key) {
            this[key] = {};
        } else {
            this.filters = {};
            this.sorted = {};
        }
    }
}