export class SimpleAction<T> {
    constructor(
        public action: string,
        public payload: T,
        public state?: {[prop: string]: any} 
    ) {}
}