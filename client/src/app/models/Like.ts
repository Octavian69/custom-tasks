import { LikeType } from '../types/like.types';

export class Like {
    constructor(
        public _id: string,
        public Type: LikeType,
        public Checked: boolean,
    ) {}
}