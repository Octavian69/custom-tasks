export class TaskFilter {
    public Title: string = null;
    public Description: string = null;
    public Completed: boolean = null;
    public StartDate: Date;
    public EndDate: Date = null;
}