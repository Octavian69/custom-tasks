import { Page } from './Page';

export class MediaRequest {
    
    page: Page;
    filters: any = {};
    sorted: any = {};
    searchString: string = '';
    state: any;

    constructor(
        public listType: 'main' | 'search',
        public playlistId: string,
        limit: number = 30,
        state: any = {}
        ) {
            this.init(limit, state);
        }

    init(limit: number, state: any): void {
        this.page = new Page(0, limit);
        this.state = state;
    }
}
