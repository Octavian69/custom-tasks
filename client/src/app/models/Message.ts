export class Message {
    constructor(
        public Text: string,
        public User: string,
        public Dialogue: string = null,
        public Created: Date = new Date(),
        public _id?: string
    ){}
}