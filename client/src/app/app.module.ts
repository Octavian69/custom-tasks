import { BrowserModule } from '@angular/platform-browser';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { JwtModule } from "@auth0/angular-jwt";
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClientJsonpModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from './shared/modules/shared.module';
import { AppRoutingModule } from './routes/app.routing.module';
import { SystemModule } from './system/system.module';
import { AuthModule } from './auth/auth.module';
import { ApiInterceptor } from './interceptors/interceptor.api';
import { AppComponent } from './app.component';
import { tokenGetter } from './shared/handlers/handlers';
import { environment } from '@env/environment';

registerLocaleData(localeRu);
const config: SocketIoConfig = { url: environment.API_URL, options: { autoConnect: false, transports: ['websocket'] } };

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SocketIoModule.forRoot(config),
    BrowserAnimationsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        blacklistedRoutes: [/auth/gi],
        whitelistedDomains: [environment.API_URL],
        authScheme: ''
      }
    }),
    ToastrModule.forRoot(),
    SharedModule,
    SystemModule,
    AuthModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'ru'
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
