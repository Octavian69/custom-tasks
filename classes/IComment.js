class IComment {
    constructor(comment) {
        this.create(comment);
    }

    create(comment) {
        const { 
            Text, 
            RefersID, 
            Avatar,
            Created,
            CreatorName, 
            User,
            _id } = comment;
        
        this.RefersID = RefersID;
        this.Text = Text;
        this.CreatorName = CreatorName;
        this.Created = Created;
        this.LikesTotal = 0;
        this.Avatar = Avatar;
        this.OwnLike = false;
        this.User = User;
        this._id = _id;
    }
}

module.exports = IComment