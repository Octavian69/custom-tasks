const { getDataByArrayKeys } = require('../handlers/db');

class IPictureAlbum {

    constructor(album, user) {
        this.init(album.toObject(), user)
    }

    init(album, { Name, Avatar }) {
       const excludeFields = getDataByArrayKeys(['excludeInterfacesField', 'picture-album']);

       Object.keys(album).forEach(key => {
            const isExclude = excludeFields.includes(key);

            if(!isExclude) this[key] = album[key];

        })

        this.CreatorName = Name;
        this.UserAvatar = Avatar;
    }
}

module.exports = IPictureAlbum;