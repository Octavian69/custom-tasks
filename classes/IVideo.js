const { getDataByArrayKeys } = require('../handlers/db');

const defaultFields = getDataByArrayKeys(['defaultInterfaceFileds', 'video']);

class IVideo {
    constructor(video, user, ownList) {
        this.init(video.toObject(), user, ownList);
    }

    init(video, user, ownList) {
        const  { Likes, _id } = video;

        defaultFields.forEach(key => this[key] = video[key]);

        this.LikesTotal = Likes.length;
        this.OwnLike = Likes.includes(user);
        this.IsHave = ownList.includes(String(_id));
    }
}

module.exports = IVideo;