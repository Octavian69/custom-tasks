class IAudio {
    constructor(audio, listPaths) {
        this.init(audio.toObject(), listPaths);
    }

    init(audio, listPaths) {
        Object.keys(audio).forEach(key => this[key] = audio[key]);
        this.IsHave = listPaths.includes(audio.Path);
    }
}

module.exports = IAudio;