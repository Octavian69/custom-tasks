const { getDataByArrayKeys } = require('../handlers/db');

class IPicture {
    constructor(picture, ownId, saved) {
        this.init(picture.toObject(), ownId, saved);
    }

    init(picture, ownId, saved) {
        const excludeFileds = getDataByArrayKeys(['excludeInterfacesField', 'picture']);

        Object.keys(picture).forEach(key => {
            const isExclude = excludeFileds.includes(key);

            if(!isExclude) this[key] = picture[key]
        });

        this.OwnLike = picture.Likes.includes(ownId);
        this.LikesTotal = picture.Likes.length;
        this.IsHave = true;
        this.IsHaveSaved = saved.includes(picture.Path)
    }
}

module.exports = IPicture;