const { getDataByArrayKeys } = require('../handlers/db');

const defaultFields = getDataByArrayKeys(['defaultInterfaceFileds', 'video-playlist']);

class IVideoPlaylist {

    constructor(playlist, user, ownCollection){
        this.init(playlist.toObject(), user, ownCollection);
    }

    init(playlist, user, ownCollection) {
        const {
            Likes,
            ListIds,
        } = playlist;

        
        defaultFields.forEach(key => this[key] = playlist[key]);
    
        this.LikesTotal = Likes.length,
        this.VideoTotal = ListIds.length,
        this.IsHave = ownCollection.includes(String(this._id));
        this.OwnLike = Likes.includes(user);
    }
}

module.exports = IVideoPlaylist;