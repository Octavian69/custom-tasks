const router = require('express').Router();
const { like } = require('../controllers/likes');

router.patch('/update', like);

module.exports = router;