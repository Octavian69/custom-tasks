const router = require('express').Router();
const { fetch, fetchById, create, update, remove } = require('../controllers/tasks');

router.post('/create', create);
router.delete('/remove', remove);
router.post('/', fetch);
router.get('/:taskId', fetchById);
router.patch('/:taskId', update);

module.exports = router;