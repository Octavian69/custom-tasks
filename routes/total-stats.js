const router = require('express').Router();
const { fetch } = require('../controllers/total-stats');

router.get('/:id', fetch);

module.exports = router;