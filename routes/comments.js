const router = require('express').Router();
const { fetch, create, edit } = require('../controllers/comments');

router.post('/fetch/:refId', fetch);
router.post('/create', create);
router.patch('/edit/:id', edit);

module.exports = router;

