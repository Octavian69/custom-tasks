const router = require('express').Router();
const { login, registration, getByLogin, refreshToken } = require('../controllers/auth');

router.post('/login', login);
router.post('/registration', registration);
router.post('/refresh_token', refreshToken);
router.post('/user', getByLogin);

module.exports = router;