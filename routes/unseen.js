const router = require('express').Router();
const { fetch, update, reset } = require('../controllers/unseen');

router.get('/fetch', fetch);
router.patch('/update', update);
router.patch('/reset', reset);

module.exports = router;