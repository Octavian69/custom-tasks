const router = require('express').Router();
const { update, remove } = require('../controllers/edit');

router.patch('/:part', update);
router.delete('/remove/:id', remove);

module.exports = router;