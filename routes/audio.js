const router = require('express').Router();
const fileConfig = require('../file-storages/file.config');
const { 
    create, 
    edit,
    fetch, 
    fetchByString, 
    getAudioById,
    getPlaylistById,
    addToPlaylist, 
    removeFromPlaylist,
    fetchPlaylists,
    getCreatedByUser,
    createPlaylist,
    editPlaylist,
    addPlaylist,
    removePlaylist
} = require('../controllers/audio');

router.get('/get-by-audio/:id', getAudioById);
router.post('/fetch/:id', fetch);
router.post('/fetch-by-string', fetchByString);
router.post('/create', fileConfig('audio', 'audio', 'audio').single('audio'), create);
router.patch('/edit', edit);
router.patch('/add-to-playlist/:id', addToPlaylist);
router.delete('/remove-from-playlist/:id', removeFromPlaylist);

router.get('/get-by-playlist/:id', getPlaylistById);
router.post('/fetch-playlists/:id', fetchPlaylists);
router.post('/get-created-by-user', getCreatedByUser);
router.post('/create-playlist', fileConfig('audio', 'audio-playlist', 'img').single('logo'), createPlaylist);
router.patch('/edit-playlist', fileConfig('audio', 'audio-playlist', 'img').single('logo'), editPlaylist);
router.patch('/add-playlist/:id',  addPlaylist);
router.delete('/remove-playlist/:id', removePlaylist);

module.exports = router;