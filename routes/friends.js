const router = require('express').Router();
const {
    fetch,
    fetchRequests,
    fetchOnline,
    fetchMutual,
    fetchIncomplete,
    add,
    remove,
    requestToAdd,
    rejectToAdd
} = require('../controllers/friends')

router.post('/fetch/:id', fetch);
router.post('/fetch-requests/:id', fetchRequests);
router.post('/fetch-mutual/:id', fetchMutual);
router.post('/fetch-online/:id', fetchOnline);
router.post('/fetch-incomplete/:id', fetchIncomplete);
router.patch('/add/:id', add);
router.patch('/request-to-add/:id', requestToAdd);
router.patch('/reject-to-add/:id', rejectToAdd);
router.delete('/remove/:id', remove);

module.exports = router;