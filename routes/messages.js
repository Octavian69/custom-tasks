const router = require('express').Router();
const fileConfig = require('../file-storages/file.config')
const { 
    singleMessage,
    fetchMessages,
    fetchDialogueById,
    fetchDialogues,
    fetchByParticipants,
    createMessage,
    editMessage,
    removeMessages,
    createDialogue,
    createDiscussion
} = require('../controllers/messages');

router.get('/get-dialogue-by-id/:id', fetchDialogueById);
router.post('/single/:id', singleMessage);
router.post('/fetch-messages/:id', fetchMessages);
router.post('/fetch-by-participants', fetchByParticipants);
router.post('/fetch-dialogues', fetchDialogues);
router.post('/create-message', createMessage);
router.patch('/edit-message', editMessage);
router.patch('/remove-messages/:id', removeMessages);
router.post('/create-dialogue/:id', createDialogue);
router.post('/create-discussion', fileConfig('img', 'dialogue', 'img').single('dialogue-logo'), createDiscussion);

module.exports = router;