const router = require('express').Router();
const { fetchInfo, fetchInfoPart, remove } = require('../controllers/account');


router.get('/info/part', fetchInfoPart);
router.get('/info/:id', fetchInfo);
router.delete('/remove', remove);

module.exports = router