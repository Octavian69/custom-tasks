const router = require('express').Router();
const fileConfig = require('../file-storages/file.config');
const { 
    fetch, 
    fetchByType,
    fetchBySlider,
    fetchById,
    create, 
    save,
    edit,
    replace,
    remove,
    fetchAlbums,
    fetchReplaceAlbums,
    fetchAlbumById,
    createAlbum, 
    removeAlbum,
    editAlbum 
} = require('../controllers/pictures');

router.post('/fetch/:id', fetch);
router.get('/fetch-slide/:id', fetchById);
router.post('/fetch-by-type/:id', fetchByType);
router.post('/fetch-by-slider/:id', fetchBySlider);
router.post('/create-picture/:id', fileConfig('img', 'picture', 'img').array('pictures', { maxCount: 5 }), create);
router.post('/save-picture', save);
router.patch('/edit-picture/:id', edit);
router.patch('/replace-picture/:id', replace);
router.delete('/remove-picture/:id', remove);
router.get('/get-album-by-id/:id', fetchAlbumById);
router.post('/fetch-albums/:id', fetchAlbums);
router.post('/fetch-replace-albums/:id', fetchReplaceAlbums);
router.post('/create-album', fileConfig('img', 'album', 'img').single('logo'), createAlbum);
router.patch('/edit-album/:id', fileConfig('img', 'album', 'img').single('logo'), editAlbum);
router.delete('/remove-album/:id', removeAlbum);

module.exports = router;