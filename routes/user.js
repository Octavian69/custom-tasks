const router = require('express').Router();
const { fetchById, createAvatar, updateAvatar, edit } = require('../controllers/user');
const fileConfig = require('../file-storages/file.config');

router.get('/get-by-id/:id', fetchById);
router.post('/create-avatar', fileConfig('img', 'picture', 'img').single('avatar'), createAvatar);
router.patch('/update-avatar', updateAvatar);
router.patch('/edit', edit);

module.exports = router;