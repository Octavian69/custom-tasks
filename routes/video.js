const router = require('express').Router();
const fileConfig = require('../file-storages/file.config');
const { 
    fetch, 
    getVideoById,
    create, 
    edit,
    add,
    remove,
    fetchPlaylists,
    getPlaylistsByCreator,
    getPlaylistById,
    createPlaylist,
    editPlaylist,
    addPlaylist,
    removePlaylist,
    updatePlaylistLogo
 } = require('../controllers/video');

router.get('/get-by-id/:id', getVideoById);
router.post('/fetch/:id', fetch);
router.post('/create', fileConfig('video', 'video', 'video').single('video'), create);
router.patch('/edit/:id', edit);
router.patch('/add/:id', add);
router.delete('/remove/:id', remove);
router.get('/get-playlist-by-id/:id', getPlaylistById);
router.post('/get-playlists-by-creator', getPlaylistsByCreator);
router.post('/fetch-playlists/:id', fetchPlaylists);
router.post('/create-playlist', fileConfig('video', 'video-playlist-logo', 'img').single('playlist-logo'), createPlaylist);
router.patch('/edit-playlist/:id', editPlaylist);
router.patch('/add-playlist/:id',  addPlaylist);
router.patch('/update-playlist-logo/:id', fileConfig('video', 'video-playlist-logo', 'img').single('playlist-logo'), updatePlaylistLogo);
router.delete('/remove-playlist/:id',  removePlaylist);

module.exports = router;